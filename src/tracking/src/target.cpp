/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "target.h"

// C++
#include <iostream>

using namespace std;

int Target::newIdLoc_ = 0;

//Target::Target()
//{
//    idLoc_ = Target::newIdLoc_++;
//}

Target::Target(const cv::Size& screenSize, cv::Mat appearance, const cv::Rect &bbox, const uint32_t img_seq_id) :
    init_img_seq_id(img_seq_id),
    screenSize_(screenSize),
    appearance_(appearance),
    bbox_(bbox),
    idGlob_(-1),
    objectType_(OBJECT_UNKNOWN)
{
    idLoc_ = Target::newIdLoc_++;;
}

Target::~Target()
{
    ;
}

cv::Point Target::getScreenLocationTopLeft()
{
    return cv::Point(bbox_.x, bbox_.y);
}

cv::Point Target::getScreenLocationCenter()
{
    return cv::Point(bbox_.x + (bbox_.width >> 1), bbox_.y + (bbox_.height >> 1));
}

void Target::updateBB(cv::Rect bb)
{
    bbox_ = bb;
}
