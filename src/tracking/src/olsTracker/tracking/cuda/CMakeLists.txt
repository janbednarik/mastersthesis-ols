cmake_minimum_required(VERSION 2.8)

find_package(CUDA REQUIRED)

INCLUDE(FindCUDA)

#set(CUDA_NVCC_FLAGS -gencode=arch=compute_20,code=sm_20 -gencode=arch=compute_30,code=sm_30 -gencode=arch=compute_50,code=sm_50)
set(CUDA_NVCC_FLAGS -gencode arch=compute_20,code=sm_20 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_35,code=sm_35 -gencode arch=compute_37,code=sm_37 -gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52)
CUDA_ADD_LIBRARY(particle_cuda particles_evaluation.cu)
