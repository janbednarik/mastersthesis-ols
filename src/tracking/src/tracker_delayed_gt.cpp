/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// project libraries
#include "tracker_delayed_gt.h"

// C++
#include <iostream>
#include <cmath>
#include <thread>
#include <chrono>

// ROS
#include <ros/ros.h>

using namespace std;

TrackerDelayedGT::TrackerDelayedGT() :
    x_(0), y_(0), mu_(0.0), sigma_(1.0),
    gen_(NULL)
{
    gen_ = new std::mt19937(rd_());
}

TrackerDelayedGT::TrackerDelayedGT(double mu, double sigma) :
    Tracker(0, 0), x_(0), y_(0), mu_(mu), sigma_(sigma),
    gen_(NULL), d_(NULL)
{
    d_ = new std::normal_distribution<double>(mu_, sigma_);
    gen_ = new std::mt19937(rd_());
}

TrackerDelayedGT::~TrackerDelayedGT()
{
    if(gen_)    delete gen_;
    if(d_)      delete d_;
}

bool TrackerDelayedGT::init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb)
{    
    x_ = bb.x;
    y_ = bb.y;
}

bool TrackerDelayedGT::track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb)
{
    // Simulate tracking delay.
    int delay = (int)std::round((*d_)(*gen_));
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));

    // Return the ground truth
    bb = cv::Rect(x_ - DEFAULT_BB_WIDTH / 2, y_ - DEFAULT_BB_HEIGHT / 2,
                  DEFAULT_BB_WIDTH, DEFAULT_BB_HEIGHT);

    return true;
}
