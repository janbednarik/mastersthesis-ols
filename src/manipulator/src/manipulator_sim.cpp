/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// C++ libraries
#include <iostream>

// ROS libraries
//#include <gazebo_msgs/LinkState.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>

// project libraries
#include "manipulator_sim.h"

using namespace std;

// Manipulator motion constants
const double ManipulatorSim::MAX_AZIMUTH_SPEED     = 400.0;
const double ManipulatorSim::MAX_ELEVATION_SPEED   = 400.0;

// Manipulator construction constraints
const double ManipulatorSim::MAX_AZIMUTH   =  3089.0;  //  177 deg
const double ManipulatorSim::MIN_AZIMUTH   = -3089.0;  // -177 deg
const double ManipulatorSim::MAX_ELEVATION =  541.0;   //  31  deg
const double ManipulatorSim::MIN_ELEVATION = -1396.0;  // -80  deg

const double ManipulatorSim::AZI_STEP = 1.0;
const double ManipulatorSim::ELE_STEP = 1.0;

ManipulatorSim::ManipulatorSim(double simTimeStep) : simTimeStep_(simTimeStep),
    controlMode_(SPEED_CONTROL)
{   
    azimuth_    = 200.0;
    elevation_  = -950.582;

    // Create regulator controlling the motion of the manipualtor during tracking.
    SpeedFunc* regSpeedFunc         = new SpeedFuncPow(0.14, 0.2, 0.008, 1.5, 0.002); // orig

    Position2DPredictor* predictor  = new Position2DPredictor(10);
    regulator_ = new Regulator(regSpeedFunc, predictor);

    // Create speed function for progressive motion control using joystick.
    joySpeed_ = new SpeedFuncPow(1.0, MAX_AZIMUTH_SPEED / 1000.0, 0.0, 6.0, 0.01);
}

ManipulatorSim::~ManipulatorSim()
{
    if(regulator_)  delete regulator_;
    if(joySpeed_)   delete joySpeed_;
}

void ManipulatorSim::update()
{
    double azi = 0.0;
    double ele = 0.0;

    oldAzimuth_     = azimuth_;
    oldElevation_   = elevation_;

    double dAzi = aziSpeed_ * simTimeStep_;
    double dEle = eleSpeed_ * simTimeStep_;

    switch(controlMode_) {
        case SPEED_CONTROL:
            setPositionStamped(azimuth_ + dAzi, elevation_ + dEle, ros::Time::now());
            break;

        case ABS_POS_CONTROL:
            if(aziSpeed_ != 0.0) {
                azi = azimuth_ + dAzi;
                if((azi == aziAbsTarget_) ||
                   ((aziSpeed_ > 0.0) && (azi > aziAbsTarget_)) ||
                   ((aziSpeed_ < 0.0) && (azi < aziAbsTarget_))) {
                    azi  = aziAbsTarget_;
                    aziSpeed_ = 0.0;
                }
            }

            if(eleSpeed_ != 0.0) {
                ele = elevation_ + dEle;
                if((ele == eleAbsTarget_) ||
                   ((eleSpeed_ > 0.0) && (ele > eleAbsTarget_)) ||
                   ((eleSpeed_ < 0.0) && (ele < eleAbsTarget_))) {
                    ele = eleAbsTarget_;
                    eleSpeed_ = 0.0;
                }
            }

            setPositionStamped(azi, ele, ros::Time::now());

            if((aziSpeed_ == 0.0) && (eleSpeed_ == 0.0)) {
                controlMode_ = SPEED_CONTROL;
            }

            break;

        default:
            break;
    }

    Manipulator::update();
}

void ManipulatorSim::move(double azimuth, double elevation)
{
    aziSpeed_ = azimuth;
    eleSpeed_ = elevation;
    controlMode_ = SPEED_CONTROL;
}

bool ManipulatorSim::moveAbsolute(double azimuth, double elevation)
{
    aziAbsTarget_ = azimuth;
    eleAbsTarget_ = elevation;
    aziSpeed_     = (azimuth_   > aziAbsTarget_) ? (-MAX_AZIMUTH_SPEED)   : ((azimuth_ < aziAbsTarget_)   ? MAX_AZIMUTH_SPEED   : 0.0);
    eleSpeed_     = (elevation_ > eleAbsTarget_) ? (-MAX_ELEVATION_SPEED) : ((elevation_ < eleAbsTarget_) ? MAX_ELEVATION_SPEED : 0.0);
    controlMode_ = ABS_POS_CONTROL;
}

bool ManipulatorSim::moveRelative(double azimuth, double elevation)
{
   return moveAbsolute(azimuth + azimuth_, elevation + elevation_);
}

void ManipulatorSim::step(int azimuth, int elevation)
{
    moveRelative((azimuth   > 0.0) ? AZI_STEP : ((azimuth   < 0.0) ? -AZI_STEP : 0.0),
                 (elevation > 0.0) ? ELE_STEP : ((elevation < 0.0) ? -ELE_STEP : 0.0));
}

void ManipulatorSim::haltPan()
{
    ;
}

void ManipulatorSim::haltTilt()
{
    ;
}

void ManipulatorSim::onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg)
{
    double azi = 0.0;
    double ele = 0.0;

    double azSpSc;
    double elSpSc;

    switch(mvMsg->mode) {
        case msgs::ManipulatorMove::MODE_ABSOLUTE:
            break;

        case msgs::ManipulatorMove::MODE_RELATIVE:
            break;

        case msgs::ManipulatorMove::MODE_CONTINUOUS:
            switch(mvMsg->speedType) {
                case msgs::ManipulatorMove::SPEED_TYPE_ABSOLUTE:
                    break;

                case msgs::ManipulatorMove::SPEED_TYPE_PROPORTIONAL:
                    azSpSc = mvMsg->aziSpeed;
                    elSpSc = mvMsg->eleSpeed;

                    if(mvMsg->aziSpeed < -1.0 || mvMsg->aziSpeed > 1.0) {
                        azSpSc = std::max(std::min(1.0, azSpSc), -1.0);
                        ROS_WARN_STREAM("onManipMove: aziSpeed not in range [-1.0, 1.0], will be clipped.");
                    }

                    if(mvMsg->eleSpeed < -1.0 || mvMsg->eleSpeed > 1.0) {
                        elSpSc = std::max(std::min(1.0, elSpSc), -1.0);
                        ROS_WARN_STREAM("onManipMove: eleSpeed not in range [-1.0, 1.0], will be clipped.");
                    }

                    azi = azSpSc * MAX_AZIMUTH_SPEED;
                    ele = elSpSc * MAX_ELEVATION_SPEED;

                    break;

                default:
                    ROS_ERROR_STREAM("onManipMove: Incorrect 'speedType' selected in ManipulatorMove message");
                    return;
            }

            move(azi, ele);

            break;

        default:
            ROS_ERROR_STREAM("onManipMove: Incorrect 'mode' selected in ManipulatorMove message");
            return;
    }
}

void ManipulatorSim::onKeyDown(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {
        // Motion control keys: UP, DOWN, LEFT, RIGHT
        case keyboard::Key::KEY_LEFT:
            switch(precState_) {
                case STEP:          step(1, 0); break;
                case CONTINUOUS:    move(MAX_AZIMUTH_SPEED, getElevationSpeed());    break;
            }
            break;
        case keyboard::Key::KEY_RIGHT:
            switch(precState_) {
                case STEP:          step(-1, 0);                            break;
                case CONTINUOUS:    move(-MAX_AZIMUTH_SPEED, getElevationSpeed());   break;
            }
            break;
        case keyboard::Key::KEY_UP:
            switch(precState_) {
                case STEP:          step(0, 1);                             break;
                case CONTINUOUS:    move(getAzimuthSpeed(), MAX_ELEVATION_SPEED);      break;
            }
            break;
        case keyboard::Key::KEY_DOWN:
            switch(precState_) {
                case STEP:          step(0, -1);                            break;
                case CONTINUOUS:    move(getAzimuthSpeed(), -MAX_ELEVATION_SPEED);     break;
            }
            break;

        case keyboard::Key::KEY_HOME:
            moveAbsolute(0.0, elevation_);
            break;

        case keyboard::Key::KEY_END:
            moveAbsolute(azimuth_, 0.0);
            break;

        // Set precision mode - STEP
        case keyboard::Key::KEY_s:  precState_ = STEP;          break;

        // Set precision mode - CONTINUOUS
        case keyboard::Key::KEY_c:  precState_ = CONTINUOUS;    break;

        // debug start/stop tracking
        case keyboard::Key::KEY_t:
            if(followTarget_) {
                disableFollowTarget();
            } else {
                enableFollowTarget();
            }
            break;

        default:
            break;
    }
}

void ManipulatorSim::onKeyUp(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {
        // Motion control keys: UP, DOWN, LEFT, RIGHT
        case keyboard::Key::KEY_LEFT:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(0.0, getElevationSpeed()); break;
            }
            break;

        case keyboard::Key::KEY_RIGHT:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(0.0, getElevationSpeed()); break;
            }
            break;

        case keyboard::Key::KEY_UP:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(getAzimuthSpeed(), 0.0); break;
            }
            break;
        case keyboard::Key::KEY_DOWN:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(getAzimuthSpeed(), 0.0); break;
            }
            break;

        default:
            break;
    }
}

void ManipulatorSim::onJoy(const sensor_msgs::Joy::ConstPtr& joy)
{
    static bool buttonDown = false;

    if(!buttonDown) {
        move(joy->axes[0] * MAX_AZIMUTH_SPEED, joy->axes[1] * MAX_ELEVATION_SPEED);
    }
    else
    {
        buttonDown = false;
    }
}
