/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TARGET_H
#define TARGET_H

// Eigen
#include <eigen3/Eigen/Eigen>

//! \brief The Target class The main data type used to store information
//! about tracked targets.
//!
class Target
{

public:
    //! \brief Target Constructor
    //!
    Target();

    //! \brief ~Target Destructor
    //!
    ~Target();

    //! \brief getId ID getter.
    //! \return
    //!
    int getId() { return id_; }

    //! \brief setPos3D 3D position setter.
    //! \param pos
    //!
    void setPos3D(Eigen::Vector3d& pos) { pos3d_ = pos; }

private:
    static int newId_;  //!< Global id counter.
    int id_;            //!< id of the target

    Eigen::Vector3d pos3d_; //!< 3D position of the target (cartesian coordinates) [m]
};

#endif // #ifndef TARGET_H
