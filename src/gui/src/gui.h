/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef GUI_H
#define GUI_H

// ROS libraries
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Joy.h>
#include <keyboard/Key.h>
#include <rosgraph_msgs/Log.h>

// project libraries
#include "crosshair.h"
#include "local_map_widget.h"
#include <camera_unit.h>
#include <msgs/TargetStateLocal.h>
#include <msgs/TargetState.h>
#include <msgs/CameraUnitInfo.h>
#include <msgs/StartDetection.h>
#include <msgs/StopDetection.h>
#include <msgs/TargetObject.h>
#include <msgs/SetTargetObject.h>

// Eigen
#include <eigen3/Eigen/Eigen>

// For testing purposes, the GUI must subscribe "joy_0" topic rather then "joy"
#define TESTING_2_UNITS 0

// Whether or not to show google maps visualization
#define GOOGLE_MAPS_VIS 1

//! \brief The Gui class Implementation of graphical user interface - it is composed
//! of OpenCV window, where camera stream, info about target and crosshair is drawn, and
//! google maps visualization.
//!
class Gui
{
public:
    int CAMERA_UNITS_COUNT;        //!< The total number of camera units in the system

    static const int DEFAULT_BB_WIDTH   = 40;   //!< Default bounding box width.
    static const int DEFAULT_BB_HEIGHT  = 40;   //!< Default bounding box height.

    static const std::string IMAGE_TOPIC;
    static const std::string JOY_TOPIC;
    static const std::string KEYDOWN_TOPIC;
    static const std::string KEYUP_TOPIC;
    static const std::string START_DETECTION_TOPIC;
    static const std::string STOP_DETECTION_TOPIC;
    static const std::string TARGET_STATE_LOCAL_TOPIC;
    static const std::string SELECT_MANIP_SRV;   
    static const std::string TARGET_STATE_TOPIC;
    static const std::string CAMERA_UNIT_INFO_TOPIC;
    static const std::string SET_TARGET_OBJECT_TOPIC;
    static const std::string NAMED_WINDOW;
    static const double FONT_SIZE;
    static const int FONT_THICKNESS;
    static const cv::Scalar CV_BLACK;
    static const cv::Scalar CV_WHITE;
    static const cv::Scalar CV_RED;
    static const cv::Scalar CV_GREEN;
    static const cv::Scalar CV_BLUE;
    static const cv::Scalar CV_CYAN;

    //! \brief The TARGET_TYPE enum Type of the target. Either ordinary moving target defined
    //! by bounding box or single static fixed point.
    //!
    enum ETargetType {
        DYNAMIC = 0,
        STATIC = 1
    };

    //! \brief The tracker type
    enum ETrackerType {
        TRACKER_GROUND_TRUTH = 0,
        TRACKER_OLS = 1,
        TRACKER_TLD = 2
    };

    static const ETrackerType DEFAULT_TRACKER;

    enum ETargetObject {
        OBJECT_UNKNOWN  = 0,
        OBJECT_DRONE    = 1,
        OBJECT_AIRCRAFT = 2,
        OBJECT_HUMAN    = 3,
        OBJECT_DOG      = 4,
    };

    static const std::string targetObjectStr[5];

public:

    //! \brief Gui Constructor.
    //!
    Gui();

    //! \brief Gui Destrctor.
    //!
    ~Gui();

    //! \brief update Updates the state of the airborne object.
    //!
    void update();

    static void onMouse(int event, int x, int y, int, void* userdata);

    void onMouse(int event, int x, int y);

private:
    // ROS communication
    ros::NodeHandle *nh_;

    ros::Subscriber onImgSub_;                  //!< Image event subscription.
    ros::Subscriber joySub_;                    //!< Joystick event subscription.
    ros::Subscriber keyDownSub_;                //!< Keyboard key pressed event subscription.
    ros::Subscriber keyUpSub_;                  //!< Keyboard key released event subscription.
    ros::Subscriber logSub_;                    //!< Subscriber of the log messages from all nodes.
    ros::Subscriber targetStateLocalSub_;       //!< Subscriber of the message TargetStateLocal
    ros::Subscriber targetStateSub_;            //!< Subscriber of the message TargetState
    ros::Subscriber *cameraUnitInfoSub_;         //!< The subscriber of the current info about the camera unit.
    ros::Publisher  startDetectionPub_;         //!< Publisher of the StartDetection message.
    ros::Publisher stopDetectionPub_;          //!< StopDetection message publisher
    ros::Publisher setTargetObjectPub_;          //!< SetTargetType message publisher
    ros::Publisher keyDownPub_;
    ros::Publisher keyUpPub_;


    ros::ServiceClient selectManipClient_;      //!< Client to call service SelectManipulator to select the manipulator for manual control.

    std::string camera_unit_ns_;
    int cameraUnit_;    //!< The index of camera unit currently chosen for communication.

    int frameW_;
    int frameH_;
    Crosshair ch_;      //!< Crosshair drawn on the image

    CameraUnit *cameraUnits_[4];     //!< The structures representing info about all camera units

    ros::Time lastFrameStamp_;

    int dwChBb_;    //!< Required change of crosshair's bounding box width.
    int dhChBb_;    //!< Required change of crosshair's bounding box height.

    // Log messages
    std::string logLevel_[17];      //!< Names of log levels.
    bool *cuInfoInitialized_;       //!< Flags - wether first cuInfo msg for givren CU was received.
    bool checkCuInfoInitialized_;

    // Google maps FOV visualizer
    LocalMapWidget *mapVis_;

    // debug
    Eigen::Vector3d tgtPos3d_;  //!< Position of the target (local cartesian coordinate frame)
    double tgtDist_;            //!< Distance of the target (in local frame)
    double tgtSpeed_;
    ETargetObject tgtObject_;
    bool *startStopDetection_;

    cv::Point *fixedPoints_;
    bool *showFixedPoints_;

    ETrackerType tracker_type_;

    int imgIdx_;

private:    

    //! \brief onLog log/info/status messages from all nodes.
    //! \param msg
    //!
    void onLog(const rosgraph_msgs::Log::ConstPtr msg);

    //! \brief onImage Video stream callback
    //! \param img
    //!
    void onImage(const sensor_msgs::Image::ConstPtr img);

    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    void onJoy(const sensor_msgs::Joy::ConstPtr& joy);

    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    void onKeyDown(const keyboard::Key::ConstPtr key);

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    void onKeyUp(const keyboard::Key::ConstPtr key);   

    //! \brief switchVideoSubscription Unsubscribes from the current
    //! video image stream topic and subscribes to a ifferent one
    //! given by the camera unit index 'idx'
    //! \param idx camera unit index
    //!
    void switchManipulator(int idx);

    //! \brief switch tracker in object detector
    void switchTracker(ETrackerType tracker_type);

    //! \brief startDetection Initiates the detection of the object
    //! in the currently displayed bounding box at the currently
    //! activated camera unit
    //!
    void startDetection(cv::Point& topLeft,
                        cv::Point& bottomRight,
                        ETargetType target_type = Gui::DYNAMIC,
                        ETrackerType tracker_type = Gui::TRACKER_TLD);

    //! \brief stopDetection Sends the message StopDetection
    //!
    void stopDetection();

    //! \brief setTargetObject Sets the object type of the tracked target.
    //! \param objType
    //!
    void setTargetObject(ETargetObject objType, uint32_t targetId = 0);

    //! \brief onTargetStateLocal Callback for processing messages TargetStateLocal from
    //! separate camera units. It mainly serves the purpose of visualizing the bounding box
    //! of the tracked object.
    //! \param stateMsg
    //!
    void onTargetStateLocal(const msgs::TargetStateLocalConstPtr stateMsg);

    //! \brief onTargetState Callback for message TargetState carrying info about
    //! estimated 3D position of the tracked target.
    //! \param targetState
    //!
    void onTargetState(const msgs::TargetState::ConstPtr targetState);

    //! \brief onCameraUnitInfo Callback for topic CameraUnitInfo. It stores all the
    //! info abot camera units.
    //! \param cuInfo
    //!
    void onCameraUnitInfo(const msgs::CameraUnitInfo::ConstPtr cuInfo);
};

#endif // #ifndef HUI_H
