#ifndef RECTIFICATION_H
#define RECTIFICATION_H

// ROS libraries
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/JointState.h>
#include <keyboard/Key.h>
#include <sensor_msgs/Joy.h>
#include <tf/transform_listener.h>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>

// project libraries
#include <msgs/ManipulatorMove.h>
#include "crosshair.h"

class Rectification {
public:
    static const std::string IMAGE_RAW_TOPIC;    //!< Name of the topic conveying camera stream.
    static const std::string KEY_DOWN_TOPIC;
    static const std::string KEY_UP_TOPIC;
    static const std::string JOY_TOPIC;
    static const std::string JOINT_STATES_TOPIC;
    static const std::string MANIP_MOVE_CMD_TOPIC;

    static const float ZOOM_STEP;
    static const float ZOOM_MIN;
    static const float ZOOM_MAX;

    enum Tool {
        CCD_ROWS,
        OPTICAL_AXIS,
        DEFAULT_ELEVATION_ANGLE,
        BETA_TL1
    };

public:
    //! \brief Rectification Constructor
    //!
    Rectification();

    //! \brief Destructor
    //! //!
    ~Rectification();

    //! \brief update Main control function called at SPIN_RATE rate.
    //!
    void update();

private:
    // ROS structures
    ros::NodeHandle* nh_;
    ros::Subscriber onImgSub_;
    ros::Subscriber keyDownSub_;        //!< Keyboard press event subscription.
    ros::Subscriber keyUpSub_;          //!< Keyboard release event subscription.
    ros::Subscriber joySub_;            //!< Joystick event subscription.
    ros::Subscriber jointStateSub_;     //!< Joint states subscriber.
    ros::Publisher  keyUpPub_;          //!< Publisher of the keyboard events.
    ros::Publisher  keyDownPub_;        //!< Publisher of the keyboard events.
    ros::Publisher  joyPub_;            //!< Publisher of the joystick events.
    ros::Publisher  manipMoveCmdPub_;   //!< Publisher of the joystick events.
    tf::TransformListener tfListener_;  //!< Listener finds transforms for azimuth and elevation joints.

    std::string master_ns_;       //!< The namespace the rectification nodes are running in.
    std::string camera_unit_ns_;
    std::string tf_prefix_;              //!< Transofrmation prefix.

    Tool toolSelection_;            //!< Currently selected tool.
    int frameWidth_, frameHeight_;  //!< Input frame size.
    std::string manipType_;

    // CV Colors
    static const cv::Scalar RED;
    static const cv::Scalar GREEN;
    static const cv::Scalar BLUE;
    static const cv::Scalar BLACK;
    static const cv::Scalar WHITE;

    // Saving data
    std::string fileOptAxis_;
    std::string fileEleAngle_;
    std::string fileTL1Angle_;
    bool lctrlDown_;                //!< Flag specifying whether left CTRL key is pressed.

    // Crosshair
//    int chX_;
//    int chY_;
//    int chLineLength_;
//    int chSpaceLength_;
//    int chVHeigth_;
//    double chVAngle_;
//    double chTanVAngle_;
//    cv::Scalar chColor_;

    Crosshair chOA_;
    Crosshair chDEA_;
    Crosshair chBTL1_;

    // OPTICAL_AXIS tool properties
    double pixelWidth_;                     //!< Camera sensor pixel width [um].
    double pixelHeight_;                    //!< Camera sensor pixel height [um].
    bool mvLeft_, mvRight_, mvUp_, mvDown_;
    int mvSpeed_;
    int mvActiveCmds_, mvActiveCmdsOld_;
    ros::Time mvStart_;

    // DEFAULT_ELEVATION_ANGLE properties
    double azimuth_;        //!< Current azimuth   of the manipulator [mrad].
    double elevation_;      //!< Current elevation of the manipulator [mrad].

    // Digital zoom
    float zoom_;

    // Lense
    double focalLength_;

private:
    //! \brief onImage Callback function for incoming images
    //! \param img
    //!
    void onImage(const sensor_msgs::Image::ConstPtr img);

    //! \brief onJointStates Callback for joint states
    //! \param joinStates
    //!
    void onJointStates(const sensor_msgs::JointState::ConstPtr jointStates);

    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    void onKeyDown(const keyboard::Key::ConstPtr key);

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    void onKeyUp(const keyboard::Key::ConstPtr key);

    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    void onJoy(const sensor_msgs::Joy::ConstPtr& joy);

    //! \brief saveDataOptAxis Saves the rectification data concerning
    //! optical axis rectification to the file.
    //!
    void saveDataOptAxis();

    //! \brief saveDataEleAngle Saves the rectification data concerning
    //! elevation angle to file.
    //!
    void saveDataEleAngle();

    //! \brief saveBetaTL1Angle Saves the rectification data concerning
    //! BetaTL1 angle to file.
    //!
    void saveBetaTL1Angle();

    //! \brief recitfyCCDRows The tool to correctly rotate the camera along X-axis
    //! so that the Y-axis of the camera would be horizontal. The function displays
    //! the left half of the image and mirrors it to the right side, so that it could
    //! be easily seen that the horizontal lines drawn on the target are straight.
    //!
    void rectifyCCDRows(cv::Mat& frame);

    //! \brief rectifyOpticalAxis The tool to make the optical axis perpendicular
    //! to the elevation axis. The function displays the crosshair in the middle of the
    //! image and enables for moving of the crosshair and finding the X and Y offset
    //! values (in pixels).
    //!
    void rectifyOpticalAxis(cv::Mat& frame);

    //! \brief findDefaultElevationAngle The tool to find the default elevation angle of
    //! the camera. The function enables to move the manipulator in along the elevation axis
    //! and find the resulting angle.
    //!
    void findDefaultElevationAngle(cv::Mat& frame);

    //! \brief findBetaTL1 The tool to find the BetaTL1 angle.
    //! \param frame
    //!
    void findBetaTL1(cv::Mat& frame);

    //! \brief setAbsPos Set absolute position of the manipulator. The angles for azimuth
    //! and elevation are read from the console (the main program loop is stopped).
    //!
    void setAbsPos();

    void updateZoom(float z);
    void zoom(cv::Mat& frame);
};

#endif // #ifndef RECTIFICATION_H
