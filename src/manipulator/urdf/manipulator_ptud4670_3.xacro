<?xml version="1.0"?>
<robot xmlns:xacro="http://www.rcesystems.cz/ols/tracking_unit.xacro" name="tracking_unit">    
  
<!-- ========================================================================================== -->
<!-- ========================================================================================== -->
<!--                                Fill in these constants                                     
		         (see manipulator_flir_ptu_d46-70-rear_view.svg)			        -->

    <!-- legs -->
    <xacro:property name="leg_length"	value="1.0" />
    <xacro:property name="leg_radius"	value="0.03" />

    <!-- Fill these constants using manipulator_legs.py                        -->
    <xacro:property name="leg_angle"	value="15.0" /> <!-- [degrees] -->
    <xacro:property name="sin_abs_leg1_roty"  value="0.258819" />
    <xacro:property name="cos_abs_leg1_rotz"  value="0.965926"   />
    
    <!-- base -->
    <xacro:property name="base_board_width"   value="0.161"  />
    <xacro:property name="base_board_length"  value="0.404"  />
    <xacro:property name="base_board_height"  value="0.005" />
    
    <xacro:property name="base_cylinder_length" value="0.066"  />
    <xacro:property name="base_cylinder_radius" value="0.033" />
    
    <!-- bottom box part of the manipulator -->
    <xacro:property name="manip_bottom_width"	value="0.043" />
    <xacro:property name="manip_bottom_length"  value="0.069" />
    <xacro:property name="manip_bottom_height"  value="0.046" />
    
    <xacro:property name="manip_bottom_2_base_cylinder_offset_y" value="0.005" />
    
    <!-- top box part of the manipulator -->
    <xacro:property name="manip_top_width"          value="0.043" />
    <xacro:property name="manip_top_length"         value="0.042" />
    <xacro:property name="manip_top_height"         value="0.065" />        
    
    <xacro:property name="pan_axis_2_manip_bottom_offset_y"	value="0.009" />
    <xacro:property name="tilt_axis_2_manip_top_offset_z" 	value="0.009" />
    
    <!-- tilt rod -->
    <xacro:property name="manip_rod_length"  value="0.356" />
    <xacro:property name="manip_rod_radius"  value="0.008" />
    
    <!-- camera -->
    <xacro:property name="camera_width"     value="0.0762" />
    <xacro:property name="camera_length"    value="0.0533" />
    <xacro:property name="camera_height"    value="0.0330" />        
    
    <xacro:property name="lense_length"     value="0.041" />
    <xacro:property name="lense_radius"     value="0.015" />              

    <!-- Output of the rectification - correction angles -->
    <!--
    Thanks to rectification, two rotation angles of the camera were found:
    rot about Z axis and rot about Y axis. Since URDF model of the joint only
    supports specifying euler angles which are then applied in the sequence
    X -> Y -> Z (or roll -> pitch -> yaw - see http://wiki.ros.org/urdf/XML/joint),
    but the cemara was first rotated about Z axis and then about Y axis, you need
    to convert the sequence xyz to the new sequence z'y'x'. To do so, inspect and run
    script ${OLS_PROJ_DIR}/src/rotation_conversion/convEulSeq.py.
    -->
    <!--
    Rectification constants:
    fixed elevation (Y) = -1.02851 rad
    fixed azimuth   (Z) = 0.00140625 rad
    -->
    <xacro:property name="camera_phi"   value="-0.002180" />  <!-- [rad] -->
    <xacro:property name="camera_psi"   value="-1.028509" />  <!-- [rad] -->
    <xacro:property name="camera_theta" value="0.001867" />  <!-- [rad] -->
 
<!-- ========================================================================================== -->
<!-- ========================================================================================== -->

	<!-- Import all Gazebo-customization elements, including Gazebo colors -->
	<xacro:include filename="$(find manipulator)/urdf/manipulator_ptud4670_3.gazebo" />
	<!-- Import Rviz colors -->
	<xacro:include filename="$(find manipulator)/urdf/materials.xacro" />			
	
    <!-- Math constants -->
    <xacro:property name="pi"           value="3.14159265" />    
    <xacro:property name="sin_pi_sixth" value="0.5" /> 
    <xacro:property name="cos_pi_sixth" value="0.866" />   

<!-- ========================================================================================== -->
    
  
    <!-- Dummy link just to give the kinematic chain required start link -->
    <link name="ground">            
		<inertial>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
        <visual>                           
            <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
			<geometry>
                <cylinder length="0.0" radius="0.0"/>
            </geometry>
            <material name="dark_gray" />        
        </visual>
		
		<collision>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />		
			<geometry>
                <cylinder length="0.0" radius="0.0"/>
            </geometry>
		</collision>

	</link>            

    <link name="legs">
        <!-- constants -->
        <xacro:property name="l"    value="${leg_length}" />  
        <xacro:property name="a"    value="${leg_angle / 180.0 * pi}" />      

        <xacro:property name="l1_rotx"  value="0.0" />
        <xacro:property name="l1_roty"  value="${a}" />
        <xacro:property name="l1_rotz"  value="0.0" />

        <xacro:property name="l1_tx"    value="${-l * sin_abs_leg1_roty / 2.0}" />
        <xacro:property name="l1_ty"    value="0.0" />
        <xacro:property name="l1_tz"    value="${l * cos_abs_leg1_rotz / 2.0}" />

        <xacro:property name="l2_rotx"  value="0.0" />
        <xacro:property name="l2_roty"  value="${-a}" />
        <xacro:property name="l2_rotz"  value="${pi / 3.0}" />

        <xacro:property name="l2_tx"    value="${sin_pi_sixth * (-l1_tx)}" />
        <xacro:property name="l2_ty"    value="${cos_pi_sixth * (-l1_tx)}" />
        <xacro:property name="l2_tz"    value="${l1_tz}" />

        <xacro:property name="l3_rotx"  value="0.0" />
        <xacro:property name="l3_roty"  value="${-a}" />
        <xacro:property name="l3_rotz"  value="${-pi / 3.0}" />

        <xacro:property name="l3_tx"    value="${l2_tx}" />
        <xacro:property name="l3_ty"    value="${-l2_ty}" />
        <xacro:property name="l3_tz"    value="${l1_tz}" />    

        <!-- leg 1 -->
		<inertial>
			<origin rpy="${l1_rotx} ${l1_roty} ${l1_rotz}" xyz="${l1_tx} ${l1_ty} ${l1_tz}" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
        <visual>
            <geometry>
                <cylinder length="${leg_length}" radius="${leg_radius}"/>
            </geometry>
            <origin rpy="${l1_rotx} ${l1_roty} ${l1_rotz}" xyz="${l1_tx} ${l1_ty} ${l1_tz}" />
            <material name="dark_gray" />        
        </visual>
		
		<collision>
			<origin rpy="${l1_rotx} ${l1_roty} ${l1_rotz}" xyz="${l1_tx} ${l1_ty} ${l1_tz}" />
			<geometry>
				<cylinder length="${leg_length}" radius="${leg_radius}"/>
			</geometry>			
		</collision>

        <!-- leg 2 -->
        <inertial>
			<origin rpy="${l2_rotx} ${l2_roty} ${l2_rotz}" xyz="${l2_tx} ${l2_ty} ${l2_tz}" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <cylinder length="${leg_length}" radius="${leg_radius}"/>
            </geometry>
            <origin rpy="${l2_rotx} ${l2_roty} ${l2_rotz}" xyz="${l2_tx} ${l2_ty} ${l2_tz}" />
            <material name="dark_gray" />        
        </visual>
		
		<collision>
			<origin rpy="${l2_rotx} ${l2_roty} ${l2_rotz}" xyz="${l2_tx} ${l2_ty} ${l2_tz}" />
			<geometry>
				<cylinder length="${leg_length}" radius="${leg_radius}"/>
			</geometry>			
		</collision>

        <!-- leg 3 -->
        <inertial>
			<origin rpy="${l3_rotx} ${l3_roty} ${l3_rotz}" xyz="${l3_tx} ${l3_ty} ${l3_tz}" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <cylinder length="${leg_length}" radius="${leg_radius}"/>
            </geometry>
            <origin rpy="${l3_rotx} ${l3_roty} ${l3_rotz}" xyz="${l3_tx} ${l3_ty} ${l3_tz}" />
            <material name="dark_gray" />              
        </visual>
		
		<collision>
			<origin rpy="${l3_rotx} ${l3_roty} ${l3_rotz}" xyz="${l3_tx} ${l3_ty} ${l3_tz}" />
			<geometry>
				<cylinder length="${leg_length}" radius="${leg_radius}"/>
			</geometry>			
		</collision>
    </link>
    
    <joint name="ground2legs" type="continuous">	      
	<origin xyz="0.0 0.0 0.0" rpy="0.0 0.0 0.0"/>
        <parent link="ground" />
        <child  link="legs" />
        <axis xyz="0.0 0.0 1.0" />
		<dynamics damping="0.7"/>  
    </joint>
    
    <!-- Base plane -->
    <link name="base">                       
        <!-- base board -->
		<inertial>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
        <visual>
            <geometry>
                <box size="${base_board_width} ${base_board_length} ${base_board_height}" />
            </geometry>
            <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
            <material name="light_gray" />
        </visual>
        
		<collision>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
			<geometry>
				<box size="${base_board_width} ${base_board_length} ${base_board_height}" />
			</geometry>			
		</collision>
		
        <!-- base cylinder -->
        <inertial>
			<origin rpy="0.0 0.0 0.0" 
                    xyz="0.0 0.0 ${base_board_height / 2.0 + base_cylinder_length / 2.0}" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <cylinder length="${base_cylinder_length}" radius="${base_cylinder_radius}" />
            </geometry>
            <origin rpy="0.0 
                         0.0 
                         0.0" 
                    xyz="0.0 
                         0.0 
                         ${base_board_height / 2.0 + base_cylinder_length / 2.0}" />
            <material name="light_gray" />
        </visual>
		
		<collision>
			<origin rpy="0.0 0.0 0.0" 
                    xyz="0.0 0.0 ${base_board_height / 2.0 + base_cylinder_length / 2.0}" />
			<geometry>
				<cylinder length="${base_cylinder_length}" radius="${base_cylinder_radius}" />
			</geometry>			
		</collision>
        
        <!-- bottom part of the manipulator (bottom motor) -->
         <inertial>
			<origin rpy="0.0 0.0 0.0" 
                    xyz="0.0 ${-manip_bottom_2_base_cylinder_offset_y} ${base_board_height / 2.0 + base_cylinder_length + manip_bottom_height / 2.0}" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <box size="${manip_bottom_width} ${manip_bottom_length} ${manip_bottom_height}" />
            </geometry>
            <origin rpy="0.0 
                         0.0 
                         0.0" 
                    xyz="0.0 
                        ${-manip_bottom_2_base_cylinder_offset_y} 
                        ${base_board_height / 2.0 + base_cylinder_length + manip_bottom_height / 2.0}" />
            <material name="light_gray" />
        </visual>
		
		<collision>
			<origin rpy="0.0 0.0 0.0" 
                    xyz="0.0 ${-manip_bottom_2_base_cylinder_offset_y} ${base_board_height / 2.0 + base_cylinder_length + manip_bottom_height / 2.0}" />
			<geometry>
				<box size="${manip_bottom_width} ${manip_bottom_length} ${manip_bottom_height}" />
			</geometry>			
		</collision>		
    </link>
    
    <joint name="legs2base" type="continuous">	
		<xacro:property name="legs2base_z"  value="${2.0 * l1_tz}" />      
        <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 ${legs2base_z}"/>
		<axis xyz="1.0 0.0 0.0" />
        <parent link="legs" />
        <child  link="base" />     
		<dynamics damping="0.7"/>  
    </joint>
    
    <!-- Upper part of the manipulator which rotates in the Z axis (pan) - upper motor -->
    <link name="manip_azi">                              
        <inertial>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 ${-tilt_axis_2_manip_top_offset_z}" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <box size="${manip_top_width} ${manip_top_length} ${manip_top_height}" />
            </geometry>
            <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 ${-tilt_axis_2_manip_top_offset_z}" />
            <material name="blue" />
        </visual>
		
		<collision>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 ${-tilt_axis_2_manip_top_offset_z}" />
			<geometry>
				<box size="${manip_top_width} ${manip_top_length} ${manip_top_height}" />
			</geometry>			
		</collision>		
    </link>
    
    <joint name="azimuth" type="continuous">
	<!-- joint position -->	
	<xacro:property name="base2azi_y"  value="${-manip_bottom_2_base_cylinder_offset_y - pan_axis_2_manip_bottom_offset_y}" />
	<xacro:property name="base2azi_z"  value="${base_board_height / 2.0 + base_cylinder_length + manip_bottom_height + manip_top_height / 2.0 +  tilt_axis_2_manip_top_offset_z}" />
	
        <origin rpy="0.0 0.0 0.0" xyz="0.0 ${base2azi_y} ${base2azi_z}"/>
        <parent link="base" />
        <child  link="manip_azi" />
        <axis xyz="0.0 0.0 1.0" />
		<dynamics damping="0.7"/>  
    </joint>
    
    <!-- Upper U-shaped part of the manipulator and the rod which rotates in the Y axis (tilt) -->
    <link name="manip_ele">                         
        <inertial>
			<origin rpy="${pi / 2.0} 0.0 0.0" xyz="0.0 0.0 0.0" />       
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <cylinder length="${manip_rod_length}" radius="${manip_rod_radius}" />
            </geometry>
            <origin rpy="${pi / 2.0} 0.0 0.0" xyz="0.0 0.0 0.0" />       
            <material name="green" />
        </visual>
		
		<collision>
			<origin rpy="${pi / 2.0} 0.0 0.0" xyz="0.0 0.0 0.0" />       
			<geometry>
				<cylinder length="${manip_rod_length}" radius="${manip_rod_radius}" />
			</geometry>			
		</collision>		
    </link>
    
    <joint name="elevation" type="continuous">	      
        <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0"/>
        <parent link="manip_azi" />
        <child  link="manip_ele" />
        <axis xyz="0.0 -1.0 0.0" />
		<dynamics damping="0.7"/>  
    </joint>
    
    <!-- Camera -->
    <link name="camera">                                         
        <inertial>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />       
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
		<visual>
            <geometry>
                <box size="${camera_width} ${camera_length} ${camera_height}" />
            </geometry>
            <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />       
            <material name="red" />
        </visual>                
        
		<collision>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />       
			<geometry>
				<box size="${camera_width} ${camera_length} ${camera_height}" />
			</geometry>			
		</collision>		
		
		<inertial>
			<origin rpy="0.0 ${pi / 2.0} 0.0" xyz="${camera_width / 2.0 + lense_length / 2.0} 0.0 0.0" />       
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
        <visual>
            <geometry>
                <cylinder length="${lense_length}" radius="${lense_radius}" />
            </geometry>
            <origin rpy="0.0 ${pi / 2.0} 0.0" xyz="${camera_width / 2.0 + lense_length / 2.0} 0.0 0.0" />       
            <material name="black" />
        </visual>
        
		<collision>
			<origin rpy="0.0 ${pi / 2.0} 0.0" xyz="${camera_width / 2.0 + lense_length / 2.0} 0.0 0.0" />       
			<geometry>
				<cylinder length="${lense_length}" radius="${lense_radius}" />
			</geometry>			
		</collision>		
    </link>        
    
    <joint name="ele2camera" type="fixed">        
		<!-- joint position -->	
		<xacro:property name="ele2camera_y"  value="${-manip_rod_length / 2.0 - camera_length / 2.0}" />	
        <origin rpy="${camera_phi} ${camera_psi} ${camera_theta}" xyz="0.0 ${ele2camera_y} 0.0"/>
        <parent link="manip_ele" />
        <child  link="camera" />        
		<dynamics damping="0.7"/>  
    </joint>
    
    <!-- Focal point of the camera -->
    <link name="focus">            
		<inertial>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
			<mass value="1"/>
			<inertia
				ixx="1.0" ixy="0.0" ixz="0.0"
				iyy="1.0" iyz="0.0"
				izz="1.0"/>
		</inertial>
		
        <visual>                           
            <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />
			<geometry>
                <cylinder length="0.0" radius="0.0"/>
            </geometry>
            <material name="dark_gray" />        
        </visual>
		
		<collision>
			<origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />		
			<geometry>
                <cylinder length="0.0" radius="0.0"/>
            </geometry>
		</collision>

    </link>  
    
    <joint name="camera2focus" type="prismatic">
        <!-- joint position -->	
        <origin rpy="0.0 0.0 0.0" xyz="0.0 0.0 0.0" />        	
        <parent link="camera" />
        <child  link="focus" />
	<axis xyz="-1.0 0.0 0.0" />
	<limit effort="1000" velocity="1000" lower="0.001" upper="0.400" />
	<dynamics damping="0.7"/>  	
    </joint>
</robot>
