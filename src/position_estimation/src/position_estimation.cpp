/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "position_estimation.h"

// C++
#include <cmath>

// Geo
#include "geo_convertor.h"

// Time
#include <sys/time.h>

using namespace std;

double GetTime(void)
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL) == -1) {
        cerr << "Position Estimation: Obtaining current system time failed" << endl;
    }

    return (double)tv.tv_sec + (double)tv.tv_usec / 1000.0;  				/* return time in [ms] */
}


const string PositionEstimation::TARGET_STATE_LOCAL_TOPIC   = "target_state_local";
const string PositionEstimation::TARGET_STATE_TOPIC         = "target_state";
const string PositionEstimation::POS_ESTIM_HISTORY_PCLOUD_TOPIC = "pos_estim_history_pc";

const int PositionEstimation::TARGET_POSITION_HISTORY_SIZE  = 3;

const int PositionEstimation::POS_3D_HISTORY_SIZE = 30;
const double PositionEstimation::OMEGA = 5.0;

#if GROUND_TRUTH_POSITION
const string PositionEstimation::POSE_TOPIC                 = "pose";
#endif

#if !GROUND_TRUTH_POSITION
PositionEstimation::PositionEstimation() : tgtPosHistory_(NULL), tgtPosHistIdx_(0), saveGPX_(0)
#else
PositionEstimation::PositionEstimation() : tphIdx_(0), tgtPosHistory_(NULL), tgtPosHistIdx_(0), saveGPX_(0)
#endif
{    
    nh_ = new ros::NodeHandle();

    if (!ros::param::get("/camera_unit_ns", cameraUnitNS_)) {
        ROS_FATAL_STREAM("Could not get parameter camera_unit_ns");
        exit(1);
    }

    if (!ros::param::get("/camera_units_count", CAMERA_UNITS_COUNT)) {
        ROS_FATAL_STREAM("Could not get parameter camera_units_count");
        exit(1);
    }

    if (!ros::param::get("/save_gpx", saveGPX_)) {
        ROS_WARN_STREAM("Could not get parameter save_gpx. Using default value false");
        saveGPX_ = false;
    }

    onTargetStateLocalSub_  = nh_->subscribe(TARGET_STATE_LOCAL_TOPIC, 30, &PositionEstimation::onTargetStateLocal, this);
    targetStatePub_        = nh_->advertise<msgs::TargetState>(TARGET_STATE_TOPIC, 10);

    // debug
    posEstimPCPub_ = nh_->advertise<sensor_msgs::PointCloud2>(POS_ESTIM_HISTORY_PCLOUD_TOPIC, 10);
    ransacLinemarkerPub_ = nh_->advertise<visualization_msgs::Marker>("/visualization_marker", 10);

#if GROUND_TRUTH_POSITION
    if (!ros::param::get("/flying_object_ns", flying_object_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter /flying_object_ns");
        exit(1);
    }

    string poseTopic = string("/") + flying_object_ns_ + string("/") + POSE_TOPIC;
    onPoseSub_ = nh_->subscribe(poseTopic, 10, &PositionEstimation::onPose, this);
#endif

    tgtPosHistory_ = new Eigen::Vector3d[TARGET_POSITION_HISTORY_SIZE];

    Eigen::Vector3d v;
    v << 0.0, 0.0, 0.0;

    for(int i = 0; i < TARGET_POSITION_HISTORY_SIZE; ++i) {
        tgtPosHistory_[i] = v;
    }

    if(saveGPX_) {
        gpxFile_.open("/home/jan/FIT/DP/ols/track.gpx");
        gpxFile_ << std::setprecision(12);
    }
}

PositionEstimation::~PositionEstimation()
{
    for(std::map<int, Pos3DHistory *>::iterator it = pos3DHistory_.begin(); it != pos3DHistory_.end(); ++it) {
        delete it->second;
    }

    if(tgtPosHistory_) delete[] tgtPosHistory_;

    if(saveGPX_) {
        gpxFile_.close();
    }
}

void PositionEstimation::onTargetStateLocal(msgs::TargetStateLocal::ConstPtr stateMsg)
{
    // New target
    if(targetsStates_.count(stateMsg->id_global) == 0) {
        std::map<int, msgs::TargetStateLocal::ConstPtr> newTgt;
        newTgt[stateMsg->camera_unit] = stateMsg;
        targetsStates_[stateMsg->id_global] = newTgt;

        // Create 3D position history for this new target.
        pos3DHistory_[stateMsg->id_global] = new Pos3DHistory(POS_3D_HISTORY_SIZE);
        pos3DHistory_[stateMsg->id_global]->setPosHistPublisher(&posEstimPCPub_);
        pos3DHistory_[stateMsg->id_global]->setRansacLinePublisher(&ransacLinemarkerPub_);

    // Existing target
    } else {
        // Add new local state to the given target's (id_global) records
        (targetsStates_[stateMsg->id_global])[stateMsg->camera_unit] = stateMsg;

        // If there are already 2 local states, send targat to proecess queue for triangulation
        // (before it gets processed, new local states might arrive and they will be added this record).
        if((targetsStates_[stateMsg->id_global]).size() == 2) {
            processQueue_[stateMsg->id_global] = &(targetsStates_[stateMsg->id_global]);
        }
    }
}

void PositionEstimation::update()
{
    // debug
    static int ts = 123456;

    // Process target local states queue
    for(std::map<int, std::map<int, msgs::TargetStateLocal::ConstPtr> *>::iterator it = processQueue_.begin(); it != processQueue_.end(); it++) {
        std::vector<Eigen::Vector3d> from;
        std::vector<Eigen::Vector3d> direction;
        std::vector<ros::Time> stamps;        
        std::vector<float> beliefs;
        int globId;
        double avgSpeed = 0.0;

        for(std::map<int, msgs::TargetStateLocal::ConstPtr>::iterator itUnits = (it->second)->begin(); itUnits != (it->second)->end(); itUnits++) {
            geometry_msgs::Vector3 msgFrom = itUnits->second->focal_point;
            geometry_msgs::Vector3 msgDirection = itUnits->second->direction;
            globId = itUnits->second->id_global;

            Eigen::Vector3d eigFrom, eigDirection;
            eigFrom << msgFrom.x ,msgFrom.y, msgFrom.z;
            eigDirection << msgDirection.x, msgDirection.y, msgDirection.z;

            from.push_back(eigFrom);
            direction.push_back(eigDirection);
            stamps.push_back(itUnits->second->imgTimeStamp);
            beliefs.push_back(itUnits->second->belief);
        }

        if (from.size() > 0)
        {
            Eigen::Vector3d targetPos = estimateTargetPosition(from, direction, beliefs);
            ros::Time avgStamp = avgTimeStamp(stamps);

            tgtPosHistory_[tgtPosHistIdx_] = targetPos;
            incCircBufIdx(tgtPosHistIdx_, TARGET_POSITION_HISTORY_SIZE);

            // Update 3D position history
            Pos3DHistory *posHist = pos3DHistory_[globId];
            posHist->addPosition(targetPos, avgStamp);

            // Estimate 3D speed vector
            Eigen::Vector3d estimSpeed;
            bool speedFound = posHist->estimateSpeed(estimSpeed);

            if(speedFound) {
                avgSpeed = estimSpeed.norm();
            }

            targetPos = getAvgPosition();

#if GROUND_TRUTH_POSITION
            geometry_msgs::PoseStamped *poseGT = getClosestStampedPose(avgTimeStamp(stamps));
            Eigen::Vector3d targetPosGT;
            targetPosGT << poseGT->pose.position.x, poseGT->pose.position.y, poseGT->pose.position.z;

            Eigen::Vector3d pd = targetPosGT - targetPos;
#endif // #if GROUND_TRUTH_POSITION

            Eigen::Matrix3d T, R;

            // zebetin
            T << 1.0, 0.0, 608708.55,
                 0.0, 1.0, 5453089.09,
                 0.0, 0.0, 1.0;

            // oprox
//            T << 1.0, 0.0, 620071.41,
//                 0.0, 1.0, 5452256.60,
//                 0.0, 0.0, 1.0;

            // vut
//            T << 1.0, 0.0, 614440.75,
//                 0.0, 1.0, 5453734.50,
//                 0.0, 0.0, 1.0;

            // zebetin
            double alpha = 0.359629608873;

            // oprox
//            double alpha = 2.09841506325;

            // vut
//            double alpha = -2.02171415813;
//            double alpha = 0.450917831332;

            double phi = -(alpha + M_PI / 2.0);

            R << std::cos(phi), -std::sin(phi), 0.0,
                 std::sin(phi),  std::cos(phi), 0.0,
                 0.0,            0.0,           1.0;

            Eigen::Matrix3d M = T * R;
            Eigen::Vector3d targetPosTmp = targetPos;

            targetPosTmp(2) = 1.0;
            Eigen::Vector3d targetPosGlob = M * targetPosTmp;

            // Send estimated position to controls node
            msgs::TargetState tgtStateMsg;
            tgtStateMsg.id = globId;
            tgtStateMsg.position.x  = targetPos(0);
            tgtStateMsg.position.y  = targetPos(1);
            tgtStateMsg.position.z  = targetPos(2);
            tgtStateMsg.avgSpeed    = avgSpeed;
            tgtStateMsg.estimSpeed.x = estimSpeed(0);
            tgtStateMsg.estimSpeed.y = estimSpeed(1);
            tgtStateMsg.estimSpeed.z = estimSpeed(2);
            tgtStateMsg.targetObject = it->second->begin()->second->targetObject;
            tgtStateMsg.easting     = targetPosGlob(0);
            tgtStateMsg.northing    = targetPosGlob(1);
            tgtStateMsg.zone        = 33;

            targetStatePub_.publish(tgtStateMsg);

            // Save data to GPX
            if(saveGPX_) {
                double lat, lon, alt;
                UTMtoLL(eWGS84, targetPosGlob(1), targetPosGlob(0), 33 , lat, lon);
                std::cout << setprecision(10) << "latitude = " << lat << ", longitude = " << lon << std::endl;
                alt = 0.0;
                newGPXdata(gpxFile_, lat, lon, alt, ts++);
            }
        }

        // Clear a record about given target (map).
        it->second->clear();
    }

    // Clear the whole process queue (map).
    processQueue_.clear();
}

Eigen::Vector3d PositionEstimation::estimateTargetPosition(std::vector<Eigen::Vector3d>& from, std::vector<Eigen::Vector3d>& direction, std::vector<float>& beliefs)
{
    using namespace Eigen;

    // Number of camera units and bases.
    int Ncu = from.size();
    int Nb  = Ncu * (Ncu - 1) / 2;

    // Bases vectors and nase centers
    std::vector<Vector3d> bases(Nb);
    std::vector<Vector3d> basesCenters(Nb);

    // Compute target estimate for each base with regards to tracker beliefs.
    std::vector<Vector3d> TNoGeom(Nb);
    int bi = 0;

    for(int ci = 0; ci < from.size() - 1; ++ci) {
        for(int cj = ci + 1; cj < from.size(); ++cj) {
            Vector3d M1 = from[ci];
            Vector3d M2 = from[cj];
            Vector3d u  = direction[ci];
            Vector3d v  = direction[cj];
            float bel1 = beliefs[ci];
            float bel2 = beliefs[cj];

            // Get normal vectors nu and nv of M1-T' and M2-T'' plane
            Vector3d m = M2 - M1;
            Vector3d nu = (m.cross(u)).normalized();
            Vector3d nv = (m.cross(v)).normalized();
            bases[bi]        = m;
            basesCenters[bi] = M1 + (m / 2.0);

            // Find angle alpha between normal vecotr nu and nv
            double a = std::acos(nu.dot(nv));

            // Find angles to rotate the direction vectors to project them into plane M12-T.
            // Angles au, av might be different to represent belief of each tracker (direction vectors),
            // but the constraint ((bel1 >= 0.0) && (bel2 >= 0.0) && (bel1 + bel2 = 1.0)) must hold!
            double au = a * (bel1 / (bel1 + bel2));
            double av = a * (bel2 / (bel1 + bel2));

            // Rotate vectors u and v along m axis to project them into the plane M12-T (and get vectors u' and v')
            Vector3d ax  = (nu.cross(nv)).normalized();

            Matrix3d cpmax, tpax; // Cross product matrix and tensor product of 'ax'.
            cpmax << 0.0, -ax(2), ax(1), ax(2), 0.0, -ax(0), -ax(1), ax(0), 0.0;
            tpax = ax * ax.transpose();

            Matrix3d Ru = std::cos(au)  * MatrixXd::Identity(3, 3) + std::sin(au)  * cpmax + (1.0 - std::cos(au))  * tpax;
            Matrix3d Rv = std::cos(-av) * MatrixXd::Identity(3, 3) + std::sin(-av) * cpmax + (1.0 - std::cos(-av)) * tpax;

            Vector3d uu = Ru * u;
            Vector3d vv = Rv * v;

            // Check whether vectors u' and v' are paralel
            Vector3d p = uu.cwiseQuotient(vv);
            if(p(0) == p(1) == p(2)) ROS_WARN_STREAM("Cannot estimate the 3D position of the target, direction vectors are paralel.");

            // Find intersection between u' and v' -> estimated position of the target T
            double s = (uu(0) * (M1(1) - M2(1)) + uu(1) * (M2(0) - M1(0))) / (uu(0) * vv(1) - uu(1) * vv(0) + 1e-9);

            TNoGeom[bi++] = M2 + s * vv;
        }
    }

    Vector3d TNG = std::accumulate(TNoGeom.begin(), TNoGeom.end(), Vector3d(0.0, 0.0, 0.0)) / Nb;

    // Compute final target estimate regarding the geometry given as target-base angle.
    std::vector<double> weights(Nb);
    for(int bi = 0; bi < Nb; ++bi) {
        weights[bi] = std::exp(OMEGA * (1.0 - std::fabs(bases[bi].normalized().dot((TNG - basesCenters[bi]).normalized()))));
    }

    double normalizer = std::accumulate(weights.begin(), weights.end(), 0.0);

    Vector3d T(0.0, 0.0, 0.0);
    for(int bi = 0; bi < Nb; ++bi) {
        T += TNoGeom[bi] * weights[bi];
    }
    T /= normalizer;

    return T;
}

ros::Time PositionEstimation::avgTimeStamp(std::vector<ros::Time>& stamps)
{
    double sum = 0;
    for(std::vector<ros::Time>::iterator it = stamps.begin(); it != stamps.end(); ++it) {
        sum += it->toSec();
    }

    double tAvg = sum / (double)stamps.size();

    return ros::Time(tAvg);
}

Eigen::Vector3d PositionEstimation::getAvgPosition()
{
    Eigen::Vector3d avgPos;
    avgPos << 0.0, 0.0, 0.0;

    int idx = ((tgtPosHistIdx_ == 0) ? (TARGET_POSITION_HISTORY_SIZE - 1) : (tgtPosHistIdx_ - 1));
    for(int i = 0; i < TARGET_POSITION_HISTORY_SIZE; ++i) {
        avgPos += tgtPosHistory_[idx];
        idx = ((idx == 0) ? (TARGET_POSITION_HISTORY_SIZE - 1) : (idx - 1));
    }

    avgPos = avgPos / TARGET_POSITION_HISTORY_SIZE;

    return avgPos;
}

void PositionEstimation::incCircBufIdx(int &idx, int size)
{
    idx = ((idx == (size - 1)) ? 0 : idx + 1);
}

void PositionEstimation::newGPXdata(ofstream& file, double latitude, double longitude, double altitude, double timestamp)
{
    gpxFile_ << latitude << ", " << longitude << ", " << altitude << ", " << timestamp << std::endl;
}

#if GROUND_TRUTH_POSITION
void PositionEstimation::onPose(const geometry_msgs::PoseStampedConstPtr &pose)
{
    tgtPoseHistory_[tphIdx_] = *pose;
    tphIdx_ = (tphIdx_ + 1) % TGT_POSE_HISTORY_SIZE;
}

geometry_msgs::PoseStamped* PositionEstimation::getClosestStampedPose(ros::Time stamp)
{
    int ci = ((tphIdx_ == 0) ? (TGT_POSE_HISTORY_SIZE - 1) : (tphIdx_ - 1));

    double tdBest = numeric_limits<double>::max();
    for(int i = 0; i < TGT_POSE_HISTORY_SIZE; ++i) {
        double td = std::abs((tgtPoseHistory_[ci].header.stamp - stamp).toSec());
        if(td < tdBest) {
            tdBest = td;
        } else {
            break;
        }

        ci = (ci == 0 ? (TGT_POSE_HISTORY_SIZE - 1) :  (tphIdx_ - 1));
    }

//    ROS_INFO_STREAM("Time diff: " << tdBest);

    return &(tgtPoseHistory_[ci]);
}

#endif

Pos3DHistory::Pos3DHistory(int maxSize) : maxSize_(maxSize), posHistPub_(NULL), ransacLinePub_(NULL)
{
    positions_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
    positions_->reserve(maxSize_);
    stamps_.reserve(maxSize_);
}

void Pos3DHistory::addPosition(Eigen::Vector3d &newPos, ros::Time stamp)
{
    positions_->push_back(pcl::PointXYZ(newPos(0), newPos(1), newPos(2)));
    stamps_.push_back(stamp);

    // If the number of records exceeds maximum allowed size, erase the oldest record.
    if(positions_->size() > maxSize_) {
        positions_->erase(positions_->begin());
        stamps_.erase(stamps_.begin());
    }

    assert(positions_->size() == stamps_.size());
}

bool Pos3DHistory::estimateSpeed(Eigen::Vector3d& speed)
{
    bool success = false;

    if(positions_->size() > 20) {
        pcl::SampleConsensusModelLine<pcl::PointXYZ>::Ptr modelLine(new pcl::SampleConsensusModelLine<pcl::PointXYZ>(positions_));
        pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(modelLine);

        ransac.setDistanceThreshold(1.0);
        ransac.computeModel();
        ransac.refineModel();

        Eigen::VectorXf modelCoeff;
        ransac.getModelCoefficients(modelCoeff);

        modelFrom_ << modelCoeff(0), modelCoeff(1), modelCoeff(2);
        modelTo_   << modelCoeff(3), modelCoeff(4), modelCoeff(5);

        // Project inliers to the line.
        pcl::PointCloud<pcl::PointXYZ>::Ptr projectedPoints(new pcl::PointCloud<pcl::PointXYZ>);
        std::vector<int> inliers;
        ransac.getInliers(inliers);
        modelLine->projectPoints(inliers, modelCoeff, *projectedPoints);

        pcl::PointCloud<pcl::PointXYZ>::Ptr projectedInliers(new pcl::PointCloud<pcl::PointXYZ>(*projectedPoints, inliers));

        // Estimate speed vector.
        if(inliers.size() >= 2) {
            success = true;

            speed << 0.0, 0.0, 0.0;

            Eigen::Vector3d start;
            Eigen::Vector3d end;
            pcl::PointXYZ* pStart;
            pcl::PointXYZ* pEnd;

            // weighted average of speeds measured between consecutive points:
            // v_avg = (dt1 * v1 + dt2 * v2 + ... + dtn * vn) / sum_{i = 1..n} dti =
            //       = (dt1 * (p2 - p1) / dt1 + ... + dtn * (pn+1 - pn) / dtn) / sum_{i = 1..n} dti =
            //       = ((p2 - p1) + ... + (pn+1 - pn)) / sum_{i = 1..n} dti
            for(int i = 1; i < inliers.size(); ++i) {
                pStart = &(projectedPoints->at(inliers[i - 1]));
                pEnd   = &(projectedPoints->at(inliers[i]));

                start << pStart->x, pStart->y, pStart->z;
                end   << pEnd->x,   pEnd->y  , pEnd->z;

                speed += end - start;
            }

            // normalize by total time
            speed /= (stamps_[inliers.back()] - stamps_[inliers.front()]).toSec();
        }

#if VISUALIZE_POS_HISTORY
        // debug - publish position history point cloud for rviz
        if(posHistPub_    != NULL) publishPosHistPC(positions_);
        if(ransacLinePub_ != NULL) {
            publishVisMarkerLine(101, modelFrom_ - 10.0 * modelTo_, modelFrom_ + 10.0 * modelTo_,
                                 1.0, 1.0, 0.0);

            if(success) {
                Eigen::Vector3d speedFrom;
                pcl::PointXYZ speedFromP = projectedPoints->back();
                speedFrom << speedFromP.x, speedFromP.y, speedFromP.z;
                publishVisMarkerLine(102, speedFrom, speedFrom + speed, 1.0, 0.0, 0.0, 0.03, 0.3);
            }
        }
#endif // #if VISUALIZE_POS_HISTORY
    }

    return success;
}

void Pos3DHistory::print()
{
    std::cout << "Position history: " << std::endl;
    for(int i = 0; i < positions_->size(); ++i) {
        std::cout << i << ": pos: " << positions_->at(i) << ", stamp: " << stamps_[i] << std::endl;
    }
}

void Pos3DHistory::publishPosHistPC(pcl::PointCloud<pcl::PointXYZ>::Ptr pcloud)
{
    sensor_msgs::PointCloud2 posHistMsg;
    pcl::toROSMsg(*pcloud, posHistMsg);
    posHistMsg.header.frame_id = "world";
    posHistPub_->publish(posHistMsg);
}

void Pos3DHistory::publishVisMarkerLine(int id, Eigen::Vector3d from, Eigen::Vector3d to,
                                        float r, float g, float b,
                                        float lineThickness, float headSize)
{
    visualization_msgs::Marker markerMsg;

    markerMsg.header.frame_id   = string("/world");
    markerMsg.header.stamp      = ros::Time::now();
    markerMsg.ns = "basic_shapes";
    markerMsg.id = id;
    markerMsg.type = visualization_msgs::Marker::ARROW;
    markerMsg.action = visualization_msgs::Marker::ADD;
    markerMsg.scale.x = lineThickness;
    markerMsg.scale.y = headSize;
    markerMsg.scale.z = 0.5;
    markerMsg.color.r = r;
    markerMsg.color.g = g;
    markerMsg.color.b = b;
    markerMsg.color.a = 1.0;
    markerMsg.lifetime = ros::Duration();
    markerMsg.frame_locked = false;

    geometry_msgs::Point ptFrom;
    ptFrom.x = from(0);
    ptFrom.y = from(1);
    ptFrom.z = from(2);
    geometry_msgs::Point ptTo;
    ptTo.x = to(0);
    ptTo.y = to(1);
    ptTo.z = to(2);

    markerMsg.points.resize(2);
    markerMsg.points[0] = ptFrom;
    markerMsg.points[1] = ptTo;

    ransacLinePub_->publish(markerMsg);
}
