//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#pragma once
#ifndef PFTRACKER_H
#define PFTRACKER_H

// IOSS libraries.
//#include "Tracker.h"
#include "PFTarget.h"
#include "BGModel.h"
//#include "ImageObservation.h"

// Cuda implementation of evaluation function for the PF.
#include "./cuda/Particles.h"
#include "./cuda/test.h"

// OpenCVX libraries.
#include <opencvx/cvparticle.h>

//! Basic class for visual tracking.
// This class runs in own thread; therefore all methods has to be thread safe!
class TrackerBase  {
public:
    //! Constructor.
    TrackerBase() {}
    //! Destructor.
    virtual ~TrackerBase() {}

    //! Returns target.
    std::shared_ptr<TargetBase> target(unsigned long long id);

/*signals:
    //! This signal is emitted when a target has been tracked (in a new observation).
    void targetTracked(BIPWrapper<TargetState> tState);
    //! This signal is emitted when the target has been removed from the TrackerBase.
    void targetRemoved(unsigned long long id);
    //! This signal is emitted when a target has been added.
    void targetAdded(BIPWrapper<TargetState> tState);
*/
public:
    //! Remove target.
    //virtual void removeTarget(unsigned long long target_id) = 0;
    //! Correct target.
    //virtual void correctTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect) = 0;
    //! Add target.
    virtual void addTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect) = 0;

    /*//! Set type of the target.
    virtual void setTargetType(unsigned long long tg_id, QString type);
    //! Set description.
    virtual void setTargetDescription(unsigned long long tg_id, QString description);*/

    //! Return targets state. Take care it is necessary to not store pointer for long time SIGSEGW
    void get_targets(std::map<unsigned long long, std::shared_ptr<TargetBase> >** targets) {
        (*targets) = &targets_;
    }

protected:
    std::map<unsigned long long, std::shared_ptr<TargetBase> > targets_;
};

// Forward declaration.
class PFTracker;

//! State of the PFTracker.
class PFTrackerState {
    friend class PFTracker;

public:
    //! Constructor.
    PFTrackerState() { ; }

    //! Constructor.
    PFTrackerState(unsigned long long id, cv::gpu::GpuMat obs, cv::gpu::GpuMat bg, cv::Mat H) :
        id_(id),
        obs_(obs),
        bg_(bg),
        H_(H)
    {
        ;
    }

    //! Destructor.
    virtual ~PFTrackerState() { ; }

    //! Returns ID of the state.
    unsigned long long getID() const { return id_; }

protected:
    unsigned long long id_;
    cv::gpu::GpuMat obs_;
    cv::gpu::GpuMat bg_;                                        //!< Background model.
    cv::Mat H_;                                                 //!< Transformation to the next image.
};

//! Visual tracker based on particle filter.
class PFTracker : public TrackerBase
{    
    //Q_OBJECT
public:
    //! Constructor.
    PFTracker();
    //! Destructor.
    virtual ~PFTracker();

    //! Load configuration from the settings.
    virtual void fromSettings(CSettings &settings);
    //! Save current configuration to the settings.
    virtual void toSettings(CSettings &);

/*public slots:
    //! Reset tracker to initial state.
    void restart();*/

public:
    //! Incoming image observation.
    virtual void observation(const cv::Mat& obs, uint32_t img_seq_id);

    // TrackerBase interface.
    //! Correct target state (user input).
    //TODO: virtual void correctTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect);

    //! Add a new target.
    virtual void addTarget( unsigned long long target_id,        //!< Identification number of the target.
                            unsigned long long obs_id,           //!< Identification number of the observation in which the target has been selected.
                            cv::Rect rect                        //!< The bounding box of the target.
                           );

    //! Remove target.
    virtual void removeTargets();

protected:    
    // Track from current state to the obs (via all data in the history).
    void trackToObservation(std::shared_ptr<PFTarget> t, unsigned long long obs_id);
    void track(unsigned long long obs_id, cv::gpu::GpuMat obs, cv::gpu::GpuMat fg, std::shared_ptr<PFTarget> t, cv::Mat H);
    void track2(unsigned long long obs_id, cv::gpu::GpuMat obs, cv::gpu::GpuMat bg, std::shared_ptr<PFTarget> t, cv::Mat H);

    void evaluateParticles(cv::gpu::GpuMat img, cv::gpu::GpuMat fg, cv::gpu::GpuMat target, cv::gpu::GpuMat mask, CvParticle *p, cv::Mat H);
    std::shared_ptr<PFTarget> createTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect);

    //! Return common part of image and rectangle.
    cv::Rect getJointPart(cv::Rect rect, cv::Size img);

private:
    void showCurrentState(cv::Mat gray_img);        //!< For debug purposes.
    void drawParticle(CvParticleState &p, cv::Mat &H, cv::Mat &img, cv::Scalar &color, bool drawBoundingBox = false, bool drawVelocityVector = false);
    void particleTransition(CvParticle *p, cv::gpu::GpuMat fg, std::shared_ptr<PFTarget>& t);
    void ParticleStateToMat(const CvParticleState &state, CvMat *state_mat);
    CvParticleState ParticleStateGet(const CvParticle *p, int p_id);
protected:
    BGModel bg_;
    uint32_t lastObs_;

    int histCapacity_;
    std::map<unsigned long long, PFTrackerState> hist_;             //!< History.
    unsigned int maxNumOfOverlappedInSequence_;

    // Cuda variables.
    Particles *p_device_;
    Particles *p_host_;
    Mat *m_host_;
    Mat *m_device_;

    // Init target parameters.
    int t_total_num_particles_;
    int t_num_particles_from_movement_;
    bool t_use_target_mask_;
    bool use_homography_;
    bool t_logprob_;
    float t_std_x_, t_std_y_;
    float t_std_vx_, t_std_vy_;
    float t_std_width_, t_std_height_;
    float t_std_angle_;
};

#endif
