/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef LENSE_H
#define LENSE_H

// C++
#include <iostream>
#include <cstdlib>

// Serial library
#include <serial/serial.h>

using namespace std;

//! \brief The Lense class Controling the zooming lens on overview camera unit.
//!
class Lense {
public:
    int MAX_ZOOM = 255;
    int MIN_ZOOM = 0;
    int MAX_FOCUS = 255;
    int MIN_FOCUS = 0;
    int MAX_IRIS = 255;
    int MIN_IRIS = 0;

public:
    //! \brief Lense Constructor - lens with variable focal length is used.
    //!
    Lense(std::string port, uint32_t baudrate, uint32_t timeout, double focalLengthMin, double focalLengthMax);

    //! \brief Lense Constructor - lens with fixed focal length is used.
    //!
    Lense(double focalLength);

    //! Destructor
    ~Lense();

    void zoom(int step);
    void focus(int step);
    void iris(int speed, int direction);

    //! \brief reset Resets zoom, iris and iris to their default values (= 0)
    //!
    void reset();

    //! \brief focalLength Returns current focal length.
    //! \return
    //!
    double focalLength();

private:
    bool adjustableLens_;

    serial::Serial* serialIf_;   //!< Serial interface, communication with the manipulator.

    double focalLengthMin_;
    double focalLengthMax_;

    int16_t zoom_;
    int16_t focus_;

private:
    //! \brief command Sends the 'cmd' message to the manipulator, reads
    //! the response message and checks whether it equals '0x00'.
    //! If not or if the manipulator responds with error message (the message
    //! includes character '!') the excpetion is thrown.
    //! \param cmd command
    //!
    void command(string cmd, uint32_t readTimeout = 0);

    //! \brief write Sends the command 'cmd' of length 'length' bytes to the manipulator.
    //! \param cmd
    //! \param length
    //!
    void write(string &cmd);

    //! \brief read Reads the response from the manipulator and saves it to the 'response'.
    //! Checks whether manipulator responds with error message (such a message includes
    //! character '!') and if so, throws the exception with this error message.
    //! \param response
    //!
    void read(string &response, uint32_t waitTime = 0);



    string prettyHexStr(string hexNums);
    string int2byteStr(uint32_t num, int bytes);
};

#endif // #ifndef LENSE_H
