#!/bin/python

## This scripts converts the given Euler angles (alpha, beta, gamma) 
## representing the specified sequence of rotations about three major axes
## (i.e. the order of axes X,Y,Z is specified by the user)
## to new angles (phi, psi, theta) representing another specified sequence 
## of rotations about three major axes, so that the resulting rotation
## transformation would remain the same.
##
## NOTE: example: if input sequence 'yxz' is required, then the input angles are
## trated as alpha = rot. about Y, beta = rot. about X, gamma = rot. about Z.
## The same applies for output sequence
##
## A triple of Euler angles can be applied/interpreted in 24 ways, which can
## be specified using a 4 character string or encoded 4-tuple:
## 
## *Axes 4-string*: e.g. 'sxyz' or 'ryxy'
## 
## - first character : rotations are applied to 's'tatic or 'r'otating frame
## - remaining characters : successive rotation axis 'x', 'y', or 'z'
## 

import sys
import transformations as tf

if __name__ == "__main__":

    # help
    if(len(sys.argv) != 6):
        print("converts the given Euler angles (alpha, beta, gamma) representing "
        "the specified sequence of rotations about three major axes (X, Y, Z) to "
        "new angles (phi, psi, theta) representing another specified sequence of "
        "rotations about three major axes (X, Y, Z) so that the resulting rotation "
        "transformation would remain the same. NOTE: example: if input sequence "
        "'yxz' is required, then the input angles are trated as alpha = rot. about "
        "Y, beta = rot. about X, gamma = rot. about Z. The same applies for output "
        "sequence\n\nUsage:\n\nconvEulSeq alpha beta gamma inSeq outSeq\n\talpha\t"
        "\t\trotation about first axis [rad]\n\tbeta\t\t\trotation about second axis "
        "[rad]\n\tgamma\t\t\trotation about third axis [rad]\n\tinSeq\t\t\tinput "
        "rotation sequence in the form 'aaa' where a in {x,y,z}\n\toutSeq\t\t\toutput "
        "rotation sequence in the form 'aaa' where a in {x,y,z}")
        
        exit(1)

    #############################################################################
    # Input parameters

    # Input rotation angles [rad]
    alpha   = float(sys.argv[1])
    beta    = float(sys.argv[2])
    gamma   = float(sys.argv[3])

    # Input rotation sequence
    rotSeqIn = 's' + str(sys.argv[4])

    # Output rotation sequence
    rotSeqOut = 's' + str(sys.argv[5])

    #############################################################################
    # Main script

    # Create rotation transform matrix from input angles and input sequence
    R = tf.euler_matrix(alpha, beta, gamma, rotSeqIn)

    # Compute euler angles from rotation transform given the output sequence
    (phi, psi, theta) = tf.euler_from_matrix(R, rotSeqOut)

    # Check that both (alpha, beta, gamma) and (phi, psi, theta) represent the same tranformation
    Rtest = tf.euler_matrix(phi, psi, theta, rotSeqOut)
    sameTf = tf.is_same_transform(R, Rtest)

    if not sameTf:
        print "ERROR: Conversion failed"
    else:
        # Print results
        print "input  : [%f, %f, %f] -> %s" % (alpha, beta, gamma, rotSeqIn)
        print "output : [%f, %f, %f] -> %s" % (phi, psi, theta, rotSeqOut)
