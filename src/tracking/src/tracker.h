/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TRACKER_H
#define TRACKER_H

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

//! \brief The Tracker class Base tracke class.
//!
class Tracker {
public:
    //! \brief Tracker Constructor
    //!
    Tracker();

    Tracker(int frameWidth, int frameHeight);

    //! \brief Tracker Destructor
    //!
    virtual ~Tracker();

    //! \brief init Initialize the tracker.
    //! \param target id
    //! \param img_seq_id
    //! \param image    Current frame.
    //! \param bb       Bounding box.
    //!
    virtual bool init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb) = 0;

    //! \brief track Main tracking functions.
    //! \param image    Current frame
    //! \param bb       Resulting bounding box (or left blank).
    //! \return true - successfully tracked object, false - object not found (cv::Rect2d bb is not filled)
    //!
    virtual bool track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb) = 0;

    virtual bool observe(uint32_t img_seq_id, const cv::Mat &image) = 0;

    virtual bool clean() = 0;

    //! \brief getBelief Returns the tracker confidence.
    //! \return
    //!
    double getBelief() { return belief_; }

protected:
    int frameWidth_;
    int frameHeight_;

    cv::Rect bb_;
    cv::Mat appearance_;
    double belief_;
};

#endif // #ifndef TRACKER_H
