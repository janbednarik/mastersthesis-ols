/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS
#include <ros/ros.h>

// Project libraries
#include "controls.h"

using namespace std;

const int SPIN_RATE = 50;

int main(int argc, char *argv[])
{
    int returnValue = 0;

    ros::init(argc, argv, "controls");
    ros::start();

    Controls controls;

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        try {
            ros::spinOnce();
        } catch (exception &e) {
            cerr << "Controls: Exception: " << e.what() << endl;
        }
        looper.sleep();
    }

    ros::shutdown();
    return returnValue;
}
