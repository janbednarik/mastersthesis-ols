/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "motion.h"

// ROS
#include <ros/ros.h>

using namespace std;

Motion::Motion()
{
    ;
}

Motion::~Motion()
{
    ;
}

PeriodicArcMotionXY::PeriodicArcMotionXY(Eigen::Vector3d center, double radius, double speed, double maxAngle):
    center_(center), radius_(radius), maxAngle_(maxAngle)
{
    speed_      = speed;
    period_     = 4 * maxAngle_ * radius_ / speed_;
    period2_    = period_ / 2.0;
    period4_    = period_ / 4.0;
    period34_   = period4_ * 3.0;
}

PeriodicArcMotionXY::~PeriodicArcMotionXY()
{
    ;
}

void PeriodicArcMotionXY::update(double t, double& x, double& y, double &z)
{
    double alpha = 0.0;

    double tp = std::fmod(t, period_);

    if(tp < period2_) {
        alpha = maxAngle_ * (1 - 4 * std::abs(tp - period4_) / period_);
    } else {
        alpha = maxAngle_ * (4 * std::abs(tp - period34_) / period_ - 1);
    }

    x = std::cos(alpha) * radius_ + center_(0);
    y = std::sin(alpha) * radius_ + center_(1);
    z = center_(2);
}

void PeriodicArcMotionXY::forceSetPosition(double x, double y, double z)
{
    ROS_ERROR_STREAM("PeriodicArcMotionXY: forceSetPosition not implemented.");
}


StaticMotion::StaticMotion(Eigen::Vector3d pos) :
    x_(pos(0)), y_(pos(1)), z_(pos(2))
{
    speed_ = 0.0;
}

StaticMotion::~StaticMotion()
{
    ;
}

void StaticMotion::update(double t, double &x, double &y, double &z)
{
    x = x_;
    y = y_;
    z = z_;
}

void StaticMotion::forceSetPosition(double x, double y, double z)
{
    x_ = x;
    y_ = y;
    z_ = z;
}


PeriodicLineSegmentMotion::PeriodicLineSegmentMotion(Eigen::Vector3d from, Eigen::Vector3d direction, double distance, double speed, double acceleration) :
    from_(from), direction_(direction), distance_(distance), acceleration_(acceleration)
{
    speed_ = speed;
    if(acceleration == 0.0) {
        period2_ = distance_ / speed_;
    } else {
        period2_ = (-speed_ + std::sqrt(speed_ * speed_ + 2.0 * acceleration_ * distance_)) / acceleration_;
    }
    period_ = period2_ * 2.0;
}

PeriodicLineSegmentMotion::~PeriodicLineSegmentMotion()
{
    ;
}

void PeriodicLineSegmentMotion::update(double t, double &x, double &y, double &z)
{
    Eigen::Vector3d position;

    double tp = std::fmod(t, period_);
    tp = ((tp < period2_) ? tp : (period_ - tp));
    position = from_ + (speed_ * tp + acceleration_ * tp * tp * 0.5) * direction_;

    x = position(0);
    y = position(1);
    z = position(2);   
}

void PeriodicLineSegmentMotion::forceSetPosition(double x, double y, double z)
{
    ROS_ERROR_STREAM("PeriodicLineSegment: forceSetPosition not implemented.");
}
