/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef CRC_H
#define CRC_H

#include <iostream>
#include <cstdint>
#include <QByteArray>

using namespace std;

//! \brief crcInit Inititalize the crc table.
//!
void crcInit();

//! \brief crc8 compute crc for 'message' of length 'len'
//! \param message
//! \param len
//! \return
//!
unsigned char crc8(QByteArray message, int len);

//! \brief checkCRC Check crc code.
//! \param message
//! \return
//!
bool checkCRC(string &message);

#endif // #ifndef CRC_H
