/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef EXCEPTION_OLS_H
#define EXCEPTION_OLS_H

#include <exception>
#include <iostream>

using namespace std;

//! \brief The ExceptionOLS class Custom OLS exceptions.
//!
class ExceptionOLS: public exception {
public:
    ExceptionOLS(string msg) : msg_(msg) { }
    ~ExceptionOLS() throw() {}
    const char* what() { return msg_.c_str(); }

private:
    string msg_;
};

//! \brief The ExceptionOLS_MaxAttemptsReadCorrectMsg class Maximum attempts to read correct message
//! from manupilator Oprox reached.
//!
class ExceptionOLS_MaxAttemptsReadCorrectMsg: public exception {
public:
    ExceptionOLS_MaxAttemptsReadCorrectMsg(int attempts): attempts_(attempts) { }
    ~ExceptionOLS_MaxAttemptsReadCorrectMsg() throw() { }
    const char* what()
    {
        stringstream strs;
        strs << "Maximum number of attempts (" <<
                attempts_ <<
                ") to read correct message from manipulator reached.";
        return strs.str().c_str();
    }

private:
    int attempts_;
};

//! \brief The ExceptionOLS_InitializationFailed class Initialization of the manipulator
//! Oprox failed.
//!
class ExceptionOLS_InitializationFailed: public ExceptionOLS {
public:
    ExceptionOLS_InitializationFailed(string msg): ExceptionOLS(msg) { }
    ~ExceptionOLS_InitializationFailed() throw() { }
};

#endif // #ifndef EXCEPTION_OLS_H
