#ifndef CAMERA_CAPTURE_H
#define CAMERA_CAPTURE_H

// C++ libraries
#include <string>

// ROS libraries
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// PCL
#include <pcl/visualization/pcl_visualizer.h>


using namespace std;

class CameraCapture
{
public:
    static const string IMAGE_RAW_TOPIC;    
//    pcl::visualization::PCLVisualizer *viewer;

public:
    CameraCapture();
    ~CameraCapture();    

private:
    ros::NodeHandle* nh_;
    ros::Subscriber onImgSub_;

private:
    void onImage(const sensor_msgs::Image::ConstPtr img);
};

#endif
