/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TRACKER_DELAYED_GT_H
#define TRACKER_DELAYED_GT_H

// project libraries
#include "tracker.h"

// OpenTLD
#include "TLD.h"

// C++
#include <cstdarg>
#include <random>

//! \brief The TrackerDelayedGT class Ground truth tracker - it knows the exact position
//! of the target in simulator. The noise and time delay can be set to simulate real world
//! tracker performance.
//!
class TrackerDelayedGT: public Tracker
{
public:
    static const int DEFAULT_BB_WIDTH   = 20;
    static const int DEFAULT_BB_HEIGHT  = 10;

public:
    //! \brief TrackerDelayedGT Constructor
    //!
    TrackerDelayedGT();

    TrackerDelayedGT(double mu, double sigma);

    //! \brief TrackerDelayedGT Destructor
    //!
    ~TrackerDelayedGT();

    //! \brief init Init function is used to set the ground truth of the tracker.
    //! \param image Not used
    //! \param bb Only bb.x and bb.y are used as center of the object (width and height
    //! are expected to be 0)
    //! \return
    //!
    virtual bool init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb);

    virtual bool track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb);

    virtual bool observe(uint32_t img_seq_id, const cv::Mat &image) {}

private:
    int x_;         //!< Ground truth pixel X coordinate of the tracked object.
    int y_;         //!< Ground truth pixel Y coordinate of the tracked object.

    double mu_;     //!< Mean of the gaussian modeling the delay [ms].
    double sigma_;  //!< Standard deviation of the gaussian modeling the delay [ms].

    std::random_device rd_;
    std::mt19937 *gen_;
    std::normal_distribution<double> *d_;
};

#endif // #ifndef TRACKER_DELAYED_GT_H
