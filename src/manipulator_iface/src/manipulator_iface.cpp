/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#include "manipulator_iface.h"

const std::string ManipulatorIface::CAMERA_UNIT_SENSORS     = "camera_unit_sensors";
const std::string ManipulatorIface::CAMERA_UNIT_INFO_TOPIC  = "camera_unit_info";
const std::string ManipulatorIface::JOINT_STATE_TOPIC       = "joint_states";

const double ManipulatorIface::DEFAULT_PIXEL_WIDTH   = 0.00000375;   // 3.75 um
const double ManipulatorIface::DEFAULT_PIXEL_HEIGHT  = 0.00000375;   // 3.75 um
const int    ManipulatorIface::DEFAULT_FRAME_WIDTH   = 1280;    // px
const int    ManipulatorIface::DEFAULT_FRAME_HEIGHT  = 960;     // px

ManipulatorIface::ManipulatorIface() : nh_(NULL), haveNewConfig_(false),
    gazeboSimulation_(false), firstSimulationFrame_(true),
    azimuth_(0.0), elevation_(0.0), oldAzimuth_(0.0), oldElevation_(0.0),
    latitude_(0.0), longitude_(0.0), altitude_(0.0),
    heading_(0.0), localHeading_(0.0), sideTilt_(0.0), frontTilt_(0.0)
{
    nh_ = new ros::NodeHandle();   

    // Load parameters from ROS parameter server.
    if (!ros::param::get("tf_prefix", tf_prefix_)) {
        ROS_FATAL_STREAM("Could not get parameter tf_prefix");
        exit(1);
    }

    if (!ros::param::get("/world_frame_id", world_frame_id_)) {
        ROS_FATAL_STREAM("Could not get parameter world_frame_id");
        exit(1);
    }

    if (!ros::param::get("unit_idx", unit_idx_)) {
        ROS_FATAL_STREAM("Could not get parameter unit_idx");
        exit(1);
    }
    unitIdx = std::stoi(unit_idx_);

    if (!ros::param::get("manipulator_iface/local_x", x_)) {
        ROS_ERROR_STREAM("Could not get parameter x. Using default value " << x_ << ".");
    }

    if (!ros::param::get("manipulator_iface/local_y", y_)) {
        ROS_ERROR_STREAM("Could not get parameter y. Using default value " << y_ << ".");
    }

    if (!ros::param::get("manipulator_iface/local_z", z_)) {
        ROS_ERROR_STREAM("Could not get parameter z. Using default value " << z_ << ".");
    }

    if (!ros::param::get("manipulator_iface/local_heading", localHeading_)) {
        ROS_ERROR_STREAM("Could not get parameter heading. Using default value " << localHeading_ << ".");
    }

    if (!ros::param::get("manipulator_iface/latitude", latitude_)) {
        ROS_ERROR_STREAM("Could not get parameter latitude. Using default value " << latitude_ << ".");
    }

    if (!ros::param::get("manipulator_iface/longitude", longitude_)) {
        ROS_ERROR_STREAM("Could not get parameter longitude. Using default value " << longitude_ << ".");
    }

    if (!ros::param::get("manipulator_iface/altitude", altitude_)) {
        ROS_ERROR_STREAM("Could not get parameter altitude. Using default value " << altitude_ << ".");
    }

    if (!ros::param::get("manipulator_iface/heading", heading_)) {
        ROS_ERROR_STREAM("Could not get parameter heading. Using default value " << heading_ << ".");
    }

    if (!ros::param::get("camera/pixel_width", pixelWidth_)) {
        pixelWidth_ = DEFAULT_PIXEL_WIDTH;
        ROS_WARN_STREAM("Could not get parameter pixel_width. Setting default value " <<
                        DEFAULT_PIXEL_WIDTH << " m.");
    }

    if (!ros::param::get("camera/pixel_height", pixelHeight_)) {
        pixelHeight_ = DEFAULT_PIXEL_HEIGHT;
        ROS_WARN_STREAM("Could not get parameter pixel_height. Setting default value " <<
                        DEFAULT_PIXEL_HEIGHT << " m.");
    }

    if (!ros::param::get("camera/width", frameWidth_)) {
        frameWidth_ = DEFAULT_FRAME_WIDTH;
        ROS_WARN_STREAM("Could not get parameter width. Setting default value " <<
                        DEFAULT_FRAME_WIDTH << " px.");
    }

    if (!ros::param::get("camera/height", frameHeight_)) {
        frameHeight_ = DEFAULT_FRAME_HEIGHT;
        ROS_WARN_STREAM("Could not get parameter height. Setting default value " <<
                        DEFAULT_FRAME_HEIGHT << " m.");
    }

    if (!ros::param::get("cfg_file", cfgFile_)) {
        ROS_FATAL_STREAM("Could not get parameter cfg_file.");
        exit(1);
    }

    if(ros::param::has("/gazebo_simulation")) {
        if(!ros::param::get("/gazebo_simulation", gazeboSimulation_)) {
            ROS_FATAL_STREAM("Could not get parameter gazebo_simulation");
            exit(1);
        }
    }

    cuSensorsSub_   = nh_->subscribe(CAMERA_UNIT_SENSORS, 5, &ManipulatorIface::onCameraUnitSensors, this);
    cuiPub_         = nh_->advertise<msgs::CameraUnitInfo>(CAMERA_UNIT_INFO_TOPIC, 5);
    jointStatePub_  = nh_->advertise<sensor_msgs::JointState>(JOINT_STATE_TOPIC, 1);

    if(gazeboSimulation_) {
        modelConfigClient_ = nh_->serviceClient<gazebo_msgs::SetModelConfiguration>(
                                 "/gazebo/set_model_configuration");
    }

    // Create names of the tf for cretain joint pairs.
    tfCamera_  = std::string("/") + tf_prefix_ + std::string("/") + std::string("camera");
    tfFocus_   = std::string("/") + tf_prefix_ + std::string("/") + std::string("focus");

    // Dynamic reconfigure callback
    dynamic_reconfigure::Server<manipulator_iface::CameraUnitConfig>::CallbackType drCallback;

    drCallback = boost::bind(&ManipulatorIface::onDynamicReconfigure, this, _1, _2);
    server.setCallback(drCallback);
}

ManipulatorIface::~ManipulatorIface()
{
    if(nh_) delete nh_;
}

void ManipulatorIface::update()
{
    // Check if dynamic reconfigure of parameters happened.
    if(haveNewConfig_) {
        haveNewConfig_ = false;

        // Save new values for dynamic parameters.
        std::string cmd = "rosparam dump " + cfgFile_ + " " + ros::this_node::getName();
        if (system(cmd.c_str()) == -1) {
            ROS_ERROR_STREAM("Cannot store the current configuration of " <<
                             ros::this_node::getName() << " to " << cfgFile_);
        }
    }
}

void ManipulatorIface::onCameraUnitSensors(const msgs::CameraUnitSensorsConstPtr &cuSensorsMsg)
{
    using namespace std;

    azimuth_    = cuSensorsMsg->azimuth;
    elevation_  = cuSensorsMsg->elevation;

    // Create and publish current JointStates message (so that robot_state_pblisher can create /tf)
    sensor_msgs::JointState jointState;

    jointState.header.stamp = cuSensorsMsg->header.stamp;

    jointState.name.resize(5);
    jointState.position.resize(5);
    jointState.name[0] ="ground2legs";
    jointState.position[0] = localHeading_;
    jointState.name[1] ="legs2base";
    jointState.position[1] = 0.0;
    jointState.name[2] ="azimuth";
    jointState.position[2] = azimuth_;
    jointState.name[3] ="elevation";
    jointState.position[3] = elevation_;
    jointState.name[4] ="camera2focus";
    jointState.position[4] = cuSensorsMsg->f;

    jointStatePub_.publish(jointState);

    // Send station transform (translation) with regards to the 'world' frame.
    tf::Transform tfStation;
    tfStation.setOrigin(tf::Vector3(x_, y_, z_));
    tfStation.setRotation(tf::Quaternion(tf::Vector3(0.0, 0.0, 1.0), 0.0));
    tfBcastStation_.sendTransform(tf::StampedTransform(tfStation,
                                                       cuSensorsMsg->header.stamp,
                                                       world_frame_id_,
                                                       tf_prefix_ + std::string("/") + std::string("ground")
                                                       ));

    // Create and publish current CameraUnitInfo message.
    msgs::CameraUnitInfo cui;

    cui.camera_unit = unitIdx;

    cui.header.stamp =  cuSensorsMsg->header.stamp;
    cui.aziumth      =  azimuth_;
    cui.elevation    =  elevation_;

    cui.gps.latitude    = latitude_;
    cui.gps.longitude   = longitude_;
    cui.gps.altitude    = altitude_;

    cui.side_tilt   = sideTilt_;
    cui.front_tilt  = frontTilt_;
    cui.orientation = heading_;

    // Set intrinsic parameters.
    double f = cuSensorsMsg->f;
    boost::array<double, 9> I = {{ f,   0.0, 0.0,
                                   0.0, f,   0.0,
                                   0.0, 0.0, 1.0  }};
    cui.I = I;

    // Set extrinsic parameters.
    tf::StampedTransform tfCamera;
    tf::StampedTransform tfFocus;    

    try {
        tfListener_.lookupTransform(tfCamera_, "world", ros::Time(0), tfCamera);
        tfListener_.lookupTransform(tfFocus_, "world",  ros::Time(0), tfFocus);        
    } catch (tf::TransformException ex) {
        ROS_ERROR("ManipulatorIface: %s",ex.what());
    }

    tf::Vector3 origin  = tfFocus.getOrigin();
    tf::Matrix3x3 basis = tfFocus.getBasis();

    tf::Vector3 Tcamera = tfFocus.getOrigin();
    tf::Matrix3x3 R     = tfFocus.getBasis();

    for(int i = 0; i < 3; ++i) {
        cui.E[i * 4 + 0] = R[i].x();
        cui.E[i * 4 + 1] = R[i].y();
        cui.E[i * 4 + 2] = R[i].z();
        cui.E[i * 4 + 3] = Tcamera.m_floats[i];
    }

    cui.fov = 2.0 * std::atan((frameWidth_ * pixelWidth_) / (2.0 * cuSensorsMsg->f));

    // Set optical axis direction
    tf::Vector3 Tfocus = tfFocus.getOrigin();
    tf::Vector3 optAxDir = (Tcamera - Tfocus).normalized();

    cui.optical_axis_dir.x = optAxDir.m_floats[0];
    cui.optical_axis_dir.y = optAxDir.m_floats[1];
    cui.optical_axis_dir.z = optAxDir.m_floats[2];

    // Send message
    cuiPub_.publish(cui);

    // Update the position of the manipualtor for the Gazebo simulation
    if(gazeboSimulation_) {
        vector<string> jointNames;
        vector<double> jointPositions;

        jointNames.push_back(string("ground2legs"));
        jointNames.push_back(string("azimuth"));
        jointNames.push_back(string("elevation"));

        if(firstSimulationFrame_) {
            firstSimulationFrame_ = false;
            sleep(3);   // Safely sleep and let Gazebo start.
            jointPositions.push_back(localHeading_);
            jointPositions.push_back(azimuth_);
            jointPositions.push_back(elevation_);
        } else {
            jointPositions.push_back(0.0);
            jointPositions.push_back(azimuth_ - oldAzimuth_);
            jointPositions.push_back(elevation_ - oldElevation_);
        }

        gazebo_msgs::SetModelConfiguration setModelCfgSrv;
        setModelCfgSrv.request.model_name       = string("camera_unit_") + unit_idx_;
        setModelCfgSrv.request.urdf_param_name  = string("camera_unit_") + unit_idx_;
        setModelCfgSrv.request.joint_names      = jointNames;
        setModelCfgSrv.request.joint_positions  = jointPositions;

        if (!modelConfigClient_.call(setModelCfgSrv) || !setModelCfgSrv.response.success) {
            ROS_ERROR_STREAM("Maniulator_ptu_d46-70: update: Failed to call service set_model_configuration");
        }
    }

    oldAzimuth_     = azimuth_;
    oldElevation_   = elevation_;
}

void ManipulatorIface::onDynamicReconfigure(manipulator_iface::CameraUnitConfig &config, uint32_t level)
{
    haveNewConfig_ = true;
}
