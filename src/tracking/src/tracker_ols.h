/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TRACKER_OLS_H
#define TRACKER_OLS_H

// project libraries
#include "tracker.h"

#include "PFTracker.h"

// C++
#include <cstdarg>
#include <random>

//! \brief The TrackerOLS class OLS tracker based on BGFG tracker adopted from authors
//! Drahansky M., Herman D., Orsag F. - Object tracking in monochromatic video sequences
//! using particle filter. In 7th Scientific International Conference - Enviromental
//! Protection of Population, pages 120–128. Karel Englis College Inc., 2012.
//!
class TrackerOLS: public Tracker
{
public:
    static const int DEFAULT_BB_WIDTH   = 20;
    static const int DEFAULT_BB_HEIGHT  = 10;

public:
    //! \brief TrackerOLS Constructor
    //!
    TrackerOLS(int frameWidth, int frameHeight, int step);

    //! \brief TrackerOLS Destructor
    //!
    ~TrackerOLS();

    //! \brief init Init function is used to set the ground truth of the Tracker.
    //! \param target id
    //! \param image Not used
    //! \param bb Only bb.x and bb.y are used as center of the object (width and height
    //! are expected to be 0)
    //! \return
    //!
    virtual bool init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb);

    virtual bool track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb);

    virtual bool observe(uint32_t img_seq_id, const cv::Mat &image);

    virtual bool clean();   
private:
    int step_;
    PFTracker tracker_;
    uint32_t target_id_;
    uint32_t last_img_seq_id_;
    cv::Mat tgtTemplate_;
    cv::Mat tgtMask_;    

private:
    //! \brief getBelief Computes the belief of the tracker.
    //! \param templ        target template
    //! \param templMask    target template mask
    //! \param cand         candidate
    //! \param candMask     candidate mask
    //! \return
    //!
    float computeBelief(cv::Mat templ, cv::Mat templMask, cv::Mat cand, cv::Mat candMask);
};

#endif // #ifndef TRACKER_DELAYED_GT_H
