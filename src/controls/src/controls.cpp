/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "controls.h"

using namespace std;

const std::string Controls::JOY_TOPIC               = "joy";
const std::string Controls::KEYDOWN_TOPIC           = "keyboard/keydown";
const std::string Controls::KEYUP_TOPIC             = "keyboard/keyup";
const std::string Controls::MANIP_MOVE_CMD_TOPIC    = "manipMove";
const std::string Controls::START_DETECTION_TOPIC   = "start_detection";
const std::string Controls::STOP_DETECTION_TOPIC    = "stop_detection";
const std::string Controls::NEW_TARGET_TOPIC        = "new_target";
const std::string Controls::NEW_TARGET_ID_TOPIC     = "new_target_id";
const std::string Controls::SELECT_MANIP_SRV        = "select_manipulator";
const std::string Controls::TARGET_STATE_TOPIC      = "target_state";
const std::string Controls::SET_TARGET_OBJECT_TOPIC = "set_target_object";

Controls::Controls(): manipControlMode_(MANIP_JOYSTICK), selectedManipIdx_(OVERVIEW_UNIT)
{
    // Initialize ROS structures, subscribers, publishers
    nh_ = new ros::NodeHandle();

    // Get parameters from the parameter server
    if (!ros::param::get("/camera_unit_ns", cameraUnitNS_)) {
        ROS_FATAL_STREAM("Could not get parameter camera_unit_ns");
        exit(1);
    }

    if (!ros::param::get("/camera_units_count", CAMERA_UNITS_COUNT)) {
        ROS_FATAL_STREAM("Could not get parameter camera_units_count");
        exit(1);
    }

//    newTargetSub_ = new ros::Subscriber[CAMERA_UNITS_COUNT];
    manipJoyPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];
    manipKeyDownPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];
    manipKeyUpPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];
    manipMoveCmdPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];
    startDetectionPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];
    stopDetectionPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];
    newTargetIdPub_ = new ros::Publisher[CAMERA_UNITS_COUNT];

    joySub_             = nh_->subscribe(JOY_TOPIC,             1,   &Controls::onJoy,             this);
    keyDownSub_         = nh_->subscribe(KEYDOWN_TOPIC,         5,   &Controls::onKeyDown,         this);
    keyUpSub_           = nh_->subscribe(KEYUP_TOPIC,           5,   &Controls::onKeyUp,           this);
    manipMoveCmdSub_    = nh_->subscribe(MANIP_MOVE_CMD_TOPIC,  1,   &Controls::onManipMoveCmd,    this);
    startDetectionSub_  = nh_->subscribe(START_DETECTION_TOPIC, 5,   &Controls::onStartDetection,  this);
    stopDetectionSub_   = nh_->subscribe(STOP_DETECTION_TOPIC,  5,   &Controls::onStopDetection,   this);
    targetStateSub_     = nh_->subscribe(TARGET_STATE_TOPIC,    20,  &Controls::onTargetState,     this);
    setTargetObjectSub_ = nh_->subscribe(SET_TARGET_OBJECT_TOPIC + string("_gui"), 5, &Controls::onSetTargetObject, this);

    newTargetSub_ = nh_->subscribe(NEW_TARGET_TOPIC, 10, &Controls::onNewTarget, this);

    for(int i = 0; i < CAMERA_UNITS_COUNT; ++i) {
        manipJoyPub_[i]         = nh_->advertise<sensor_msgs::Joy>(JOY_TOPIC                    + string("_") + to_string(i), 1);
        manipKeyDownPub_[i]     = nh_->advertise<keyboard::Key>(KEYDOWN_TOPIC                   + string("_") + to_string(i), 5);
        manipKeyUpPub_[i]       = nh_->advertise<keyboard::Key>(KEYUP_TOPIC                     + string("_") + to_string(i), 5);
        manipMoveCmdPub_[i]     = nh_->advertise<msgs::ManipulatorMove>(MANIP_MOVE_CMD_TOPIC    + string("_") + to_string(i), 1);
        startDetectionPub_[i]   = nh_->advertise<msgs::StartDetection>(START_DETECTION_TOPIC    + string("_") + to_string(i), 5);
        stopDetectionPub_[i]    = nh_->advertise<msgs::StopDetection>(STOP_DETECTION_TOPIC      + string("_") + to_string(i), 5);
        newTargetIdPub_[i]      = nh_->advertise<msgs::NewTargetID>(NEW_TARGET_ID_TOPIC         + string("_") + to_string(i), 5);
    }

    setTargetObjectPub_ = nh_->advertise<msgs::SetTargetObject>(SET_TARGET_OBJECT_TOPIC, 5);

    // services
    selectManipSrv_ = nh_->advertiseService(SELECT_MANIP_SRV,   &Controls::selectManipulator,   this);
}

Controls::~Controls()
{
    // Delete subscribers/publishers
    delete[] manipJoyPub_;
    delete[] manipKeyDownPub_;
    delete[] manipKeyUpPub_;
    delete[] manipMoveCmdPub_;
    delete[] startDetectionPub_;
    delete[] stopDetectionPub_;
    delete[] newTargetIdPub_;

    // Delete targets
    for(unordered_map<int, Target*>::iterator it = targets_.begin(); it != targets_.end(); ++it) {
        delete it->second;
    }
}

void Controls::onJoy(const sensor_msgs::Joy::ConstPtr &joy)
{
    manipJoyPub_[selectedManipIdx_].publish(joy);

    for(int i = 0; i < 9; ++i) {
        if(joy->buttons[i]) std::cout << "joy: button " << i << std::endl;
    }
}

void Controls::onKeyDown(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {        
        case keyboard::Key::KEY_KP0:
        case keyboard::Key::KEY_KP1:
        case keyboard::Key::KEY_KP2:
        case keyboard::Key::KEY_KP3:
            break;

        default:
            manipKeyDownPub_[selectedManipIdx_].publish(key);
            break;
    }
}

void Controls::onKeyUp(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {
        // Manipulator to control choice keys -> do nothing.
        case keyboard::Key::KEY_KP0:
        case keyboard::Key::KEY_KP1:
        case keyboard::Key::KEY_KP2:
        case keyboard::Key::KEY_KP3:
            break;

        default:
            manipKeyUpPub_[selectedManipIdx_].publish(key);
            break;
    }
}

void Controls::onManipMoveCmd(const msgs::ManipulatorMove cmd)
{
    manipMoveCmdPub_[selectedManipIdx_].publish(cmd);
}

bool Controls::selectManipulator(controls::SelectManipulator::Request &req, controls::SelectManipulator::Response &res)
{
    if(req.id > (CAMERA_UNITS_COUNT - 1)) {
        return false;
    }

    selectedManipIdx_ = (eManipulators)req.id;

    res.ack = true;
    return true;
}

void Controls::onStartDetection(const msgs::StartDetection msg)
{
    // Do some checking before forwarding the request.
    if(msg.idCU < 0 || msg.idCU > CAMERA_UNITS_COUNT) {
        ROS_ERROR_STREAM("Wrong manipulator index " << msg.idCU <<
                         ". Index must be in range [0, " << CAMERA_UNITS_COUNT << "].");
    }

    msgs::StartDetection startDetMsg;
    startDetMsg = msg;
//    startDetMsg.idT = t.id_;

    startDetectionPub_[msg.idCU].publish(startDetMsg);
}

void Controls::onStopDetection(const msgs::StopDetection::ConstPtr msg)
{
    // Do some checking before forwarding the request.
    if(msg->camera_unit < 0 || msg->camera_unit > CAMERA_UNITS_COUNT) {
        ROS_ERROR_STREAM("Wrong manipulator index " << msg->camera_unit <<
                         ". Index must be in range [0, " << CAMERA_UNITS_COUNT << "].");
    }

    msgs::StopDetection stopDetMsg;
    stopDetMsg = *msg;

    stopDetectionPub_[msg->camera_unit].publish(stopDetMsg);
}

//void Controls::newTarget(int detectorID, const msgs::NewTarget::ConstPtr target)
void Controls::onNewTarget(const msgs::NewTarget::ConstPtr target)
{
    int detectorID = target->id_cu;

    ROS_INFO_STREAM("New target found by detector " << detectorID << ".");

    // new target found by a camera unit
    if(detectorID < CAMERA_UNITS_COUNT) {

        // Create a new target
        Target *t = new Target;
        targets_[t->getId()] = t;

        // Send the system wide unique ID of the target to the detector
        msgs::NewTargetID newIDmsg;
        newIDmsg.id_local   = target->id_local;
        newIDmsg.id_global  = 0;

        newTargetIdPub_[detectorID].publish(newIDmsg);

    // new target obtained from network
    } else if(detectorID == CAMERA_UNITS_COUNT + 1) {

    // new target obtained from somewhere else
    } else {

    }
}

void Controls::onTargetState(const msgs::TargetState::ConstPtr targetState)
{
    ;
}

void Controls::onSetTargetObject(const msgs::SetTargetObject::ConstPtr stoMsg)
{
    msgs::SetTargetObject sto = *stoMsg;
    setTargetObjectPub_.publish(sto);
}
