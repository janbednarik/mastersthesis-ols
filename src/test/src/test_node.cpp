// ROS libraries
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/JointState.h>

// C++ libraries
#include <iostream>
#include <cmath>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

// Eigen
#include <eigen3/Eigen/Eigen>

using namespace std;

const int SPIN_RATE = 50;

int main(int argc, char *argv[])
{    
    ros::init(argc, argv, "test_node");
    ros::NodeHandle nh;
    ros::start(); 

//    ros::Rate looper(SPIN_RATE);
//    while(ros::ok()) {
//        ros::spinOnce();

//        looper.sleep();
//    }


    std::vector<int> v;

    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    for(std::vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << *it << ", ";
    }
    cout << endl;

    v.erase(v.begin());

    for(std::vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << *it << ", ";
    }

    ros::shutdown();

    return 0;
}

