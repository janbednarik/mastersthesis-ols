/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "tracker.h"

Tracker::Tracker() :
    frameWidth_(0), frameHeight_(0), belief_(1.0)
{
    ;
}

Tracker::Tracker(int frameWidth, int frameHeight) : frameWidth_(frameWidth), frameHeight_(frameHeight), belief_(1.0)
{
    ;
}

Tracker::~Tracker()
{
    ;
}
