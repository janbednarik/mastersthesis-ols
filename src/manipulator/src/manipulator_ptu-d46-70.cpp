/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// C++ libraries
#include <iostream>
#include <exception>
#include <string>
#include <cmath>

// Qt libraries
#include <QString>
#include <QDebug>


// project libraries
#include "manipulator_ptu-d46-70.h"
#include "exceptions_ols.h"
#include <crc.h>

using namespace std;

const double ManipulatorPtuD4670::AZI_UPPER_SPEED     = 400.0; // 400.0
const double ManipulatorPtuD4670::AZI_LOWER_SPEED     = 0.0;
const double ManipulatorPtuD4670::AZI_BASE_SPEED      = 400.0;
const double ManipulatorPtuD4670::AZI_ACCELERATION    = 6200.0;
const double ManipulatorPtuD4670::ELE_UPPER_SPEED     = 400.0; // 400.0
const double ManipulatorPtuD4670::ELE_LOWER_SPEED     = 0.0;
const double ManipulatorPtuD4670::ELE_BASE_SPEED      = 400.0;
const double ManipulatorPtuD4670::ELE_ACCELERATION    = 6200.0;

// Manipulator motion constants
const double ManipulatorPtuD4670::MAX_AZIMUTH_SPEED     = ManipulatorPtuD4670::AZI_UPPER_SPEED;
const double ManipulatorPtuD4670::MAX_ELEVATION_SPEED   = ManipulatorPtuD4670::ELE_UPPER_SPEED;

// Manipulator construction constraints
const double ManipulatorPtuD4670::MAX_AZIMUTH   =  3089.0;  //  177 deg
const double ManipulatorPtuD4670::MIN_AZIMUTH   = -3089.0;  // -177 deg
const double ManipulatorPtuD4670::MAX_ELEVATION =  541.0;   //  31  deg
const double ManipulatorPtuD4670::MIN_ELEVATION = -1396.0;  // -80  deg

const int    ManipulatorPtuD4670::AZIMUTH_SECTIONS  = 4;
const double ManipulatorPtuD4670::AZIMUTH_SECTION_W = (MAX_AZIMUTH - MIN_AZIMUTH) / AZIMUTH_SECTIONS;

ManipulatorPtuD4670::ManipulatorPtuD4670(std::string port, uint32_t baudrate, uint32_t timeout):
    motionState_(INDEPENDENT_CTRL), posControlMode_(SPEED_CONTROL),
    aziStep_(0.0), eleStep_(0.0), aziSpeedSteps_(0), eleSpeedSteps_(0),
    absMotionAziTarget_(0.0), absMotionEleTarget_(0.0),
    absAziReached_(true), absEleReached_(true), testSynchrony_(false),
    panInMotion_(false), tiltInMotion_(false), aziSectionIdx_(2), aziSections_(0)
{
    string cmd;    

#if !SIMULATION
    // Create and initialize the serial interface.
    serialIf_ = new serial::Serial(serial_if_name_, baudrate, serial::Timeout::simpleTimeout(timeout));

    // debug
    cout << "Serial port open: " << (serialIf_->isOpen() ? "YES" : "NO") << endl;

    // setTimeout(inter_byte_timeout, read_timeout_constant, read_timeout_multiplier, write_timeout_constant, write_timeout_multiplier)
    // The read/wright limits must be long for setting the motion properties otherwise the CVUT control unit fails
    serialIf_->setTimeout(0, 320, 0, 320, 0);

#if USE_CVUT_CONTROL_UNIT && INITIALIZE_CONTROL_UNIT

    // Initialize CVUT contorl unit (sets quarter step, sets speed 2000 positions/s)
    cmd = 0x13;
    try {
        command(cmd, 150000);
    } catch(ExceptionOLS &e) {
        cerr << e.what() << endl;
    }

#endif

    // Get current azimuth and elevation position resolution (step).
    cmd = 0xa5;
    uint32_t panResArc  = query(cmd, 4);
    cmd = 0xa6;
    uint32_t tiltResArc = query(cmd, 4);

    // Compute number of mrads per azimuthal and elevation step.
    aziStep_ = (double)panResArc  / 60.0 / 3600.0 / 180.0 * M_PI * 1000;
    eleStep_ = (double)tiltResArc / 60.0 / 3600.0 / 180.0 * M_PI * 1000;

#if INITIALIZE_MOTION_PROPS
    // Initialize the motionp properties forced by the Flir control unit
    initMotionProps();
#endif

#else // #if SIMULATION
    aziStep_ = 0.11234;
    eleStep_ = 0.11234;
#endif // #if SIMULATION

    // SET_PURE_VELOCITY_CONTROL_MODE
#if VELOCITY_CONTROL
    setMotionState(PURE_VELOCITY_CTRL);
#else
    setMotionState(INDEPENDENT_CTRL);
#endif

    // Initialize azimuth motion sections
    aziSections_ = new double[AZIMUTH_SECTIONS];
    for(int i = 0; i < AZIMUTH_SECTIONS + 1; ++i) {
        aziSections_[i] = MIN_AZIMUTH + (i * AZIMUTH_SECTION_W);
    }

#if !SIMULATION
    // Now we can process messages much faster.
    serialIf_->setTimeout(0, 10, 1, 100, 1);
#endif

    // Create regulator controlling the motion of the manipualtor during tracking.
    SpeedFunc* regSpeedFunc         = new SpeedFuncPow(0.14, 0.2, 0.008, 1.5, 0.002);
    Position2DPredictor* predictor  = new Position2DPredictor(10);
    regulator_ = new Regulator(regSpeedFunc, predictor);

    // Create speed function for progressive motion control using joystick.
    joySpeed_ = new SpeedFuncPow(1.0, MAX_AZIMUTH_SPEED / 1000.0, 0.005, 10.0, 0.01);
}

ManipulatorPtuD4670::~ManipulatorPtuD4670()
{
    try {
        setMotionState(INDEPENDENT_CTRL);
    } catch(ExceptionOLS &e) {
        cerr << "ERROR: Could not set 'independent control mode' in the manipulator "
                "before exiting the program. Please turn off and on the (Flir) control unit"
                "of the manipulator. Exception: " << e.what() << endl;
    }

    haltPan();
    haltTilt();

#if !SIMULATION
    serialIf_->close();
    delete serialIf_;
#endif

    if(aziSections_)    delete[] aziSections_;
    if(joySpeed_)       delete joySpeed_;
    if(regulator_)      delete regulator_;
}

void ManipulatorPtuD4670::initMotionProps()
{
    // PAN_SET_ACCEL
    try { command(string(1, 0xc9) + int2byteStr((uint32_t)angle2steps(AZI_ACCELERATION,  aziStep_), 4)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // PAN_SET_UPPER_SPEED_LIMIT
    try { command(string(1, 0x8b) + int2byteStr((uint32_t)angle2steps(AZI_UPPER_SPEED,   aziStep_), 2)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // PAN_SET_LOWER_SPEED_LIMIT
    try { command(string(1, 0x8d) + int2byteStr((uint32_t)angle2steps(AZI_LOWER_SPEED,   aziStep_), 2)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // PAN_SET_BASE_SPEED
    try { command(string(1, 0x89) + int2byteStr((uint32_t)angle2steps(AZI_BASE_SPEED,    aziStep_), 2)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // TILT_SET_ACCEL
    try { command(string(1, 0xca) + int2byteStr((uint32_t)angle2steps(ELE_ACCELERATION,  aziStep_), 4)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // TILT_SET_UPPER_SPEED_LIMIT
    try { command(string(1, 0x8c) + int2byteStr((uint32_t)angle2steps(ELE_UPPER_SPEED,   aziStep_), 2)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // TILT_SET_LOWER_SPEED_LIMIT
    try { command(string(1, 0x8e) + int2byteStr((uint32_t)angle2steps(ELE_LOWER_SPEED,   aziStep_), 2)); }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }

    // TILT_SET_BASE_SPEED
    try { command(string(1, 0x8a) + int2byteStr((uint32_t)angle2steps(ELE_BASE_SPEED,    aziStep_), 2));  }
    catch(ExceptionOLS &e) { cerr << "ExceptionOLS: initMotionProps: " << e.what() << endl; }
}

void ManipulatorPtuD4670::update()
{    
#if !SIMULATION
    string response;

    // Get current azimuth and elevation
    string aziPosQry(1, 0x91);
    string elePosQry(1, 0x92);   

    int16_t azi =  query(aziPosQry, 2);
    int16_t ele =  query(elePosQry, 2);    

    // Save current azimuth and elevation.
    setPositionStamped(azi * aziStep_, ele * eleStep_, ros::Time::now());

    // Speed control mode
    if(posControlMode_ == SPEED_CONTROL) {
        // Check azimuth regions
        if(aziSpeed_ != 0.0) {
            double atsb = getAziTargetSectionBoundary(azimuth_, aziSpeed_);

            if(atsb != aziTargetSectionBoundary_) {
                aziTargetSectionBoundary_ = atsb;
                moveCommand(0x81, angle2steps(aziTargetSectionBoundary_, aziStep_));
            }
        }
    }
    // Absolute position mode
    else {
        if(angle2steps(azimuth_, aziStep_) ==
           angle2steps(absMotionAziTarget_, aziStep_) && !absAziReached_) {
            aziSpeed_ = 0.0;
            aziSpeedSteps_ = 0;
            absAziReached_ = true;
        } else {
            double newAziTarget = getAbsPosAziTarget(azimuth_, aziSpeed_, absMotionAziTarget_);
            if(absMotionCurrentAziTarget_ != newAziTarget) {
                absMotionCurrentAziTarget_ = newAziTarget;
                moveCommand(0x81, angle2steps(absMotionCurrentAziTarget_, aziStep_));
            }
        }

        if(angle2steps(elevation_, eleStep_) ==
           angle2steps(absMotionEleTarget_, eleStep_) && !absEleReached_) {
            eleSpeed_ = 0.0;
            eleSpeedSteps_ = 0;
            absEleReached_ = true;
        }

        if(absAziReached_ && absEleReached_) {
            posControlMode_ = SPEED_CONTROL;
        }
    }

    // debug - test synchrony
    if(testSynchrony_) {
        if(testSynStep_ < testSynSteps_) {
            if(absAziReached_ && absEleReached_) {
                cout << "azimuth:   " <<  azimuth_   << endl;
                cout << "elevation: " <<  elevation_ << endl;
                moveRelative(testSynAziStep_, testSynEleStep_);
                testSynStep_++;
            }
        } else {
            moveAbsolute(0.0, 0.0);
            testSynchrony_ = false;
        }
    }

    read(response, 0);


#endif // #if SIMULATION

    Manipulator::update();
}

#if VELOCITY_CONTROL

void ManipulatorPtuD4670::move(double azimuth, double elevation)
{
    if(movingInRange(azimuth_, azimuth, MIN_AZIMUTH, MAX_AZIMUTH)) {
        setMotionState(PURE_VELOCITY_CTRL);
        if(azimuth != aziSpeed_) {
            aziSpeed_ = azimuth;
            moveCommand(0x87, azimuth,   aziStep_); // PAN_SET_ABS_SPEED
        }
    }

    if(movingInRange(elevation_, elevation, MIN_ELEVATION, MAX_ELEVATION)) {
        setMotionState(PURE_VELOCITY_CTRL);
        if(elevation != eleSpeed_) {
            eleSpeed_ = elevation;
            moveCommand(0x88, elevation, eleStep_); // TILT_SET_ABS_SPEED
        }
    }
}

#else

void ManipulatorPtuD4670::move(double azimuth, double elevation)
{
    posControlMode_ = SPEED_CONTROL;

    int aziSteps = angle2steps(azimuth, aziStep_);
    int eleSteps = angle2steps(elevation, eleStep_);

    bool aziSpeedChangedDirection = false;
    bool eleSpeedChangedDirection = false;

    // Set azimuth speed
    if(aziSteps != aziSpeedSteps_) {
        if((aziSteps > 0 && aziSpeedSteps_ < 0) || (aziSteps < 0 && aziSpeedSteps_ > 0)) {
            aziSpeedChangedDirection = true;
        }
        aziSpeedSteps_ = aziSteps;
        aziSpeed_ = steps2angle(aziSpeedSteps_, aziStep_);
        moveCommand(0x87, std::abs(aziSpeedSteps_));
    }

    // Set elevation speed
    if(eleSteps != eleSpeedSteps_) {
        if((eleSteps > 0 && eleSpeedSteps_ < 0) || (eleSteps < 0 && eleSpeedSteps_ > 0)) {
            eleSpeedChangedDirection = true;
        }
        eleSpeedSteps_ = eleSteps;
        eleSpeed_ = steps2angle(eleSpeedSteps_, eleStep_);
        moveCommand(0x88, std::abs(eleSpeedSteps_));
    }

    if(aziSpeedSteps_ == 0.0) {
        panInMotion_ = false;
    } else if(!panInMotion_ || aziSpeedChangedDirection) {
        panInMotion_ = true;
        aziTargetSectionBoundary_ = getAziTargetSectionBoundary(azimuth_, aziSpeed_);        
        moveCommand(0x81, angle2steps(aziTargetSectionBoundary_, aziStep_));
    }

    if(eleSpeedSteps_ == 0.0) {
        tiltInMotion_= false;
    } else if(!tiltInMotion_ || eleSpeedChangedDirection) {
        tiltInMotion_ = true;
        moveCommand(0x82, (elevation > 0.0) ? angle2steps(MAX_ELEVATION, eleStep_) : angle2steps(MIN_ELEVATION, eleStep_));
    }
}

#endif

bool ManipulatorPtuD4670::moveAbsolute(double azimuth, double elevation)
{
    posControlMode_ = ABS_POS_CONTROL;

    absAziReached_ = false;
    absEleReached_ = false;

    if(!valInRange(azimuth, MIN_AZIMUTH, MAX_AZIMUTH)) {
        cerr << "Required azimuth position " << azimuth <<
                " mrad is not within allowed range [" <<
                MIN_AZIMUTH << ", " << MAX_AZIMUTH <<
                "]. It will be clipped." << endl;
        azimuth = getValInRange(azimuth, MIN_AZIMUTH, MAX_AZIMUTH, 1.0);
    }

    if(!valInRange(elevation, MIN_ELEVATION, MAX_ELEVATION)) {
        cerr << "Required elevation position " << elevation <<
                " mrad is not within allowed range [" <<
                MIN_ELEVATION << ", " << MAX_ELEVATION <<
                "]. It will be clipped." << endl;

        elevation = getValInRange(elevation, MIN_ELEVATION, MAX_ELEVATION, 1.0);
    }   

    absMotionAziTarget_         = azimuth;
    absMotionEleTarget_         = elevation;

    aziSpeedSteps_  = angle2steps(MAX_AZIMUTH_SPEED, aziStep_)   * (absMotionAziTarget_ > azimuth_     ? 1.0 : -1.0);
    eleSpeedSteps_  = angle2steps(MAX_ELEVATION_SPEED, eleStep_) * (absMotionEleTarget_ > elevation_   ? 1.0 : -1.0);

    aziSpeed_   = steps2angle(aziSpeedSteps_, aziStep_);
    eleSpeed_   = steps2angle(eleSpeedSteps_, eleStep_);

    absMotionCurrentAziTarget_  = getAbsPosAziTarget(azimuth_, aziSpeed_, absMotionAziTarget_);

    if(azimuth_   != absMotionCurrentAziTarget_)    moveCommand(0x81, angle2steps(absMotionCurrentAziTarget_, aziStep_));    // PAN_SET_ABS_POS
    if(elevation_ != absMotionEleTarget_)           moveCommand(0x82, angle2steps(absMotionEleTarget_, eleStep_));    // TILT_SET_ABS_POS

    moveCommand(0x87, angle2steps(MAX_AZIMUTH_SPEED / 4.0, aziStep_));
    moveCommand(0x88, angle2steps(MAX_ELEVATION_SPEED / 4.0, eleStep_));

    return true;
}

bool ManipulatorPtuD4670::moveRelative(double azimuth, double elevation)
{
    return moveAbsolute(azimuth + azimuth_, elevation + elevation_);
}

void ManipulatorPtuD4670::step(int azimuth, int elevation)
{
    moveRelative((azimuth   > 0) ? aziStep_ : ((azimuth   < 0) ? -aziStep_ : 0.0),
                 (elevation > 0) ? eleStep_ : ((elevation < 0) ? -eleStep_ : 0.0));
}

void ManipulatorPtuD4670::moveCommand(uint8_t cmd, int stepsSpeed)
{

    string cmdStr = string(1, cmd) + int2byteStr(stepsSpeed, 2);

    try {
        command(cmdStr);
    } catch(ExceptionOLS &e) {
        cerr << e.what() << endl;
    }
}

void ManipulatorPtuD4670::command(string cmd, uint32_t readTimeout)
{
    string response;

    write(cmd);   
    read(response, readTimeout);

    if(response.length() != 1) {
        throw(ExceptionOLS("Wrong response length received after sending command. Command: " + prettyHexStr(cmd) +
                           ". Message(" + to_string(response.length()) + " bytes): '" + prettyHexStr(response) + "'."));
    } else if(response[0] != 0) {
        throw(ExceptionOLS("Command: '" + prettyHexStr(cmd) +
                           "' resulted with error code " + to_string((int)response[0])));
    }
}

uint32_t ManipulatorPtuD4670::query(string &cmd, int respLen, uint32_t waitReadble)
{
    uint32_t value;
    string response;

    if(respLen < 1 || respLen > 4) {
        throw(ExceptionOLS("Response length 'respLen' must be in range [1, 4]."));
    }

    // send message to serial interface
    write(cmd);

    // read message to serial interface
    read(response, waitReadble);

    if(response.length() != respLen) {
        throw(ExceptionOLS("Response of unexpected length. Received " + to_string(response.length()) +
                           " bytes, expected " + to_string(respLen) + " bytes. Response (hex): " +
                            prettyHexStr(response)));
    }

    value = (uint8_t)response[0];
    for(int i = 1; i < respLen; ++i) {
        value <<= 8;
        value += (uint8_t)response[i];
    }

    return value;
}

void ManipulatorPtuD4670::read(string &response, uint32_t waitTime)
{
    string l = "";
    response = "";

    if(waitTime != 0) {
        serial::Timeout toOld, toNew;
        toOld = serialIf_->getTimeout();
        toNew = toOld;
        toNew.read_timeout_constant = waitTime;

        serialIf_->setTimeout(toNew);
        serialIf_->waitReadable();
        serialIf_->setTimeout(toOld);
    }

    while(!(l = serialIf_->readline()).empty()) {
        response += l;
    }

#if USE_CVUT_CONTROL_UNIT
    if(response.length() > 0) {
        // Check and remove CRC from the CVUT control unit
        if(!checkCRC(response)) {
            throw(ExceptionOLS("read: CRC missmatch."));
        }

        response.erase(0, 1);
        response.pop_back();
    }
#endif

}

void ManipulatorPtuD4670::write(string &cmd)
{
#if USE_CVUT_CONTROL_UNIT
    // Adding CRC for the CVUT control unit
    cmd += int2byteStr(crc8(QByteArray(cmd.c_str(), cmd.length()), cmd.length()), 1);
#endif

    // Send command
    size_t bytesWritten = serialIf_->write(cmd);

    if(bytesWritten != cmd.size()) {
        ostringstream strs;
        for(int i = 0; i < cmd.size(); ++i) {
            strs << std::hex << (int)cmd[i] << " ";
        }
        throw(ExceptionOLS(string("Sending command '") + strs.str() + "' failed, only sent " +
                           to_string(bytesWritten) + " / " + to_string(cmd.length()) + " bytes."));
    }
}

inline void ManipulatorPtuD4670::setMotionState(eMotionState state)
{
    if(motionState_ != state) {
        motionState_ = state;
        command(string(1, ((state == INDEPENDENT_CTRL) ? 0xcc : 0xcd)));
    }
}

inline string ManipulatorPtuD4670::int2byteStr(uint32_t num, int bytes)
{
    if(bytes < 1 || bytes > 4) {
        throw(ExceptionOLS("hex2str: Wrong string length (" + to_string(bytes) +
                           ") required. Supported range is [1, 4]."));
    }

    string hexStr((char *)&num, bytes);

    // reverse string
    return string(hexStr.rbegin(), hexStr.rend());
}

inline int ManipulatorPtuD4670::angle2steps(double angle, double step)
{
    return (int)(std::abs(angle) / step + 0.5) * ((angle >= 0.0) ? 1 : -1);
}

inline double ManipulatorPtuD4670::steps2angle(int steps, double step)
{
    return steps * step;
}

inline bool ManipulatorPtuD4670::valInRange(double val, double min, double max)
{
    if(max < min) throw(ExceptionOLS("posInRange: max cannot be lower value than min."));

    return val >= min && val <= max;
}

inline bool ManipulatorPtuD4670::movingInRange(double pos, double speed, double min, double max)
{
    if(max < min) throw(ExceptionOLS("movInRange: max cannot be lower value than min."));

    return (pos < max || speed <= 0.0) && (pos > min || speed >= 0.0);
}

inline double ManipulatorPtuD4670::getValInRange(double val, double min, double max, double margin)
{
    if(margin < 0.0) throw(ExceptionOLS("getPosInRange: margin must be a positive value."));
    if(max < min)                    throw(ExceptionOLS("getPosInRange: max cannot be lower value than min."));

    return (std::abs(std::min(max, std::max(min, val))) - margin) * ((val < 0.0) ? -1.0 : 1.0);
}

void ManipulatorPtuD4670::haltPan()
{
    aziSpeed_ = 0.0;
    aziSpeedSteps_ = 0;
    command(string(1, 0xa9));

    // debug
    cout << "HALTING PAN!" << endl;
}

void ManipulatorPtuD4670::haltTilt()
{
    eleSpeed_ = 0.0;
    eleSpeedSteps_ = 0;
    command(string(1, 0xaa));

    // debug
    cout << "HALTING TILT!" << endl;
}

void ManipulatorPtuD4670::onKeyDown(const keyboard::Key::ConstPtr key)
{
    const double maxSpeed = 225.0;
    switch(key->code) {

        // Motion control keys: UP, DOWN, LEFT, RIGHT
        case keyboard::Key::KEY_LEFT:
            switch(precState_) {
                case STEP:          step(1, 0);                             break;
                case CONTINUOUS:    move(MAX_AZIMUTH_SPEED, getElevationSpeed());    break;
            }
            break;
        case keyboard::Key::KEY_RIGHT:
            switch(precState_) {
                case STEP:          step(-1, 0);                            break;
                case CONTINUOUS:    move(-MAX_AZIMUTH_SPEED, getElevationSpeed());   break;
            }
            break;
        case keyboard::Key::KEY_UP:
            switch(precState_) {
                case STEP:          step(0, 1);                             break;
                case CONTINUOUS:    move(getAzimuthSpeed(), MAX_ELEVATION_SPEED);      break;
            }
            break;
        case keyboard::Key::KEY_DOWN:
            switch(precState_) {
                case STEP:          step(0, -1);                            break;
                case CONTINUOUS:    move(getAzimuthSpeed(), -MAX_ELEVATION_SPEED);     break;
            }
            break;

        // Set precision mode - STEP
        case keyboard::Key::KEY_s:  precState_ = STEP;          break;
        // Set precision mode - CONTINUOUS
        case keyboard::Key::KEY_c:  precState_ = CONTINUOUS;    break;

        // Debug - test synchrony
        case keyboard::Key::KEY_INSERT:
            testSynchrony(300, false, true);
            break;
        case keyboard::Key::KEY_DELETE:
            testSynchrony(300, true, true);
            break;
        case keyboard::Key::KEY_PAGEUP:
            moveAbsolute(MAX_AZIMUTH, 0.0);
            break;
        case keyboard::Key::KEY_PAGEDOWN:
            moveAbsolute(MIN_AZIMUTH, 0.0);
            break;
        
        case keyboard::Key::KEY_e:
            moveAbsolute(0.0, elevation_);
            break;
        case keyboard::Key::KEY_a:
            moveAbsolute(azimuth_, 0.0);
            break;       

        case keyboard::Key::KEY_BACKSPACE:
            moveAbsolute(0.0, 0.0);
            break;

        case keyboard::Key::KEY_SPACE:
            cout << "Azimuth:   " << azimuth_   << endl;
            cout << "Elevation: " << elevation_ << endl;
            break;

        // debug start/stop tracking
        case keyboard::Key::KEY_t:
            if(followTarget_) {
                disableFollowTarget();
            } else {
                enableFollowTarget();
            }


        default:
            break;
    }
}

void ManipulatorPtuD4670::onKeyUp(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {
        // Motion control keys: UP, DOWN, LEFT, RIGHT
        case keyboard::Key::KEY_LEFT:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(0.0, getElevationSpeed()); break;
            }
            break;
        case keyboard::Key::KEY_RIGHT:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(0.0, getElevationSpeed()); break;
            }
            break;
        case keyboard::Key::KEY_UP:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(getAzimuthSpeed(), 0.0);   break;
            }
            break;
        case keyboard::Key::KEY_DOWN:
            switch(precState_) {
                case STEP:          break;
                case CONTINUOUS:    move(getAzimuthSpeed(), 0.0);   break;
            }
            break;
        default:
            break;
    }
}


#if DEBUG_MANIP_PTUD4670
void ManipulatorPtuD4670::onJoy(const sensor_msgs::Joy::ConstPtr& joy)
{
    static bool buttonDown = false;

    if(!buttonDown) {
        if (joy->buttons[4])
        {
            buttonDown = true;            
            move(0.1 * MAX_AZIMUTH_SPEED, 0.0);
        }
        else if (joy->buttons[5])
        {
            buttonDown = true;
            move(0.1 * -MAX_AZIMUTH_SPEED, 0.0);
        }
        else if (joy->buttons[8])
        {
            buttonDown = true;
            haltPan();
            haltTilt();
        }
        else
        {
            // debug
//            cout << joy->axes[0] << ", " << joy->axes[1] << endl;
            move(joy->axes[0] * MAX_AZIMUTH_SPEED, joy->axes[1] * MAX_ELEVATION_SPEED);
        }
    }
    else
    {
        buttonDown = false;
    }
}
#else
void ManipulatorPtuD4670::onJoy(const sensor_msgs::Joy::ConstPtr& joy)
{
    static bool buttonDown = false;

    if(!buttonDown) {
        if(joy->buttons[0])
        {
            buttonDown = true;
        } else if(joy->buttons[1]) {
            buttonDown = true;
        } else if(joy->buttons[2]) {
            buttonDown = true;
        } else if(joy->buttons[3]) {
            buttonDown = true;
        } else if(joy->buttons[4]) {
            moveAbsolute(0.0, elevation_);
            buttonDown = true;
        } else if(joy->buttons[5]) {
            moveAbsolute(azimuth_, 0.0);
            buttonDown = true;
        }

        else {
            double x = joySpeed_->getY(joy->axes[0]) * 1000.0;
            double y = joySpeed_->getY(joy->axes[1]) * 1000.0;

            move(joySpeed_->getY(joy->axes[0]) * 1000.0, joySpeed_->getY(joy->axes[1]) * 1000.0);
        }
    }
    else
    {
        buttonDown = false;
    }
}
#endif

inline double ManipulatorPtuD4670::getAziTargetSectionBoundary(double azimuth, double speed)
{
    int aziIdx;
    int targetAziIdx;

    aziIdx          = (int)((azimuth - MIN_AZIMUTH) / AZIMUTH_SECTION_W);
    targetAziIdx    = std::max(std::min(AZIMUTH_SECTIONS, aziIdx + (speed > 0.0 ? 2 : -1)), 0);

    return aziSections_[targetAziIdx];
}

inline double ManipulatorPtuD4670::getAbsPosAziTarget(double azimuth, double speed, double target)
{
    double newBoundary = getAziTargetSectionBoundary(azimuth, speed);

    return ((speed > 0.0) ? std::min(target, newBoundary) :
                            std::max(target, newBoundary));
}

void ManipulatorPtuD4670::onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg)
{
    double azi = 0.0;
    double ele = 0.0;

    double azSpSc;
    double elSpSc;

    switch(mvMsg->mode) {
        case msgs::ManipulatorMove::MODE_ABSOLUTE:
            break;

        case msgs::ManipulatorMove::MODE_RELATIVE:
            break;

        case msgs::ManipulatorMove::MODE_CONTINUOUS:
            switch(mvMsg->speedType) {
                case msgs::ManipulatorMove::SPEED_TYPE_ABSOLUTE:
                    break;

                case msgs::ManipulatorMove::SPEED_TYPE_PROPORTIONAL:
                    azSpSc = mvMsg->aziSpeed;
                    elSpSc = mvMsg->eleSpeed;

                    if(mvMsg->aziSpeed < -1.0 || mvMsg->aziSpeed > 1.0) {
                        azSpSc = std::max(std::min(1.0, azSpSc), -1.0);
                        ROS_WARN_STREAM("onManipMove: aziSpeed not in range [-1.0, 1.0], will be clipped.");
                    }

                    if(mvMsg->eleSpeed < -1.0 || mvMsg->eleSpeed > 1.0) {
                        elSpSc = std::max(std::min(1.0, elSpSc), -1.0);
                        ROS_WARN_STREAM("onManipMove: eleSpeed not in range [-1.0, 1.0], will be clipped.");
                    }

                    azi = azSpSc * MAX_AZIMUTH_SPEED;
                    ele = elSpSc * MAX_ELEVATION_SPEED;

                    break;

                default:
                    ROS_ERROR_STREAM("onManipMove: Incorrect 'speedType' selected in ManipulatorMove message");
                    return;
            }

            move(azi, ele);

            break;

        default:
            ROS_ERROR_STREAM("onManipMove: Incorrect 'mode' selected in ManipulatorMove message");
            return;
    }
}

// debug
void ManipulatorPtuD4670::forceStop()
{
    // debug
    cout << "Force stop the manipulator" << endl;
    move(0.0, 0.0);
}

// debug
string ManipulatorPtuD4670::prettyHexStr(string hexNums)
{
    string pretty;

    for(int i = 0; i < hexNums.length(); ++i) {
        int whole = ((uint8_t)hexNums[i]) / 16;
        int resid = ((uint8_t)hexNums[i]) % 16;

        if(whole >= 10)
            pretty += (char)((whole - 10) + 'a');
        else
            pretty += (char)('0' + whole);

        if(resid >= 10)
            pretty += (char)((resid - 10) + 'a');
        else
            pretty += (char)('0' + resid);

        pretty += " ";
    }

    return pretty;
}

void ManipulatorPtuD4670::testSynchrony(int steps, bool aziEle, bool cw)
{
    testSynchrony_ = true;
    testSynSteps_ = steps;
    testSynStep_ = 0;

    // test azimuth
    if(aziEle) {
        testSynAziStep_ = (((MAX_AZIMUTH - MIN_AZIMUTH) - 200.0) / steps) * (cw ? 1.0 : -1.0);
        testSynEleStep_ = 0.0;

        moveAbsolute(cw ? MIN_AZIMUTH : MAX_AZIMUTH, 0.0);

    // test elevation
    } else {
        testSynEleStep_ = (((MAX_ELEVATION - MIN_ELEVATION) - 200.0) / steps) * (cw ? (1.0) : (-1.0));
        testSynAziStep_ = 0.0;

        moveAbsolute(0.0, cw ? MIN_ELEVATION : MAX_ELEVATION);
    }

    cout << "azimuth step:   " << testSynAziStep_ << endl;
    cout << "elevation step: " << testSynEleStep_ << endl;
}
