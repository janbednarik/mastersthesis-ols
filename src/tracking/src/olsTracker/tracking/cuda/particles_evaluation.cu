//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#include "test.h"
#include "Particles.cu"

#include "particles_evaluation_kernel.cu"

#include <iostream>

// CUDA includes
#include <cuda_runtime.h>
#include <vector_types.h>

#include <assert.h>

#include <device_functions.h>
#include <cmath>
// #include "TicTac.h"

// TEST!!
void testGPUMatFunction(float *d, int step, int rows, int cols)
{
    // call the kernel
    dim3 threads(16, 16, 1);
    dim3 blocks(cols / threads.x, rows / threads.y, 1);
    intensityMul <<< blocks, threads >>> (d, step, rows, cols);
    assert(cudaGetLastError()==0);

}


// Computes the sum of elements in the image.
void img_add(struct Img *a, struct Img *b)
{
    // Set up kernel sources.
    // Create texture.
    cudaChannelFormatDesc channelDescA = cudaCreateChannelDesc<float>();
    cudaChannelFormatDesc channelDescB = cudaCreateChannelDesc<float>();
    cudaBindTexture2D(NULL, t_img1,  a->data_, channelDescA, a->width_, a->height_, a->pitch_data_);
    cudaBindTexture2D(NULL, t_img2,  b->data_, channelDescB, b->width_, b->height_, b->pitch_data_);

    dim3 threads(16, 16, 1);
    dim3 blocks(a->width_ / threads.x, a->height_ / threads.y, 1);
    struct Img *dptr_a = img_create_dev_ptr(a);
    struct Img *dptr_b = img_create_dev_ptr(b);

    // Lunch kernel
    add_kernel <<< blocks, threads >>> (dptr_a, dptr_b);
    assert(cudaGetLastError()==0);

    // Free kernel sources.
    img_free_dev_ptr(&dptr_a);
    img_free_dev_ptr(&dptr_b);

    cudaUnbindTexture(t_img1);
    cudaUnbindTexture(t_img2);

    // std::cout << "Sum " << std::endl;
}


// Parralel sum of elements in the array. Size has to be multiplication of two.
float sum_array(float *data, int num)
{
    float sum = 0.0;

    // Temporary results.
    float *sub_sums;
    cudaMalloc(&sub_sums, sizeof(float) * (num / 2));

    dim3 threads(1, 1, 1);
    dim3 blocks(1, 1, 1);
    threads.x = num / 2; threads.y = 1; threads.z = 1;

    // First round
    sub_sum <<< blocks, threads >>> (sub_sums, data, (float *) &(data[threads.x]));
    assert(cudaGetLastError()==0);

    threads.x = threads.x / 2;
    while (threads.x > 0) {
        sub_sum <<< blocks, threads >>> (sub_sums, sub_sums, (float *) &(sub_sums[threads.x]));
        assert(cudaGetLastError()==0);
        threads.x = (int) (threads.x / 2);
    }

    // Sum has been computed... copy result.
    cudaMemcpy(&sum, &(sub_sums[0]), sizeof(float), cudaMemcpyDeviceToHost);

    // Deallocate sources.
    cudaFree(sub_sums);

    return sum;
}

//! Normalization.
void particle_normalization(struct Particles *p)
{
    float sum = sum_array(p->fitness_, p->count_);

    // For a particle.
    dim3 blocks(1, 1, 1);
    dim3 threads(16, 1, 1);
    threads.x = 16 * 16; threads.y = 1; threads.z = 1;
    blocks.x = p->count_ / threads.x; blocks.y = 1;

    struct Particles *dptr_p = particles_create_dev_ptr(p);
    particle_normalization_kernel <<< blocks, threads >>> (dptr_p, sum);
    assert(cudaGetLastError()==0);
    particles_free_dev_ptr(&dptr_p);
}

//! Resampling.
void particle_resampling(struct Particles *dst, struct Particles *src)
{
    dim3 blocks(1, 1, 1);
    dim3 threads(16, 1, 1);
    threads.x = 16 * 16; threads.y = 1; threads.z = 1;
    blocks.x = dst->count_ / threads.x; blocks.y = 1;

    struct Particles *dptr_dst = particles_create_dev_ptr(dst);
    struct Particles *dptr_src = particles_create_dev_ptr(src);

    particle_resampling_kernel <<< blocks, threads >>> (dptr_dst, dptr_src);
    assert(cudaGetLastError()==0);

    particles_free_dev_ptr(&dptr_dst);
    particles_free_dev_ptr(&dptr_src);
}

//! Translation.
void particle_translation(struct Particles *p)
{
    dim3 blocks(1, 1, 1);
    dim3 threads(16, 1, 1);
    threads.x = 16 * 16; threads.y = 1; threads.z = 1;
    blocks.x = p->count_ / threads.x; blocks.y = 1;

    struct Particles *dptr_p = particles_create_dev_ptr(p);

    // Add noise to every particle with respect to noise configuration...(define as constant in the cuda.)
    particle_translation_kernel  <<< blocks, threads >>> (dptr_p);
    assert(cudaGetLastError()==0);

    particles_free_dev_ptr(&dptr_p);
}

// All data has to bee in the device memory!
void evaluate_particles(struct Particles *p, struct Img *img, struct Img *fg, struct Img *target, struct Img *mask, double *M)
{
    // Set textures.
    t_img1.addressMode[0] = cudaAddressModeWrap; t_img1.addressMode[1] = cudaAddressModeWrap; t_img1.filterMode = cudaFilterModeLinear; t_img1.normalized = false;
    t_img2.addressMode[0] = cudaAddressModeWrap; t_img2.addressMode[1] = cudaAddressModeWrap; t_img2.filterMode = cudaFilterModeLinear; t_img2.normalized = false;
    t_img3.addressMode[0] = cudaAddressModeWrap; t_img3.addressMode[1] = cudaAddressModeWrap; t_img3.filterMode = cudaFilterModePoint; t_img3.normalized = false;
    t_img4.addressMode[0] = cudaAddressModeWrap; t_img4.addressMode[1] = cudaAddressModeWrap; t_img4.filterMode = cudaFilterModePoint; t_img4.normalized = false;

    // Compute fitness values.
    // For a particle.
    dim3 blocks(1, 1, 1);
    dim3 threads(16, 1, 1);
    threads.x = target->width_; threads.y = 1; threads.z = 1;
    blocks.x = p->count_; blocks.y = 1;

    // Set up kernel sources.
    // Create texture.
    // t_img1 is obseration texture...
    cudaChannelFormatDesc channelDescA = cudaCreateChannelDesc<float>();
    cudaChannelFormatDesc channelDescB = cudaCreateChannelDesc<float>();
    cudaChannelFormatDesc channelDescC = cudaCreateChannelDesc<float>();
    cudaChannelFormatDesc channelDescD = cudaCreateChannelDesc<float>();
    cudaBindTexture2D(NULL, t_img1,  img->data_, channelDescA, img->width_, img->height_, img->pitch_data_);
    cudaBindTexture2D(NULL, t_img2,  fg->data_, channelDescB, fg->width_, fg->height_, fg->pitch_data_);
    cudaBindTexture2D(NULL, t_img3,  target->data_, channelDescC, target->width_, target->height_, target->pitch_data_);
    cudaBindTexture2D(NULL, t_img4,  mask->data_, channelDescD, mask->width_, mask->height_, mask->pitch_data_);

    // Move structures from the host memory to the device memory.
    struct Img *dptr_img = img_create_dev_ptr(img);
    struct Img *dptr_fg = img_create_dev_ptr(fg);
    struct Img *dptr_target = img_create_dev_ptr(target);
    struct Img *dptr_mask = img_create_dev_ptr(mask);
    struct Particles *dptr_p = particles_create_dev_ptr(p);

    // Lunch kernel
    evaluate_particles_kernel <<< blocks, threads >>> (dptr_p, dptr_img, dptr_fg, dptr_target, dptr_mask, M);
    assert(cudaGetLastError()==0);

    threads.x = target->width_; threads.y = 1; threads.z = 1;
    blocks.x = p->count_ / threads.x; blocks.y = 1;
    addSubSum <<< blocks, threads >>>(dptr_p, target->width_);
    assert(cudaGetLastError()==0);

    particles_free_dev_ptr(&dptr_p);
    img_free_dev_ptr(&dptr_img);
    img_free_dev_ptr(&dptr_fg);
    img_free_dev_ptr(&dptr_target);
    img_free_dev_ptr(&dptr_mask);

    cudaUnbindTexture(t_img1);
    cudaUnbindTexture(t_img2);
    cudaUnbindTexture(t_img3);
    cudaUnbindTexture(t_img4);
}

//! Warp perspective.
void warp_perspective(struct Img *in, struct Img *out, double *M)
{
    // Setup texture ref.
    t_img1.addressMode[0] = cudaAddressModeClamp; t_img1.addressMode[1] = cudaAddressModeClamp;
    t_img2.addressMode[0] = cudaAddressModeClamp; t_img2.addressMode[1] = cudaAddressModeClamp;
    t_img1.filterMode = cudaFilterModeLinear;
    t_img2.filterMode = cudaFilterModeLinear;
    t_img1.normalized = false;
    t_img2.normalized = false;

    // Create texture.
    cudaChannelFormatDesc channelDescA = cudaCreateChannelDesc<float>();
    assert(cudaGetLastError()==cudaSuccess);
    cudaChannelFormatDesc channelDescB = cudaCreateChannelDesc<float>();
    assert(cudaGetLastError()==cudaSuccess);
    cudaBindTexture2D(NULL, t_img1,  in->data_, channelDescA, in->width_, in->height_, in->pitch_data_);
    assert(cudaGetLastError()==cudaSuccess);
    cudaBindTexture2D(NULL, t_img2,  out->data_, channelDescB, out->width_, out->height_, out->pitch_data_);
    assert(cudaGetLastError()==cudaSuccess);

    dim3 threads(16, 16);
    dim3 blocks(ceil(static_cast<float>(in->width_) / threads.x), ceil(static_cast<float>(in->height_) / threads.y));
    struct Img *dptr_a = img_create_dev_ptr(in);
    struct Img *dptr_b = img_create_dev_ptr(out);

    // Lunch kernel
    warp_perspective_kernel <<< blocks, threads >>> (dptr_a, dptr_b, M);
    assert(cudaGetLastError()==cudaSuccess);

    // Free kernel sources.
    img_free_dev_ptr(&dptr_a);
    img_free_dev_ptr(&dptr_b);

    cudaUnbindTexture(t_img1);
    cudaUnbindTexture(t_img2);
}

// Compute mask for transformation (transformation has been inverse!).
void warp_perspective_get_mask(unsigned char *data, int width, int height, size_t pitch_data, double *M, unsigned char in_val, unsigned char out_val)
{
    dim3 threads(16, 16, 1);
    dim3 blocks(ceil(static_cast<float>(width) / threads.x), ceil(static_cast<float>(height) / threads.y), 1);

    warp_perspective_get_mask_kernel <<< blocks, threads >>>(data, width, height, pitch_data, M, in_val, out_val);
    assert(cudaGetLastError()==0);
}


// All data has to bee in the device memory!
void test_particles2(struct Particles *p, struct Img *obs, struct Img *pattern, float *M)
{
    // TIME;
    /*
    cudaEvent_t start, stop;
    float time;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord( start, 0 );
    */


    // Compute fitness values.
    // For a particle.
    dim3 blocks(1, 1, 1);
    dim3 threads(16, 1, 1);
    threads.x = pattern->width_; threads.y = 1; threads.z = 1;
    blocks.x = p->count_; blocks.y = 1;

    // Set up kernel sources.
    // Create texture.
    // t_img1 is obseration texture...
    cudaChannelFormatDesc channelDescA = cudaCreateChannelDesc<float>();
    cudaChannelFormatDesc channelDescB = cudaCreateChannelDesc<float>();
    cudaBindTexture2D(NULL, t_img1,  obs->data_, channelDescA, obs->width_, obs->height_, obs->pitch_data_);
    cudaBindTexture2D(NULL, t_img2,  pattern->data_, channelDescB, pattern->width_, pattern->height_, pattern->pitch_data_);

    struct Img *dptr_pattern = img_create_dev_ptr(pattern);
    struct Img *dptr_obs = img_create_dev_ptr(obs);
    struct Particles *dptr_p = particles_create_dev_ptr(p);

    // Lunch kernel
    evaluateParticles <<< blocks, threads >>> (dptr_p, dptr_pattern, dptr_obs, M);

    threads.x = pattern->width_; threads.y = 1; threads.z = 1;
    blocks.x = p->count_ / threads.x; blocks.y = 1;
    addSubSum <<< blocks, threads >>>(dptr_p, pattern->width_);


    particles_free_dev_ptr(&dptr_p);
    img_free_dev_ptr(&dptr_obs);
    img_free_dev_ptr(&dptr_pattern);

    cudaUnbindTexture(t_img1);
    cudaUnbindTexture(t_img2);

    // Time
    /*
    cudaEventRecord( stop, 0 );
    cudaEventSynchronize( stop );

    cudaEventElapsedTime( &time, start, stop );
    cudaEventDestroy( start );
    cudaEventDestroy( stop );

    std::cout << "Elapsed time in ms: " << time << "."<< std::endl;
    */
}


void test_particles(unsigned char *img_data, int img_step, int img_width, int img_height,
                    unsigned char *pattern_data, int pattern_step, int pattern_width, int pattern_height,
                    int n_particles)
{


    // TIME;
    cudaEvent_t start, stop;
    float time;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord( start, 0 );


    // Compute fitness values.
    dim3 blocks(1, 1, 1);
    dim3 threads(1, 1, 1);

    /*
    threads.x = 16; threads.y = 16;
    blocks.x = (p->count_ / 16) * 2;
    blocks.y = 32;
    */

    // Kernel..


    // Time
    cudaEventRecord( stop, 0 );
    cudaEventSynchronize( stop );

    cudaEventElapsedTime( &time, start, stop );
    cudaEventDestroy( start );
    cudaEventDestroy( stop );

    std::cout << "Elapsed time in ms: " << time << "."<< std::endl;

    // testKernel <<<1,1 >>> ();


    // TIME;

    /*
    TicTac t;
    t.tic();
    // Control results...
    evaluateParticles_HOST(img_data, img_step, pattern_data, pattern_step, p);
    std::cout << "Elapsed time in ms: " << t.tac() * 1000.0 << "."<< std::endl;
    particles_free(p);
    */

}




/*


// Allocates particles.
void particles_alloc(struct Particles *p, int count)
{
    // Number of particles...
    p->count_ = count;

    // Host memory... - page locked memory allocation.
    cudaMallocHost(&(p->h_x_),  count * sizeof(float));
    cudaMallocHost(&(p->h_y_),   count * sizeof(float));
    cudaMallocHost(&(p->h_width_),  count * sizeof(float));
    cudaMallocHost(&(p->h_height_), count * sizeof(float));
    cudaMallocHost(&(p->h_yaw_),    count * sizeof(float));
    cudaMallocHost(&(p->h_fitness_), count * sizeof(float));

    // Device memory...
    cudaMalloc(&(p->d_x_), count * sizeof(float));
    cudaMalloc(&(p->d_y_), count * sizeof(float));
    cudaMalloc(&(p->d_width_), count * sizeof(float));
    cudaMalloc(&(p->d_height_), count * sizeof(float));
    cudaMalloc(&(p->d_yaw_), count * sizeof(float));
    cudaMalloc(&(p->d_fitness_), count * sizeof(float));
}

// Fill the structure of particles by random values...
void particles_randomize_host(struct Particles *p, int width, int height)
{
    // Randomize host memory w.r.t. the size of the input image.
    for (int i = 0; i < p->count_; i++) {
        p->h_width_[i] = 32.0f;
        p->h_height_[i] = 32.0f;
        p->h_x_[i] = rand() % (width - 64) + 32 ;
        p->h_y_[i] = rand() % (height - 64) + 32;
        p->h_yaw_[i] = 0.0f;
        p->h_fitness_[i] = 0.0f;
    }
}

// Download to host memory.
void particles_copy_device_to_host(struct Particles *p)
{
    cudaMemcpyAsync(p->h_x_,         p->d_x_,        p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_y_,         p->d_y_,        p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_width_,     p->d_width_,    p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_height_,    p->d_height_,   p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_yaw_,       p->d_yaw_,      p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_fitness_,   p->d_fitness_,  p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
}

// Upload to device memory.
void particles_copy_host_to_device(struct Particles *p)
{
    cudaMemcpyAsync(p->d_x_,         p->h_x_,        p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_y_,         p->h_y_,        p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_width_,     p->h_width_,    p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_height_,    p->h_height_,   p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_yaw_,       p->h_yaw_,      p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_fitness_,   p->h_fitness_,  p->count_ * sizeof(float), cudaMemcpyHostToDevice);
}

void particles_free(struct Particles *p)
{
    cudaFreeHost(p->h_x_);
    cudaFreeHost(p->h_y_);
    cudaFreeHost(p->h_height_);
    cudaFreeHost(p->h_width_);
    cudaFreeHost(p->h_yaw_);
    cudaFreeHost(p->h_fitness_);

    cudaFree(p->d_x_);
    cudaFree(p->d_y_);
    cudaFree(p->d_height_);
    cudaFree(p->d_width_);
    cudaFree(p->d_yaw_);
    cudaFree(p->d_fitness_);
}


*/

/*
struct gpuImg* gpuImg_create_from(unsigned char* data, int step, int width, int height, texture<unsigned char, 2> *tex)
{
    // Create structure...
    struct gpuImg* img = (gpuImg *) malloc(sizeof(struct gpuImg));
    img->height_ = height;
    img->width_ = width;

    // Allocate device memory and copy data.
    cudaMallocPitch(&(img->d_data_), &(img->pitch_), width, height);
    cudaMemcpy2D(img->d_data_, img->pitch_, data, step, width, height, cudaMemcpyHostToDevice);

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();
    cudaBindTexture2D(NULL, *tex,  img->d_data_, channelDesc, img->height_, img->width_, img->pitch_);


    return img;
}
*/



/*
// CPU implementation...
void evaluateParticles_HOST(unsigned char *img_data, size_t img_pitch, unsigned char * pattern_data, size_t pattern_pitch, struct Particles *p)
{
    for (int i = 0; i < p->count_; i++) {       // Over all particles...

        float sum_diff = 0.0;
        // Calculate SAD.
        for (int y = 0; y < 32; y++) {
            for (int x = 0; x < 32; x++) {
                // std::cout << " y " <<  p->h_y_[i] <<  "  " <<  ((int) p->h_y_[i] + y) * img_pitch << std::endl;
                // std::cout << "data 1 " << img_data[0] << std::endl;
                // std::cout << " data " << img_data[((int) p->h_y_[i] + y) * img_pitch ] << std::endl;
                // std::cout << " ok " << std::endl;
                sum_diff += (float) abs(img_data[((int) p->h_y_[i] + y) * img_pitch + (int) p->h_x_[i] + x] - pattern_data[x + y * pattern_pitch]);

                //sum_diff += pattern_data[x + y * pattern_pitch];
            }
        }

        // std::cout << "Sum diff " << sum_diff << std::endl;
        // std::cout << "GPU computed: " <<  p->h_fitness_[i] << std::endl;
        if (p->h_fitness_[i] == sum_diff) {
            ; //std::cout << "Particle " << i << ": pass" << std::endl;
        }
        else
            std::cout << "Particle " << i << ": not pass with error " << abs(p->h_fitness_[i] - sum_diff) << std::endl;


    }

}
*/


void test_sum_array()
{
    float array[128];
    float sum = 0.0;
    for (int i = 0; i < 128; i++) {
        array[i] = (float) i;
        sum += array[i];
    }

    std::cout << "Host sum " << sum << std::endl;

    float *array_device;
    cudaMalloc(&array_device, sizeof(float) * 128);

    // Copy.
    cudaMemcpy(array_device, array, sizeof(float) * 128, cudaMemcpyHostToDevice);

    float cuda_sum = sum_array(array_device, 128);

    std::cout << "Cuda sum " << cuda_sum << std::endl;

}
