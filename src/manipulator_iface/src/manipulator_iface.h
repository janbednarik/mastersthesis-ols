/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef MANIPULATOR_IFACE_H
#define MANIPULATOR_IFACE_H

// C++
#include <string>
#include <vector>

// ROS
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/JointState.h>
#include <dynamic_reconfigure/server.h>
#include <gazebo_msgs/SetModelConfiguration.h>

// Project libraries
#include <msgs/CameraUnitSensors.h>
#include <msgs/CameraUnitInfo.h>
#include <manipulator_iface/CameraUnitConfig.h>

//! \brief The ManipulatorIface class Serves the purpose of the hardware interface between
//! the manipulator and the rest of the system. When it starts it parses the input parameters
//! and config files containing the initla stationing info of the camera station and the intrinsics
//! of the camera. It then continuously receives the ManipualtorPoistion messages
//! form the P&T hardware (azimuth, elevationg angles) and creates and pub;ishes the CameraUnitInfo
//! messages.
//!
class ManipulatorIface {
public:
    static const std::string CAMERA_UNIT_SENSORS;
    static const std::string CAMERA_UNIT_INFO_TOPIC;
    static const std::string JOINT_STATE_TOPIC;

    // Default camera properties
    static const double DEFAULT_PIXEL_WIDTH;
    static const double DEFAULT_PIXEL_HEIGHT;
    static const int    DEFAULT_FRAME_WIDTH;
    static const int    DEFAULT_FRAME_HEIGHT;

public:
    //! \brief ManipulatorIface Constructor
    //!
    ManipulatorIface();

    //! \brief ManipulatorIface Destructor
    //!
    ~ManipulatorIface();

    //! \brief update Update function called after each ros::spin
    //!
    void update();

private:    
    ros::NodeHandle *nh_;   //!< Node handle

    // Publishers and subscribers
    ros::Subscriber cuSensorsSub_;
    ros::Publisher cuiPub_;         //!< Publisher of the CamerUnitInfo messages
    ros::Publisher jointStatePub_;  //!< Joint states publisher - for robot_state_publisher node

    // Transforms /tf
    tf::TransformBroadcaster tfBcastStation_;
    tf::TransformListener tfListener_;

    // Gazebo
    ros::ServiceClient modelConfigClient_;  //!< A service client to send the azimuth/elebation values to Gazebo simulation

    // Dynamic reconfigure
    dynamic_reconfigure::Server<manipulator_iface::CameraUnitConfig> server;

    std::string tf_prefix_;      //!< Name of the refernece frame for all stations in the system (= the origin).
    std::string world_frame_id_; //!< Prefix for ROS tf -> sending translation from 'world' (origin) to the position of this station (manipulator).

    std::string unit_idx_;
    int unitIdx;

    // Dynamic parameters
    bool haveNewConfig_;    //!< Flag - config change occured
    std::string cfgFile_;

    std::string tfCamera_;  //!< Name of the tf transform from 'world' to 'camera'.
    std::string tfFocus_;   //!< Name of the tf transform from 'world' to 'focus'.

    // Manipulator position
    double azimuth_;        //!< Current azimuth of the manipulator [rad].
    double elevation_;      //!< Current elevation of the manipulator [rad].
    double oldAzimuth_;     //!< Previous azimuth of the manipulator [rad].
    double oldElevation_;   //!< Previous elevation of the manipulator [rad].

    // The local position (cartesian coordinate frame)
    double x_;              //!< X coordinate in the world (global) coordinate space of the GROUND joint.
    double y_;              //!< Y coordinate in the world (global) coordinate space of the GROUND joint.
    double z_;              //!< Z coordinate in the world (global) coordinate space of the GROUND joint.

    // The global position (WGS84)
    double latitude_;
    double longitude_;
    double altitude_;

    double heading_;        //!< Inital Z axis rotation of the station [rad] (0 = geographic north).
    double localHeading_;   //!< Initial Z axis orientation of the station in the local coordinate frame (0 = parallel with the axis perpendicular to the base (line between two stations))
    double sideTilt_;       //!< Side tilt of the base plane [rad].
    double frontTilt_;      //!< Front tilt of the base plane [rad].

    // Camera properties
    double pixelWidth_;     //!< Horizontal cell size [m].
    double pixelHeight_;    //!< Vertical cell size [m].
    int frameWidth_;     //!< Horizontal frame size [px].
    int frameHeight_;    //!< Vertical frame size [px].

    // Gazebo simulation
    bool gazeboSimulation_; //! If set the Gazebo simulation is in progress.
    bool firstSimulationFrame_;

private:

    //! \brief onManipulatorPosition ManipulatorPosition message callback.
    //! \param manipPosMsg
    //!
    void onCameraUnitSensors(const msgs::CameraUnitSensorsConstPtr& cuSensorsMsg);

    //! \brief callback Dynamic recongigure callback
    //! \param config
    //! \param level
    //!
    void onDynamicReconfigure(manipulator_iface::CameraUnitConfig &config, uint32_t level);
};

#endif // #ifndef MANIPULATOR_IFACE_H
