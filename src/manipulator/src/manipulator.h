/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef MANIPULATOR_H
#define MANIPULATOR_H

// C++ libraries
#include <string>

// ROS libraries
#include <ros/ros.h>
#include <keyboard/Key.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/JointState.h>

// Project libraries
#include <msgs/ManipulatorMove.h>
#include <msgs/CameraUnitSensors.h>
#include <msgs/Regulate.h>
#include <crc.h>
#include "lense.h"
#include "exceptions_ols.h"
#include "regulator.h"

#define DEBUG 0

using namespace std;

//! \brief The Manipulator class Base class for controling the motion
//! of general P\&T unit.
//!
class Manipulator
{
public:
    enum motionDir {
        M_CCW,
        M_CW,
        M_UP,
        M_DOWN
    };

    //! Precision of motion control modes
    enum ePrecisionState {
        STEP,                   //!< One key/joystick press = one motor step
        CONTINUOUS              //!< Continuous motion.
    };

    static const string KEY_DOWN_TOPIC_SUFFIX;
    static const string KEY_UP_TOPIC_SUFFIX;
    static const string JOY_TOPIC_SUFFIX;
    static const string MANIP_MOVE_TOPIC;
    static const string REGULATE_TOPIC;
    static const string CAMERA_SENSORS_TOPIC;

    // Default camera properties
    static const double DEFAULT_FOCAL_LENGTH;
    static const double DEFAULT_PIXEL_WIDTH;
    static const double DEFAULT_PIXEL_HEIGHT;

    // Default regulator properties
    static const double DEFAULT_MOTION_PREDICTION_PERIOD;

public:
    //! Constructor
    Manipulator();

    //! Destructor
    virtual ~Manipulator();

    //! Reads the message from the manipulator and stores current position.
    virtual void update();

    //! \brief move Starts the azimuth/elevation motion with speed
    //! 'azimuth' / 'elevation' mrad/s.
    //! \param azimuth Azimuth speed [mrad/s] (+ turn CCW, - turn CW)
    //! \param elevation Elevation speed [mrad/s] (+ move UP, - move DOWN)tfCamera_
    //!
    virtual void move(double azimuth, double elevation) = 0;

    //! \brief moveAbsolute Moves the manipulator to the given absolute position.
    //! \param azimuth Target azimuth.
    //! \param elevation Target elevation.
    //! \return Flag stating whether the absolute motion was successfully started.
    //!
    virtual bool moveAbsolute(double azimuth, double elevation) = 0;

    //! \brief moveRelative Moves the manipulator relatively with regards to the current position.
    //! \param azimuth Target azimuth diffrence.
    //! \param elevation Target elevation difference.
    //! \return Flag stating whether the relative motion was successfully started.
    //!
    virtual bool moveRelative(double azimuth, double elevation) = 0;

    //! \brief step Performs one step in the azimuth, elevation or both at the same time
    //! in the given direction. Here a "step" means one step in the case of stepper
    //! motors or the smallest step given by the maximum resolution in case of the
    //! other motors.
    //! \param azimuth >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //! \param elevation >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //!
    virtual void step(int azimuth, int elevation) = 0;

    //! \brief haltPan Halts pan motion and sets the azimuthal speed appropriately.
    //!
    virtual void haltPan() = 0;

    //! \brief haltTilt Halts tilt motion and sets the elevation speed appropriately.
    //!
    virtual void haltTilt() = 0;

    //! \brief getAzimuth Extracts the azimuth from the response message from the manipulator.
    //! \return Current azimuth [mrad]
    //!
    double getAzimuth() { return azimuth_; }

    //! \brief getElevation Extracts the elevation from the response message from the manipulator.
    //! \return Current elevation [mrad]
    //!
    double getElevation() { return elevation_; }

    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    virtual void onKeyDown(const keyboard::Key::ConstPtr key) = 0;

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    virtual void onKeyUp(const keyboard::Key::ConstPtr key) = 0;

    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    virtual void onJoy(const sensor_msgs::Joy::ConstPtr& joy) = 0;

    //! \brief onManipMove
    //! \param mvMsg
    //!
    virtual void onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg) = 0;

    //! \brief onRegulate Regulates the motion of the manipulator during tracking.
    //! \param regulMsg
    //!
    void onRegulate(const msgs::RegulateConstPtr& regulMsg);   

    //! \brief enableFollowTarget Disables the regulator motion controled by the
    //! target tracker output.
    //!
    void enableFollowTarget();

    //! \brief disableFollowTarget
    //!
    void disableFollowTarget();

    //! \brief setPositionStamped
    //! \param azimuth
    //! \param elevation
    //! \param stamp
    //!
    void setPositionStamped(double azimuth, double elevation, ros::Time stamp);     

protected:
    string master_ns_;
    string unit_idx_;
    int unitIdx;

    string KEY_DOWN_TOPIC;
    string KEY_UP_TOPIC;
    string JOY_TOPIC;   

    // Azimuth/elevation state of the manipulator
    double azimuth_;        //!< Current manipulator azimuth [mrad].
    double elevation_;      //!< Current manipulator elevation [mrad].
    ros::Time azieleStamp_; //!< Time stamp of the most recetn azimuth and elevation values.
    double oldAzimuth_;     //!< Previous manipulator azimuth [mrad].
    double oldElevation_;   //!< Previous manipulator elevation [mrad].
    double aziSpeed_;       //!< Current azimuthal speed [mrad/s].
    double eleSpeed_;       //!< Current elevation speed [mrad/s].

    std::string command;    //!< Last command sent to the manipulator.   

    ePrecisionState precState_;             //!< Precision of the motion control state.

    Regulator* regulator_;  //!< Motion regulator used during tracking.
    SpeedFunc* joySpeed_;    //!< Motion regulator used during manual control by joystick.

    // Camera properties
//    double focalLength_;
    double focalLengthMax_; //!< [m]
    double focalLengthMin_; //!< [m]
    double pixelWidth_;     //!< Horizontal cell size [m].
    double pixelHeight_;    //!< Vertical cell size [m].

    // Lense properties
    std::string lensePort_;
    Lense *lense_;
    bool zooming_;
    bool focusing_;
    bool changingIris_;

    // Reguator properties
    ros::Duration motionPredictionPeriod_;

    // ROS structures.
    ros::NodeHandle *nh_;                       //!< Node handle.
    ros::Subscriber keyDownSub_;                //!< Keyboard press event subscription.
    ros::Subscriber keyUpSub_;                  //!< Keyboard release event subscription.
    ros::Subscriber joySub_;                    //!< Joystick event subscription.
    ros::Subscriber manipMoveSub_;
    ros::Subscriber regulateSub_;               //!< Subscirber of the message 'Regulate' - motion control during tracking
    ros::Publisher cuSensorsPub_;           //!< Publisher of the topic camera_sensors.

    string tf_prefix_;      //!< Name of the refernece frame for all stations in the system (= the origin).
    string world_frame_id_; //!< Prefix for ROS tf -> sending translation from 'world' (origin) to the position of this station (manipulator).   
    string serial_if_name_;

    // debug
    bool followTarget_;

protected:
    // zooming lens
    void zoom(int step);
    void focus(int step);
    void moveIris(int speed, int direction);

    double getFocalLength();

    string tfCanonicName(string tfFrame);
};

#endif // #define MANIPULATOR_H
