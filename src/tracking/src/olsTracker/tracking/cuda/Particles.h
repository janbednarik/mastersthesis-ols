//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#pragma once

#ifndef PARTICLES_H
#define PARTICLES_H

#include <stdint.h>

class ImageObservation
{
public:
    ImageObservation() : id_(0) {}
    uint32_t getID() {return id_; }

protected:
    uint32_t id_;
};

//! Particles.
struct Particles
{
    // Translation vector
    float *x_;              // Center coordinates.
    float *y_;

    // Scale
    float *s_x_;        // Scale factor. In the initial is 1.0.
    float *s_y_;

    // Rotation
    float *yaw_;

    // Fitness
    float *fitness_;
    float *fitness_temp_;

    // Mask sum.
    float *mask_sum_;
    float *mask_sum_temp_;

    float sum_;

    unsigned int* rnd_;   //!< For random generator.

    // Count.
    int count_;
};

struct ParticlesNoiseParameters
{
    float x_mean_;      // mean
    float x_s_;         // standard deviation

    float y_mean_;      // mean
    float y_s_;         // standard deviation

    float s_x_mean_;    // mean
    float s_x_s_;       // standard deviation

    float s_y_mean_;    // mean
    float s_y_s_;       // standard deviation

    float yaw_mean_;    // mean
    float yaw_s_;       // standard deviation
};


//! Image.
struct Img {
    int width_;
    int height_;
    size_t pitch_data_;         //!< Step.
    float *data_;               //!< Data.
};

//! Transformation matrix.

struct Mat {    
    double *data_;
};

// ---------- TRANSFORMATION MATRIX FUNCTION ---
Mat* mat_create_host();
Mat* mat_create_device();
void mat_copy_host2device(Mat* dest, Mat *src);
void mat_free_host(Mat **m);
void mat_free_device(Mat **m);


// ---------- IMAGE FUNCTION -------------------
Img* img_create_dev_ptr(struct Img* i);
void img_free_dev_ptr(struct Img** i);


Img* img_create_host(int width, int height);
Img *img_create_device(int width, int height);
void img_copy_raw2device(Img* dest, float *data, int pitch_data);
void img_copy_host2raw(float *dest, int pitch_dest, Img *src);

void img_copy_device2device(Img *dst, Img *src, float x, float y, float sx, float sy, float yaw);
void img_copy_host2device(Img* dest, Img *src);
void img_copy_device2host(Img* dest, Img *src);

bool img_is_equal_host(Img *a, Img *b);

void img_free_host(Img **i);
void img_free_device(Img **i);

//! Create header without deep copy of the data.
Img* img_create_header(int width, int height, int pitch, float *data);
//! Deallocate header.
void img_free_header(Img **i);


// ---------- PARTICLES FUNCTIONS --------------
//! Returns pointer to Particles structure which is all stored in the device memory - this pointer is definitely not useful for the host.
// Input pointer has been crated by particles_create_device function!
Particles* particles_create_dev_ptr(Particles *p);
//! Frees memory allocated in the device.
void particles_free_dev_ptr(Particles **p);


//! Returns allocated Particles structure in the host memory - everything will be stored in the host memory..
struct Particles* particles_create_host(int count);

//! Returns allocated Particles structure in the device/host memory. Pointer points to the host memory, but all aggregated data will be stored in the device memory.
struct Particles* particles_create_device(int count);

//! Frees memory allocated in the host.
void particles_free_host(Particles **p);

//! Frees memory allocated in the host/device.
void particles_free_device(Particles **p);

//! Copies data from the host structure to the device structure.
void particles_copy_host2device(Particles *h, Particles *d);

//! Copies data from the device structure to the host structure.
void particles_copy_device2host(Particles *h, Particles *d);

//! Pattern
void particles_evaluate(Particles *p);

//! Pattern
void particles_generate2host(Particles *p, ParticlesNoiseParameters *params);

//! Print.
void particles_print_host(Particles *p);

//!
void particles_copy_raw2device(Particles *p, float *raw);

// Warp perspective (transformation has been inverse!).
void warp_perspective(struct Img *in, struct Img *out, double *M);
// Compute mask for transformation (transformation has been inverse!).
void warp_perspective_get_mask(unsigned char *data, int width, int height, size_t pitch_data,  // Allocated mask.
                                                                double *M,                                                       // Transformation matrix (3x3).
                                                                unsigned char in_val,
                                                                unsigned char out_val);

#endif
