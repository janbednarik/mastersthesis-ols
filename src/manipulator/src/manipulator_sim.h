/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef MANIPULATOR_SIM_H
#define MANIPULATOR_SIM_H

// ROS libraries
#include <ros/ros.h>
#include <keyboard/Key.h>
#include <sensor_msgs/Joy.h>

// Project libraries.
#include "manipulator.h"
#include "regulator.h"

using namespace std;

//! \brief The ManipulatorSim class Controlling the simulated manipulator (rviz and gazebo).
//!
class ManipulatorSim: public Manipulator
{
public:
    // Manipulator properties set by this program
    static const double MAX_AZIMUTH_SPEED;      //!< Maxium allowed azimuth motion speed [mrad/s].
    static const double MAX_ELEVATION_SPEED;    //!< Maxium allowed elevation motion speed [mrad/s].
    static const double MAX_AZIMUTH;            //!< Maximum reachable azimuth [mrad] (given by construction constraints).
    static const double MIN_AZIMUTH;            //!< Minimum reachable azimuth [mrad] (given by construction constraints).
    static const double MAX_ELEVATION;          //!< Maximum reachable elevation [mrad] (given by construction constraints).
    static const double MIN_ELEVATION;          //!< Minimum reachable elevation [mrad] (given by construction constraints).

    static const double AZI_STEP;               //!< Minimum azimuth step [mrad].
    static const double ELE_STEP;               //!< Minimum elevation step [mrad].

    static const string LINK_STATE_TOPIC;       //!< The name of the LinkState messages topic

    //! Position contorl modes of the manipulator
    enum ePositionControlMode {
        SPEED_CONTROL,
        ABS_POS_CONTROL
    };

public:

    //! \brief ManipulatorSim Constructor
    //!
    ManipulatorSim(double simTimeStep);

    //! \brief ~ManipulatorSim Destructor
    //!
    virtual ~ManipulatorSim();

    //! \brief update Reads the message from the manipulator and stores current position.
    //!
    virtual void update();

    //! \brief move
    //! \param azimuth
    //! \param elevation
    //!
    virtual void move(double azimuth, double elevation);

    //! \brief moveAbsolute Moves the manipulator to the given absolute position [mrad].
    //! \param azimuth
    //! \param elevation
    //!
    virtual bool moveAbsolute(double azimuth, double elevation);

    //! \brief moveRelative Moves the manipulator relatively with regards to the current position [mrad].
    //! \param azimuth
    //! \param elevation
    //!
    virtual bool moveRelative(double azimuth, double elevation);

    //! \brief step Performs one step in the azimuth, elevation or both at the same time
    //! in the given direction. Here a "step" means one step in the case of stepper
    //! motors or the smallest step given by the maximum resolution in case of the
    //! other motors.
    //! \param azimuth >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //! \param elevation >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //!
    virtual void step(int azimuth, int elevation);

    //! \brief haltPan Halts pan motion and sets the azimuthal speed appropriately.
    //!
    virtual void haltPan();

    //! \brief haltTilt Halts tilt motion and sets the elevation speed appropriately.
    //!
    virtual void haltTilt();

    //! \brief onManipMove
    //! \param mvMsg
    //!
    virtual void onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg);

private:
    double simTimeStep_;   

    ePositionControlMode controlMode_;

    double aziAbsTarget_;
    double eleAbsTarget_;


private:
    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    virtual void onKeyDown(const keyboard::Key::ConstPtr key);

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    virtual void onKeyUp(const keyboard::Key::ConstPtr key);

    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    virtual void onJoy(const sensor_msgs::Joy::ConstPtr& joy);

    //! \brief getAzimmuthSpeed Returns azimuth speed.
    //! \return
    //!
    double getAzimuthSpeed() { return aziSpeed_; }

    //! \brief getElevationSpeed Returns elevation speed.
    //! \return
    //!
    double getElevationSpeed() { return eleSpeed_; }
};

#endif // #define MANIPULATOR_SIM_H
