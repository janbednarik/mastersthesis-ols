/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "gui.h"
#include <controls/SelectManipulator.h>

// ROS libraries
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

//C++
#include <string>

using namespace std;

const string Gui::IMAGE_TOPIC               = "processed_image";
const string Gui::KEYDOWN_TOPIC             = "keyboard/keydown";
const string Gui::KEYUP_TOPIC               = "keyboard/keyup";
const string Gui::START_DETECTION_TOPIC     = "start_detection";
const string Gui::STOP_DETECTION_TOPIC      = "stop_detection";
const string Gui::TARGET_STATE_LOCAL_TOPIC  = "target_state_local";
const string Gui::TARGET_STATE_TOPIC        = "target_state";
const string Gui::SELECT_MANIP_SRV          = "select_manipulator";
const string Gui::CAMERA_UNIT_INFO_TOPIC    = "camera_unit_info";
const string Gui::SET_TARGET_OBJECT_TOPIC   = "set_target_object_gui";
const string Gui::NAMED_WINDOW              = "camera_stream";

const double Gui::FONT_SIZE                 = 0.75;
const int Gui::FONT_THICKNESS               = 2;

const cv::Scalar Gui::CV_BLACK = cv::Scalar(0, 0, 0);
const cv::Scalar Gui::CV_WHITE = cv::Scalar(255, 255, 255);
const cv::Scalar Gui::CV_RED = cv::Scalar(0, 0, 255);
const cv::Scalar Gui::CV_GREEN = cv::Scalar(0, 255, 0);
const cv::Scalar Gui::CV_BLUE = cv::Scalar(255, 0, 0);
const cv::Scalar Gui::CV_CYAN = cv::Scalar(255, 255, 0);

const Gui::ETrackerType Gui::DEFAULT_TRACKER = Gui::TRACKER_OLS;

const string Gui::targetObjectStr[5] = {
    string("unknown"),
    string("drone"),
    string("aircraft"),
    string("human"),
    string("dog")
};

#if TESTING_2_UNITS
const string Gui::JOY_TOPIC = "joy_0";
#else
const string Gui::JOY_TOPIC = "joy";
#endif

Gui::Gui(): cameraUnit_(0), dwChBb_(0), dhChBb_(0), cameraUnitInfoSub_(NULL), startStopDetection_(NULL),
    tgtDist_(0.0), tgtSpeed_(0.0), fixedPoints_(NULL), showFixedPoints_(NULL),
    cuInfoInitialized_(NULL), checkCuInfoInitialized_(true),
    tgtObject_(OBJECT_UNKNOWN),
    mapVis_(NULL), imgIdx_(0)
{    
    // Initializae ROS structures and communication
    nh_ = new ros::NodeHandle();       

    // Load parameters from ROS parameter server.
    if (!ros::param::get("/camera_unit_ns", camera_unit_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter camera_unit_ns");
        exit(1);
    }

    if (!ros::param::get("/camera_units_count", CAMERA_UNITS_COUNT)) {
        ROS_FATAL_STREAM("Could not get parameter camera_units_count");
        exit(1);
    }

    for(int i = 0; i < CAMERA_UNITS_COUNT; ++i) {
        cameraUnits_[i] = new CameraUnit(i);

        // Frame size of camera at camera unit i
        if (!ros::param::get(string("/") + camera_unit_ns_ + string("_") + to_string(i) + string("/camera/width"), cameraUnits_[i]->camW_)) {
            ROS_FATAL_STREAM("Could not get parameter width");
        }

        if (!ros::param::get(string("/") + camera_unit_ns_ + string("_") + to_string(i) + string("/camera/height"), cameraUnits_[i]->camH_)) {
            ROS_FATAL_STREAM("Could not get parameter height");
        }
    }

    // Get user set bounding box size or set default.
    int bbWidth     = DEFAULT_BB_WIDTH;
    int bbHeight    = DEFAULT_BB_HEIGHT;

    ros::param::get("~bb_width", bbWidth);
    ros::param::get("~bb_height", bbHeight);

    ch_.setWidth(bbWidth);
    ch_.setHeight(bbHeight);

    // Get user set tracker type size or set default.
    tracker_type_ = DEFAULT_TRACKER;
    std::string ttStr = "";

    ros::param::get("~tracker_type", ttStr);

    if(ttStr.compare("tracker_gt") == 0) {
        tracker_type_ = TRACKER_GROUND_TRUTH;
    } else if(ttStr.compare("tracker_ols") == 0) {
        tracker_type_ = TRACKER_OLS;
    } else if(ttStr.compare("tracker_tld") == 0) {
        tracker_type_ = TRACKER_TLD;
    }

    // At first subscribe to the overview unit (camera unit 0)
    logSub_     = nh_->subscribe("/rosout", 50, &Gui::onLog, this);
    onImgSub_   = nh_->subscribe(string("/") + camera_unit_ns_ + string("_0/") + IMAGE_TOPIC, 30, &Gui::onImage, this);
    joySub_     = nh_->subscribe(JOY_TOPIC, 1, &Gui::onJoy, this);
    keyDownSub_ = nh_->subscribe(KEYDOWN_TOPIC, 5, &Gui::onKeyDown, this);
    keyUpSub_   = nh_->subscribe(KEYUP_TOPIC,   5, &Gui::onKeyUp,   this);
    targetStateLocalSub_ = nh_->subscribe(TARGET_STATE_LOCAL_TOPIC, 5,  &Gui::onTargetStateLocal, this);
    targetStateSub_      = nh_->subscribe(TARGET_STATE_TOPIC,       10, &Gui::onTargetState,    this);    

    cameraUnitInfoSub_ = new ros::Subscriber[CAMERA_UNITS_COUNT];
    for(int i = 0; i < CAMERA_UNITS_COUNT; ++i) {
        cameraUnitInfoSub_[i] = nh_->subscribe(string("/") + camera_unit_ns_ + string("_") + to_string(i) + string("/") + CAMERA_UNIT_INFO_TOPIC,  5,  &Gui::onCameraUnitInfo, this);
    }

    startDetectionPub_ = nh_->advertise<msgs::StartDetection>(START_DETECTION_TOPIC, 5);
    stopDetectionPub_  = nh_->advertise<msgs::StopDetection>(STOP_DETECTION_TOPIC, 5);
    setTargetObjectPub_  = nh_->advertise<msgs::SetTargetObject>(SET_TARGET_OBJECT_TOPIC, 5);
    selectManipClient_ = nh_->serviceClient<controls::SelectManipulator>(SELECT_MANIP_SRV);

    keyDownPub_ = nh_->advertise<keyboard::Key>(KEYDOWN_TOPIC, 5);
    keyUpPub_   = nh_->advertise<keyboard::Key>(KEYUP_TOPIC, 5);

    // Initialize crosshair
    ch_.x(cameraUnits_[0]->camW_ / 2);
    ch_.y(cameraUnits_[0]->camH_ / 2);

    // Initializace log levels names
    logLevel_[1] = string("DEBUG");
    logLevel_[2] = string("INFO");
    logLevel_[4] = string("WARN");
    logLevel_[8] = string("ERROR");
    logLevel_[16] = string("FATAL");

    // debug
    tgtPos3d_ << 0., 0., 0.;

    cv::namedWindow(NAMED_WINDOW, cv::WINDOW_NORMAL);
    cv::setMouseCallback(NAMED_WINDOW, Gui::onMouse, this);

    startStopDetection_ = new bool[CAMERA_UNITS_COUNT];
    fixedPoints_ = new cv::Point[CAMERA_UNITS_COUNT];
    showFixedPoints_ = new bool[CAMERA_UNITS_COUNT];
    cuInfoInitialized_ = new bool[CAMERA_UNITS_COUNT];

    for(int i = 0; i < CAMERA_UNITS_COUNT; ++i) {
        showFixedPoints_[i] = false;
        startStopDetection_[i] = true;
        cuInfoInitialized_[i] = false;
    }

    mapVis_ = new LocalMapWidget(CAMERA_UNITS_COUNT);

#if GOOGLE_MAPS_VIS
    mapVis_->show();
#endif
}

Gui::~Gui()
{
    for(int i = 0; i < CAMERA_UNITS_COUNT; ++i) {
        delete cameraUnits_[i];        
    }

    if(cameraUnitInfoSub_) delete[] cameraUnitInfoSub_;
    if(startStopDetection_) delete[] startStopDetection_;
    if(fixedPoints_) delete[] fixedPoints_;
    if(showFixedPoints_) delete[] showFixedPoints_;
    if(cuInfoInitialized_) delete[] cuInfoInitialized_;
    if(mapVis_) delete mapVis_;
}

void Gui::update()
{
    // Update size of the bounding box of the crosshair.
    if(dwChBb_) ch_.updateWidth(dwChBb_);
    if(dhChBb_) ch_.updateHeight(dhChBb_);

    // Update google maps visualizer
    if(!checkCuInfoInitialized_) {
        mapVis_->up_pos();
    }

    int c = cv::waitKey(20) & 255;
    bool success = false;

    keyboard::Key keymsg;

    switch(c) {
        // Select manipulator
        case 48: // KEY_0
            switchManipulator(0);
            break;

        case 49: // KEY_1
            switchManipulator(1);
            break;

        case 50: // KEY_2
            switchManipulator(2);
            break;

        case 51: // KEY_3
            switchManipulator(3);
            break;

        // Select tracker
        case 105: // KEY_i
            switchTracker(TRACKER_GROUND_TRUTH);
            break;

        case 121: // KEY_y
            switchTracker(TRACKER_OLS);
            break;

        case 117: // KEY_u
            switchTracker(TRACKER_TLD);
            break;

        // Start detection
        case 10: // KEY_RETURN
            if(startStopDetection_[cameraUnit_]) {
                startStopDetection_[cameraUnit_] = false;

                cv::Point tl;
                tl.x = ch_.x() - (ch_.width() / 2);
                tl.y = ch_.y() - (ch_.height() / 2);
                cv::Point br;
                br.x = ch_.x() + (ch_.width() / 2);
                br.y = ch_.y() + (ch_.height() / 2);

                startDetection(tl, br, Gui::DYNAMIC, tracker_type_);
            } else {
                startStopDetection_[cameraUnit_] = true;
                stopDetection();
            }
            break;

        case 190: // KEY_F1
            setTargetObject(OBJECT_UNKNOWN);
            break;

        case 191: // KEY_F2
            setTargetObject(OBJECT_DRONE);
            break;

        case 192: // KEY_F3
            setTargetObject(OBJECT_AIRCRAFT);
            break;

        case 193: // KEY_F4
            setTargetObject(OBJECT_HUMAN);
            break;

        case 194: // KEY_F5
            setTargetObject(OBJECT_DOG);
            break;

        // bounding box size
        case 184: // KEY_KP8
            ch_.updateHeight(2);
            break;

        case 178: // KEY_KP2
            ch_.updateHeight(-2);
            break;

        case 182: // KEY_KP6
            ch_.updateWidth(2);
            break;

        case 180: // KEY_KP4
            ch_.updateWidth(-2);
            break;

       default:
            break;
    }
}

void Gui::onImage(const sensor_msgs::Image::ConstPtr img)
{
    cv_bridge::CvImagePtr cv_img;
    cv::Mat imgReceived;    

    try {
        cv_img = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);        
        (cv_img->image).copyTo(imgReceived);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }   

    // save the timestamp
    lastFrameStamp_ = img->header.stamp;

    // Draw a crosshair
    ch_.draw(cv_img->image);

    // Draw a bounding box of the tracked object
    cv::Rect bb;

    // text background
    cv::rectangle(cv_img->image, cv::Rect(20, 3, 610, 130), CV_BLACK, CV_FILLED);

    // print 3D object position
    std::ostringstream ss;
    ss << "object position: [" << tgtPos3d_(0) << ", " << tgtPos3d_(1) << ", " << tgtPos3d_(2) << "]";
    cv::putText(cv_img->image, ss.str(), cv::Point(30, 30), cv::FONT_HERSHEY_SIMPLEX, FONT_SIZE, CV_WHITE, FONT_THICKNESS, CV_AA);

    // print target distance
    std::ostringstream ssDist;
    ssDist << "object distance: " << tgtDist_ << " m";
    cv::putText(cv_img->image, ssDist.str(), cv::Point(30, 60), cv::FONT_HERSHEY_SIMPLEX, FONT_SIZE, CV_WHITE, FONT_THICKNESS, CV_AA);

    // print target speed
    std::ostringstream ssSpeed;

    double speed = (std::isnan(tgtSpeed_) ? 0.0 : tgtSpeed_);
    ssSpeed << "target speed: " << speed << " m/s, " << speed * 3.6 << " km/h";
    cv::putText(cv_img->image, ssSpeed.str(), cv::Point(30, 90), cv::FONT_HERSHEY_SIMPLEX, FONT_SIZE, CV_WHITE, FONT_THICKNESS, CV_AA);

    // print target object type
    std::ostringstream ssObject;
    ssObject << "target type: " << targetObjectStr[(int)tgtObject_];
    cv::putText(cv_img->image, ssObject.str(), cv::Point(30, 120), cv::FONT_HERSHEY_SIMPLEX, FONT_SIZE, CV_WHITE, FONT_THICKNESS, CV_AA);

    // print manipualtor position
    std::ostringstream ssManPos;
    ssManPos << "azimuth: " << cameraUnits_[cameraUnit_]->getAzi() << " rad, elevation: " << cameraUnits_[cameraUnit_]->getEle() << " rad";
    cv::putText(cv_img->image, ssManPos.str(), cv::Point(650, 950), cv::FONT_HERSHEY_SIMPLEX, FONT_SIZE, CV_GREEN, FONT_THICKNESS, CV_AA);

    if(showFixedPoints_[cameraUnit_]) {
        cv::circle(cv_img->image, fixedPoints_[cameraUnit_], 2, cv::Scalar(0, 255, 0), 2);
    }

    // print manipualtor position
    std::ostringstream ssTracker;
    ssTracker << "Tracker: ";
    switch (tracker_type_)
    {
        case Gui::TRACKER_GROUND_TRUTH:
            ssTracker << "TRACKER_GROUND_TRUTH";
            break;
        case Gui::TRACKER_OLS:
            ssTracker << "TRACKER_OLS";
            break;
        case Gui::TRACKER_TLD:
            ssTracker << "TRACKER_TLD";
            break;
    }

    cv::putText(cv_img->image, ssTracker.str(), cv::Point(30, 165), cv::FONT_HERSHEY_SIMPLEX, FONT_SIZE, CV_CYAN, FONT_THICKNESS, CV_AA);

    cv::imshow(NAMED_WINDOW, cv_img->image);
}

void Gui::onJoy(const sensor_msgs::Joy::ConstPtr &joy)
{
    static bool buttonDown = false;

    if(!buttonDown) {
        if(joy->buttons[2]) {
            buttonDown = true;
            switchManipulator(0);

        } else if(joy->buttons[1]) {
            buttonDown = true;
            switchManipulator(1);

        } else if(joy->buttons[3]) {
            buttonDown = true;
            switchManipulator(2);

        } else if(joy->buttons[0]) {
            buttonDown = true;
            switchManipulator(3);

        } else if(joy->buttons[8]) {    // start detection
            startStopDetection_[cameraUnit_] = true;
            stopDetection();
        } else if(joy->buttons[9]) {    // stop detection
            if(startStopDetection_[cameraUnit_]) {
                startStopDetection_[cameraUnit_] = false;

                cv::Point tl;
                tl.x = ch_.x() - (ch_.width() / 2);
                tl.y = ch_.y() - (ch_.height() / 2);
                cv::Point br;
                br.x = ch_.x() + (ch_.width() / 2);
                br.y = ch_.y() + (ch_.height() / 2);

                startDetection(tl, br);
            }
        }

    } else {
        buttonDown = false;
    }

    // Update size of the bounding box of the crosshair.
    dwChBb_ = (-joy->axes[2]);
    dhChBb_ = ( joy->axes[3]);
}

void Gui::switchManipulator(int idx)
{
    // Switch displayed video stream
    if(cameraUnit_ != idx) {
        cameraUnit_ = idx;
        onImgSub_ = nh_->subscribe(string("/") + camera_unit_ns_ + string("_") + std::to_string(idx) +
                                   string("/") + IMAGE_TOPIC, 30, &Gui::onImage, this);
    }

    // Call Controls node's service to change manipulator
    controls::SelectManipulator selManSrv;
    selManSrv.request.id = idx;

    if(selectManipClient_.call(selManSrv)) {
        ROS_INFO("Switched to camera unit %d", idx);
    } else {
        ROS_ERROR("Failed to call service select_manipulator");
    }
}


void Gui::switchTracker(ETrackerType tracker_type)
{
    tracker_type_ = tracker_type;
}

void Gui::startDetection(cv::Point &topLeft, cv::Point &bottomRight, ETargetType target_type, ETrackerType tracker_type)
{
    msgs::StartDetection msg;

    msg.targetType = (int8_t)(target_type);
    msg.trackerType = (int8_t)(tracker_type);
    msg.imgTimeStamp = lastFrameStamp_;
    msg.idCU = cameraUnit_;

    geometry_msgs::Point tl;
    geometry_msgs::Point br;

    tl.x = topLeft.x;
    tl.y = topLeft.y;
    br.x = bottomRight.x;
    br.y = bottomRight.y;

    msg.topLeft = tl;
    msg.bottomRight = br;

    startDetectionPub_.publish(msg);
}

void Gui::stopDetection()
{
    msgs::StopDetection msg;

    msg.camera_unit = cameraUnit_;
    msg.id_global = 0;

    stopDetectionPub_.publish(msg);
}

void Gui::setTargetObject(Gui::ETargetObject objType, uint32_t targetId)
{
    msgs::SetTargetObject toMsg;
    msgs::TargetObject to;

    to.targetObject = objType;
    toMsg.id_global     = targetId;
    toMsg.targetObject  = to;

    setTargetObjectPub_.publish(toMsg);
}

void Gui::onTargetStateLocal(const msgs::TargetStateLocalConstPtr stateMsg)
{
    cv::Rect bb(stateMsg->roiTopLeft.x,
                stateMsg->roiTopLeft.y,
                stateMsg->roiBottomRight.x - stateMsg->roiTopLeft.x,
                stateMsg->roiBottomRight.y - stateMsg->roiTopLeft.y);

    if(stateMsg->camera_unit > 0 || stateMsg->camera_unit < 4) {
        cameraUnits_[stateMsg->camera_unit]->setBB(bb, stateMsg->imgTimeStamp);
    }
}

void Gui::onTargetState(const msgs::TargetState::ConstPtr targetState)
{
    // DEBUG - just print 3D coordinates
    tgtPos3d_ << targetState->position.x, targetState->position.y, targetState->position.z;
    tgtDist_ = tgtPos3d_.norm();    
    tgtSpeed_ = targetState->avgSpeed;
    tgtObject_ = (ETargetObject)targetState->targetObject.targetObject;

    // Visualize target in Google maps
    mapVis_->update_target_utm(0, targetState->easting, targetState->northing, targetState->zone);
}

void Gui::onCameraUnitInfo(const msgs::CameraUnitInfo::ConstPtr cuInfo)
{
    int cui = cuInfo->camera_unit;

    cameraUnits_[cui]->setAzi(cuInfo->aziumth);
    cameraUnits_[cui]->setEle(cuInfo->elevation);
    cameraUnits_[cui]->setLatitude(cuInfo->gps.latitude);
    cameraUnits_[cui]->setLongitude(cuInfo->gps.longitude);
    cameraUnits_[cui]->setAltitude(cuInfo->gps.altitude);

    // Update info for Google maps visualizer
    mapVis_->setLatitude(cui,       cuInfo->gps.latitude);
    mapVis_->setLongitude(cui,      cuInfo->gps.longitude);
    mapVis_->setAzimuth(cui,      -(cuInfo->aziumth));
    mapVis_->setFOV(cui,            cuInfo->fov);
    mapVis_->setVisionRange(cui,    2000.0);

    cuInfoInitialized_[cui] = true;

    if(checkCuInfoInitialized_) {
        int i;
        for(i = 0; i < CAMERA_UNITS_COUNT; ++i) {
            if(!cuInfoInitialized_[i]) break;
        }

        if(i == CAMERA_UNITS_COUNT) {
            checkCuInfoInitialized_ = false;
            mapVis_->initialize();
        }
    }
}

void Gui::onKeyDown(const keyboard::Key::ConstPtr key)
{
    // DEBUG - select camer unit by key
    switch(key->code) {
        case keyboard::Key::KEY_0:
            switchManipulator(0);
            break;
        case keyboard::Key::KEY_1:
            switchManipulator(1);
            break;

        case keyboard::Key::KEY_2:
            switchManipulator(2);
            break;

        case keyboard::Key::KEY_3:
            switchManipulator(3);
            break;

        case keyboard::Key::KEY_i:
            switchTracker(TRACKER_GROUND_TRUTH);
            break;

        case keyboard::Key::KEY_y:
            switchTracker(TRACKER_OLS);
            break;

        case keyboard::Key::KEY_u:
            switchTracker(TRACKER_TLD);
            break;

        // move crosshair
        case keyboard::Key::KEY_w:
            break;

        case keyboard::Key::KEY_s:
            break;

        case keyboard::Key::KEY_d:
            break;

        case keyboard::Key::KEY_a:
            break;

        // bounding box size
        case keyboard::Key::KEY_KP8:
            ch_.updateHeight(2);
            break;
        case keyboard::Key::KEY_KP2:
            ch_.updateHeight(-2);
            break;
        case keyboard::Key::KEY_KP6:
            ch_.updateWidth(2);
            break;
        case keyboard::Key::KEY_KP4:
            ch_.updateWidth(-2);
            break;

        // start detection
        case keyboard::Key::KEY_RETURN:
            if(startStopDetection_[cameraUnit_]) {
                startStopDetection_[cameraUnit_] = false;

                cv::Point tl;
                tl.x = ch_.x() - (ch_.width() / 2);
                tl.y = ch_.y() - (ch_.height() / 2);
                cv::Point br;
                br.x = ch_.x() + (ch_.width() / 2);
                br.y = ch_.y() + (ch_.height() / 2);

                startDetection(tl, br, Gui::DYNAMIC, tracker_type_);
            } else {
                startStopDetection_[cameraUnit_] = true;
                stopDetection();
            }
            break;



        default:
            break;
    }
}

void Gui::onKeyUp(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {

        default:
            break;
    }

}

void Gui::onMouse(int event, int x, int y, int, void *userdata)
{
    Gui *gui = reinterpret_cast<Gui *>(userdata);
    gui->onMouse(event, x, y);
}

void Gui::onMouse(int event, int x, int y)
{
    if(event == CV_EVENT_LBUTTONDOWN) {
        cv::Point tl(x - ch_.width() / 2.0, y - ch_.height() / 2.0);
        cv::Point br(x + ch_.width() / 2.0, y + ch_.height() / 2.0);
        startDetection(tl, br, Gui::DYNAMIC, tracker_type_);

        fixedPoints_[cameraUnit_] = cv::Point(x, y);
        showFixedPoints_[cameraUnit_] = false;
    }
}

void Gui::onLog(const rosgraph_msgs::Log::ConstPtr msg)
{
    string level;
}
