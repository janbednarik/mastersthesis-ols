#!/bin/python

import sys
import math

if __name__ == "__main__":
    
    # help
    if(len(sys.argv) != 5):
        print("Computes the angle beta, by which the optical axis is rotated along Z axis with regards to the direction m, in which the manipulator outputs azimuth = 0\n\nUsage:\n\nbeta center_x width cell_size f\n\tcenter_x\t\tMeasured distance between optical axis and target cross [px]\n\twidth\t\t\tWidth of the image [px]\n\tcell_size\t\thorizontal size of the pixel [m]\n\tf\t\t\tfocal size [m]")        
        
        exit(1)
    
    center_x    = int(sys.argv[1])
    width       = int(sys.argv[2])
    cs          = float(sys.argv[3])
    f           = float(sys.argv[4])
    
    # debug
    print "center_x:  ", center_x, "px"
    print "width:     ", width, "px"
    print "cell_size: ", cs, "m"
    print "focus:     ", f, "m"

    # computation
    beta = math.atan((cs * (center_x - width / 2.0)) / f)

    # output
    print
    print "beta = ", beta, "rad"
    print "beta = ", beta * 1000.0, "mrad"
    