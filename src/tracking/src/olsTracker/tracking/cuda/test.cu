//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#include "test.h"
#include "kernel.cu"
#include "kernel_inversion.cu"
#include <iostream>

// CUDA includes
#include <cuda_runtime.h>
// #include <cutil_inline.h>
// #include <cutil_gl_inline.h>
// #include <cutil_gl_error.h>
// #include <cuda_gl_interop.h>
#include <vector_types.h>
//#include "TicTac.h"



// Allocates particles.
void particles_alloc(struct Particles *p, int count)
{
    // Number of particles...
    p->count_ = count;

    // Host memory... - page locked memory allocation.
    cudaMallocHost(&(p->h_x_),  count * sizeof(float));
    cudaMallocHost(&(p->h_y_),   count * sizeof(float));
    cudaMallocHost(&(p->h_width_),  count * sizeof(float));
    cudaMallocHost(&(p->h_height_), count * sizeof(float));
    cudaMallocHost(&(p->h_yaw_),    count * sizeof(float));
    cudaMallocHost(&(p->h_fitness_), count * sizeof(float));

    // Device memory...
    cudaMalloc(&(p->d_x_), count * sizeof(float));
    cudaMalloc(&(p->d_y_), count * sizeof(float));
    cudaMalloc(&(p->d_width_), count * sizeof(float));
    cudaMalloc(&(p->d_height_), count * sizeof(float));
    cudaMalloc(&(p->d_yaw_), count * sizeof(float));
    cudaMalloc(&(p->d_fitness_), count * sizeof(float));
}

// Fill the structure of particles by random values...
void particles_randomize_host(struct Particles *p, int width, int height)
{
    // Randomize host memory w.r.t. the size of the input image.
    for (int i = 0; i < p->count_; i++) {
        p->h_width_[i] = 32.0f;
        p->h_height_[i] = 32.0f;
        p->h_x_[i] = rand() % (width - 64) + 32 ;
        p->h_y_[i] = rand() % (height - 64) + 32;
        p->h_yaw_[i] = 0.0f;
        p->h_fitness_[i] = 0.0f;                
    }
}

// Download to host memory.
void particles_copy_device_to_host(struct Particles *p)
{
    cudaMemcpyAsync(p->h_x_,         p->d_x_,        p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_y_,         p->d_y_,        p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_width_,     p->d_width_,    p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_height_,    p->d_height_,   p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_yaw_,       p->d_yaw_,      p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpyAsync(p->h_fitness_,   p->d_fitness_,  p->count_ * sizeof(float), cudaMemcpyDeviceToHost);
}

// Upload to device memory.
void particles_copy_host_to_device(struct Particles *p)
{
    cudaMemcpyAsync(p->d_x_,         p->h_x_,        p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_y_,         p->h_y_,        p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_width_,     p->h_width_,    p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_height_,    p->h_height_,   p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_yaw_,       p->h_yaw_,      p->count_ * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(p->d_fitness_,   p->h_fitness_,  p->count_ * sizeof(float), cudaMemcpyHostToDevice);
}

void particles_free(struct Particles *p)
{
    cudaFreeHost(p->h_x_);
    cudaFreeHost(p->h_y_);
    cudaFreeHost(p->h_height_);
    cudaFreeHost(p->h_width_);
    cudaFreeHost(p->h_yaw_);
    cudaFreeHost(p->h_fitness_);

    cudaFree(p->d_x_);
    cudaFree(p->d_y_);
    cudaFree(p->d_height_);
    cudaFree(p->d_width_);
    cudaFree(p->d_yaw_);
    cudaFree(p->d_fitness_);
}


struct gpuImg* gpuImg_create_from(unsigned char* data, int step, int width, int height, texture<unsigned char, 2> *tex)
{
    // Create structure...
    struct gpuImg* img = (gpuImg *) malloc(sizeof(struct gpuImg));
    img->height_ = height;
    img->width_ = width;

    // Allocate device memory and copy data.
    cudaMallocPitch(&(img->d_data_), &(img->pitch_), width, height);
    cudaMemcpy2D(img->d_data_, img->pitch_, data, step, width, height, cudaMemcpyHostToDevice);

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();
    cudaBindTexture2D(NULL, *tex,  img->d_data_, channelDesc, img->height_, img->width_, img->pitch_);


    return img;
}

// CPU implementation...
void evaluateParticles_HOST(unsigned char *img_data, size_t img_pitch, unsigned char * pattern_data, size_t pattern_pitch, struct Particles *p)
{
    for (int i = 0; i < p->count_; i++) {       // Over all particles...

        float sum_diff = 0.0;
        // Calculate SAD.
        for (int y = 0; y < 32; y++) {
            for (int x = 0; x < 32; x++) {
                // std::cout << " y " <<  p->h_y_[i] <<  "  " <<  ((int) p->h_y_[i] + y) * img_pitch << std::endl;
                // std::cout << "data 1 " << img_data[0] << std::endl;
                // std::cout << " data " << img_data[((int) p->h_y_[i] + y) * img_pitch ] << std::endl;
                // std::cout << " ok " << std::endl;
                sum_diff += (float) abs(img_data[((int) p->h_y_[i] + y) * img_pitch + (int) p->h_x_[i] + x] - pattern_data[x + y * pattern_pitch]);
                /*
                for (int i = 0; i < 10; i++) {
                    sum_diff += sum_diff;

                }
                */
                //sum_diff += pattern_data[x + y * pattern_pitch];
            }
        }

        // std::cout << "Sum diff " << sum_diff << std::endl;
        // std::cout << "GPU computed: " <<  p->h_fitness_[i] << std::endl;
        if (p->h_fitness_[i] == sum_diff) {
            ; //std::cout << "Particle " << i << ": pass" << std::endl;
        }
        else
            std::cout << "Particle " << i << ": not pass with error " << abs(p->h_fitness_[i] - sum_diff) << std::endl;


    }

}

void test_particles(unsigned char *img_data, int img_step, int img_width, int img_height,
                    unsigned char *pattern_data, int pattern_step, int pattern_width, int pattern_height,
                    int n_particles)
{


    texture<unsigned char, 2> tex_;

    // TIME;
    cudaEvent_t start, stop;
    float time;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord( start, 0 );


    struct gpuImg* img = gpuImg_create_from(img_data, img_step, img_width, img_height, &t_img);
    struct gpuImg* pattern = gpuImg_create_from(pattern_data, pattern_step, pattern_width, pattern_height, &t_pattern);

    // cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);


    // C
    // cudaMemcpyToSymbol("const_pattern", pattern_data, 32 * 32 * sizeof(unsigned char));


    // Create images.

    // Allocate particles.
    struct Particles *p;
    cudaMallocHost(&p, sizeof(struct Particles));
    particles_alloc(p, n_particles);
    particles_randomize_host(p, img->width_, img->height_);

    // Copy paticles to memory...
    particles_copy_host_to_device(p);

    // Compute fitness values.
    dim3 blocks(1, 1, 1);
    dim3 threads(1, 1, 1);

    // Compute fitness values.
    /*
    threads.x = 32; threads.y = 1;
    blocks.x = p->count_;
    blocks.y = 32;
    */


    threads.x = 16; threads.y = 16;
    blocks.x = (p->count_ / 16) * 2;
    blocks.y = 32;
/*
    threads.x = 1; threads.y = 32;
    blocks.x = p->count_ / 32;
    blocks.y = 32;
    */



    // Version next...
            /*
    threads.x = 8; threads.y = 8;
    blocks.x = p->count_; blocks.y = 1;
    */


    // Allocate temp. buffer for fitness values.
    float *buff;
    size_t buff_pitch = 0;
    cudaMallocPitch(&buff, &buff_pitch, 32 * p->count_ * sizeof(float), 32 * sizeof(float));

    // Evalute particles.
    evaluateParticles <<< blocks, threads >>> (img->d_data_, img->pitch_, pattern->d_data_, pattern->pitch_, p->d_x_, p->d_y_, buff, buff_pitch);


    // Compute fitness values.
    threads.x = 32; threads.y = 1;
    blocks.x = p->count_; blocks.y = 32;
    computeFitness1 <<< blocks, threads >>> (p->d_fitness_, buff, buff_pitch);

    // Change view of data before final computation.
    threads.x = 32; threads.y = 1;
    blocks.x = p->count_ / 32; blocks.y = 1;
    computeFitness2 <<< blocks, threads >>> (p->d_fitness_, buff, buff_pitch);

    // Copy back.
    particles_copy_device_to_host(p);

    // Time
    cudaEventRecord( stop, 0 );
    cudaEventSynchronize( stop );

    cudaEventElapsedTime( &time, start, stop );
    cudaEventDestroy( start );
    cudaEventDestroy( stop );

    std::cout << "Elapsed time in ms: " << time << "."<< std::endl;



    // TIME;

    //TicTac t;
    //t.tic();
    // Control results...
    //evaluateParticles_HOST(img_data, img_step, pattern_data, pattern_step, p);



//    std::cout << "Elapsed time in ms: " << t.tac() * 1000.0 << "."<< std::endl;

    particles_free(p);
    cudaFreeHost(p);
    cudaFree(buff);
}





void foo(char *data, int step, int width, int height)
{
    // std::cout << "Size of input data: " << size << std::endl;

    // int* hostArray;
    // int* deviceArray;
    // const int arrayLength = 16;
    // const unsigned int memSize = sizeof(int) * arrayLength;

    // TIME;
    cudaEvent_t start, stop;
    float time;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord( start, 0 );

    // Allocate memory in device.
    unsigned char *input_img;
    size_t pitch;
    cudaMallocPitch(&input_img, &pitch, width, height);

    // Load image to the device memory.
    cudaMemcpy2D(input_img, pitch, data, step, width , height, cudaMemcpyHostToDevice);

    // Computation... (my kernel) - inversion
    dim3 blocks(1, 1, 1);
    dim3 threads(1, 1, 1);
    threads.x = 32; threads.y = 1;
    blocks.x = width / threads.x;
    blocks.y = 32;

    inversion <<< blocks, threads >>> (input_img, width);

    // Load back...
    cudaMemcpy2D(data, step, input_img, pitch, width, height, cudaMemcpyDeviceToHost);

    // Free memory.
    cudaFree(input_img);

    // Time
    cudaEventRecord( stop, 0 );
    cudaEventSynchronize( stop );

    cudaEventElapsedTime( &time, start, stop );
    cudaEventDestroy( start );
    cudaEventDestroy( stop );

    std::cout << "Elapsed time in ms: " << time << "."<< std::endl;


    /*
    hostArray = (int*)malloc(memSize);
    cudaMalloc((void**) &deviceArray, memSize);

    std::cout << "Before device\n";
    for(int i=0;i<arrayLength;i++)
    {
        hostArray[i] = i+1;
        std::cout << hostArray[i] << "\n";
    }
    std::cout << "\n";

    // Time
    cudaEvent_t start, stop;
    float time;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord( start, 0 );


    cudaMemcpy(deviceArray, hostArray, memSize, cudaMemcpyHostToDevice);
    TestDeviceAA <<< 4, 4 >>> (deviceArray);
    cudaMemcpy(hostArray, deviceArray, memSize, cudaMemcpyDeviceToHost);


    // Time
    cudaEventRecord( stop, 0 );
    cudaEventSynchronize( stop );

    cudaEventElapsedTime( &time, start, stop );
    cudaEventDestroy( start );
    cudaEventDestroy( stop );

    std::cout << "Elapsed time in ms: " << time << "."<< std::endl;


    std::cout << "After device\n";
    for(int i=0;i<arrayLength;i++)
    {
        std::cout << hostArray[i] << "\n";
    }

    cudaFree(deviceArray);
    free(hostArray);
    */

    std::cout << "Done\n";
}
