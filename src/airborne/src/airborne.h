/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef AIRBORNE_H
#define AIRBORNE_H

// ROS libraries
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <keyboard/Key.h>
#include <dynamic_reconfigure/server.h>

// Project libraries
#include "motion.h"
#include <airborne/AirborneConfig.h>

//! \brief The Airborne class The simulator of the motion of the airborne object in 3D. The class
//! publishes current position of the airborne object as "/gazebo/set_model_state" for gazebo and
//! "pose" for other ROS nodes.
//!
class Airborne
{
public:
    static const std::string MODEL_STATE_TOPIC;
    static const std::string POSE_TOPIC;

    static const double MAX_X;
    static const double MAX_Y;
    static const double MAX_Z;
    static const double MAX_ROLL;
    static const double MAX_PITCH;
    static const double MAX_YAW;
    static const double MIN_X;
    static const double MIN_Y;
    static const double MIN_Z;
    static const double MIN_ROLL;
    static const double MIN_PITCH;
    static const double MIN_YAW;

    enum EMotionType {
        MOTION_STATIC,
        MOTION_LINE_SEGMENT,
        MOTION_ARC_XY
    };

public:
    //! \brief Airborne Constructor
    //!
    Airborne(double simTimeStep);

    //! \brief ~Airborne Destructor
    //!
    ~Airborne();


    //! \brief update Updates the state of the airborne object.
    //!
    void update();

private:
    // ROS communication
    ros::NodeHandle *nh_;
    ros::Publisher modelStatePub_;
    ros::Publisher posePub_;        //!< Redundant, only used since in 'detecion' node the time synchronizer does not work with gazebo_msgs.
    tf::TransformBroadcaster tfBcast_;

    // Dynamic reconfigure
    dynamic_reconfigure::Server<airborne::AirborneConfig> server;

    // Dynamic parameters
    bool haveNewConfig_;    //!< Flag - config change occured
    std::string cfgFile_;
    bool firstTimeDynamicReconfigure_;  //!< Dynamic reconfigure is called during initialization but it overwrites the set values from roslaunch.

    std::string worldFrameId_; //!< Name of the world frame.
    std::string tfPrefix_;
    std::string flyingObjectNS_;
    std::string modelName_; //!< Model name.
    double simTimeStep_;    //!< Time step of the simulation [s].
    double simTimeT_;       //!< Current simulation time whithin one period T.

    // position of the model
    double x_;
    double y_;
    double z_;

    // rotation of the model
    double roll_;
    double pitch_;
    double yaw_;

    double motionPeriod_;         //!< The period of motion (in case of periodic motion) [s].

    // Motion properties
    EMotionType motionType_;
    Eigen::Vector3d from_;
    Eigen::Vector3d dir_;
    double distance_;
    double maxAngle_;
    double radius_;
    double speed_;
    double acceleration_;

    Motion* motion_;              //!< Computation of the motion of the object.

    // Gazebo simulation
    bool gazeboSimulation_; //! If set the Gazebo simulation is in progress.

private:
    //! \brief callback Dynamic recongigure callback
    //! \param config
    //! \param level
    //!
    void onDynamicReconfigure(airborne::AirborneConfig &config, uint32_t level);
};

#endif // #define AIRBORNE_H
