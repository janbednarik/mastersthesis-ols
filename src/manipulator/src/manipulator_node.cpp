/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS
#include <ros/ros.h>

// C++
#include <iostream>
#include <cctype>
#include <algorithm>

// project libraries
#include "manipulator.h"
#include "manipulator_oprox.h"
#include "manipulator_ptu-d46-70.h"
#include "manipulator_sim.h"
#include "exceptions_ols.h"

using namespace std;

const int SPIN_RATE = 50;

const string PORT = "/dev/ttyUSB0";
const char CR = 0x0d;
const char LF = 0x0a;

const string MANIP_OPROX = "oprox";
const string MANIP_FLIR  = "ptud4670";
const string MANIP_SIM   = "sim";

int main(int argc, char *argv[])
{
    int returnValue = 0;
    string argManip;

    // Process parameters
    if(argc < 2) {
        cerr << "No manipulator specifed, falling back to default option [Oprox]." << endl;
        argManip = MANIP_OPROX;
    } else {
        argManip = argv[1];
        transform(argManip.begin(), argManip.end(), argManip.begin(), ::tolower);
    }


    ros::init(argc, argv, "manipulator");
    Manipulator *manipulator;

    try {
        if(argManip == MANIP_FLIR) {
            // debug
            cout << "Creating manipulator PTU-D46-70" << endl;

            manipulator = new ManipulatorPtuD4670();
        } else if(argManip == MANIP_OPROX) {
            // debug
            cout << "Creating manipulator Oprox" << endl;

            manipulator = new ManipulatorOprox();
        } else if(argManip == MANIP_SIM) {
            manipulator = new ManipulatorSim(1.0 / SPIN_RATE);
        } else {
            cerr << "Wrong manipulator name '" << argManip << "'. Allowed options are: " << MANIP_OPROX << " | " << MANIP_FLIR << ". Falling back to default option [oprox]" << endl;
        }
    } catch (ExceptionOLS& e) {
        cerr << e.what() << endl;
        exit(1);
    } catch (ExceptionOLS_MaxAttemptsReadCorrectMsg& e) {
        cerr << e.what() << endl;
        exit(1);
    }
    catch (exception& e) {
        cerr << e.what();
        exit(1);
    }

    ros::start();        

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {        
        try {
            ros::spinOnce();
            manipulator->update();
        } catch (ExceptionOLS_InitializationFailed &e) {
            cerr << "ExceptionOLS_InitializationFailed: " << e.what() << endl << " Exiting program with error code 1." << endl;
            returnValue = 1;
            break;
        } catch (ExceptionOLS_MaxAttemptsReadCorrectMsg &e) {
            cerr << "ExceptionOLS_MaxAttemptsReadCorrectMsg: " << e.what() << endl << " Exiting program with error code 1." << endl;
            returnValue = 1;
            break;
        } catch (ExceptionOLS &e) {
            cerr << "ExceptionOLS: " << e.what() << endl;
        }
        catch (exception &e) {
            cerr << "Manipulator: Exception: " << e.what() << endl;
        }
        looper.sleep();
    }

    delete manipulator;   
    ros::shutdown();  

    return returnValue;
}

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

//#include <termios.h>    // POSIX terminal control definitions
//#include <errno.h>      // Error number definitions
//#include <fcntl.h>      // File control definitions
//#include <unistd.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <stdio.h>

//int
//set_interface_attribs (int fd, int speed, int parity)
//{
//    struct termios tty;
//    memset (&tty, 0, sizeof tty);
//    if (tcgetattr (fd, &tty) != 0)
//    {
//        cerr << "error" << errno << "from tcgetattr";
//        return -1;
//    }

//    cfsetospeed (&tty, speed);
//    cfsetispeed (&tty, speed);

//    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
//    // disable IGNBRK for mismatched speed tests; otherwise receive break
//    // as \000 chars
//    tty.c_iflag &= ~IGNBRK;         // disable break processing
//    tty.c_lflag = 0;                // no signaling chars, no echo,
//    // no canonical processing
//    tty.c_oflag = 0;                // no remapping, no delays
//    tty.c_cc[VMIN]  = 0;            // read doesn't block
//    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

//    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

//    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
//    // enable reading
//    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
//    tty.c_cflag |= parity;
//    tty.c_cflag &= ~CSTOPB;
//    tty.c_cflag &= ~CRTSCTS;

//    if (tcsetattr (fd, TCSANOW, &tty) != 0)
//    {
//        //                error_message ("error %d from tcsetattr", errno);
//        return -1;
//    }
//    return 0;
//}

//void
//set_blocking (int fd, int should_block)
//{
//    struct termios tty;
//    memset (&tty, 0, sizeof tty);
//    if (tcgetattr (fd, &tty) != 0)
//    {
//        //                error_message ("error %d from tggetattr", errno);
//        return;
//    }

//    tty.c_cc[VMIN]  = should_block ? 1 : 0;
//    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

//    if (tcsetattr (fd, TCSANOW, &tty) != 0) {
//        //                error_message ("error %d setting term attributes", errno);
//    }
//}

//#define BAUDRATE B115200
//#define MODEMDEVICE "/dev/ttyUSB0"
//#define _POSIX_SOURCE 1 /* POSIX compliant source */
//#define FALSE 0
//#define TRUE 1

//volatile int STOP=FALSE;


// old code for communication using serial interface RS 232

//    int fd, c, res;
//    struct termios oldtio, newtio;
//    char buf[255];

//    fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY );
//    if (fd <0) {perror(MODEMDEVICE); exit(-1); }

//    tcgetattr(fd,&oldtio); /* save current port settings */

//    bzero(&newtio, sizeof(newtio));
//    newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
//    newtio.c_iflag = IGNPAR;
//    newtio.c_oflag = 0;

//    /* set input mode (non-canonical, no echo,...) */
//    newtio.c_lflag = 0;

//    newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
//    newtio.c_cc[VMIN]     = 20;   /* blocking read until 5 chars received */

//    tcflush(fd, TCIFLUSH);
//    tcsetattr(fd,TCSANOW,&newtio);


//    while (STOP==FALSE) {       /* loop for input */
//        res = read(fd,buf,255);   /* returns after 5 chars have been input */
//        buf[res]=0;               /* so we can printf... */
//        printf(":%s:%d\n", buf, res);
//        if (buf[0]=='0') STOP=TRUE;
//    }
//    tcsetattr(fd,TCSANOW,&oldtio);

////
//    char *portname = "/dev/ttyUSB0";
//    int fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
//    if (fd < 0)
//    {
////            error_message ("error %d opening %s: %s", errno, portname, strerror (errno));
//            return 1;
//    }

//    set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
//    set_blocking (fd, 0);                // set no blocking

//    write(fd, "7,0003,0001,FF,\r\n", 16);

//    usleep((16 + 20) * 100);             // sleep enough to transmit the 7 plus
//                                         // receive 25:  approx 100 uS per char transmit
//    char buf [100];
//    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read

//    cout << "bytes read: " << n << endl;
//    cout << "message: " << buf << endl;
//    for(int i = 0; i < 20; ++i) {
//        cout << (int)buf[i] << ", ";
//    }
//    cout << endl;

///////////

//    // open port
//    int USB = open( "/dev/ttyUSB0", O_RDWR| O_NOCTTY );

//    // set parameters
//    struct termios tty;
//    struct termios tty_old;
//    memset (&tty, 0, sizeof tty);

//    /* Error Handling */
//    if ( tcgetattr ( USB, &tty ) != 0 ) {
//       std::cout << "Error " << errno << " from tcgetattr: " << strerror(errno) << std::endl;
//    }

//    /* Save old tty parameters */
//    tty_old = tty;

//    /* Set Baud Rate */
//    cfsetospeed (&tty, (speed_t)B115200);
//    cfsetispeed (&tty, (speed_t)B115200);

//    /* Setting other Port Stuff */
//    tty.c_cflag     &=  ~PARENB;            // Make 8n1
//    tty.c_cflag     &=  ~CSTOPB;
//    tty.c_cflag     &=  ~CSIZE;
//    tty.c_cflag     |=  CS8;

//    tty.c_cflag     &=  ~CRTSCTS;           // no flow control
//    tty.c_cc[VMIN]   =  1;                  // read doesn't block
//    tty.c_cc[VTIME]  =  5;                  // 0.5 seconds read timeout
//    tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines

//    /* Make raw */
//    cfmakeraw(&tty);

//    /* Flush Port, then applies attributes */
//    tcflush( USB, TCIFLUSH );
//    if ( tcsetattr ( USB, TCSANOW, &tty ) != 0) {
//       std::cout << "Error " << errno << " from tcsetattr" << std::endl;
//    }

//    // write
//    unsigned char cmd[] = "7,0003,0001,FF,\r\n";
//    int n_written = 0,
//        spot = 0;

//    do {
//        n_written = write( USB, &cmd[spot], 1 );
//        spot += n_written;
//    } while (cmd[spot-1] != '\n' && n_written > 0);

//    // read
//    int n = 0,
//        spot2 = 0;
//    char buf = '\0';

//    /* Whole response*/
//    char response[1024];
//    memset(response, '\0', sizeof response);

//    do {
//        n = read( USB, &buf, 1 );
//        sprintf( &response[spot2], "%c", buf );
//        spot2 += n;
//    } while( buf != '\n' && n > 0 && spot2 < 20);

//    if (n < 0) {
//        std::cout << "Error reading: " << strerror(errno) << std::endl;
//    }
//    else if (n == 0) {
//        std::cout << "Read nothing!" << std::endl;
//    }
//    else {
//        std::cout << "Read bytes: " << spot2 << endl;
//        std::cout << "Response: " << response << std::endl;
//        for(int i = 0; i < 20; ++i) {
//            std::cout << (int)response[i] << ", ";
//        }
//        std::cout << std::endl;
//    }

/////////////////
