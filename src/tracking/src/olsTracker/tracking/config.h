//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>

struct CSettings
{
    int32_t hist_capacity = 25;
    int32_t max_number_overlapped_in_sequence = 50;

    // Init settings for a new target.
    int32_t t_total_num_particles = 1024;
    int32_t t_num_particles_from_movement = 900;

    bool t_use_target_mask = true;
    bool use_particle_median = false;
    bool use_homography = false;

    bool t_logprob = true;
    float t_std_x = 5.0;
    float t_std_y = 5.0;
    float t_std_vx = 0.0;
    float t_std_vy = 0.0;
    float t_std_width = 0.01;
    float t_std_height = 0.01;
    float t_std_angle = 0.0;

    int rWidth = 1280;
    int rHeight = 960;

    double penalty_threshold = 1000.0;
    uint32_t max_num_of_unestimated_transformation_in_sequence = 10;

    float gamma = 10;
    float beta = 0.05;
    float alpha = 10;
    float kappa = 5.02;
    float delta = 0.00025;
};

#endif // CONFIG_H
