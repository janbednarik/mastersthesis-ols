/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef CONTROLS_H
#define CONTROLS_H

// ROS libraries
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <keyboard/Key.h>
#include <tf/transform_listener.h>

// C++
#include <unordered_map>

// Project libraries
#include <controls/SelectManipulator.h>
#include <msgs/ManipulatorMove.h>
#include <msgs/StartDetection.h>
#include <msgs/NewTarget.h>
#include <msgs/TargetState.h>
#include <msgs/NewTargetID.h>
#include <msgs/StopDetection.h>
#include <msgs/SetTargetObject.h>
#include "target.h"

// debug
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

//! \brief The Controls class The main node of the OLS which conrols the operation
//! of other nodes. It keeps the information about camera units and tracked targets,
//! distributes the global target ids and manages manipulator selection.
//!
class Controls
{
public:
    int CAMERA_UNITS_COUNT;

    static const std::string JOY_TOPIC;
    static const std::string KEYDOWN_TOPIC;
    static const std::string KEYUP_TOPIC;
    static const std::string MANIP_MOVE_CMD_TOPIC;
    static const std::string START_DETECTION_TOPIC;
    static const std::string STOP_DETECTION_TOPIC;
    static const std::string NEW_TARGET_TOPIC;
    static const std::string NEW_TARGET_ID_TOPIC;
    static const std::string SELECT_MANIP_SRV;
    static const std::string TARGET_STATE_TOPIC;
    static const std::string SET_TARGET_OBJECT_TOPIC;

    //! \brief The eManipulators enum Each station is assigned unique index.
    //!
    enum eManipulators {
        OVERVIEW_UNIT =     0,
        TRACKING_UNIT_1 =   1,
        TRACKING_UNIT_2 =   2,
        TRACKING_UNIT_3 =   3
    };

    //! \brief The eManipControlMode enum The modes of controlling the motion of the manipulator.
    //!
    enum eManipControlMode {
        MANIP_DISABLED,
        MANIP_TRACKER,
        MANIP_JOYSTICK,
        MANIP_KEYBOARD,
        MANIP_NETWORK,
        MANIP_GUI
    };

public:
    //! \brief Controls constructor
    Controls();

    //! \brief ~Controls destructor
    ~Controls();




private:
    eManipControlMode manipControlMode_;
    std::string cameraUnitNS_;

    // ROS structures.
    ros::NodeHandle *nh_;                               //!< Node handle.

    ros::Subscriber joySub_;                            //!< Joystick event subscription.
    ros::Subscriber keyDownSub_;                        //!< Keyboard key pressed event subscription.
    ros::Subscriber keyUpSub_;                          //!< Keyboard key released event subscription.
    ros::Subscriber manipMoveCmdSub_;                   //!< Subscriber of the motion events addressed to the manipulator.
    ros::Subscriber startDetectionSub_;                 //!< Subscriber, start detection message from 'gui' node
    ros::Subscriber targetStateSub_;
    ros::Subscriber stopDetectionSub_;
    ros::Subscriber newTargetSub_;          //!< NewTarget message (either from 'detection', 'gui' node or from network)
    ros::Subscriber setTargetObjectSub_;    //!< SetTargetObject message
    ros::Publisher  *manipJoyPub_;          //!< Publishers of the joystick events. Each publisher manages communication with deifferent unit.
    ros::Publisher  *manipKeyDownPub_;      //!< Publishers of the keyboard events. Each publisher manages communication with deifferent unit.
    ros::Publisher  *manipKeyUpPub_;        //!< Publishers of the keyboard events. Each publisher manages communication with deifferent unit.
    ros::Publisher  *manipMoveCmdPub_;      //!< Publishers of the motion events addressed to the manipulator
    ros::Publisher  *startDetectionPub_;    //!< Publisher of the StartDetection message, relay from 'gui' to 'detection'
    ros::Publisher  *stopDetectionPub_;     //!< Publisher of the StopDetection message, relay from 'gui' to 'detection'
    ros::Publisher  *newTargetIdPub_;       //!< Publishers of the new target unique ID.
    ros::Publisher   setTargetObjectPub_;   //!< Publishers of the target object type.

    ros::ServiceServer selectManipSrv_;                 //!< Service SelectManipulator for choosing the manipulator for manual control.
    ros::ServiceServer newTargetIdSrv_;                 //!< Service NewTargetId generating system-wide unique ID for a new target

    tf::TransformListener tfListener_;

    eManipulators selectedManipIdx_;                //!< Index of the manipulator which is allowed to accept joystick motion control commands.

    std::unordered_map<int, Target *> targets_;       //!< All targets currently being tracked by the system

private:
    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    void onJoy(const sensor_msgs::Joy::ConstPtr& joy);

    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    void onKeyDown(const keyboard::Key::ConstPtr key);

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    void onKeyUp(const keyboard::Key::ConstPtr key);

    //! \brief onManipMoveCmd Callback for the motion command addressed to the manipulator.
    //! \param cmd
    //!
    void onManipMoveCmd(const msgs::ManipulatorMove cmd);

    //! \brief selectManipulator Service SelectManipulator callback
    //! \param req
    //! \param res
    //! \return
    //!
    bool selectManipulator(controls::SelectManipulator::Request &req, controls::SelectManipulator::Response &res);

    //! \brief startDetection Callback to receiving the start detection command
    //! from 'gui' and resending the message to the given 'detection' node
    //! \param msg
    //!
    void onStartDetection(const msgs::StartDetection msg);

    //! \brief onStopDetection
    //! \param msg
    //!
    void onStopDetection(const msgs::StopDetection::ConstPtr msg);

    //! \brief newTarget NewTarget messages aggregator. Creates a new target and
    //! commands the appropriate camera unit(s) to find it.
    //! \param detectorID
    //! \param target
    //!
    void onNewTarget(const msgs::NewTarget::ConstPtr target);

    void onTargetState(const msgs::TargetState::ConstPtr targetState);

    void onSetTargetObject(const msgs::SetTargetObject::ConstPtr stoMsg);
};

#endif // #ifndef CONTROLS_H
