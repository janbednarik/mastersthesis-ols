/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS
#include <ros/ros.h>

// Project libraries
#include "tracking.h"

using namespace std;

const int SPIN_RATE = 15;

int main(int argc, char *argv[])
{
    int returnValue = 0;

    ros::init(argc, argv, "detection_and_classification");
    ros::start();

    Tracking tracking;

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        try {
            ros::spinOnce();
            tracking.update();
        } catch (exception &e) {
            cerr << "Detection: Exception: " << e.what() << endl;
        }
        looper.sleep();
    }

    ros::shutdown();
    return returnValue;
}
