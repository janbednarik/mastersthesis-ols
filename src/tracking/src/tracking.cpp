/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "tracking.h"
#include <msgs/NewTarget.h>

using namespace std;

const string Tracking::IMAGE_RAW_TOPIC             = "camera/image_raw";
const string Tracking::IMAGE_PROCESSED_TOPIC       = "processed_image";
const string Tracking::START_DETECTION_TOPIC       = "start_detection";
const string Tracking::STOP_DETECTION_TOPIC        = "stop_detection";
const string Tracking::NEW_TARGET_ID_TOPIC         = "new_target_id";
const string Tracking::NEW_TARGET_TOPIC            = "new_target";
const string Tracking::TARGET_STATE_LOCAL_TOPIC    = "target_state_local";
const string Tracking::CAMERA_UNIT_INFO_TOPIC      = "camera_unit_info";
const string Tracking::REGULATE_TOPIC              = "regulation";
const string Tracking::SET_TARGET_OBJECT_TOPIC     = "set_target_object";
const std::string Tracking::ENV_MODEL_TOPIC        = "env_model";

#if GROUND_TRUTH_TRACKER
const string Tracking::POSE_TOPIC      = "pose";
const double Tracking::GT_TRAKER_MU    = 1.0;
const double Tracking::GT_TRAKER_SIGMA = 1.0;
const double Tracking::PX_NOISE_MU     = 0.0;
const double Tracking::PX_NOISE_SIGMA  = 30.0;
#endif

const double Tracking::DEF_CAMERA_FPS = 30.0;

double getTime(void)
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL) == -1) {
        cerr << "getTime: gettimeofday returned -1" << endl;
        exit(-2);
    }

    return (double)tv.tv_sec + (double)tv.tv_usec/1000000.;
}

Tracking::Tracking() :
    idxBI_(0), haveNewFrame_(false), manipInMotion_(false), tracker_(nullptr), ols_tracker_running_(false),
    trackingSuccessful_(false), haveEnvModel_(false), currentTracker_(TRACKER_NO_TRACKER)
{
    nh_ = new ros::NodeHandle();

    // Get the parameters from the parameter server.
    if (!ros::param::get("unit_idx", unit_idx_)) {
        ROS_FATAL_STREAM("Could not get parameter unit_idx");
        exit(1);
    }

    if (!ros::param::get("tf_prefix", tf_prefix_)) {
        ROS_FATAL_STREAM("Could not get parameter tf_prefix");
        exit(1);
    }

    if (!ros::param::get("/master_ns", master_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter master_ns");
        exit(1);
    }

    if (!ros::param::get("camera/width", screenWidth_)) {
        ROS_FATAL_STREAM("Could not get parameter pixel_width.");
        exit(1);
    }

    if (!ros::param::get("camera/height", screenHeight_)) {
        ROS_FATAL_STREAM("Could not get parameter pixel_height.");
        exit(1);
    }   

    if (!ros::param::get("camera/pixel_width", pixelWidth_)) {
        ROS_FATAL_STREAM("Could not get parameter pixel_width.");
        exit(1);
    }

    if (!ros::param::get("camera/pixel_height", pixelHeight_)) {
        ROS_FATAL_STREAM("Could not get parameter pixel_height.");
        exit(1);
    }   

    if (!ros::param::get("camera/acquisition_rate", fps_)) {
        ROS_WARN_STREAM("Could not get parameter fps, using default value = " << DEF_CAMERA_FPS << ".");
        fps_ = DEF_CAMERA_FPS;
    }

    std::string envModelFile = "";
    if (!ros::param::get("~env_model_file", envModelFile)) {
        ROS_WARN_STREAM("Could not get parameter env_model_file, no model is importd.");
    }

#if GROUND_TRUTH_TRACKER
    if (!ros::param::get("~start_gt_tracker", startGTTrackerFlag_)) {
        startGTTrackerFlag_ = false;
    }
#endif

    if(envModelFile.size() != 0) {
        envModel_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
        if (pcl::io::loadPCDFile<pcl::PointXYZ>(envModelFile, *envModel_) == -1) {
            ROS_ERROR_STREAM("Couldn't read file" << envModelFile << ". No 3D model will be used.");
            haveEnvModel_ = false;
        } else {
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcColor(new pcl::PointCloud<pcl::PointXYZRGB>);
            for(pcl::PointCloud<pcl::PointXYZ>::iterator it = envModel_->begin(); it != envModel_->end(); ++it) {
                pcl::PointXYZRGB p(0, 0, 255);
                p.x = it->x;
                p.y = it->y;
                p.z = it->z;

                pcColor->push_back(p);
            }

            pcl::toROSMsg(*pcColor, envModelMsg_);
            envModelMsg_.header.frame_id = "world";
            haveEnvModel_ = true;
        }
    }

    onImgSub_               = nh_->subscribe(IMAGE_RAW_TOPIC, 1, &Tracking::onImage, this);
    startDetectionSub_      = nh_->subscribe(string("/") + master_ns_ + string("/") + START_DETECTION_TOPIC + string("_") + unit_idx_, 5, &Tracking::onStartDetection, this);
    stopDetectionSub_       = nh_->subscribe(string("/") + master_ns_ + string("/") + STOP_DETECTION_TOPIC + string("_") + unit_idx_, 5, &Tracking::onStopDetection, this);
    newTargetIdSub_         = nh_->subscribe(string("/") + master_ns_ + string("/") + NEW_TARGET_ID_TOPIC + string("_")   + unit_idx_, 5, &Tracking::onNewTargetId, this);
    cameraUnitInfoSub_      = nh_->subscribe(CAMERA_UNIT_INFO_TOPIC, 1, &Tracking::onCameraUnitInfo, this);
    setTargetObjectSub_     = nh_->subscribe(string("/") + master_ns_ + string("/") + SET_TARGET_OBJECT_TOPIC, 5, &Tracking::onSetTargetObject, this);

    newTargetPub_           = nh_->advertise<msgs::NewTarget>(string("/") + master_ns_ + string("/") + NEW_TARGET_TOPIC, 1);
    localTargetStatePub_    = nh_->advertise<msgs::TargetStateLocal>(string("/") + master_ns_ + string("/") + TARGET_STATE_LOCAL_TOPIC, 1);
    regulatePub_            = nh_->advertise<msgs::Regulate>(REGULATE_TOPIC, 1);
    imageProcessedPub_      = nh_->advertise<sensor_msgs::Image>(IMAGE_PROCESSED_TOPIC, 1);
    pcPub_                  = nh_->advertise<sensor_msgs::PointCloud2>(ENV_MODEL_TOPIC, 10);

    tfCamera_  = string("/") + tf_prefix_ + string("/") + string("camera");
    tfFocus_   = string("/") + tf_prefix_ + string("/") + string("focus");

    frameCenter_ = cv::Point(screenWidth_ / 2, screenHeight_ / 2);

    // Maximum allowed time difference between ours and received image (= half of the frame rate period).
    maxImgTimeDiff_ = 1.0 / (2.0 * fps_);

    tfROS2screen_ << 0., -1.,  0.,
                     0.,  0., -1.,
                     1.,  0.,  0.;

    tfCamera2Screen_ << -1.0,  0.0, screenWidth_  / 2.0,
                         0.0, -1.0, screenHeight_ / 2.0,
                         0.0,  0.0, 1.0;

#if GROUND_TRUTH_TRACKER
    if (!ros::param::get("/flying_object_ns", flying_object_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter /flying_object_ns");
        exit(1);
    }

    flyingObjectPoseSyncSub_.subscribe(*nh_, string("/") + flying_object_ns_ + string("/") + POSE_TOPIC, 10);
    cameraUnitInfoSyncSub_.subscribe(*nh_, CAMERA_UNIT_INFO_TOPIC, 10);

    timeSync_ = new message_filters::Synchronizer<TimeSyncPolicy>(TimeSyncPolicy(10), flyingObjectPoseSyncSub_, cameraUnitInfoSyncSub_);
    timeSync_->registerCallback(boost::bind(&Tracking::onPoseAndCameraUnitInfo, this, _1, _2));

    gen_ = NULL;
    d_   = NULL;
    gen_ = new std::mt19937(rd_());
    d_ = new std::normal_distribution<double>(GT_TRAKER_MU, GT_TRAKER_SIGMA);

    gtTrackerTimer_ = nh_->createTimer(ros::Duration(GT_TRAKER_MU), &Tracking::onGTtrackTimer, this, false);
    gtTrackerTimer_.stop();

    if(startGTTrackerFlag_) {
        startGTTrackeTimer_ = nh_->createTimer(ros::Duration(3.0), &Tracking::startGTTracker, this, true);
    }

    pxDistFile_.open("/home/jan/FIT/DP/ols/dist.txt");

    dPXNoise_ = new std::normal_distribution<double>(PX_NOISE_MU, PX_NOISE_SIGMA);

#endif // #if GROUND_TRUTH_TRACKER

#if DEBUG
    markerPub_          = nh_->advertise<visualization_msgs::Marker>("/visualization_marker", 1);   
#endif

//    cv::namedWindow(string("debug_window_") + unit_idx_);
}

Tracking::~Tracking()
{
    // Delete targets
    for(unordered_map<int, Target*>::iterator it = targets_.begin(); it != targets_.end(); ++it) {
        delete it->second;
    }

    // Delete trackers
    for(unordered_map<int, Tracker*>::iterator it = trackers_.begin(); it != trackers_.end(); ++it) {
        delete it->second;
    }

#if GROUND_TRUTH_TRACKER
    if(gen_)    delete gen_;
    if(d_)      delete d_;
#endif

#if DEBUG
    pxDistFile_.close();
    delete dPXNoise_;
#endif
}

void Tracking::update()
{

    if(haveNewFrame_) {
        cv::Mat pubImg_;

        double t0 = getTime();

        haveNewFrame_ = false;

        cv::Mat img;
        uint32_t img_seq_id = 0;

        bool show = false;

        try {
            img = cv_bridge::toCvCopy(lastFrame_, sensor_msgs::image_encodings::BGR8)->image;
            img_seq_id = lastFrame_.header.seq;

            if(show) {
                ROS_WARN_STREAM("SHOWING BROKEN IMAGE");
                cv::imshow(string("debug_window_") + unit_idx_, img);
                cv::waitKey(10);
            }
        } catch(cv_bridge::Exception& e) {
            ROS_ERROR_STREAM("Detection(" << unit_idx_ << "): cv_bridge exception: " << e.what());
            return;
        }

        img.copyTo(pubImg_);

        // Perform tracking of each target
        for(auto it = targets_.begin(); it != targets_.end(); ++it) {            
            cv::Rect bb;
            Target* target = it->second;

            if((trackers_.count(target->getIdLoc()) != 0) && (currentTracker_ != TRACKER_GROUND_TRUTH)) {
                try {
                    trackingSuccessful_ = trackers_[target->getIdLoc()]->observe(img_seq_id, img);
                    trackingSuccessful_ = trackers_[target->getIdLoc()]->track(img_seq_id, img, bb);
                } catch(std::bad_alloc e) {
                    ROS_ERROR_STREAM("update: " << e.what());
                }

                // Tracking successfull
                if(trackingSuccessful_) {
                    // Move the manipualtor so that the object would get to the center.
                    target->updateBB(bb);
                    cv::Point objCenter(bb.x + bb.width / 2, bb.y + bb.height / 2);
                    double obj2center = euclideanDistance(frameCenter_, objCenter);

                    regulateManip(frameCenter_.x - objCenter.x, frameCenter_.y - objCenter.y, lastFrame_.header.stamp);

                    // Send info about tracked target to position_estimation_node
                    msgs::TargetStateLocal targetStateMsg;

                    tf::Vector3 focalPoint;
                    tf::Vector3 objectDirection;
                    targetDirection(target->getIdLoc(), focalPoint, objectDirection, lastFrame_.header.stamp);

                    geometry_msgs::Vector3 focalPointMsg;
                    geometry_msgs::Vector3 objectDirectionMsg;

                    focalPointMsg.x = focalPoint.x();
                    focalPointMsg.y = focalPoint.y();
                    focalPointMsg.z = focalPoint.z();

                    objectDirectionMsg.x = objectDirection.x();
                    objectDirectionMsg.y = objectDirection.y();
                    objectDirectionMsg.z = objectDirection.z();

                    targetStateMsg.camera_unit      = static_cast<uint32_t>(std::stoi(unit_idx_));
                    targetStateMsg.id_global        = static_cast<uint32_t>(target->getIdGlob());
                    targetStateMsg.imgTimeStamp     = lastFrame_.header.stamp;
                    targetStateMsg.roiTopLeft.x     = bb.x;
                    targetStateMsg.roiTopLeft.y     = bb.y;
                    targetStateMsg.roiBottomRight.x = bb.x + bb.width;
                    targetStateMsg.roiBottomRight.y = bb.y + bb.height;
                    targetStateMsg.focal_point      = focalPointMsg;
                    targetStateMsg.direction        = objectDirectionMsg;

                    msgs::TargetObject to;
                    to.targetObject = target->getObjectType();
                    targetStateMsg.targetObject = to;

                    targetStateMsg.belief = trackers_[target->getIdLoc()]->getBelief();

                    localTargetStatePub_.publish(targetStateMsg);

                    // debug
                    drawLine(focalPoint, objectDirection, lastFrame_.header.stamp);

                    // Draw a bounding box
                    bbToDraw_ = bb;

                // Tracking unsuccessfull
                } else {
                    ROS_WARN_STREAM("Failed to track target with id_g = " << target->getIdGlob() << ", id_l = " << target->getIdLoc() << ".");
                    regulateManip(0, 0, lastFrame_.header.stamp);
                }
            }
        }

        double t1 = getTime();

        if(trackingSuccessful_) {
            cv::rectangle(pubImg_, bbToDraw_, cv::Scalar(0, 255, 0));
        }

        cv_bridge::CvImage pubImgMsg(lastFrame_.header, sensor_msgs::image_encodings::BGR8, pubImg_);

        imageProcessedPub_.publish(*(pubImgMsg.toImageMsg()));
    }

    if(haveEnvModel_) {
        pcPub_.publish(envModelMsg_);
    }

    trackingSuccessful_ = false;
}

void Tracking::onImage(const sensor_msgs::Image::ConstPtr img)
{
    haveNewFrame_ = true;
    lastFrame_ = *img;

    // Store the image to the buffer
    bufImg_[idxBI_] = *img;
    idxBI_ = (idxBI_ + 1) % IMAGE_BUFFER_SIZE;
}

void Tracking::onStartDetection(const msgs::StartDetection msg)
{
    // Find the image corresponding to the start-detection request (using timestamp)
    sensor_msgs::Image imgSensor;
    if(!findStampedImage(imgSensor, msg.imgTimeStamp)) {
        ROS_ERROR_STREAM("Cannot find image frame with the given time stamp.");
    }

    // Extract object model
    cv::Mat img = cv_bridge::toCvCopy(imgSensor, sensor_msgs::image_encodings::BGR8)->image;
    uint32_t img_seq_id = imgSensor.header.seq;

    cv::Point2i tl(msg.topLeft.x,       msg.topLeft.y);
    cv::Point2i br(msg.bottomRight.x,   msg.bottomRight.y);

    // Select only the roi of object specifying the object
    cv::Rect roi(tl, br);
    cv::Mat object = img(roi);

    // Appearance model
    cv::resize(object, object, cv::Size(15, 15));

    targets_.clear();
    // Save object info
    Target *t = new Target(cv::Size(screenWidth_, screenHeight_), object, roi, img_seq_id);
    targets_[t->getIdLoc()] = t;

    cv::Mat gray(img.rows, img.cols, CV_8UC1);

    ols_tracker_running_ = false;

    if(msg.targetType == msgs::StartDetection::STATIC) {
         trackers_[t->getIdLoc()] = new TrackerFixedPoint();
    }
    else
    {
        switch (msg.trackerType)
        {
            case msgs::StartDetection::TRACKER_GROUND_TRUTH:
            {
                currentTracker_ = TRACKER_GROUND_TRUTH;
    // Create a new tracker
#if (GROUND_TRUTH_TRACKER == 1)
                xGTDelayed_ = xGT_;
                yGTDelayed_ = yGT_;
                stampGTDelayed_ = stampGT_;
                gtTrackerTimer_.setPeriod(ros::Duration(getNewDelay()));
                gtTrackerTimer_.start();
#endif // #if (GROUND_TRUTH_TRACKER == 1)
            }
            break;

            case msgs::StartDetection::TRACKER_TLD:
            {
    #if (TLD_TRACKER == 1)
                currentTracker_ = TRACKER_TLD;

                // Configure tracker
                cv::cvtColor(img, gray, CV_BGR2GRAY);

                Tracker *tracker = new TrackerTLD((int)screenWidth_, (int)screenHeight_, (int)gray.step);
                trackers_[t->getIdLoc()] = tracker;

                // Initialize tracker with gray frame !!
                tracker->init(t->getIdLoc(), imgSensor.header.seq, img, roi);
    #endif // #if (TLD_TRACKER == 1)
            }
                break;

            case msgs::StartDetection::TRACKER_OLS:
            {
#if (OLS_TRACKER == 1)
                currentTracker_ = TRACKER_OLS;

                // Configure tracker
                cv::cvtColor(img, gray, CV_BGR2GRAY);

                if (tracker_ == nullptr)
                {
                    tracker_ = new TrackerOLS((int)screenWidth_, (int)screenHeight_, (int)gray.step);
                }

                trackers_[t->getIdLoc()] = tracker_;

                ols_tracker_running_ = true;

                // Initialize tracker
                tracker_->clean();
                tracker_->init(t->getIdLoc(), imgSensor.header.seq, img, roi);
#endif // #if OLS_TRACKER
            }
        }
    }

    ROS_INFO_STREAM("Detection of the object id=" << t->getIdLoc() << " successfull.");

    // Send info about new detected target to Controls node    
    msgs::TargetStateLocal targetStateMsg;

    tf::Vector3 focalPoint;
    tf::Vector3 objectDirection;
    targetDirection(t->getIdLoc(), focalPoint, objectDirection, lastFrame_.header.stamp);

    geometry_msgs::Vector3 focalPointMsg;
    geometry_msgs::Vector3 objectDirectionMsg;

    focalPointMsg.x = focalPoint.x();
    focalPointMsg.y = focalPoint.y();
    focalPointMsg.z = focalPoint.z();

    objectDirectionMsg.x = objectDirection.x();
    objectDirectionMsg.y = objectDirection.y();
    objectDirectionMsg.z = objectDirection.z();

    targetStateMsg.focal_point       = focalPointMsg;
    targetStateMsg.direction         = objectDirectionMsg;

    sensor_msgs::Image sensorImg;
    cv_bridge::CvImage appearance;
    appearance.encoding = sensor_msgs::image_encodings::BGR8;
    appearance.image = object;

    msgs::NewTarget newTargetMsg;
    newTargetMsg.id_local = t->getIdLoc();
    newTargetMsg.id_cu = std::stoi(unit_idx_);
    newTargetMsg.type = "unknown";
    newTargetMsg.state = targetStateMsg;
    newTargetMsg.appearance = *(appearance.toImageMsg());

    newTargetPub_.publish(newTargetMsg);

#if DEBUG
    drawLine(focalPoint, objectDirection, msg.imgTimeStamp);
#endif

}

void Tracking::onStopDetection(const msgs::StopDetection::ConstPtr stopMsg)
{
    targets_.clear();
    trackers_.clear();
}

bool Tracking::findStampedImage(sensor_msgs::Image& img, ros::Time stamp)
{
    bool found = false;

    int idxStart = (idxBI_ == 0) ? (IMAGE_BUFFER_SIZE - 1 + IMAGE_BUFFER_SIZE) : (idxBI_ - 1 + IMAGE_BUFFER_SIZE);
    for(int i = 0; i < IMAGE_BUFFER_SIZE; ++i) {
        int idx = (idxStart - i) % IMAGE_BUFFER_SIZE;
        double diff = (bufImg_[idx].header.stamp - stamp).toSec();
        if((std::abs(diff) <= maxImgTimeDiff_) || (diff < 0.0)) {
            if(diff < 0.0) {
                ROS_WARN_STREAM("System time of this camera unit (" << unit_idx_ << ") falls behind at least " << std::abs(diff) << " seconds.");
            }

            img = bufImg_[idx];
            found = true;
            break;
        }
    }

    return found;
}

void Tracking::onNewTargetId(const msgs::NewTargetID::ConstPtr msg)
{
    unordered_map<int, Target *>::iterator targetIt;
    Target *t;

    if((targetIt  = targets_.find(msg->id_local)) == targets_.end()) {
        ROS_ERROR_STREAM("The target with local id = " << msg->id_local << " was not found.");
        return;
    }

    t = targetIt->second;
    t->setIdGlob(msg->id_global);

    // debug
    ROS_INFO_STREAM("Assigned target global id " << t->getIdGlob() << " to local id " << t->getIdLoc());
}

void Tracking::onCameraUnitInfo(const msgs::CameraUnitInfo::ConstPtr cui)
{
    ;
}

void Tracking::onSetTargetObject(const msgs::SetTargetObject::ConstPtr stoMsg)
{
    if(targets_.count(stoMsg->id_global) != 0) {
        targets_[stoMsg->id_global]->setObjectType((Target::ETargetObject)(stoMsg->targetObject.targetObject));
    }
}

#if GROUND_TRUTH_TRACKER

double Tracking::getNewDelay()
{
    return (*d_)(*gen_) / 1000.0;
}

void Tracking::onPoseAndCameraUnitInfo(const geometry_msgs::PoseStampedConstPtr& pose,
                                        const msgs::CameraUnitInfoConstPtr& cuInfo)
{        
    // 3D position of the object
    Eigen::Vector4d P;
    P << pose->pose.position.x, pose->pose.position.y, pose->pose.position.z, 1.0;

    // Extrinsic camera parameters (matrix 3 x 4)
    Eigen::MatrixXd E(3, 4);
    for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 4; ++j) {
            E(i, j) = cuInfo->E[i * 4 + j];
        }
    }

    // Intrinsic camera parameters (matrix 3 x 3)
    Eigen::Matrix3d I;
    for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
            I(i, j) = cuInfo->I[i * 3 + j];
        }
    }

    Eigen::Vector3d Pproj = I * tfROS2screen_ * E * P;

    // Dividing by Z to get screen coordinates.
    Pproj /= Pproj(2);

    // Dividing by pixel width to get pixel coordinates.
    int xNoise = std::round((*dPXNoise_)(*gen_));
    int yNoise = std::round((*dPXNoise_)(*gen_));

    xGT_ = (std::round(Pproj(0) / 3.75e-6) + screenWidth_ / 2)  + xNoise;
    yGT_ = (std::round(Pproj(1) / 3.75e-6) + screenHeight_ / 2) + yNoise;

    stampGT_ = pose->header.stamp;
}

void Tracking::onGTtrackTimer(const ros::TimerEvent&)
{    
    gtTrackerTimer_.stop();

    if((currentTracker_ == TRACKER_GROUND_TRUTH) && (targets_.size() != 0)) {
        // expecting GT target on index 0.
        Target *target = targets_.begin()->second;
        cv::Rect bb(xGTDelayed_ - 10, yGTDelayed_ - 5, 20, 10);
        target->updateBB(bb);

        int dx = frameCenter_.x - xGTDelayed_;
        int dy = frameCenter_.y - yGTDelayed_;

        regulateManip(frameCenter_.x - xGTDelayed_, frameCenter_.y - yGTDelayed_, stampGTDelayed_);
        savePXDist(dx);

        // Send info about tracked target to position_estimation_node
        msgs::TargetStateLocal targetStateMsg;

        tf::Vector3 focalPoint;
        tf::Vector3 objectDirection;
        targetDirection(target->getIdLoc(), focalPoint, objectDirection, stampGTDelayed_);

        geometry_msgs::Vector3 focalPointMsg;
        geometry_msgs::Vector3 objectDirectionMsg;

        focalPointMsg.x = focalPoint.x();
        focalPointMsg.y = focalPoint.y();
        focalPointMsg.z = focalPoint.z();

        objectDirectionMsg.x = objectDirection.x();
        objectDirectionMsg.y = objectDirection.y();
        objectDirectionMsg.z = objectDirection.z();

        targetStateMsg.camera_unit      = std::stoi(unit_idx_);
        targetStateMsg.id_global        = target->getIdGlob();
        targetStateMsg.imgTimeStamp     = stampGTDelayed_;
        targetStateMsg.roiTopLeft.x     = bb.x;
        targetStateMsg.roiTopLeft.y     = bb.y;
        targetStateMsg.roiBottomRight.x = bb.x + bb.width;
        targetStateMsg.roiBottomRight.y = bb.y + bb.height;
        targetStateMsg.focal_point      = focalPointMsg;
        targetStateMsg.direction        = objectDirectionMsg;

        msgs::TargetObject to;
        to.targetObject = target->getObjectType();
        targetStateMsg.targetObject = to;

        targetStateMsg.belief = 1.0;

        localTargetStatePub_.publish(targetStateMsg);

        trackingSuccessful_ = true;
        bbToDraw_ = bb;

        // debug
        drawLine(focalPoint, objectDirection, stampGTDelayed_);

        // Save current target position and schedule a new ground trut tracking.
        xGTDelayed_ = xGT_;
        yGTDelayed_ = yGT_;
        stampGTDelayed_ = stampGT_;

        gtTrackerTimer_.setPeriod(ros::Duration(getNewDelay()));
        gtTrackerTimer_.start();
    }
}

void Tracking::startGTTracker(const ros::TimerEvent &event)
{
    msgs::StartDetection sdMsg;
    sdMsg.imgTimeStamp = ros::Time::now();
    sdMsg.trackerType = msgs::StartDetection::TRACKER_GROUND_TRUTH;
    sdMsg.bottomRight.x = 10;
    sdMsg.bottomRight.y = 10;

    onStartDetection(sdMsg);
}
#endif // #if GROUND_TRUTH_TRACKER

void Tracking::targetDirection(int id, tf::Vector3& focalPoint, tf::Vector3& direction, ros::Time stamp)
{
    tf::StampedTransform tfCamera;
    tf::StampedTransform tfFocus;

    try {
        tfListener_.waitForTransform("world", tfCamera_, stamp, ros::Duration(1.0));
        tfListener_.lookupTransform("world", tfCamera_, stamp, tfCamera);

        tfListener_.waitForTransform("world", tfFocus_, stamp, ros::Duration(1.0));
        tfListener_.lookupTransform("world", tfFocus_, stamp, tfFocus);
    } catch (tf::TransformException ex) {
        ROS_ERROR("Detection: %s",ex.what());
    }

    // Get the object position and transform it from screen to camera frame (trnaslation by -screenWidth / 2 and -screenHeight/2)
    cv::Point objScreenLocation = targets_[id]->getScreenLocationCenter();
    Eigen::Vector3d objCamLocation;
    objCamLocation << objScreenLocation.x, objScreenLocation.y, 1.0;
    objCamLocation = tfCamera2Screen_ * objCamLocation;

    // Get rotation of the camera CCD chip with regards to the world coordinate frame
    tf::Quaternion qCamera      = tfCamera.getRotation();
    tf::Quaternion qCameraConj  = qCamera.operator-();
    qCameraConj.setW(-qCameraConj.getW());

    // Get the vector of the pixel-offset of the object on the CCD chip with regards to the CCD chip center
    tf::Quaternion objScreenOffsetLoc(0.0, objCamLocation(0) * pixelWidth_, objCamLocation(1) * pixelHeight_, 0.0);
    tf::Quaternion objScreenOffsetGlob = qCamera.normalized() * objScreenOffsetLoc * qCameraConj.normalized();

    tf::Vector3 objCenterOffset(objScreenOffsetGlob.getX(), objScreenOffsetGlob.getY(), objScreenOffsetGlob.getZ());
    tf::Vector3 projectedObject = tfCamera.getOrigin() + objCenterOffset;

    focalPoint = tfFocus.getOrigin();
    direction = (projectedObject - focalPoint).normalized();
}

void Tracking::regulateManip(int dx, int dy, ros::Time stamp)
{
    msgs::Regulate regMsg;
    regMsg.header.stamp = stamp;
    regMsg.dx = dx;
    regMsg.dy = dy;

    regulatePub_.publish(regMsg);
}

inline double Tracking::euclideanDistance(cv::Point p, cv::Point q)
{
    int dx = p.x - q.x;
    int dy = p.y - q.y;

    return std::sqrt(dx * dx + dy * dy);
}

void Tracking::pointsInCone(Eigen::Matrix3Xf &points, Eigen::Vector3f &coneAxis, double angle, std::vector<int> &indices)
{
    Eigen::MatrixXf angles = points * coneAxis;
    double acosAngle = std::acos(angle);

    indices.clear();
    for(int i = 0; i < angles.cols(); ++i) {
        if(std::abs(angles(i)) <= acosAngle) {
            indices.push_back(i);
        }
    }
}

#if DEBUG

void Tracking::drawLine(tf::Vector3& from, tf::Vector3& to, ros::Time stamp)
{
    tf::StampedTransform tfWorld2Focus;

    try {
        tfListener_.waitForTransform(tfFocus_, "world" , stamp, ros::Duration(0.1));
        tfListener_.lookupTransform(tfFocus_, "world" , stamp, tfWorld2Focus);
    } catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
    }

    tf::Quaternion qWorld2Focus     = tfWorld2Focus.getRotation();
    tf::Quaternion qWorld2FocusConj = qWorld2Focus.operator-();
    qWorld2FocusConj.setW(-qWorld2FocusConj.getW());

    tf::Quaternion qDirectionWorld(to.x(), to.y(), to.z(), 0.0);
    tf::Quaternion qDirectionFocus = qWorld2Focus.normalized() * qDirectionWorld * qWorld2FocusConj.normalized();
    tf::Vector3 directionVec(qDirectionFocus.getX(), qDirectionFocus.getY(), qDirectionFocus.getZ());

    visualization_msgs::Marker markerMsg;

    markerMsg.header.frame_id   = string("/") + tf_prefix_ + string("/") + "focus";
    markerMsg.header.stamp      = stamp;
    markerMsg.ns = "basic_shapes";
    markerMsg.id = std::stoi(unit_idx_);
    markerMsg.type = visualization_msgs::Marker::ARROW;
    markerMsg.action = visualization_msgs::Marker::ADD;

    markerMsg.scale.x = 0.1;

    markerMsg.color.r = 0.0;
    markerMsg.color.g = 1.0;
    markerMsg.color.b = 0.0;
    markerMsg.color.a = 1.0;

    markerMsg.lifetime = ros::Duration();
    markerMsg.frame_locked = false;

    geometry_msgs::Point ptFrom;
    ptFrom.x = 0.0;
    ptFrom.y = 0.0;
    ptFrom.z = 0.0;

    tf::Vector3 vecTo = 2000 * directionVec;
    geometry_msgs::Point ptTo;
    ptTo.x = vecTo.x();
    ptTo.y = vecTo.y();
    ptTo.z = vecTo.z();

    markerMsg.points.resize(2);
    markerMsg.points[0] = ptFrom;
    markerMsg.points[1] = ptTo;

    markerPub_.publish(markerMsg);
}

void Tracking::savePXDist(double dist)
{
    pxDistFile_ << dist << endl;
}

#endif // #if DEBUG
