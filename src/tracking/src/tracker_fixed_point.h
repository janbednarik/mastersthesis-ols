/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TRACKER_FIXED_POINT_H
#define TRACKER_FIXED_POINT_H

#include "tracker.h"

//! \brief The TrackerFixedPoint class Not a real tracker, just dummy
//! class used for measuring the distance to targets in multi-view system.
//!
class TrackerFixedPoint : public Tracker
{
public:
    //! \brief TrackerFixedPoint Constructor
    //!
    TrackerFixedPoint();

    //! \brief ~TrackerFixedPoint Destructor
    ~TrackerFixedPoint();

    virtual bool init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb);

    virtual bool track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb);

    virtual bool observe(uint32_t img_seq_id, const cv::Mat &image) {}

    virtual bool clean() {}
};

#endif // TRACKER_FIXED_POINT_H
