/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "camera_unit.h"

CameraUnit::CameraUnit(int id) :
    id_(id), azimuth_(0.0), elevation_(0.0),
    camW_(0), camH_(0), bb_(cv::Rect(0, 0, 0, 0)), bbStamp_(ros::Time(0)),
    latitude_(0.0), longitude_(0.0), altitude_(0.0)
{
    ;
}

CameraUnit::~CameraUnit()
{
    ;
}

void CameraUnit::setBB(cv::Rect &bb, ros::Time stamp)
{
    bb_ = bb;
    bbStamp_ = stamp;
}


bool CameraUnit::getNewBB(cv::Rect &bb, ros::Time stamp, ros::Duration age)
{
    bool found = false;

    if((bbStamp_ - stamp) < age) {
        bb = bb_;
        found = true;
    }

    return found;
}
