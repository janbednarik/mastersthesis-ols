//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#pragma once
#ifndef PFTARGET_H
#define PFTARGET_H

// IOSS libraries.
//#include "Target.h"
//#include "ImageObservation.h"
#include "ParticleState.h"
#include "config.h"
// OpenCV libraries.
#include <cv.h>
#include <opencv2/gpu/gpu.hpp>

// Qt libraries.
//#include <QList>

#include <string>
#include <list>

// Eigen libraries.
#ifdef _WIN32
    #include <Eigen>
#else
    #include <eigen3/Eigen/Eigen>
#endif

// Forward declarations.
struct CvParticle;
struct CvParticleState;

//! Interface for the state of the target.
class TargetState {
    friend class Target;
public:
    //! Constructor.
    TargetState() { ; }

    //! Destructor.
    virtual ~TargetState() { ; }

    //! Return id of the observation.
    virtual unsigned long long getObsID() const = 0;
    //! Return identification number of the target.
    virtual unsigned long long getTargetID() const = 0;
    //! Return the bounding box of the target in this state in the image coordinate system.
    virtual cv::Rect getBoundingBox() = 0;
    //! Return true if the target is overlapped.
    virtual bool isOverlapped() const = 0;
//    //! Set flag overlapped flag.
//    virtual void setIsOverlapped(bool isOverlapped) = 0;
    //! Return type of the target.
    virtual std::string getType() const = 0;
    //! Return description of the target.
    virtual std::string getDescription() const = 0;
    //! Return geometrical center point of the target.
    virtual cv::Point2f getCenterPoint() {
        cv::Rect r = getBoundingBox();
        return cv::Point2f(r.x + r.width / 2.0, r.y + r.height / 2.0);
    }

    //! Return template of the target which corresponds to bounding box.
    virtual cv::Mat getTemplate() { return cv::Mat(); }
    //! Return template mask of the target which corresponds to boundig box.
    virtual cv::Mat getTemplateMask() { return cv::Mat(); }
};

//! Target state.
class PFTargetState : public TargetState
{
public:
    //! Constructor.
    PFTargetState() { ; }

    //! Constructor.
    PFTargetState(unsigned long long target_id, unsigned long long obs_id,
                  bool isOverlapped = false, std::string type = "unknown", std::string description = "") :
        tg_id_(target_id),
        obs_id_(obs_id),
        isOverlapped_(isOverlapped),
        type_(type),
        description_(description)
    {
        ;
    }

    //! Destructor.
    virtual ~PFTargetState() { ; }

    //! Return id of the observation.
    virtual unsigned long long getObsID() const { return obs_id_; }
    //! Return identification number of the target.
    virtual unsigned long long getTargetID() const { return tg_id_; }

    //! Recturns the bounding box of the target in this state.
    virtual cv::Rect getBoundingBox();

    //! Return true if the target is overlapped.
    bool isOverlapped() const { return isOverlapped_; }
    //! Set flag.
    void setIsOverlapped(bool isOverlapped) { isOverlapped_ = isOverlapped; }
    //! Return type of the target.
    std::string getType() const { return type_; }
    //! Return description of the target.
    std::string getDescription() const { return description_; }

    //! Return template of the taret which corresponds to bounding box.
    virtual cv::Mat getTemplate() { return template_; }
    //! Return template mask of the target which corresponds to boundig box.
    virtual cv::Mat getTemplateMask() { return templateMask_; }

public:        
    unsigned long long obs_id_;         //!< ID of the related observation.
    unsigned long long tg_id_;          //!< ID of the target.
    bool isOverlapped_;                 //!< Determines wheter the target is overlapped or not.

    std::string type_;                      //!< Type of the target.
    std::string description_;               //!< Description of the target.

    cv::Mat template_;                  //!< Template (corresponds to bounding box).
    cv::Mat templateMask_;              //!< Template mask (related to bounding box).
    cv::Rect boundingRect_;             //!< Bounding rectangle in related image observation.

    CvParticleState s_;                 //!< Current state in the particle coordinate system.
    cv::Mat H_;                         //!< Transformation from particle coordinate system to the image coordinate system.
};


//! Basic class for the target.
class TargetBase {
public:
    //! Constructor.
    TargetBase(unsigned long long id, unsigned long long obs_id);
    //! Destructor.
    virtual ~TargetBase();

    //! Return target ID.
    unsigned long long getID() { return id_; }
    //! Return current observation ID.
    unsigned long long getCurrentObsID() { return currentObsId_; }
    //! Set current observation id.
    void setCurrentObservationID(unsigned long long id) { currentObsId_ = id; }

    //! Store the current state to the history.
    // This method does not check if the state for the specified observation has been included or not.
    virtual void toHistory();
    //! Remove all data in history from obs_id to now inclusive.
    virtual void eraseHistoryFrom(unsigned long long obs_id);
    //! Remove all data in history.
    virtual void eraseAllHistory();

    //! Return state related to specified observation.
    virtual std::shared_ptr<TargetState> history(unsigned long long obs_id);
    //! Transform current state to TargetState structure.
    virtual std::shared_ptr<TargetState> state() = 0;

    //! Return type of the target.
    std::string getType() const { return type_; }
    //! Set type of the target.
    void setType(const std::string &type) { type_ = type; }

    //! Return description of the target.
    std::string getDescription() const { return description_; }
    //! Set description of the target.
    void setDescription(const std::string &description) { description_ = description; }
    //! Return true if the target is main target.

protected:
    unsigned long long id_;                                     //!< Target ID (has to be unique).
    unsigned long long currentObsId_;                           //!< Current ID observation.
    std::string type_;                                              //!< Type of the target.
    std::string description_;                                       //!< Description of the target.
    std::map<unsigned long long, std::shared_ptr<TargetState> > hist_;   //!< History of the target states.
};

//! Target definition for TrackerBase based on the particle filter.
class PFTarget : public TargetBase
{
    friend class PFTracker;
public:           
    //! Constructor.
    PFTarget(unsigned long long id, unsigned long long obs_id);
    //! Constructor.
    PFTarget(unsigned long long target_id, unsigned long long obs_id, float cx, float cy, cv::gpu::GpuMat pattern, cv::gpu::GpuMat mask,
             int num_particles = 512, bool logprob = true,
             float std_x = 5.0f, float std_y = 5.0f,
             float std_vx = 0.0f, float std_vy = 0.0f,
             float std_width = 0.01f, float std_height = 0.01f,
             float std_angle = 0.0f, cv::Rect bouding_box = cv::Rect(0, 0, 10, 10), bool target_particle_median=false);

    //! Destructor.
    virtual ~PFTarget();

    //! Return the current state of the target.
    virtual std::shared_ptr<TargetState> state();

    //! Return true if the target is overlapped.
    bool isOverlapped();
    //! Return number of overlapped in sequence.
    unsigned int getNumOfOverlappedInSequence() const { return numOfOverllapedInSequence_; }

    //! Return mean of velocity.
    Eigen::Vector2d getMeanVel();

    //! Return particles median.
    cv::Point getParticlesMedian();

    //! Returns particles variance.
    cv::Point2f getParticlesVariance(cv::Point mean);

    //! Return bounding box/rectangle for the current state.
    cv::Rect getBoundingBox();

    cv::Rect getOriginBox();

    double getBestParticleWeight();

protected:
    CvParticle *particles_;                     //!< Particles (current state).
    cv::gpu::GpuMat pattern_;                   //!< Pattern (stored in the device memory).
    cv::gpu::GpuMat pattern_mask_;              //!< Pattern mask (stored in the device memory).
    cv::Mat H_;                                 //!< Transformation from the observation when the target has been created to the current observation.

    // Current  state related to the last observation.
    cv::Mat template_;
    cv::Mat templateMask_;

    cv::Rect bouding_box_;
    bool target_particle_median_;

    unsigned int numOfOverllapedInSequence_;

};

#endif
