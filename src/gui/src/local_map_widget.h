/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#pragma once

#ifndef LOCALMAPWIDGET_H
#define LOCALMAPWIDGET_H

// C++ libraries
#include <iostream>
#include <sstream>
#include <iomanip>

// Qt libraries.
#include <QtGui>
//#include <QtWebKit/QtWebKit>
#include <QtWebKitWidgets/QWebView>
#include <QWebFrame>
#include <QDebug>

// zebetin
const double HEADING_OFFSET = -2.781963;
const double LOCAL_HEADING_OFFSETS[2] = {0.049400, -0.0253818};

// oprox
//const double HEADING_OFFSET = -1.04317759;
//const double LOCAL_HEADING_OFFSETS[4] = {0.0, 0.0, 0.0, 0.0};

// vut
//const double HEADING_OFFSET = 1.119878496;
//const double LOCAL_HEADING_OFFSETS[2] = {0.0421125759, 0.0326291968};

//! \brief The LocalMapWidget class Vizualization of camera units and target in Google Maps.
//!
class LocalMapWidget : public QWebView
{
    Q_OBJECT
public:
    //! Constructor.
    LocalMapWidget(int unitsCount) : unitsCount_(unitsCount)
    {
        settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
        QString fileName =  qApp->applicationDirPath() + "/map.html";
        QUrl url =  QUrl::fromLocalFile( fileName );
        load(url);

        latitudes.resize(unitsCount_,       0.0);
        longitudes.resize(unitsCount_,      0.0);
        azimuths.resize(unitsCount_,        HEADING_OFFSET);
        fieldsOfView.resize(unitsCount_,    0.0);
        visionRanges.resize(unitsCount_,    0.0);
    }

    //! Destructor.
    virtual ~LocalMapWidget()
    {
        ;
    }

public:
    void setLatitude(int cuId, double lat)      { latitudes[cuId]       = lat;  }
    void setLongitude(int cuId, double lon)     { longitudes[cuId]      = lon;  }
    void setAzimuth(int cuId, double azi)       { azimuths[cuId]        = azi + HEADING_OFFSET + LOCAL_HEADING_OFFSETS[cuId]; }
    void setFOV(int cuId, double fov)           { fieldsOfView[cuId]    = fov;  }
    void setVisionRange(int cuId, double vran)  { visionRanges[cuId]    = vran; }

    void initialize()
    {
        qDebug() << "Initialize";
        // convert pos to utm.
        double n, e;
        int z;

        std::string lats = numVec2jsArrayStr(latitudes);
        std::string lons = numVec2jsArrayStr(longitudes);
        std::string azis = numVec2jsArrayStr(azimuths);
        std::string fovs = numVec2jsArrayStr(fieldsOfView);
        std::string vran = numVec2jsArrayStr(visionRanges);

        page()->currentFrame()->evaluateJavaScript("initialize(" + QString(lats.c_str()) + ", " + QString(lons.c_str())  + ", "
                                                   +  QString(azis.c_str()) + ", " + QString(fovs.c_str()) + ", " + QString(vran.c_str()) +
                                                   ")");
    }

    void up_pos() {
        std::string lats = numVec2jsArrayStr(latitudes);
        std::string lons = numVec2jsArrayStr(longitudes);
        std::string azis = numVec2jsArrayStr(azimuths);
        std::string fovs = numVec2jsArrayStr(fieldsOfView);
        std::string vran = numVec2jsArrayStr(visionRanges);

        page()->currentFrame()->evaluateJavaScript("updatePOED(" + QString(lats.c_str()) + ", " + QString(lons.c_str())  + ", "
                                                   +  QString(azis.c_str()) + ", " + QString(fovs.c_str()) + ", " + QString(vran.c_str()) +
                                                   ");");
    }

    void add_target(int id, double latitude, double longitude)
    {
        page()->currentFrame()->evaluateJavaScript("addTarget(" +
                                                   QString::number(id) + ", " +
                                                   QString::number(latitude) + ", " +
                                                   QString::number(longitude) +
                                                   ");");
    }

    void add_target_utm(int id, double easting, double northing, int zone)
    {
        page()->currentFrame()->evaluateJavaScript("addTargetUTM(" +
                                                   QString::number(id) + ", " +
                                                   QString::number(easting) + ", " +
                                                   QString::number(northing) + ", " +
                                                   QString::number(zone) +
                                                   ");");
    }

    void remove_target(int id)
    {
        ;
    }

    void update_target(int id, double latitude, double longitude)
    {
        page()->currentFrame()->evaluateJavaScript("updateTarget(" +
                                                   QString::number(id) + ", " +
                                                   QString::number(latitude) + ", " +
                                                   QString::number(longitude) +
                                                   ");");
    }

    void update_target_utm(int id, double easting, double northing, int zone)
    {
        std::ostringstream ss;
        ss << std::setprecision(10) << "updateTargetUTM(" <<
                                       id       << ", " <<
                                       easting  << ", " <<
                                       northing << ", " <<
                                       zone << ");";
        page()->currentFrame()->evaluateJavaScript(QString(ss.str().c_str()));
    }

private:
    int unitsCount_;    //!< Camera units count to be visualized.

    std::vector<double> latitudes;
    std::vector<double> longitudes;
    std::vector<double> azimuths;
    std::vector<double> fieldsOfView;
    std::vector<double> visionRanges;

private:
    std::string numVec2jsArrayStr(std::vector<double> nums) {
        std::ostringstream ss;

        ss << std::setprecision(10);
        ss << "[";
        for(int i = 0; i < nums.size(); ++i) {
            ss << nums[i];
            if(i < nums.size() - 1) {
                ss << ", ";
            }
        }
        ss << "]";

        return ss.str();
    }

};

#endif
