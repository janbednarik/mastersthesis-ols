//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#ifndef KERNEL_INVERSION
#define KERNEL_INVERSION
// compilation
// nvcc --machine 32 test.cu --shared -o a.dll

#include <stdio.h>
#include <stdlib.h>
#include <device_functions.h>

texture<unsigned char, 2> t_img;
texture<unsigned char, 2> t_pattern;


#define fmin(a, b)  ((a) < (b)) ? (a) : (b)


// Constant memory for pattern
__constant__ unsigned char const_pattern[32 * 32];

//! Particles.
struct Particles {
    // --- Host memory ---
    // Translation
    float *h_x_;                //!< Position of the center
    float *h_y_;

    // Rotation.
    float *h_yaw_;

    // --- Device memory ---
    // Translation
    float *d_x_;                //!< Position of the center
    float *d_y_;

    // Rotation.
    float *d_yaw_;


    // Output
    float *d_fitness_;          //!< Computed fitness value.

    // Output
    float *h_fitness_;          //!< Computed fitness value.

    // Number of particles.
    int count_;


    // Obsolete...
    float *d_width_;            // Width
    float *d_height_;

    float *h_width_;
    float *h_height_;

};


struct Particles2
{
    // Translation vector
    float *x_;              // Center coordinates.
    float *y_;

    // Scale
    float *scale_x_;        // Scale factor. In the initial is 1.0.
    float *scale_y_;

    // Rotation
    float *yaw_;

    // Fitness
    float *fitness_;

};


// Image in the device memory.
struct gpuImg
{    
    int width_;
    int height_;
    size_t pitch_;
    unsigned char *d_data_;
};



__global__ void inversion(unsigned char *img, int width)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    img[x + y * width] = 255 - img[x + y * width];
}

 __device__ inline void MyAtomicAdd(float *address, float value)
 {
  int oldval, newval, readback;

  oldval = __float_as_int(*address);
  newval = __float_as_int(__int_as_float(oldval) + value);
  while ((readback=atomicCAS((int *)address, oldval, newval)) != oldval) {
    oldval = readback;
    newval = __float_as_int(__int_as_float(oldval) + value);
  }
 }



 __global__ void evaluateParticles(unsigned char* img_data, size_t img_pitch,           // Image
                                   unsigned char* pattern_data, size_t pattern_pitch,   // Pattern
                                   float *p_x, float *p_y,                              // Particles.
                                   float *buff, size_t buff_pitch)                     // Buffer
 {
     /*
    __shared__ float i_data[32 * 32];       // Pattern.

    int p = blockIdx.x;
    int x = p_x[p];
    int y = p_y[p];

    #pragma unroll
    for (int l = 0; l < 4; l++) {
        #pragma unroll
        for (int k = 0; k < 4; k++) {
            int i = threadIdx.x * 4 + k;
            int j = threadIdx.y * 4 + l;

            //i_data[j * 32 + i] = abs(pattern_data[j * pattern_pitch + i] - img_data[(y + j) * img_pitch + x + i]);
            buff[j * buff_pitch + 32 * p + i] = abs(pattern_data[j * pattern_pitch + i] - img_data[(y + j) * img_pitch + x + i]);
        }
    }

    __syncthreads();
    */
    // Compute sum.



    // Version 0 - the best :]

    // uchar4 i_dat =
/*
    float diff = abs(pattern_data[pattern_pitch * j + i] - img_data[(y + j) * img_pitch + x + i]);
    buff[j * buff_pitch +  32 * p + i] = diff;
    */

    /*
    // Version 1
    int i = threadIdx.x;
    int j = blockIdx.y;

    int p = blockIdx.x;   // Particle index.

    // Position in img...
    int x = p_x[p];
    int y = p_y[p];



    float diff = abs(const_pattern[32 * j + i] - img_data[(y + j) * img_pitch + x + i]);
    buff[j * buff_pitch + 32 * p + i] = diff;

    */

      // Version 2 - best
     // Pattern image coordinates

     int i = threadIdx.x + (blockIdx.x % 2) * 16;
     int j = blockIdx.y;

     int p = (blockIdx.x / 2) * 16 + threadIdx.y;   // Particle index.

     // Position in img...
     int x = p_x[p];
     int y = p_y[p];

     // Texture variant...
     float diff = abs(tex2D(t_img, x + i, y  +j)- tex2D(t_pattern, i, j));
     buff[j * buff_pitch +  32 * p + i] = diff;

     // Global memory variant
     /*
     float diff = abs(pattern_data[pattern_pitch * j + i] - img_data[(y + j) * img_pitch + x + i]);
     buff[j * buff_pitch +  32 * p + i] = diff;
     */

     // Version 3 - worst
/*
     // Pattern image coordinates
     int j = blockIdx.y;
     int p = threadIdx.y + blockIdx.x * 32;   // Particle index.

     // Position in img...
     int x = p_x[p];
     int y = p_y[p];


     #pragma unroll
     for (int i = 0; i < 32; i++) {
         float diff = abs(const_pattern[32 * j + i] - img_data[(y + j) * img_pitch + x + i]);
         buff[j * buff_pitch +  32 * p + i] = diff;
     }
     */


 }

__global__ void computeFitness1(float *fitness, float *buff, size_t buff_pitch)
{
    if (blockIdx.y < 31) {
        buff[(blockIdx.y  + 1) * buff_pitch + blockIdx.x * 32 + threadIdx.x] += buff[blockIdx.y * buff_pitch + blockIdx.x * 32 + threadIdx.x];
    }    
}

__global__ void computeFitness2(float *fitness, float *buff, size_t buff_pitch)
{
    // This is very time consuming... - this should be computed in cascade... 
    int p = blockIdx.x * 32 + threadIdx.x;
    int j = 31 * buff_pitch + (1024 * blockIdx.x) + 32 * threadIdx.x;

    #pragma unroll
    for (int i = 0; i < 32; i++) {
        fitness[p] += buff[j + i];
    }    
}

#endif
