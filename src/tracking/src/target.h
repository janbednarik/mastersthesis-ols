/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TARGET_H
#define TARGET_H

// Opencv
#include <opencv2/core/core.hpp>

//! \brief The Target class The representation of tracked target.
//!
class Target
{
public:
    enum ETargetObject {
        OBJECT_UNKNOWN  = 0,
        OBJECT_DRONE    = 1,
        OBJECT_AIRCRAFT = 2,
        OBJECT_HUMAN    = 3,
        OBJECT_DOG      = 4,
    };

public:
    //! \brief Target Constructor
    //! \param appearance
    //! \param bbox
    //!
    Target(const cv::Size &screenSize,cv::Mat appearance, const cv::Rect& bbox, const uint32_t img_seq_id);

    //! \brief ~Target Destructor
    //!
    ~Target();

    //! \brief getIdLoc Getter of the target local ID.
    //! \return
    //!
    int getIdLoc() { return idLoc_; }

    //! \brief getIdGlob Target global ID getter.
    //! \return
    //!
    int getIdGlob() { return idGlob_; }

    //! \brief setIdGlob Target global ID setter.
    //! \param id
    //!
    void setIdGlob(int id) { idGlob_ = id; }

    //! \brief getScreenLocationTopLeft Returns the 2D screen position
    //! of the object given by the coordinates of the top left corner of
    //! the bounding box. [0, 0] pixel si considered to be in the top left
    //! corner of the screen.
    //! \return
    //!
    cv::Point getScreenLocationTopLeft();

    //! \brief getScreenLocationCenter Returns the 2D screen position
    //! of the object given by the center of the corresponding boudning box.
    //! [0, 0] pixel si considered to be the center of the screen.
    //! \return
    //!
    cv::Point getScreenLocationCenter();

    //! \brief updateBB Updates bounding box (size and position) according
    //! to tracking results.
    //! \param bb
    //!
    void updateBB(cv::Rect bb);

    //! \brief setObjectType Object type setter.
    //! \param type
    //!
    void setObjectType(ETargetObject type) { objectType_ = type; }

    //! \brief getObjectType Object type getter.
    //! \return
    //!
    ETargetObject getObjectType() { return objectType_; }

private:
    static int    newIdLoc_;     //!< Local id counter.

    uint32_t      init_img_seq_id;
    int           idLoc_;        //!< local id of the target
    int           idGlob_;       //!< global id of the target

    cv::Size      screenSize_;   //!< Image screen size

    cv::Mat       appearance_;
    cv::Rect      bbox_;

    ETargetObject objectType_;
};

#endif // #ifndef TARGET_H
