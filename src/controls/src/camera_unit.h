/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef CAMERA_UNIT_H
#define CAMERA_UNIT_H

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>

// ROS
#include <ros/ros.h>

//! \brief The CameraUnit class The main data type used to store information
//! about camera units.
//!
class CameraUnit
{
public:
    //! \brief CameraUnit Constructor
    //!
    CameraUnit(int id);

    //! \brief ~CameraUnit Destructor
    //!
    ~CameraUnit();

    //! \brief setBB_ Sets the new bounding box
    //! \param bb A bounding box.
    //! //! \param bb A time stamp of the bounding box.
    //!
    void setBB(cv::Rect &bb, ros::Time stamp);

    //! \brief getBB Fills 'bb' with the current bounding box
    //! \param bb Bounding box to be filled.
    //! \param stamp A time stamp for which the bounding box is required
    //! \param age Maximum age of the newest bounding box.
    //! \return
    //!
    bool getNewBB(cv::Rect &bb, ros::Time stamp, ros::Duration age = ros::Duration(0.5));

    double getAzi() { return azimuth_; }
    void setAzi(double azi) { azimuth_ = azi; }

    double getEle() { return elevation_; }
    void setEle(double ele) { elevation_ = ele; }

    double getLatitude()    { return latitude_; }
    void setLatitude(double latitude)   { latitude_ = latitude; }

    double getLongitude()   { return longitude_; }
    void setLongitude(double longitude)   { longitude_ = longitude; }

    double getAltitude()    { return altitude_; }
    void setAltitude(double altitude)   { altitude_ = altitude; }

public:
    int id_;            //!< id of the camera unit (0 = overview unit, 1-3 = tracking units)   

    int camW_;          //!< Camera frame width.
    int camH_;          //!< Camera frame height.

    // mod rizeni manipulatoru (MANUAL / AUTO)
    // mod detekce (MANUAL / AUTO)

private:
    double azimuth_;    //!< Current azimuth of the manipulator [mrad].
    double elevation_;  //!< Current elevation of the manipulator [mrad].

    // WGS-84 (GPS) coordinates
    double latitude_;
    double longitude_;
    double altitude_;

    cv::Rect bb_;       //!< A bounding box of a tracked object.
    ros::Time bbStamp_; //!< A timestamp of the lst received bounding box.
};

#endif // #ifndef CAMERA_UNIT_H
