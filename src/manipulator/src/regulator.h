/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef REGULATOR_H
#define REGULATOR_H

// C++
#include <limits>
#include <cmath>
#include <iostream>

// ROS
#include <ros/ros.h>

//! \brief The SpeedFunc class maps the position value (x) to the speed value (y)
//! by the given function (see derived classes representing separate functions).
//!
class SpeedFunc
{
public:

    //! \brief Regulator
    //! \param a
    //! \param b
    //!
    SpeedFunc(double a, double b);

    //! \brief Regulator
    //!
    SpeedFunc();

    //! \brief Regulator Destructor
    //!
    ~SpeedFunc();

public:
    //! \brief operator () Returns the angular speed given the difference angle.
    //! \param angle
    //! \return
    //!
    virtual double getY(double x) = 0;

    //! \brief setXMin Setter
    //! \param xMin
    //!
    void setXMin(double xMin) { xMin_ = xMin; }

    //! \brief setXMax Setter
    //! \param xMax
    //!
    void setXMax(double xMax) { xMax_ = xMax; }

protected:
    double a_;
    double b_;
    double xMin_;
    double xMax_;
};

//! \brief The SpeedFuncLin class represents the linear mapping function.
//!
class SpeedFuncLin : public SpeedFunc
{
public:
    //! \brief RegulatorLin Constructor sets terms 'a' and 'b' for the function y = a * x + b
    //! \param a
    //! \param b
    //!
    SpeedFuncLin(double a, double b);

    //! \brief RegulatorLin Constructor computes the term 'a'
    //! for the function y = a * x + b from given params 'xMax', 'yMax'
    //! as a = (yMax - b) / xMax
    //! \param xMax
    //! \param yMax
    //! \param b
    //! \param xMin
    //!
    SpeedFuncLin(double xMax, double yMax, double b, double xMin = 0.0);

    //! \brief RegulatorLin Destructor
    //!
    ~SpeedFuncLin();

public:
    virtual double getY(double x);
};

//! \brief The SpeedFuncPow class represents the power mapping function.
//!
class SpeedFuncPow : public SpeedFunc
{
public:

    //! \brief RegulatorPow Constructor sets terms 'a', 'b', 'N'
    //! for the function y = a * x^N + b.
    //! \param a
    //! \param b
    //! \param N
    //!
    SpeedFuncPow(double a, double b, double N);

    //! \brief RegulatorPow Constructor computes term 'a' for the
    //! function y = a * x^N + b from given params as:
    //! a = (yMax - b) / xMax^N
    //! \param xMax
    //! \param yMax
    //! \param b
    //! \param N
    //! \param xMin
    //!
    SpeedFuncPow(double xMax, double yMax, double b, double N, double xMin = 0.0);

    ~SpeedFuncPow();

public:
    virtual double getY(double x);

private:
    double N_;

};

//! \brief The Position2DPredictor class keeps track of last N angular speeds
//! of the tracke dobjects and last N time periods between successive calls
//! to getNextPos() and estimates the next 2D position of the target.
//!
class Position2DPredictor
{
public:
    static const int OPT_AXIS_POS_HISTORY_SIZE;

    enum eState {
        INITIALIZATION = 0,
        BUILDING_HISTORY,
        PREDICTION
    };

    //! \brief The StampedPos struct History of position of the optical axis.
    //! [rad], [rad], [s]
    //!
    struct StampedPos {
        StampedPos(double x = 0.0, double y = 0.0, ros::Time stamp = ros::Time()) :
            x_(x), y_(y), stamp_(stamp) { }

        double x_;
        double y_;
        ros::Time stamp_;
    };

    //! \brief PositionPredictor Constructor
    //! \param n the length of speed and time period history
    //!
    Position2DPredictor(int predHistSize);

    //! \brief PositionPredictor Desturctor
    //!
    ~Position2DPredictor();

    //! \brief getNextPos Estimates the next relative angular position of the target
    //! with respect to the current position of the optical axis.
    //! \param dx Current angular distance between target and the center in X axis [rad].
    //! The resulting estimated angular distance will be saved in this parameter.
    //! \param dy Current angular distance between target and the center in Y axis [rad].
    //! The resulting estimated angular distance will be saved in this parameter.
    //! \param dxEstim Estimated relative azimuthal position of the target at time now + 'dt' seconds [rad].
    //! \param dyEstim Estimated relative elevation position of the target at time now + 'dt' seconds [rad].
    //! \param stamp Time stamp of the frame for twhich the differnece angles 'dx' and 'dy' were measured.
    //! \param dt Time difference to estimate the position for. If dt = 0, the estimated moving average
    //! of the last n durations between the consecutive function calls is used.
    //!
    void getNextPos(double dx, double dy, double &dxEstim, double &dyEstim, ros::Time stamp, ros::Duration dt = ros::Duration(0.0));

    //! \brief reset Resets the history of the predictor and starts creating a new history.
    //!
    void reset();

    //! \brief updateStampedPos
    //! \param azi  [rad]
    //! \param ele  [rad]
    //! \param stamp
    //!
    void updateStampedPos(double azi, double ele, ros::Time stamp);

    //! \brief findStampedPos
    //! \param stamp
    //! \return
    //!
    StampedPos *findStampedPos(ros::Time& stamp);

private:
    int optAxisPosHistSize_;    //!< Size of the history of the position of the optical axis.
    StampedPos* posHistory_;    //!< History of position of the optical axis.
    int iPosHist_;              //!< Current index in the position history array.

    int predHistSize_; //!< The length of history of target speed prediction.

    ros::Duration *t_;  //!< Time period history [s].
    double *wx_;        //!< X axis angular speed history [rad/s].
    double *wy_;        //!< Y axis angular speed history [rad/s].

    ros::Duration tAvg_;    //!< Average time [t] between consecutive calls to func getNextPos.
    double wxAvg_;          //!< Average angular speed in X axis [rad/s].
    double wyAvg_;          //!< Average abgular speed in Y axis [rad/s].

    int it_;        //!< Current index to t_ array;
    int iwx_;       //!< Current index to wx_ array;
    int iwy_;       //!< Current index to wy_ array;
    int invoc_;     //!< Number of invocations - no history building during first m invocations.

    double xOld_;
    double yOld_;
    ros::Time tOld_;

    eState state_;

    double xmOld_;
    double ymOld_;
    double xTgtDiffOld_;
    double yTgtDiffOld_;
    ros::Time stampOld_;


private:
    //! \brief incIdx Increments the index pointing to circular buffer.
    //! \param i
    //! \param n
    //!
    void incCircBufIdx(int& i, int n) { i = (i + 1) % n; }

    //! \brief decCircBufIdx
    //! \param i
    //! \param n
    //!
    void decCircBufIdx(int&i, int n) { i = (i == 0) ? (n - 1) : (i - 1); }

    //! \brief valAverage Returns the average of 'num' values in array 'valArry'.
    //! \param valArray
    //! \param num
    //! \return
    //!
    double valAverage(double* valArray, int num);

    //! \brief timeAverage Returns the time duration average of 'num' durations in array 'timeArray'.
    //! \param timeArray
    //! \param num
    //! \return
    //!
    ros::Duration timeAverage(ros::Duration* timeArray, int num);


    // debug
    void printValArray(double *valArray);
    void printTimeArray(ros::Duration *timeArray);
};

//! \brief The Regulator class performs the ragulation of the motion
//! of the manipulator in order to keep the tracked target in the middle
//! of the screen (the optical axis od the camera should intersect the target).
//!
class Regulator {
public:
    //! \brief Regulator Constructor
    //! \param speedFunc Prefered and configured function mapping angular
    //! position to angular speed.
    //! \param predictor Predictor of the 2D position of the tracked target.
    //!
    Regulator(SpeedFunc* speedFunc, Position2DPredictor* predictor);

    //! \brief ~Regulator Destructor
    ~Regulator();

    //! \brief regulate Computes the new angular speeds [rad/s] for the azimuth
    //! and elevation.
    //! \param aziSpeed
    //! \param eleSpeed
    //! \param aziAng   Current difference angle [rad] between optical axis and target in azimuth.
    //! \param eleAng   Current difference angle [rad] between optical axis and target in elevation.
    //! \param stamp    Time stamp of the frame for twhich the differnece angles 'aziAng' and 'eleAng' were measured
    //! \param period   Time period for which to predict the position in the future.
    //!
    void regulate(double aziAng, double eleAng, double& aziSpeed, double& eleSpeed, ros::Time stamp, ros::Duration period);

    //! \brief reset Resets the predictor.
    //!
    void reset();

    //! \brief updateStampedPos
    //! \param azi Azimuthal position of the manipulator [rad].
    //! \param ele Elevation position of the manipulator [rad].
    //! \param stamp Time stamp
    //!
    void updateStampedPos(double azi, double ele, ros::Time stamp);

private:
    SpeedFunc* sf_;             //!< Speed function.
    Position2DPredictor* pred_; //!< Position predictor.
};


#endif // #define REGULATOR_H
