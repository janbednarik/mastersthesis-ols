//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

// Declaration.
void test_particles(unsigned char *img_data, int img_step, int img_width, int img_height,
                                                     unsigned char *pattern_data, int pattern_step, int pattern_width, int pattern_height,
                                                     int n_particles);

// Declaration.
void test_particles2(struct Particles *p, struct Img *obs, struct Img *pattern, float *M);


void particle_translation(struct Particles *p);
void particle_normalization(struct Particles *p);
void particle_resampling(struct Particles *dst, struct Particles *src);

// float sum_array(float *data, int num);

void test_sum_array();


// Declaration.
void img_add(Img *a, Img *b);


void testGPUMatFunction(float *d, int step, int rows, int cols);

// Evaluate particles.
void evaluate_particles(struct Particles *p, struct Img *img, struct Img *fg, struct Img *target, struct Img *mask, double *M);


// Warp perspective (transformation has been inverse!).
void warp_perspective(struct Img *in, struct Img *out, double *M);
// Compute mask for transformation (transformation has been inverse!).
void warp_perspective_get_mask(unsigned char *data, int width, int height, size_t pitch_data,  // Allocated mask.
                                                                double *M,                                                       // Transformation matrix (3x3).
                                                                unsigned char in_val,
                                                                unsigned char out_val);
