/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS libraries
#include <ros/ros.h>

// C++ libraries
#include <iostream>

// Project libraries
#include "gui.h"

// Qt libraries
#include <QApplication>


const int SPIN_RATE = 30;

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "gui_node");

    QApplication app(argc, argv);
    Gui gui;

    ros::start();
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        ros::spinOnce();
        gui.update();
        app.processEvents();
        looper.sleep();
    }

    ros::shutdown();

    return 0;
}
