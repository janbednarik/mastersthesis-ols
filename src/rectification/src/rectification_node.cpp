// ROS
#include <ros/ros.h>

// C++
#include <string>

// project libraries
#include "rectification.h"

using namespace std;

const string DEFAULT_FILE_NAME = "parameters.yaml";
const int SPIN_RATE = 60;

//void printUsage()
//{
//    cerr << "rectification" << endl << endl;
//    cerr << "Usage:" << endl;
//    cerr << "rectification file_name" << endl;
//    cerr << "\tfilename\t\tName of the output file to save rectification parameters." << endl;
//}

int main(int argc, char *argv[])
{
    int returnValue = 0;
    string file;

//    if(argc != 2) {
//        printUsage();
//        cerr << "WARNING: No output file specified, using default file name '" <<
//                DEFAULT_FILE_NAME << "'" << endl;

//        file = DEFAULT_FILE_NAME;
//    } else {
//        file = argv[1];
//    }

    ros::init(argc, argv, "rectification");
    Rectification rectification;

    ros::start();

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        ros::spinOnce();
        rectification.update();
        looper.sleep();
    }

//    ros::spin();

    ros::shutdown();

    return returnValue;
}
