/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#include "lense.h"
#include "exceptions_ols.h"
#include "crc.h"

using namespace std;

Lense::Lense(string port, uint32_t baudrate, uint32_t timeout, double focalLengthMin, double focalLengthMax) :
    focalLengthMin_(focalLengthMin), focalLengthMax_(focalLengthMax)
{
    adjustableLens_ = true;

    // Create and initialize the serial interface.
    serialIf_ = new serial::Serial(port, baudrate, serial::Timeout::simpleTimeout(timeout));

    // debug
    cout << "Serial port for lense open: " << (serialIf_->isOpen() ? "YES" : "NO") << endl;

    serialIf_->setTimeout(0, 20, 0, 10, 0);

    // Initializace CRC computation
    crcInit();

    // Initialize camera
    reset();
}

Lense::Lense(double focalLength)
{
    adjustableLens_ = false;

    focalLengthMin_ = focalLengthMax_ = focalLength;

    reset();
}

Lense::~Lense()
{
    ;
}

void Lense::zoom(int step)
{
    if(adjustableLens_) {
        zoom_ = std::min(MAX_ZOOM, std::max(MIN_ZOOM, zoom_ + step));
        string cmd = {0x01, (uint8_t)zoom_, 0x01, 0x00, 0x00};

        command(cmd);
    }
}

void Lense::focus(int step)
{
    if(adjustableLens_) {
        focus_ = std::min(MAX_FOCUS, std::max(MIN_FOCUS, focus_ + step));
        string cmd = {0x02, (uint8_t)focus_, 0x00, 0x00, 0x00};

        command(cmd);
    }
}

void Lense::iris(int speed, int direction)
{
    if(adjustableLens_) {
        if((direction == 0) && (speed != 0)) {
            throw ExceptionOLS("Lense::iris: Parameter direction cannot be 0 unless speed is 0.");
        }

        string cmd = {0x04, (uint8_t)speed, ((direction < 0) ? 0x0f : ((direction > 0) ? 0xf0 : 0x00)), 0x00, 0x00};
        command(cmd);
    }
}

void Lense::reset()
{
    zoom_   = 0;
    focus_  = 0;

    if(adjustableLens_) {
        string cmd = {0x05, 0x00, 0x00, 0x00, 0x00};
        command(cmd);
    }
}


void Lense::command(string cmd, uint32_t readTimeout)
{
    string response;

    write(cmd);
    read(response, readTimeout);
}

void Lense::read(string &response, uint32_t waitTime)
{
    string l;
    response = "";

    if(waitTime != 0) {
        serial::Timeout toOld, toNew;
        toOld = serialIf_->getTimeout();
        toNew = toOld;
        toNew.read_timeout_constant = waitTime;

        serialIf_->setTimeout(toNew);
        serialIf_->waitReadable();
        serialIf_->setTimeout(toOld);
    }


    while(!(l = serialIf_->readline()).empty()) {
        response += l;
    }

    if(response.length() > 0) {
        // Check and remove CRC from the CVUT control unit
        if(!checkCRC(response)) {
            throw(ExceptionOLS("read: CRC missmatch."));
        }

        response.erase(0, 1);
        response.pop_back();
    }
}


void Lense::write(string &cmd)
{
    // Adding CRC for the CVUT control unit
    cmd += int2byteStr(crc8(QByteArray(cmd.c_str(), cmd.length()), cmd.length()), 1);

    // debug
    cout << "Sending command: " << prettyHexStr(cmd) << endl;

    // Send command
    size_t bytesWritten = serialIf_->write(cmd);

    if(bytesWritten != cmd.size()) {
        ostringstream strs;
        for(int i = 0; i < cmd.size(); ++i) {
            strs << std::hex << (int)cmd[i] << " ";
        }
        throw(ExceptionOLS(string("Sending command '") + strs.str() + "' failed, only sent " +
                           to_string(bytesWritten) + " / " + to_string(cmd.length()) + " bytes."));
    }
}

string Lense::prettyHexStr(string hexNums)
{
    string pretty;

    for(int i = 0; i < hexNums.length(); ++i) {
        int whole = ((uint8_t)hexNums[i]) / 16;
        int resid = ((uint8_t)hexNums[i]) % 16;

        if(whole >= 10)
            pretty += (char)((whole - 10) + 'a');
        else
            pretty += (char)('0' + whole);

        if(resid >= 10)
            pretty += (char)((resid - 10) + 'a');
        else
            pretty += (char)('0' + resid);

        pretty += " ";
    }

    return pretty;
}

inline string Lense::int2byteStr(uint32_t num, int bytes)
{
    if(bytes < 1 || bytes > 4) {
        throw(ExceptionOLS("hex2str: Wrong string length (" + to_string(bytes) +
                           ") required. Supported range is [1, 4]."));
    }

    string hexStr((char *)&num, bytes);

    // reverse string
    return string(hexStr.rbegin(), hexStr.rend());
}

double Lense::focalLength()
{
    double f = (focalLengthMax_ - focalLengthMin_) * (((double)zoom_ - (double)MIN_ZOOM) / ((double)MAX_ZOOM - (double)MIN_ZOOM)) + focalLengthMin_;

    return f;
}
