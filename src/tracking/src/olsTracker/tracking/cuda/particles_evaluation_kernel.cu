//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#pragma once

#ifndef PARTICLES_EVALUATION_KERNEL_CU
#define PARTICLES_EVALUATION_KERNEL_CU

#include <stdio.h>
#include <stdlib.h>
#include <device_functions.h>
#include <vector_types.h>
#include <cuda_runtime.h>

#include "Particles.h"

/*
extern struct Particles;
extern struct Img;
extern struct Mat;
*/
// nvcc -arch sm_11 --machine 32 test.cu --shared -o a.dll
// sm_11 because of atomicCAS function.


texture<float, 2> t_img1;
texture<float, 2> t_img2;
texture<float, 2> t_img3;
texture<float, 2> t_img4;



__global__ void intensityMul(float *d, int step, int cols, int rows)
{    
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;

    d[y * (step  / sizeof(float)) + x] *= 2.0;
}


__global__ void add_kernel(Img *a, Img *b)
{

    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;

    // This should be done without interpolation.
    a->data_[y * a->width_ + x] = tex2D(t_img1, x, y) + tex2D(t_img2, x, y);
}

__device__ inline void MyAtomicAdd(float *address, float value)
{
    int oldval, newval, readback;

    oldval = __float_as_int(*address);
    newval = __float_as_int(__int_as_float(oldval) + value);
    while ((readback = atomicCAS((int *)address, oldval, newval)) != oldval) {
        oldval = readback;
        newval = __float_as_int(__int_as_float(oldval) + value);
    }
}


__global__ void img_copy_kernel(Img *dst, Img *src, double *T)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;

    double x_s = T[0] * x + T[1] * y + T[2] * 1.0;
    double y_s = T[3] * x + T[4] * y + T[5] * 1.0;
    double z_s = T[6] * x + T[7] * y + T[8] * 1.0;

    // This should be done without interpolation.
    dst->data_[y * (dst->pitch_data_ / sizeof(float)) + x] = tex2D(t_img1, x_s / z_s, y_s / z_s);
}

__global__ void addSubSum(Particles *p, int width)
{
    // 32 hard-coded
    float sum = 0.0f; float mask_sum = 0.0f;

    #pragma unroll
    for (int i = 0; i < width; i++) {
        sum += p->fitness_temp_[blockIdx.x * blockDim.x * width + threadIdx.x * width  + i];
        mask_sum += p->mask_sum_temp_[blockIdx.x * blockDim.x * width + threadIdx.x * width  + i];
    }

    p->fitness_[blockIdx.x * blockDim.x + threadIdx.x] = sum;
    p->mask_sum_[blockIdx.x * blockDim.x + threadIdx.x] = mask_sum;
}

__device__ void uniform_randf32(float *rnd, unsigned int *x)
{
    unsigned int M = 4294967296;
    *x = (1664525 * (*x) + 1013904223) % M;
    *rnd  = (float) *x / (float) M;             // Normalization.
}


__device__ void gaussian_randf32(float *a, float *b, float u1, float u2)
{
    u1 += 1.0e-6;
    u2 += 1.0e-6;;
    float r = sqrt(-2 * log(u1));
    float theta = 2.0 * 3.141592653589 * u2;

    *a = r*sin(theta);
    *b = r*cos(theta);
}

__global__ void sub_sum(float *res, float *d1, float *d2)
{
    // Compute index.
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    res[i] = d1[i] + d2[i];
}

// Add noise.
__global__ void particle_translation_kernel(Particles *p)
{
    // Index.
    int i = threadIdx.x + blockDim.x * blockIdx.x;


    // Position.
    float u1, u2;
    float g1, g2;
    uniform_randf32(&u1, &(p->rnd_[i]));
    uniform_randf32(&u2, &(p->rnd_[i]));
    gaussian_randf32(&g1, &g2, u1, u2);

    p->x_[i] += g1 * 5.0;
    p->y_[i] += g2 * 5.0;

    // Scale.
    uniform_randf32(&u1, &(p->rnd_[i]));
    uniform_randf32(&u2, &(p->rnd_[i]));
    gaussian_randf32(&g1, &g2, u1, u2);

    p->s_x_[i] += g1 * 1.5;
    p->s_y_[i] += g2 * 1.5;
}

//
__global__ void particle_resampling_kernel(Particles *dst, Particles *src)
{
    // Index.
    int i = threadIdx.x + blockDim.x * blockIdx.x;

    // Generate random number.
    float rndf;
    uniform_randf32(&rndf, &(src->rnd_[i]));

    int i_c = 0; float sum_fitness = 0.0;
    while (sum_fitness <= rndf && i_c < src->count_) {
        sum_fitness += src->fitness_[i_c];
        i_c++;
    }

    // Copy  -1
    dst->x_[i] =  src->x_[i_c - 1];
    dst->y_[i] =  src->y_[i_c - 1];
    dst->s_x_[i] =  src->s_x_[i_c - 1];
    dst->s_y_[i] =  src->s_y_[i_c - 1];
}

//! Normalization of the particle fitness.
__global__ void particle_normalization_kernel(Particles *p, float sum)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    p->fitness_[i] /= sum;
}

// Evaluate particles...
__global__ void  evaluate_particles_kernel(Particles *p, Img *img, Img *fg, Img *target, Img *mask, double *M)
{
    int i_p = blockIdx.y * blockDim.x + blockIdx.x;
    // Transformation matrix.
    __shared__ double T[9];      // Patricle Matrix.
    __shared__ double cM[9];     // cached M
    __shared__ double F[9];      // Final matrix.

    double sx = p->s_x_[i_p];
    double sy = p->s_y_[i_p];
    double sin_yaw = sin(p->yaw_[i_p]);
    double cos_yaw = cos(p->yaw_[i_p]);
    double dx = p->x_[i_p];
    double dy = p->y_[i_p];
    double tc_x = -(target->width_ - 1.0f) / 2.0f;
    double tc_y = -(target->height_ - 1.0f) / 2.0f;

    // Computes transformation matrix... (I)
    switch (threadIdx.x) {
        // First row.
        case 0: T[threadIdx.x] = sx * cos_yaw; break;
        case 1: T[threadIdx.x] = -sx * sin_yaw; break;
        case 2: T[threadIdx.x] = sx * (cos_yaw * tc_x - sin_yaw * tc_y) + dx; break;

        // Second row.
        case 3: T[threadIdx.x] = sy * sin_yaw; break;
        case 4: T[threadIdx.x] = sy * cos_yaw; break;
        case 5: T[threadIdx.x] = sy * (sin_yaw * tc_x + cos_yaw * tc_y) + dy; break;

        // Third row.
        case 6: T[threadIdx.x] = 0.0f; break;
        case 7: T[threadIdx.x] = 0.0f; break;
        case 8: T[threadIdx.x] = 1.0f; break;

        // Create cache
        case 9:     cM[0] = M[0]; cM[1] = M[1]; break;
        case 10:    cM[2] = M[2]; cM[3] = M[3]; break;
        case 11:    cM[4] = M[4]; break;
        case 12:    cM[5] = M[5]; break;
        case 13:    cM[6] = M[6]; break;
        case 14:    cM[7] = M[7]; break;
        case 15:    cM[8] = M[8]; break;

        /*case 9:     cM[0] = 1.0; cM[1] = 0.0; break;
        case 10:    cM[2] = 0.0; cM[3] = 0.0; break;
        case 11:    cM[4] = 1.0; break;
        case 12:    cM[5] = 0.0; break;
        case 13:    cM[6] = 0.0; break;
        case 14:    cM[7] = 0.0; break;
        case 15:    cM[8] = 1.0; break;*/
    }

    __syncthreads();            // Synchronization...

    // Matrix multiplication.
    switch (threadIdx.x) {
        // First row.
        case 0: F[0] = cM[0] * T[0] + cM[1] * T[3] + cM[2] * T[6]; break;
        case 1: F[1] = cM[0] * T[1] + cM[1] * T[4] + cM[2] * T[7]; break;
        case 2: F[2] = cM[0] * T[2] + cM[1] * T[5] + cM[2] * T[8]; break;

        // Second row.
        case 3: F[3] = cM[3] * T[0] + cM[4] * T[3] + cM[5] * T[6]; break;
        case 4: F[4] = cM[3] * T[1] + cM[4] * T[4] + cM[5] * T[7]; break;
        case 5: F[5] = cM[3] * T[2] + cM[4] * T[5] + cM[5] * T[8]; break;

        // Third row.
        case 6: F[6] = cM[6] * T[0] + cM[7] * T[3] + cM[8] * T[6]; break;
        case 7: F[7] = cM[6] * T[1] + cM[7] * T[4] + cM[8] * T[7]; break;
        case 8: F[8] = cM[6] * T[2] + cM[7] * T[5] + cM[8] * T[8]; break;
    }

    __syncthreads();            // Synchronization...

    // Evaluation of a particle.
    // Every thread has to evaluate 64 pixels.
    float sub_sum = 0.0f; float sub_mask_sum = 0.0f;
    float e_intensity, e_mask;
    #pragma unroll
    for (int i = 0; i < (target->width_ * target->width_) / blockDim.x; i++) {
        int k = i * blockDim.x + threadIdx.x;

        // Coordinates...
        int x_p = k % target->width_;
        int y_p = k / target->width_;        

        // Coordinates of the pixel in the observation image.
        double coeff = 1.0 / (F[6] * (float) x_p + F[7] * (float) y_p + F[8]);
        double x_o = coeff * (F[0] * (float) x_p + F[1] * (float) y_p + F[2]);
        double y_o = coeff * (F[3] * (float) x_p + F[4] * (float) y_p + F[5]);


        // t_img1 = obs
        // t_img2 = fg
        // t_img3 = target
        // t_img4 = mask

        if (x_o < img->width_ && y_o < img->height_) {
            // Compute difference -- evaluate pixel correspondence.
            // sub_sum += abs(pattern->data_[y_p * (pattern->pitch_data_ / sizeof(float)) + x_p] - tex2D(t_img1, x_o, y_o));
            // without weighting (ONLY SSD is computed).
            e_intensity = 1.0 - abs(tex2D(t_img3, x_p, y_p) - tex2D(t_img1, x_o + 0.5, y_o + 0.5));
            //e_intensity = 1.0 - abs(tex2D(t_img4, x_p, y_p) - tex2D(t_img2, x_o + 0.5, y_o + 0.5));
            //e_mask = (tex2D(t_img4, x_p, y_p) > tex2D(t_img2, x_o + 0.5, y_o + 0.5)) ? tex2D(t_img2, x_o + 0.5, y_o + 0.5) : tex2D(t_img4, x_p, y_p);
            e_mask = (tex2D(t_img4, x_p, y_p) > tex2D(t_img2, x_o + 0.5, y_o + 0.5)) ? tex2D(t_img2, x_o + 0.5, y_o + 0.5) : tex2D(t_img4, x_p, y_p);
            //e_mask = tex2D(t_img2, x_o + 0.5, y_o + 0.5);


            sub_sum += exp(5.0 * e_mask) * (e_intensity * e_intensity);
            //sub_sum += exp(5.0 * e_mask) * (e_intensity * e_intensity);
            sub_mask_sum += e_mask;
        }
        else {
            sub_sum -= 1e6;
            sub_mask_sum -= 1e6;
        }
    }

    __syncthreads();            // Thread synchronization.

    // Compute fitness.
    p->fitness_temp_[i_p * target->width_ + threadIdx.x] = sub_sum;
    p->mask_sum_temp_[i_p * target->width_ + threadIdx.x] = sub_mask_sum;
}

// Evaluate particles..
// Img *src, Img *pattern, Particles *
__global__ void evaluateParticles(Particles *p, Img *pattern, Img *obs, float *M)
{
    int i_p = blockIdx.y * blockDim.x + blockIdx.x;
    // Transformation matrix.
    __shared__ double T[9];      // Patricle Matrix.
    __shared__ double cM[9];     // cached M
    __shared__ double F[9];      // Final matrix.

    double sx = p->s_x_[i_p];
    double sy = p->s_y_[i_p];
    double sin_yaw = sin(p->yaw_[i_p]);
    double cos_yaw = cos(p->yaw_[i_p]);
    double dx = p->x_[i_p];
    double dy = p->y_[i_p];
    double tc_x = -(pattern->width_ - 1.0f) / 2.0f;
    double tc_y = -(pattern->height_ - 1.0f) / 2.0f;

    // Computes transformation matrix... (I)   
    switch (threadIdx.x) {
        // First row.
        case 0: T[threadIdx.x] = sx * cos_yaw; break;
        case 1: T[threadIdx.x] = -sx * sin_yaw; break;
        case 2: T[threadIdx.x] = sx * (cos_yaw * tc_x - sin_yaw * tc_y) + dx; break;

        // Second row.
        case 3: T[threadIdx.x] = sy * sin_yaw; break;
        case 4: T[threadIdx.x] = sy * cos_yaw; break;
        case 5: T[threadIdx.x] = sy * (sin_yaw * tc_x + cos_yaw * tc_y) + dy; break;

        // Third row.
        case 6: T[threadIdx.x] = 0.0f; break;
        case 7: T[threadIdx.x] = 0.0f; break;
        case 8: T[threadIdx.x] = 1.0f; break;

        // Create cache
        case 9:     cM[0] = M[0]; cM[1] = M[1]; break;
        case 10:    cM[2] = M[2]; cM[3] = M[3]; break;
        case 11:    cM[4] = M[4]; break;
        case 12:    cM[5] = M[5]; break;
        case 13:    cM[6] = M[6]; break;
        case 14:    cM[7] = M[7]; break;
        case 15:    cM[8] = M[8]; break;

    }

    __syncthreads();            // Synchronization...

    // Matrix multiplication.
    switch (threadIdx.x) {
        // First row.
        case 0: F[0] = cM[0] * T[0] + cM[1] * T[3] + cM[2] * T[6]; break;
        case 1: F[1] = cM[0] * T[1] + cM[1] * T[4] + cM[2] * T[7]; break;
        case 2: F[2] = cM[0] * T[2] + cM[1] * T[5] + cM[2] * T[8]; break;

        // Second row.
        case 3: F[3] = cM[3] * T[0] + cM[4] * T[3] + cM[5] * T[6]; break;
        case 4: F[4] = cM[3] * T[1] + cM[4] * T[4] + cM[5] * T[7]; break;
        case 5: F[5] = cM[3] * T[2] + cM[4] * T[5] + cM[5] * T[8]; break;

        // Third row.
        case 6: F[6] = cM[6] * T[0] + cM[7] * T[3] + cM[8] * T[6]; break;
        case 7: F[7] = cM[6] * T[1] + cM[7] * T[4] + cM[8] * T[7]; break;
        case 8: F[8] = cM[6] * T[2] + cM[7] * T[5] + cM[8] * T[8]; break;
    }

    __syncthreads();            // Synchronization...


    // Evaluation of a particle.
    // Every thread has to evaluate 64 pixels.
    float sub_sum = 0.0;
    float t_sum;
    #pragma unroll    
    for (int i = 0; i < (pattern->width_ * pattern->width_) / blockDim.x; i++) {
        int k = i * blockDim.x + threadIdx.x;

        // Coordinates...
        int x_p = k % pattern->width_;
        int y_p = k / pattern->width_;
        int z_p = 1;

        // Coordinates of the pixel in the observation image.
        float x_o = (F[0] * (float) x_p) + (F[1] * (float) y_p) + (F[2] * (float) z_p);
        float y_o = (F[3] * (float) x_p) + (F[4] * (float) y_p) + (F[5] * (float) z_p);
        float z_o = (F[6] * (float) x_p) + (F[7] * (float) y_p) + (F[8] * (float) z_p);


        /*
        float dxf = (x_p - (pattern->width_ / 2.0)) / (float) (pattern->width_ / 2.0);
        float dyf = (y_p - (pattern->height_ / 2.0)) / (float)(pattern->height_ / 2.0);
        float w = 1.0 - sqrt(dxf * dxf + dyf * dyf);
        */


        // Compute difference -- evaluate pixel correspondence.        
        // sub_sum += abs(pattern->data_[y_p * (pattern->pitch_data_ / sizeof(float)) + x_p] - tex2D(t_img1, x_o, y_o));
        t_sum = (1.0 - abs(tex2D(t_img2, x_p, y_p) - tex2D(t_img1, x_o / z_o, y_o / z_o)));

        sub_sum += t_sum * t_sum;
    }

    __syncthreads();            // Thread synchronization.

    // Compute fitness.
    p->fitness_temp_[i_p * pattern->width_ + threadIdx.x] = sub_sum;
    // MyAtomicAdd(&(p->fitness_[i_p]), sub_sum);
}


__global__ void warp_perspective_kernel(Img *in, Img *out, double *M)
{
    unsigned int x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < in->width_ && y < in->height_) {
        double coeff = 1.0 / (M[6] * (double) x + M[7] * (double) y + M[8]);
        double x_s = coeff * (M[0] * (double) x + M[1] * (double) y + M[2]);
        double y_s = coeff * (M[3] * (double) x + M[4] * (double) y + M[5]);


        // This should be done without interpolation.
        // NOTE: (!!!)
        // Texture addressing is always done using floating point coordinates and the texture data is voxel centred, thus 0.5 is added to each coordinate to ensure
        // the read comes from the centroid of each interpolation area or volume within the texture.
        out->data_[y * (out->pitch_data_ / sizeof(float)) + x] = tex2D(t_img1, x_s + 0.5f , y_s + 0.5f);
    }
}

__global__ void warp_perspective_get_mask_kernel(unsigned char *data, int width, int height, size_t pitch_data, double *M, unsigned char in_val, unsigned char out_val)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < width && y < height) {
        // Compute coordinates.
        double coeff = 1.0 / (M[6] * (double) x + M[7] * (double) y + M[8]);
        double x_s = coeff * (M[0] * (double) x + M[1] * (double) y + M[2]);
        double y_s = coeff * (M[3] * (double) x + M[4] * (double) y + M[5]);


        if (x_s >= 0.0 && x_s <= width - 1.0 && y_s >= 0.0 && y_s <= height - 1.0) data[y * (pitch_data / sizeof(unsigned char)) + x] = in_val;
        else data[y * (pitch_data / sizeof(unsigned char)) + x] = out_val;
    }
}

#endif


