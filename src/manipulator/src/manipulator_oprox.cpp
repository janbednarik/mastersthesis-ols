/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// C++ libraries
#include <iostream>
#include <exception>
#include <string>
#include <cmath>

// project libraries
#include "manipulator_oprox.h"
#include "exceptions_ols.h"

#define DEBUG_MANIP_OPROX 1

using namespace std;

// Operational range of the manipulator.
const double ManipulatorOprox::MAX_AZIMUTH =    3250.0;
const double ManipulatorOprox::MIN_AZIMUTH =   -3150.0;
const double ManipulatorOprox::MAX_ELEVATION =  1130.0;
const double ManipulatorOprox::MIN_ELEVATION = -1550.0;
const double ManipulatorOprox::MAX_AZIMUTH_SPEED = 200.0;
const double ManipulatorOprox::MAX_ELEVATION_SPEED = 200.0;

const double ManipulatorOprox::MOTION_DELTA = 1.0;
const double ManipulatorOprox::FULL_TURN_MRAD = 2.0 * M_PI * 1000.0;
const double ManipulatorOprox::HALF_TURN_MRAD = M_PI * 1000.0;
const double ManipulatorOprox::MOVE_ABSOLUTE_DELTA = 0.1;

// Constants used for slowdown motion during absolute position control.
const double ManipulatorOprox::AP_MIN_DISTANCE = 3.0;
const double ManipulatorOprox::AP_MAX_DISTANCE = 50.0;
const double ManipulatorOprox::AP_MIN_VELOCITY = 1.0;
const double ManipulatorOprox::AP_MAX_VELOCITY = ManipulatorOprox::MAX_AZIMUTH_SPEED;
const double ManipulatorOprox::AP_LIN_A = (ManipulatorOprox::AP_MAX_VELOCITY - ManipulatorOprox::AP_MIN_VELOCITY) /
                                          (ManipulatorOprox::AP_MAX_DISTANCE - ManipulatorOprox::AP_MIN_DISTANCE);
const double ManipulatorOprox::AP_LIN_C = ManipulatorOprox::AP_MIN_DISTANCE;
const double ManipulatorOprox::AP_SIGM_A = 2.0;

const string ManipulatorOprox::commands[] = {
    "7,0003,0000,FF\r\n",
    "7,0003,0001,FF\r\n",
    "2,0003,0001,FF\r\n",
    "3,0003,0000,FF\r\n"
};

ManipulatorOprox::ManipulatorOprox(std::string port, uint32_t baudrate, uint32_t timeout) :
    responseReady_(true), controlState_(S_INIT), nextState_(S_INIT), readCorrectMsgTrials_(0), correctMsgReceived_(false),
    inMotion_(false), stepDuration_(ros::Duration(0.1)), zoomSpeed_(0), focusSpeed_(0)
{
//    serialIf = new serial::Serial(port, baudrate, serial::Timeout::simpleTimeout(timeout));
    serialIf = new serial::Serial(serial_if_name_, baudrate, serial::Timeout::simpleTimeout(timeout));

    // If the read timeout is hihger than 50 ms, keystrokes (keyboard) are often missed!
    // setTimeout(inter_byte_timeout, read_timeout_constant, read_timeout_multiplier, write_timeout_constant, write_timeout_multiplier)
    serialIf->setTimeout(0, 50, 0, 50, 0);

    // debug
    cout << "Serial port open: " << (serialIf->isOpen() ? "YES" : "NO") << endl;

    // Read messages until the message of correct format is received.
    while(!checkResponseFormat(serialIf->readline(RESPONSE_LENGTH, "\n"))) {
        readCorrectMsgTrials_ += 1;
        if(readCorrectMsgTrials_ > MAX_INIT_READ_TRIALS) {
            throw(ExceptionOLS_MaxAttemptsReadCorrectMsg(MAX_INIT_READ_TRIALS));
        }
    }
    readCorrectMsgTrials_ = 0;

    responseReadyTimer_ = nh_->createTimer(ros::Duration(0.09), &ManipulatorOprox::resetResponseReady, this);
    stepTimer_ = nh_->createTimer(stepDuration_, &ManipulatorOprox::stopMotion, this);
    stepTimer_.stop();

    // Create regulator controlling the motion of the manipualtor during tracking.
    SpeedFunc* regSpeedFunc         = new SpeedFuncPow(0.14, 0.2, 0.008, 1.5, 0.002);
    Position2DPredictor* predictor  = new Position2DPredictor(10);
    regulator_ = new Regulator(regSpeedFunc, predictor);

    // Create speed function for progressive motion control using joystick.
    joySpeed_ = new SpeedFuncPow(1.0, MAX_AZIMUTH_SPEED / 1000.0, 0.005, 10.0, 0.01);
}

ManipulatorOprox::~ManipulatorOprox()
{        
    delete serialIf;
    if(joySpeed_)       delete joySpeed_;
    if(regulator_)      delete regulator_;
}

void ManipulatorOprox::update()
{
    if(responseReady_) {
        responseReady_ = false;

        // Read the message from the manipulator.
        string response = serialIf->readline(RESPONSE_LENGTH, "\n");
        correctMsgReceived_ = checkResponseFormat(response);

        if(correctMsgReceived_) {            
            // Save previous azimuth and elevation
            oldAzimuth_     = azimuth_;
            oldElevation_   = elevation_;

            // Get current azimuth and elevation
            double azi = 0.0;
            double ele = 0.0;
            extractAzimuthElevation(response, azi, ele);
            setPositionStamped(azi, ele, ros::Time::now());

            // Check whether the manipulator is in motion.
            inMotion_ = inMotion();
        }

        // Check whether pan and tilt is out of allowed motion range and if so, halt.
        if(!movingInRange(azimuth_,   aziSpeed_, MIN_AZIMUTH,   MAX_AZIMUTH))   haltPan();
        if(!movingInRange(elevation_, eleSpeed_, MIN_ELEVATION, MAX_ELEVATION)) haltTilt();

        switch (controlState_)
        {
            case S_INIT:
                send(commands[MANIPULATOR_POWER_UP]);
                controlState_ = S_WAIT_CORRECT_MSG;
#if DEBUG_MANIP_OPROX
                nextState_ = S_MANUAL_CONTROL;
#else
                nextState_ = S_FIND_INDEX;
#endif
                break;               

            case S_WAIT_CORRECT_MSG:
                if(!correctMsgReceived_) {
                    readCorrectMsgTrials_ += 1;
                    std::cout << "read trials = " << readCorrectMsgTrials_ << std::endl;
                    if(readCorrectMsgTrials_ > MAX_INIT_READ_TRIALS) {
                        throw ExceptionOLS_MaxAttemptsReadCorrectMsg(MAX_INIT_READ_TRIALS);
                    }
                } else {
                    readCorrectMsgTrials_ = 0;
                    controlState_ = nextState_;
                }

                break;

            case S_INIT_CHECK_POWER:
                // Try to move the manipulator (tilt) in one of both possible directions and check whether the position changed.

                // debug
                ROS_INFO_STREAM("Checking whether manipulator is powered up");

                changePosStart_ = ros::Time::now();
                moveRelative(0.0, 5.0);
                controlState_ = S_INIT_WAIT_POSITION_CHANGED;
                break;

            case S_INIT_WAIT_POSITION_CHANGED:
                if(updateAbsoluteMotion()) {
#if DEBUG_MANIP_OPROX
                    controlState_ = S_MANUAL_CONTROL;
#else
                    controlState_ = S_FIND_INDEX;
#endif
                } else if(ros::Time::now() - changePosStart_ > ros::Duration(5, 0)) {
                    controlState_ = S_INIT;
                    throw(ExceptionOLS_InitializationFailed("update: update: Initialization failed: Manipulator failed to power up during initialization. "
                                                            "Try to power the manipulator down and up again"));
                }
                break;

            case S_FIND_INDEX:

                // debug
                ROS_INFO_STREAM("Searching for index");

                send(commands[MANIPULATOR_FIND_INDEX]);
                controlState_ = S_WAIT_INDEX_FOUND;
                break;

            case S_WAIT_INDEX_FOUND:
                if(!inMotion_ && (ros::Time::now() - lastMotion_ > ros::Duration(3, 0))) {
                    controlState_ = S_RESET;
                }
                break;

            case S_RESET:

                // debug
                ROS_INFO_STREAM("Reseting azimuth and elevation");

                send(commands[MANIPULATOR_RESET_ZERO]);

                // debug
                ROS_INFO_STREAM("Manipulator is READY");

                controlState_ = S_MANUAL_CONTROL;
                break;

            case S_MANUAL_CONTROL:
                break;

            case S_ABSOLUTE_CONTROL:
                if(updateAbsoluteMotion()) {
                    controlState_ = S_MANUAL_CONTROL;
                }
                break;

            case S_POWERED_DOWN:
                break;

            default:
                break;
        }

        if(!correctMsgReceived_) {
            throw ExceptionOLS("Wrong format of the response of the manipulator. Received response: >>>" + response + "<<<");
        }

        Manipulator::update();
    }

    // Lense control
    if(zooming_) {
        zoom(zoomSpeed_);
    }

    if(focusing_) {
        focus(focusSpeed_);
    }
}

bool ManipulatorOprox::inMotion()
{
    bool motion = std::abs(oldAzimuth_ - azimuth_) > MOTION_DELTA ||
                  std::abs(oldElevation_ - elevation_) > MOTION_DELTA;

    if(motion) {
        lastMotion_ = ros::Time::now();
    }

    return motion;
}

void ManipulatorOprox::move(double azimuth, double elevation)
{
    ostringstream strsAzi, strsEle;
    strsAzi << setfill('0') << setw(4) << internal;
    strsEle << setfill('0') << setw(4) << internal;

    command.erase();
    command = "1,0000,0000,FF\r\n";

    int azi = (int)round(getValInRange(azimuth,   -MAX_AZIMUTH_SPEED,   MAX_AZIMUTH_SPEED,   0.0));
    int ele = (int)round(getValInRange(elevation, -MAX_ELEVATION_SPEED, MAX_ELEVATION_SPEED, 0.0));


    if(movingInRange(azimuth_, azi, MIN_AZIMUTH, MAX_AZIMUTH)) {
        aziSpeed_ = azi;
        strsAzi << azi;
        command.replace(2, 4, strsAzi.str());
    }

    if(movingInRange(elevation_, ele, MIN_ELEVATION, MAX_ELEVATION)) {
        eleSpeed_ = ele;
        strsEle << ele;
        command.replace(7, 4, strsEle.str());
    }

    send(command);
}

bool ManipulatorOprox::moveAbsolute(double azimuth, double elevation)
{
    if(controlState_ == S_MANUAL_CONTROL ||
       controlState_ == S_ABSOLUTE_CONTROL ||
       controlState_ == S_INIT_CHECK_POWER) {

        controlState_ = S_ABSOLUTE_CONTROL;

        if(!valInRange(azimuth, MIN_AZIMUTH, MAX_AZIMUTH)) {
            cerr << "Required azimuth position " << azimuth <<
                    " mrad is not within allowed range [" <<
                    MIN_AZIMUTH << ", " << MAX_AZIMUTH <<
                    "]. It will be clipped." << endl;
            azimuth = getValInRange(azimuth, MIN_AZIMUTH, MAX_AZIMUTH, 10.0);
        }

        if(!valInRange(elevation, MIN_ELEVATION, MAX_ELEVATION)) {
            cerr << "Required elevation position " << elevation <<
                    " mrad is not within allowed range [" <<
                    MIN_ELEVATION << ", " << MAX_ELEVATION <<
                    "]. It will be clipped." << endl;

            elevation = getValInRange(elevation, MIN_ELEVATION, MAX_ELEVATION, 10.0);
        }

        initAbsoluteMotion();

        targetAzimuth_ = azimuth;
        targetElevation_ = elevation;

        return true;
    }

    return false;
}

bool ManipulatorOprox::updateAbsoluteMotion()
{
    bool moveRequired = false;
    double newVel = 0.0;

    if(!azimuthReached_ || !elevationReached_) {
        double azimuthDiff   = getAzimuth() - targetAzimuth_;
        double elevationDiff = getElevation() - targetElevation_;

        if(!azimuthReached_) {
            newVel = dist2vel(azimuthDiff);
            if(newVel != aziSpeed_ || newVel == 0.0) {
                aziSpeed_ = newVel;
                moveRequired = true;
                if(aziSpeed_ == 0.0) {
                    azimuthReached_ = true;
                }
            }
        }

        if(!elevationReached_) {
            newVel = dist2vel(elevationDiff);
            if(newVel != eleSpeed_ || newVel == 0.0) {
                eleSpeed_ = newVel;
                moveRequired = true;
                if(eleSpeed_ == 0.0) {
                    elevationReached_ = true;
                }
            }
        }

        if(moveRequired) {
            move(aziSpeed_, eleSpeed_);
        }
    }

    return azimuthReached_ && elevationReached_;
}

bool ManipulatorOprox::moveRelative(double azimuth, double elevation)
{
    return moveAbsolute(azimuth + azimuth_, elevation + elevation_);
}

void ManipulatorOprox::step(int azimuth, int elevation)
{
    double stepSpeed = 1.0;

    move((azimuth   > 0) ? stepSpeed : ((azimuth   < 0) ? -stepSpeed : 0.0),
                 (elevation > 0) ? stepSpeed : ((elevation < 0) ? -stepSpeed : 0.0));
}

void ManipulatorOprox::haltPan()
{
    // debug
    cout << "HALTING PAN!" << endl;

    if(controlState_ == S_ABSOLUTE_CONTROL) {
        stopAbsoluteMotion();
    }
    controlState_ = S_MANUAL_CONTROL;
    move(0.0, eleSpeed_);
}

void ManipulatorOprox::haltTilt()
{
    // debug
    cout << "HALTING TILT!" << endl;

    if(controlState_ == S_ABSOLUTE_CONTROL) {
        stopAbsoluteMotion();
    }
    controlState_ = S_MANUAL_CONTROL;
    move(aziSpeed_, 0.0);
}

void ManipulatorOprox::send(const string cmd)
{
    // Send command
    size_t bytesWritten = serialIf->write(cmd);

    if(bytesWritten != 16) {
        throw(ExceptionOLS(string("Sending command '") + cmd + "' failed."));
    }
}

bool ManipulatorOprox::checkResponseFormat(string msg)
{
    bool correct = false;

    if(msg.length() == RESPONSE_LENGTH) {
        /* matching regex "^0,[0-9-][0-9][0-9][0-9][0-9][0-9],[0-9-][0-9][0-9][0-9][0-9][0-9],FF\r\n$"
           <regex> library (C++11) does not work in gcc 4.8.4 */
        correct =   isdigit(msg[0])  &&
//                    msg[0]  == '0'   &&
                    msg[1]  == ','   &&
                    msg[2]  == '-' || isdigit(msg[2])  &&
                    isdigit(msg[3])  &&
                    isdigit(msg[4])  &&
                    isdigit(msg[5])  &&
                    isdigit(msg[6])  &&
                    isdigit(msg[7])  &&
                    msg[8]  == ','   &&
                    msg[9]  == '-' || isdigit(msg[9])  &&
                    isdigit(msg[10]) &&
                    isdigit(msg[11]) &&
                    isdigit(msg[12]) &&
                    isdigit(msg[13]) &&
                    isdigit(msg[14]) &&
                    msg[15] == ','   &&
                    msg[16] == 'F'   &&
                    msg[17] == 'F'   &&
                    msg[18] == '\r'  &&
                    msg[19] == '\n';
    }

    return correct;
}

void ManipulatorOprox::extractAzimuthElevation(string& msg, double &azimuth, double &elevation)
{
    int azi, ele;

    try {
        azi = stoi(msg.substr(2, 6), nullptr);
        ele = stoi(msg.substr(9, 6), nullptr);
    } catch(const invalid_argument& ia) {
        cerr << "Exception: Invalid argument: " << ia.what() << endl;
    }

    azimuth = (double)azi / 10.0;
    elevation = (double)ele / 10.0;
}

void ManipulatorOprox::initAbsoluteMotion()
{
    aziSpeed_ = 0.0;
    eleSpeed_ = 0.0;
    azimuthReached_   = false;
    elevationReached_ = false;
    oldAzimuthDiff_   = std::numeric_limits<double>::max();
    oldElevationDiff_ = std::numeric_limits<double>::max();
}

inline double ManipulatorOprox::getValInRange(double val, double min, double max, double margin)
{
    if(margin < 0.0) throw(ExceptionOLS("getPosInRange: margin must be a positive value."));
    if(max < min)    throw(ExceptionOLS("getPosInRange: max cannot be lower value than min."));

    return (std::abs(std::min(max, std::max(min, val))) - margin) * ((val < 0.0) ? -1.0 : 1.0);
}

double ManipulatorOprox::dist2vel(double distance, eSlowdownFunction f)
{
    double speed;
    bool positive = (distance >= 0.0);
    distance = std::abs(distance);

    if(distance <= (MOVE_ABSOLUTE_DELTA + 0.000001)) { // Add small constant to overcome FP imprecision
        speed = 0.0;
    } else {
        switch(f) {
            // Linear funcion
            //
            // y = a * (x - c)
            //
            // Best settings found so far (empiric):
            // AP_MAX_DISTANCE = 50
            // AP_MIN_DISTANCE = 2
            // AP_MAX_VELOCITY = 200
            // AP_MIN_VELOCITY = 1
            //
            case SD_LIN:
                distance = max(min(distance, AP_MAX_DISTANCE), AP_MIN_DISTANCE) - AP_MIN_DISTANCE;
                speed = (AP_LIN_A * distance + AP_MIN_VELOCITY) * (positive ? -1 : 1);
                break;

            case SD_EXP:
                break;

            // Logarithmic function
            case SD_LOG:
                distance = max(min(distance, AP_MAX_DISTANCE), AP_MIN_DISTANCE);
                speed = ((log(distance / AP_MIN_DISTANCE) / log(AP_MAX_DISTANCE / AP_MIN_DISTANCE)) *
                         (AP_MAX_VELOCITY - AP_MIN_VELOCITY) + AP_MIN_VELOCITY) *
                        (positive ? -1 : 1);
                break;

            // Truncated sigmoid function
            //  - sigmoid function if distance in (AP_MIN_DISTANCE, AP_MAX_DISTANCE)
            //  - AP_MIN_VELOCITY or AP_MAX_VELOCITY otherwise
            //
            //          1
            // --------------------
            //           (-a*x)
            //      1 + e
            //
            // Best settings found so far (empiric):
            // AP_MAX_DISTANCE = 50.0
            // AP_MIN_DISTANCE = 3.0
            // AP_MAX_VELOCITY = 200.0
            // AP_MIN_VELOCITY = 1.0
            // AP_SIGM_A = 2.0
            //
            case SD_SIGM:
                if(distance >= AP_MAX_DISTANCE) {
                    speed = AP_MAX_VELOCITY * (positive ? -1 : 1);
                } else if(distance <= AP_MIN_DISTANCE) {
                    speed = AP_MIN_VELOCITY * (positive ? -1 : 1);
                } else {
                    speed = ((1.0 / (1.0 + exp(-AP_SIGM_A * ( -1.0 + (( distance - AP_MIN_DISTANCE) / (AP_MAX_DISTANCE - AP_MIN_DISTANCE)) * 2.0 )))) *
                             (AP_MAX_VELOCITY - AP_MIN_VELOCITY) + AP_MIN_VELOCITY) *
                            (positive ? -1 : 1);
                }
                break;
        }
    }

    return speed;
}

inline bool ManipulatorOprox::valInRange(double val, double min, double max)
{
    if(max < min) throw(ExceptionOLS("posInRange: max cannot be lower value than min."));

    return val >= min && val <= max;
}

inline bool ManipulatorOprox::movingInRange(double pos, double speed, double min, double max)
{
    if(max < min) throw(ExceptionOLS("movInRange: max cannot be lower value than min."));

    return (pos < max || speed <= 0.0) && (pos > min || speed >= 0.0);
}

void ManipulatorOprox::onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg)
{
    double azi = 0.0;
    double ele = 0.0;

    double azSpSc;
    double elSpSc;

    switch(mvMsg->mode) {
        case msgs::ManipulatorMove::MODE_ABSOLUTE:
            break;

        case msgs::ManipulatorMove::MODE_RELATIVE:
            break;

        case msgs::ManipulatorMove::MODE_CONTINUOUS:
            switch(mvMsg->speedType) {
                case msgs::ManipulatorMove::SPEED_TYPE_ABSOLUTE:
                    break;

                case msgs::ManipulatorMove::SPEED_TYPE_PROPORTIONAL:
                    azSpSc = mvMsg->aziSpeed;
                    elSpSc = mvMsg->eleSpeed;

                    if(mvMsg->aziSpeed < -1.0 || mvMsg->aziSpeed > 1.0) {
                        azSpSc = std::max(std::min(1.0, azSpSc), -1.0);
                        ROS_WARN_STREAM("onManipMove: aziSpeed not in range [-1.0, 1.0], will be clipped.");
                    }

                    if(mvMsg->eleSpeed < -1.0 || mvMsg->eleSpeed > 1.0) {
                        elSpSc = std::max(std::min(1.0, elSpSc), -1.0);
                        ROS_WARN_STREAM("onManipMove: eleSpeed not in range [-1.0, 1.0], will be clipped.");
                    }

                    azi = azSpSc * MAX_AZIMUTH_SPEED;
                    ele = elSpSc * MAX_ELEVATION_SPEED;

                    break;

                default:
                    ROS_ERROR_STREAM("onManipMove: Incorrect 'speedType' selected in ManipulatorMove message");
                    return;
            }

            move(azi, ele);

            break;

        default:
            ROS_ERROR_STREAM("onManipMove: Incorrect 'mode' selected in ManipulatorMove message");
            return;
    }
}

void ManipulatorOprox::onKeyDown(const keyboard::Key::ConstPtr key)
{
    try {
        switch(key->code) {
            // Motion control keys: UP, DOWN, LEFT, RIGHT
            case keyboard::Key::KEY_LEFT:
                switch(precState_) {                    
                    case STEP:
                        stepTimer_.setPeriod(stepDuration_);
                        stepTimer_.start();
                        step(1, 0);
                        break;
                    case CONTINUOUS:
                        move(MAX_AZIMUTH_SPEED, 0.0);
                        break;
                }
                break;
            case keyboard::Key::KEY_RIGHT:
                switch(precState_) {
                    case STEP:
                        stepTimer_.setPeriod(stepDuration_);
                        stepTimer_.start();
                        step(-1, 0);
                        break;
                    case CONTINUOUS:
                        move(-MAX_AZIMUTH_SPEED, 0.0);
                        break;
                }
                break;
            case keyboard::Key::KEY_UP:
                switch(precState_) {
                    case STEP:
                        stepTimer_.setPeriod(stepDuration_);
                        stepTimer_.start();
                        step(0, 1);
                        break;
                    case CONTINUOUS:
                        move(0.0, MAX_ELEVATION_SPEED);
                        break;
                }
                break;
            case keyboard::Key::KEY_DOWN:
                switch(precState_) {
                    case STEP:
                        stepTimer_.setPeriod(stepDuration_);
                        stepTimer_.start();
                        step(0, -1);
                        break;

                    case CONTINUOUS:
                        move(0.0, -MAX_ELEVATION_SPEED);
                        break;
                }
                break;

            // Move manipulator to the 0 position
            case keyboard::Key::KEY_e:
                moveAbsolute(0.0, elevation_);
                break;
            case keyboard::Key::KEY_a:
                moveAbsolute(azimuth_, 0.0);
                break;

            // Force stop the motion
            case keyboard::Key::KEY_SPACE:
                forceStop();

            // Find position 'index'
            case keyboard::Key::KEY_i:
                send(commands[MANIPULATOR_FIND_INDEX]);
                break;

            // Reset position to zero
            case keyboard::Key::KEY_r:
                send(commands[MANIPULATOR_RESET_ZERO]);                
                break;

            // Power up the motors
            case keyboard::Key::KEY_p:
                send(commands[MANIPULATOR_POWER_UP]);
                break;

            case keyboard::Key::KEY_s:
                precState_ = STEP;
                break;
            case keyboard::Key::KEY_c:
                precState_ = CONTINUOUS;
                break;

            // Lense
            case keyboard::Key::KEY_PAGEUP:
                zooming_ = true;
                zoomSpeed_ = ZOOM_STEP;
                break;
            case keyboard::Key::KEY_PAGEDOWN:
                zooming_ = true;
                zoomSpeed_ = -ZOOM_STEP;
                break;
            case keyboard::Key::KEY_HOME:
                focusing_ = true;
                focusSpeed_ = FOCUS_STEP;
                break;
            case keyboard::Key::KEY_END:
                focusing_ = true;
                focusSpeed_ = -FOCUS_STEP;
                break;
            case keyboard::Key::KEY_INSERT:
                moveIris(IRIS_MAX_SPEED, 1);
                break;
            case keyboard::Key::KEY_DELETE:
                moveIris(IRIS_MAX_SPEED, -1);
                break;

            default:
                break;
        }
    } catch (ExceptionOLS &e) {
        cerr << "ExceptionOLS: " << e.what() << endl;
    }
}

void ManipulatorOprox::onKeyUp(const keyboard::Key::ConstPtr key)
{
    switch(key->code) {
        // Motion control keys: UP, DOWN, LEFT, RIGHT
        case keyboard::Key::KEY_LEFT:
        case keyboard::Key::KEY_RIGHT:
        case keyboard::Key::KEY_UP:
        case keyboard::Key::KEY_DOWN:
            switch(precState_) {
                case STEP: break;
                case CONTINUOUS: move(0.0, 0.0); break;
            }
            break;

        // Lense
        case keyboard::Key::KEY_PAGEUP:
        case keyboard::Key::KEY_PAGEDOWN:
            zooming_ = false;
            zoomSpeed_ = 0;
            break;

        case keyboard::Key::KEY_HOME:
        case keyboard::Key::KEY_END:
            focusing_ = false;
            focusSpeed_ = 0;
            break;

        case keyboard::Key::KEY_INSERT:
        case keyboard::Key::KEY_DELETE:
            changingIris_   = false;
            moveIris(0, 0);
            break;

        default:
            break;
    }
}

void ManipulatorOprox::onJoy(const sensor_msgs::Joy::ConstPtr& joy)
{
    static bool buttonDown = false;

    // Only react on joystick when the manipulator is in these 'ready' states.
    if(controlState_ == S_MANUAL_CONTROL ||
       controlState_ == S_ABSOLUTE_CONTROL ||
       controlState_ == S_POWERED_DOWN) {
        if(!buttonDown) {
            if (joy->buttons[0])
            {
                buttonDown = true;

            }
            else if (joy->buttons[1])
            {
                buttonDown = true;
                ;
            }
            else if (joy->buttons[6])
            {
                send(commands[MANIPULATOR_FIND_INDEX]);
                buttonDown = true;
            }
            else if (joy->buttons[8])
            {
                buttonDown = true;
                send(commands[MANIPULATOR_RESET_ZERO]);
            }

            else
            {                
                if (controlState_ == S_MANUAL_CONTROL || controlState_ == S_ABSOLUTE_CONTROL) {
                    if (controlState_ == S_ABSOLUTE_CONTROL) {
                        stopAbsoluteMotion();
                    }

                    move(joy->axes[0] * MAX_AZIMUTH_SPEED, joy->axes[1] * MAX_ELEVATION_SPEED);

                    controlState_ = S_MANUAL_CONTROL;
                }
            }
        } else {
            buttonDown = false;
        }
    }
}

void ManipulatorOprox::stopMotion(const ros::TimerEvent&)
{
    move(0.0, 0.0);
    stepTimer_.stop();
}

// debug
void ManipulatorOprox::forceStop()
{
    // debug
    cout << "Force stop the manipulator" << endl;
    move(0.0, 0.0);
}

string ManipulatorOprox::controlStateStr(state s)
{
    string stateStr = "";

    switch(s) {
        case S_INIT:                            stateStr = "S_INIT";                        break;
        case S_WAIT_CORRECT_MSG:                stateStr = "S_WAIT_CORRECT_MSG";            break;
        case S_INIT_CHECK_POWER:                stateStr = "S_INIT_CHECK_POWER";            break;
        case S_INIT_WAIT_POSITION_CHANGED:      stateStr = "S_INIT_WAIT_POSITION_CHANGED";  break;
        case S_FIND_INDEX:                      stateStr = "S_FIND_INDEX";                  break;
        case S_WAIT_INDEX_FOUND:                stateStr = "S_WAIT_INDEX_FOUND";            break;
        case S_RESET:                           stateStr = "S_RESET";                       break;
        case S_MANUAL_CONTROL:                  stateStr = "S_MANUAL_CONTROL";              break;
        case S_ABSOLUTE_CONTROL:                stateStr = "S_ABSOLUTE_CONTROL";            break;
        case S_POWERED_DOWN:                    stateStr = "S_POWERED_DOWN";                break;
        default:                                stateStr = "UNKNONWN_STATE";                break;
    }

    return stateStr;
}
