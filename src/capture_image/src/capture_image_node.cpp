// ROS libraries
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

// C++ libraries
#include <iostream>
#include <vector>
#include <exception>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

const int SPIN_RATE = 30;

const std::string IMAGE_RAW_TOPIC = "camera/image_raw";
const std::string WINDOW_NAME = "image";

bool quit = false;

cv::Mat newestFrame;

void onImage(const sensor_msgs::Image::ConstPtr img) {
    newestFrame = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8)->image;
}

int main(int argc, char *argv[])
{
    int imgIdx = 0;

    ros::init(argc, argv, "image_capture_node");

    ros::NodeHandle nh_;
    ros::Subscriber onImgSub_ = nh_.subscribe(IMAGE_RAW_TOPIC, 1, &onImage);

    cv::namedWindow(WINDOW_NAME);

    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);

    ros::start();
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        ros::spinOnce();

        if(newestFrame.rows != 0) {
            cv::imshow(WINDOW_NAME, newestFrame);
        }

        bool success = false;

        int key = cv::waitKey(10) & 255;
        switch (key) {
            case 10:    // Enter
                try {
                    success = cv::imwrite(std::string("img_") +
                                          std::to_string(imgIdx) +
                                          std::string(".jpg"),
                                          newestFrame, compression_params);

                    if(success) {
                        ROS_INFO_STREAM("Saved image");
                        imgIdx++;
                    }
                } catch (std::runtime_error& ex) {
                    ROS_ERROR_STREAM("Exception converting image to jpg format: " << ex.what());
                }

                break;

            case 113:   // q
                quit = true;
                break;

            case 255:
                break;

            default:
                break;
        }

        if(quit) break;

        looper.sleep();        
    }

    ros::shutdown();

    return 0;
}

//<node pkg="rosbag" type="play" name="bag_player" output="screen"
//args="--loop --clock --delay=0 --start=1 --rate=1.0 $(arg input_base)/$(arg input)">
//    <remap from="/camera_unit_0/camera/image_raw" to="/camera/image_raw" />
//</node>
