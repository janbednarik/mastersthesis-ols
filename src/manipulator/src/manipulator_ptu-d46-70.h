/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef MANIPULATOR_PTUD4670_H
#define MANIPULATOR_PTUD4670_H

// Unless the CVUT control unit is used as the interim to communicate
// with the Flir PTU-D46-70 manipulator, set USE_CRC 0.
#define USE_CVUT_CONTROL_UNIT 0

// Initialization takes cca 120 seconds, this it can be switched off.
#define INITIALIZE_CONTROL_UNIT 0

// Defines whether function initMotionProps shoul be called at startup (takes long time).
#define INITIALIZE_MOTION_PROPS 0

// Debugging manipulator PTU-D46-70. Changed:
//  - different implementation of onJoy()
#define DEBUG_MANIP_PTUD4670 0

// Specifies the implementation of motion control, velocity control or absolute
// position control. In velocity control mode it is note possible to exceed
// the hardware pan/tilt position limits even though the limits are disabled.
#define VELOCITY_CONTROL 0

// If set to 1, the serial interface is not opened and the real HW manipulator
// is not used at all. This option is only used to visualize the static
// manipulator model without the need to have the real HW.
#define SIMULATION 0

// Serial library (http://docs.ros.org/jade/api/serial/html/)
#include <serial/serial.h>

// ROS libraries
#include <ros/ros.h>
#include <keyboard/Key.h>
#include <sensor_msgs/Joy.h>

// Qt libraries
#include <QByteArray>

// Eigen library
#include <eigen3/Eigen/Eigen>

// Project libraries.
#include "manipulator.h"

using namespace std;

//! \brief The ManipulatorPtuD4670 class Controlling the motion of the manipulator Flir PTU D46-70
//!
class ManipulatorPtuD4670: public Manipulator
{
public:
    // Manipulator motion properties forced by the Flir control unit
    static const double AZI_UPPER_SPEED;
    static const double AZI_LOWER_SPEED;
    static const double AZI_BASE_SPEED;
    static const double AZI_ACCELERATION;
    static const double ELE_UPPER_SPEED;
    static const double ELE_LOWER_SPEED;
    static const double ELE_BASE_SPEED;
    static const double ELE_ACCELERATION;

    // Manipulator properties set by this program
    static const double MAX_AZIMUTH_SPEED;      //!< Maxium allowed azimuth motion speed [mrad/s].
    static const double MAX_ELEVATION_SPEED;    //!< Maxium allowed elevation motion speed [mrad/s].
    static const double MAX_AZIMUTH;            //!< Maximum reachable azimuth [mrad] (given by construction constraints).
    static const double MIN_AZIMUTH;            //!< Minimum reachable azimuth [mrad] (given by construction constraints).
    static const double MAX_ELEVATION;          //!< Maximum reachable elevation [mrad] (given by construction constraints).
    static const double MIN_ELEVATION;          //!< Minimum reachable elevation [mrad] (given by construction constraints).

    //! Motion modes of the manipualtor
    enum eMotionState {
        INDEPENDENT_CTRL,                       //!< 0xCC - SET_INDEPENDENT_CONTROL_MODE
        PURE_VELOCITY_CTRL                      //!< 0xCD - SET_PURE_VELOCITY_CONTROL_MODE
    };

    //! Position contorl modes of the manipulator
    enum ePositionControlMode {
        SPEED_CONTROL,
        ABS_POS_CONTROL
    };

    //! Cartesian 3dim coordinate system axes names.
    enum eAxis {
        X = 0,
        Y = 1,
        Z = 2
    };

public:
    //! \brief ManipulatorPtuD4670 Constructor.
    //!     - Powers up the manipulator
    //!     - Initializes the stream of incoming messages.
    //! \param port
    //! \param baudrate
    //! \param timeout
    //!
#if USE_CVUT_CONTROL_UNIT
    ManipulatorPtuD4670(std::string port = "/dev/ttyUSB0", uint32_t baudrate = 115200, uint32_t timeout = 1000);
#else
    ManipulatorPtuD4670(std::string port = "/dev/ttyUSB0", uint32_t baudrate = 9600, uint32_t timeout = 1000);
#endif

    //! \brief ~ManipulatorPtuD4670 Destructor.
    //!     - Powers down the manipulator.
    //!
    virtual ~ManipulatorPtuD4670();

    //! \brief update Reads the message from the manipulator and stores current position.
    //!
    virtual void update();

    //! \brief move
    //! \param azimuth
    //! \param elevation
    //!
    virtual void move(double azimuth, double elevation);

    //! \brief moveAbsolute Moves the manipulator to the given absolute position [mrad].
    //! \param azimuth
    //! \param elevation
    //!
    virtual bool moveAbsolute(double azimuth, double elevation);

    //! \brief moveRelative Moves the manipulator relatively with regards to the current position [mrad].
    //! \param azimuth
    //! \param elevation
    //!
    virtual bool moveRelative(double azimuth, double elevation);

    //! \brief step Performs one step in the azimuth, elevation or both at the same time
    //! in the given direction. Here a "step" means one step in the case of stepper
    //! motors or the smallest step given by the maximum resolution in case of the
    //! other motors.
    //! \param azimuth >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //! \param elevation >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //!
    virtual void step(int azimuth, int elevation);

    //! \brief haltPan Halts pan motion and sets the azimuthal speed appropriately.
    //!
    virtual void haltPan();

    //! \brief haltTilt Halts tilt motion and sets the elevation speed appropriately.
    //!
    virtual void haltTilt();

    //! \brief onManipMove
    //! \param mvMsg
    //!
    virtual void onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg);

private:
    static const int    AZIMUTH_SECTIONS;  //!< The number of fractions of the azimuth motion span.
    static const double AZIMUTH_SECTION_W; //!< The width [mrad] of one azimuth section.

    serial::Serial* serialIf_;   //!< Serial interface, communication with the manipulator.

    eMotionState motionState_;              //!< Motion control mode of the manipulator.
    ePositionControlMode posControlMode_;   //!< Current position control mode.

    // Manipulator properties
    double aziStep_;            //!< Current steper motor azimuthal step [mrad].
    double eleStep_;            //!< Current steper motor elevation step [mrad].
    int aziSpeedSteps_;      //!< Current azimuthal speed [steps/s].
    int eleSpeedSteps_;      //!< Current elevation speed [steps/s].


    // Properties for checking whether manipulator is in motion
    bool panInMotion_;          //!< Panning in progress.
    bool tiltInMotion_;         //!< Tilting in progress.
    double lastAzimuth_;        //!< Last observed azimuth.
    double lastElevation_;      //!< Last observed elevation.

    // Properties for motion controlling using absolute motion control (INDEPENDENT_CONTROL_MODE)
    double *aziSections_;
    int aziSectionIdx_;
    double aziTargetSectionBoundary_;

    // Absolute motion
    double absMotionAziTarget_;         //!< Azimuth   target of last absolute motion command [mrad].
    double absMotionEleTarget_;         //!< Elevation target of last absolute motion command [mrad].
    double absMotionCurrentAziTarget_;  //!< Current azimuth target [mrad].
    bool absAziReached_;                //!< Flag specifying whether the required absolute azimuth   position was already reached.
    bool absEleReached_;                //!< Flag specifying whether the required absolute elevation position was already reached.

    // ------------------------------------- DEBUG ---------------------------------------- //

    // Testing manipulator motors synchrony when carrying heavy payload
    bool testSynchrony_;
    double testSynAziStep_;
    double testSynEleStep_;
    int  testSynSteps_;
    int  testSynStep_;    

private:    

    // ----------------------------- Manipulator motion --------------------------------- //
    //! \brief setMotionState Sets the motion state of the manipulator variable and
    //! sends the corresponding command to the manipulator. If current motion
    //! state equals 'state', nothing is done.
    //! \param state New motion state.
    //!
    void setMotionState(eMotionState state);

    //! \brief command Sends the 'cmd' message to the manipulator, reads
    //! the response message and checks whether it equals '0x00'.
    //! If not or if the manipulator responds with error message (the message
    //! includes character '!') the excpetion is thrown.
    //! \param cmd command
    //!
    void command(string cmd, uint32_t readTimeout = 0);

    //! \brief query Sends the query message to the manipulator, reads
    //! the response message and returns it.
    //! \param cmd
    //! \param respLen
    //! \return
    //!
    uint32_t query(string &cmd, int respLen, uint32_t waitReadble = 0);

    //! \brief write Sends the command 'cmd' of length 'length' bytes to the manipulator.
    //! \param cmd
    //! \param length
    //!
    void write(string &cmd);

    //! \brief read Reads the response from the manipulator and saves it to the 'response'.
    //! Checks whether manipulator responds with error message (such a message includes
    //! character '!') and if so, throws the exception with this error message.
    //! \param response
    //!
    void read(string &response, uint32_t waitTime = 0);

    //! \brief moveCommand Executes the move command 'cmd' given position 'pos' in mrad.
    //! First it converts tje position in mrad to the number of manipualator steps given
    //! the current step 'step'.
    //!
    //!     0x81    PAN_SET_ABS_POS
    //!     0x82    TILT_SET_ABS_POS
    //!     0x83    PAN_SET_REL_POS
    //!     0x84    TILT_SET_REL_POS
    //!     0x87    PAN_SET_ABS_SPEED
    //!     0x88    TILT_SET_ABS_SPEED
    //!
    //! \param cmd Command
    //! \param pos Position in miliradians
    //!
    void moveCommand(uint8_t cmd, int stepsSpeed);

    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    virtual void onKeyDown(const keyboard::Key::ConstPtr key);

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    virtual void onKeyUp(const keyboard::Key::ConstPtr key);

    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    virtual void onJoy(const sensor_msgs::Joy::ConstPtr& joy);

    //! \brief getAzimmuthSpeed Returns azimuth speed.
    //! \return
    //!
    double getAzimuthSpeed() { return aziSpeed_; }

    //! \brief getElevationSpeed Returns elevation speed.
    //! \return
    //!
    double getElevationSpeed() { return eleSpeed_; }

    //! \brief hex2str Converts the 32bit integer number 'num' to string
    //! of 8bit hexa values (in format XX YY ZZ ...). The string will be
    //! 'bytes' characters long (1-4 bytes) with MSB at str[0] and LSB
    //! at str[bytes].
    //! \param value
    //! \param str
    //! \return Resultring string.
    //!
    string int2byteStr(uint32_t num, int bytes);

    //! \brief angle2steps Converts 'angle' to the integer number of required
    //! steps with step of size 'step'.
    //! \param angle [rad]
    //! \param step  step size [rad]
    //! \return Number of steps to reach the position 'pos'
    //!
    int angle2steps(double angle, double step);

    //! \brief steps2angle Converts steps to angle
    //! \param steps
    //! \param step  [rad]
    //! \return
    //!
    double steps2angle(int steps, double step);

    //! \brief valInRange Checks whether given value falls within
    //! allowed range.
    //! \param val Value to ne checked
    //! \param min Minimum allowed value.
    //! \param max Maximum allowed value.
    //! \return
    //!
    bool valInRange(double val, double min, double max);

    //! \brief movingInRange Checks whether position 'pos' is in allowed range
    //! and if not it checks whether the position is at least not changing
    //! to the restricted direction.
    //! \param pos Current position.
    //! \param speed Current speed.
    //! \param min Minimum allowed position.
    //! \param max Maximum allowed position.
    //! \return
    //!
    bool movingInRange(double pos, double speed,double min, double max);

    //! \brief getValInRange Clips the value 'pos' so that it falls within
    //! range ['min', 'max']
    //! \param val Value to change.
    //! \param min Minimum range limit.
    //! \param max Maximum range limit.
    //! \param margin Specifies how big margin will be between new clipped
    //! value of 'pos' and boundary limits min/max [mrad].
    //! \return
    //!
    double getValInRange(double val, double min, double max, double margin);   

    //! \brief initMotionProps Sets the motion properties (speeds, accelerations) of
    //! manipulator using pre-set constants.
    //!
    void initMotionProps();

    //! \brief getAziTargetSectionBoundary Returns the azimuth location [mrad] which
    //! should be the current absolute motion target given current position and speed.
    //! \param azimuth  Current azimuthal position [mrad]
    //! \param speed    Current azimuthal speed [mrad/s]
    //! \return
    //!
    double getAziTargetSectionBoundary(double azimuth, double speed);

    //! \brief getAbsPosAziTarget Computes and returns current azimuth target [mrad]
    //! which the manipulator must reach to fulfill the last issued absolute motion command.
    //! \param azimuth  Current azimuthal position [mrad]
    //! \param speed    Current azimuthal speed [mrad/s]
    //! \param target   The ultimate absolute motion target [mrad]
    //! \return
    //!
    double getAbsPosAziTarget(double azimuth, double speed, double target);

    // ----------------------------------- debug ----------------------------------- //
    // Force stops the manipulator.
    void forceStop();

    //! \brief prettyHexStr takes the string where each character (uchar) represents one unsigned value [0, 255] and
    //! creates the string representing the same hexa values in format XX YY ZZ ..., where XX, YY, ZZ is a 16bit hexa
    //! number.
    //! \param hexNums Input string
    //! \return
    //!
    string prettyHexStr(string hexNums);

    //! \brief testSynchrony
    //! \param steps
    //! \param aziEle true = azimuth, false = elevation
    //! \param cw true = move clockwise, false = move counter-clockwise
    //!
    void testSynchrony(int steps, bool aziEle, bool cw);
};

#endif // #define MANIPULATOR_PTUD4670_H
