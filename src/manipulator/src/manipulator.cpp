/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#include "manipulator.h"

// ROS constatns
const string Manipulator::KEY_DOWN_TOPIC_SUFFIX     = "keyboard/keydown";
const string Manipulator::KEY_UP_TOPIC_SUFFIX       = "keyboard/keyup";
const string Manipulator::JOY_TOPIC_SUFFIX          = "joy";
const string Manipulator::MANIP_MOVE_TOPIC          = "manip_move";
const string Manipulator::REGULATE_TOPIC            = "regulation";
const string Manipulator::CAMERA_SENSORS_TOPIC      = "camera_unit_sensors";

const double Manipulator::DEFAULT_FOCAL_LENGTH  = 0.016;        // 16 mm
const double Manipulator::DEFAULT_PIXEL_WIDTH   = 0.00000375;   // 3.75 um
const double Manipulator::DEFAULT_PIXEL_HEIGHT  = 0.00000375;   // 3.75 um

const double Manipulator::DEFAULT_MOTION_PREDICTION_PERIOD = 0.5; // s

#if DEBUG

Manipulator::Manipulator()
{
    KEY_DOWN_TOPIC  = string("/") + KEY_DOWN_TOPIC_SUFFIX;
    KEY_UP_TOPIC    = string("/") + KEY_UP_TOPIC_SUFFIX;
    JOY_TOPIC       = string("/") + JOY_TOPIC_SUFFIX;

    // Initialize ROS structures, subscribers, publishers
    nh_ = new ros::NodeHandle();
    keyDownSub_     = nh_->subscribe(KEY_DOWN_TOPIC, 5, &Manipulator::onKeyDown, this);
    keyUpSub_       = nh_->subscribe(KEY_UP_TOPIC,   5, &Manipulator::onKeyUp,   this);
    joySub_         = nh_->subscribe(JOY_TOPIC,      1, &Manipulator::onJoy,     this);
}

#else

Manipulator::Manipulator() : precState_(CONTINUOUS), lense_(NULL), lensePort_(""),
    zooming_(false), focusing_(false), changingIris_(false),
    azimuth_(0.0), elevation_(0.0), oldAzimuth_(0.0), oldElevation_(0.0),
    regulator_(NULL), joySpeed_(NULL), followTarget_(true),
    aziSpeed_(0.0), eleSpeed_(0.0)
{
    // Initialize the CRC table
    crcInit();

    // Load parameters from ROS parameter server.
    if (!ros::param::get("/master_ns", master_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter overview_unit_ns");
        exit(1);
    }

    if (!ros::param::get("tf_prefix", tf_prefix_)) {
        ROS_FATAL_STREAM("Could not get parameter tf_prefix");
        exit(1);
    }

    if (!ros::param::get("/world_frame_id", world_frame_id_)) {
        ROS_FATAL_STREAM("Could not get parameter world_frame_id");
        exit(1);
    }

    if (!ros::param::get("unit_idx", unit_idx_)) {
        ROS_FATAL_STREAM("Could not get parameter unit_idx");
        exit(1);
    }   
    unitIdx = stoi(unit_idx_);

    if (!ros::param::get("serial_if", serial_if_name_)) {
        ROS_FATAL_STREAM("Could not get parameter serial_if");
        exit(1);
    }

    if (!ros::param::get("camera/focal_length_max", focalLengthMax_)) {
        focalLengthMax_ = DEFAULT_FOCAL_LENGTH;
        ROS_WARN_STREAM("Could not get parameter focal_length_max. Setting default value " <<
                        DEFAULT_FOCAL_LENGTH << " m.");
    }

    if (!ros::param::get("camera/focal_length_min", focalLengthMin_)) {
        focalLengthMin_ = DEFAULT_FOCAL_LENGTH;
        ROS_WARN_STREAM("Could not get parameter focal_length_min. Setting default value " <<
                        DEFAULT_FOCAL_LENGTH << " m.");
    }

    if (!ros::param::get("camera/pixel_width", pixelWidth_)) {
        pixelWidth_ = DEFAULT_PIXEL_WIDTH;
        ROS_WARN_STREAM("Could not get parameter pixel_width. Setting default value " <<
                        DEFAULT_PIXEL_WIDTH << " m.");
    }

    if (!ros::param::get("camera/pixel_height", pixelHeight_)) {
        pixelHeight_ = DEFAULT_PIXEL_HEIGHT;
        ROS_WARN_STREAM("Could not get parameter pixel_height. Setting default value " <<
                        DEFAULT_PIXEL_HEIGHT << " m.");
    }        

    if (!ros::param::get("lense_port", lensePort_)) {
        ROS_WARN_STREAM("Could not get parameter lense_port. Assuming no configurable lense is used.");
    }

    double motPredPer = DEFAULT_MOTION_PREDICTION_PERIOD;
    if (!ros::param::get("~motion_prediction_period", motPredPer)) {
        ROS_WARN_STREAM("Could not get parameter ~motion_prediction_period. Using default value " << DEFAULT_MOTION_PREDICTION_PERIOD << " s.");
    }
    motionPredictionPeriod_ = ros::Duration(motPredPer);

    KEY_DOWN_TOPIC  = string("/") + master_ns_ + string("/") + KEY_DOWN_TOPIC_SUFFIX + string("_") + unit_idx_;
    KEY_UP_TOPIC    = string("/") + master_ns_ + string("/") + KEY_UP_TOPIC_SUFFIX + string("_") + unit_idx_;
    JOY_TOPIC       = string("/") + master_ns_ + string("/") + JOY_TOPIC_SUFFIX + string("_") + unit_idx_;

    // Initialize ROS structures, subscribers, publishers
    nh_ = new ros::NodeHandle();
    keyDownSub_     = nh_->subscribe(KEY_DOWN_TOPIC,    5, &Manipulator::onKeyDown,     this);
    keyUpSub_       = nh_->subscribe(KEY_UP_TOPIC,      5, &Manipulator::onKeyUp,       this);
    joySub_         = nh_->subscribe(JOY_TOPIC,         1, &Manipulator::onJoy,         this);
    manipMoveSub_   = nh_->subscribe(MANIP_MOVE_TOPIC,  5, &Manipulator::onManipMove,   this);
    regulateSub_    = nh_->subscribe(REGULATE_TOPIC,    5, &Manipulator::onRegulate,    this);
    cuSensorsPub_   = nh_->advertise<msgs::CameraUnitSensors>(CAMERA_SENSORS_TOPIC, 5);

    // Lense
    try {
        if(lensePort_.length() != 0) {
            lense_ = new Lense(lensePort_, 9600, 1000, focalLengthMin_, focalLengthMax_);
        } else {
            lense_ = new Lense(focalLengthMin_);
        }
    } catch(ExceptionOLS &e) {
            ROS_FATAL_STREAM("ExceptionOLS: Manipulator(): " << e.what());
    }
}

#endif // #if DEBUG

Manipulator::~Manipulator()
{
    // debug
    cout << "~Manipulator() called." << endl;

    if(lense_ != NULL) delete lense_;
}

void Manipulator::update()
{    

    // Update manipualtor position history for the regulator.
    regulator_->updateStampedPos(azimuth_ / 1000.0, elevation_ / 1000.0, azieleStamp_);

    // Create and publis message CameraUnitSensors
    msgs::CameraUnitSensors cusMsg;

    cusMsg.header.stamp = ros::Time::now();
    cusMsg.azimuth      = azimuth_ / 1000.0;
    cusMsg.elevation    = elevation_ / 1000.0;
    cusMsg.f            = lense_->focalLength();

    cuSensorsPub_.publish(cusMsg);
}

void Manipulator::onRegulate(const msgs::RegulateConstPtr &regulMsg)
{
    if(regulator_ == NULL) {
        ROS_ERROR_STREAM("No regulator was created, cannot regulate manipulator motion.");
        return;
    }

    if(followTarget_) {
        double angAzi = std::atan(regulMsg->dx * pixelWidth_ / lense_->focalLength());
        double angEle = std::atan(regulMsg->dy * pixelHeight_ / lense_->focalLength());

        double aziSpeed;
        double eleSpeed;
        regulator_->regulate(angAzi, angEle, aziSpeed, eleSpeed, regulMsg->header.stamp, motionPredictionPeriod_);

        move(aziSpeed * 1000.0, eleSpeed * 1000.0);
    }
}

void Manipulator::enableFollowTarget()
{
    if(!followTarget_) {
        followTarget_ = true;
        regulator_->reset();
    }
}

void Manipulator::disableFollowTarget()
{
    if(followTarget_) {
        followTarget_ = false;
    }
}

void Manipulator::setPositionStamped(double azimuth, double elevation, ros::Time stamp)
{
    azimuth_ = azimuth;
    elevation_ = elevation;
    azieleStamp_ = stamp;
}

void Manipulator::zoom(int step)
{
    if(lense_) {
        lense_->zoom(step);
    }
}

void Manipulator::focus(int step)
{
    if(lense_) {
        lense_->focus(step);
    }
}

void Manipulator::moveIris(int speed, int direction)
{
    if(lense_) {
        lense_->iris(speed, direction);
    }
}

double Manipulator::getFocalLength()
{
    double f;

    if(lense_) {
        f = lense_->focalLength();
    } else {
        f = focalLengthMin_;
    }

    return f;
}

string Manipulator::tfCanonicName(string tfFrame)
{
    return string("/") + tf_prefix_ + string("/") + string(tfFrame);
}
