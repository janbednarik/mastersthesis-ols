#!/bin/bash

brctl addbr br0
ifconfig eth0 0.0.0.0 mtu 9000 down
ifconfig eth2 0.0.0.0 mtu 9000 down
brctl addif br0 eth0
brctl addif br0 eth2
ifconfig eth0 up
ifconfig eth2 up
ifconfig br0 up
ifconfig br0 192.168.90.3
