// project libraries
#include "camera_capture.h"

// ROS
#include <ros/ros.h>

// Qt
#include <QtGui>
#include <QtQml>
#include <QVector>

// C++
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    // init ROS
    ros::init(argc, argv, "demo_app");

    // init Qt
    QGuiApplication app(argc, argv);
    QUrl source(QUrl("qrc:/../src/main.qml"));
    QQmlApplicationEngine engine;
    engine.load(source);

    // Test Qt
    QVector<double> v;
    v.push_back(1.23);
    v.push_back(2.34);
    cout << "vector v: " << endl;
    foreach (double d, v) {
        cout << d << endl;
    }

    CameraCapture cc;

    ros::Rate looper(100); // 100 Hz
    while(ros::ok()) {
//        cc.viewer.spinOnce();
        app.processEvents();
        ros::spinOnce();
        looper.sleep();
    }



    return 0;
}
