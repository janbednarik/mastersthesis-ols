/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#include "tracker_fixed_point.h"

TrackerFixedPoint::TrackerFixedPoint()
{
    ;
}

TrackerFixedPoint::~TrackerFixedPoint()
{
    ;
}

bool TrackerFixedPoint::init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat &image, const cv::Rect &bb)
{
    bb_ = bb;
}

bool TrackerFixedPoint::track(uint32_t img_seq_id, const cv::Mat &image, cv::Rect &bb)
{
    bb = bb_;

    return true;
}
