//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//! Description: Grayscale visual TrackerBase based on the particle filter.
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

// Ioss libraries.
#include "PFTracker.h"
//#include "OCVUtils.h"
//#include "//MLOG.h"
//#include "TicTac.h"

// OpenCVX libraries.
#include "opencvx/cvparticle.h"
#include "ParticleState.h"
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>

// OpenMP
#include <omp.h>

// Namespaces.

// Extern function.
extern CvParticleState cvParticleStateFromMat(const CvMat* state);
extern CvParticleState cvParticleStateGet(const CvParticle* p, int p_id);
extern void cvParticleStateSet(CvParticle* p, int p_id, const CvParticleState& state);

// Macros.
#define TEMPLATE_SIZE 32


/*
// Remove target.
void TrackerBase::removeTarget(unsigned long long id)
{
    //MLOG_DBG_3("in");

    if (targets_.contains(id)) {
        targets_.remove(id);
        emit targetRemoved(id);
    }
    else {
        //MLOG_ERR("Target " + QString::number(id) + " does not exist");
    }
}

// Return target.
BIPWrapper<Target> TrackerBase::target(unsigned long long id)
{
    MLOG_DBG_3("in");

    BIPWrapper<Target> t;
    if (targets_.contains(id))
        t = targets_.find(id).value();

    return t;
}



// Set type of the target.
void TrackerBase::setTargetType(unsigned long long tg_id, QString type)
{
    MLOG_DBG_3("in");

    if (targets_.contains(tg_id)) {
        BIPWrapper<Target> t = targets_.find(tg_id).value();
        t->setType(type);
    }
    else
        MLOG_ERR("Attempt to set type of non-existing target");
}

// Set description.
void TrackerBase::setTargetDescription(unsigned long long tg_id, QString description)
{
    MLOG_DBG_3("in");

    if (targets_.contains(tg_id)) {
        BIPWrapper<Target> t = targets_.find(tg_id).value();
        t->setDescription(description);
    }
    else
        MLOG_ERR("Attempt to set description of non-existing target");
}
*/

// Constructor.
PFTracker::PFTracker() :
    histCapacity_(10),
    maxNumOfOverlappedInSequence_(0),
    p_device_(0),
    p_host_(0),
    m_device_(mat_create_device()),
    m_host_(mat_create_host())
{
    //MLOG_DBG_3("in");
    //TODO: qRegisterMetaType<std::shared_ptr<PFTargetState> >("std::shared_ptr<PFTargetState>");
}

// Destructor.
PFTracker::~PFTracker()
{
    //MLOG_DBG_3("in");
    cv::gpu::setDevice(0);

    particles_free_device(&p_device_);
    particles_free_host(&p_host_);

    mat_free_device(&m_device_);
    mat_free_host(&m_host_);
}

// Load configuration from the settings.
void PFTracker::fromSettings(CSettings &settings)
{
    histCapacity_ = settings.hist_capacity;
    maxNumOfOverlappedInSequence_ = settings.max_number_overlapped_in_sequence;

    // Init settings for a new target.
    t_total_num_particles_ = settings.t_total_num_particles;
    t_num_particles_from_movement_ = settings.t_num_particles_from_movement;
    t_use_target_mask_ = settings.t_use_target_mask;
    use_homography_ = settings.use_homography;
    t_logprob_ = settings.t_logprob;
    t_std_x_ = settings.t_std_x;
    t_std_y_ = settings.t_std_y;
    t_std_vx_ = settings.t_std_vx;
    t_std_vy_ = settings.t_std_vy;
    t_std_width_ = settings.t_std_width;
    t_std_height_ = settings.t_std_height;
    t_std_angle_ = settings.t_std_angle;

    // Change the section to BG model and load settings for them.
    //settings.beginGroup("BGModel");
    bg_.fromSettings(settings);
    //settings.endGroup();

}

// Save current configuration to the settings.
void PFTracker::toSettings(CSettings &)
{
    ;
}

// Reset TrackerBase to initial state.
// TODO: restart
/*void PFTracker::restart()
{
    // Remove all tracked targets.
    unsigned long long tg_id;
    QList<unsigned long long> tgs = targets_.keys();
    foreach (tg_id, tgs) removeTarget(tg_id);

    targets_.clear();
    hist_.clear();

    // Restart BG model.
    bg_.restart();

    // Delete last observation.
    lastObs_ = 0;
}


// Correct the target state.
void PFTracker::correctTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect)
{
    //MLOG_DBG_3("in");
    //MLOG_INF("Correct target " + std::string::number(target_id));

    // Obtain visual data.
    if (hist_.contains(obs_id) && targets_.contains(target_id)) {
        std::shared_ptr<Target> ot = targets_.find(target_id).value();

        PFTarget *t = createTarget(target_id, obs_id, rect);
        t->setType(ot->getType());
        t->setDescription(ot->getDescription());

        targets_.insert(target_id, std::shared_ptr<Target>(t));

        // Emit current state of the target.
        std::shared_ptr<PFTargetState> ts = t->state();
        emit targetTracked(ts);

        trackToObservation(std::shared_ptr<Target>(t), (--hist_.end()).value().id_);

        //MLOG_INF("Target " + std::string::number(target_id) + " has been corrected");
    }
    else {
        //MLOG_ERR("Required target or/and observation was not found");
    }
}*/

// Tracks objects and builts own background model.
void PFTracker::observation(const cv::Mat& obs, uint32_t img_seq_id)
{
    //MLOG_DBG_3("in");
    // //MLOG_INF("Received obs " + std::string::number(obs->getID()));

    //TicTac t;
    //t.tic();

    if (obs.empty()) {
        //MLOG_WRN("Observation is null");
        return;
    }

    /*if (!lastObs_.isNull() && (lastObs_->getID() + 1 != obs->getID())) {
        //MLOG_WRN("Jump into the past/future has been detected. TrackerBase is going to be restarted")
        restart();
        //MLOG_WRN("TrackerBase was restarted")
    }*/
    if (img_seq_id == lastObs_)
    { // Image was already tracked
        return;
    }

    cv::Mat gray, gray_F;
    cv::cvtColor(obs , gray, cv::COLOR_BGR2GRAY );
    gray.convertTo(gray_F, CV_32FC1, 1.0/255.0);
    // Update background model.
    cv::gpu::GpuMat gobs_;
    gobs_.upload(gray_F);
    cv::Mat H = bg_.update(gobs_);

    // Store the current state to the history.
    if (!H.empty()) {
        hist_[img_seq_id] = PFTrackerState(img_seq_id, gobs_, bg_.getFG(), H);
        if (hist_.size() > histCapacity_) hist_.erase(hist_.begin());
    }
    else
    {
        int a = 1;
        a = a;
    }

    // Track all targets in the map...
    std::map<unsigned long long, std::shared_ptr<TargetBase> >::iterator it = targets_.begin();
//    QList<unsigned long long> rtg;
    while (it != targets_.end()) {
        std::shared_ptr<PFTarget> t = std::dynamic_pointer_cast<PFTarget>(it->second);
        trackToObservation(t, img_seq_id);

//        if (t->getNumOfOverlappedInSequence() > maxNumOfOverlappedInSequence_)
//            rtg.push_back(t->getID());

        ++it;
    }

    // Remove targets which are overlapped for long time.
//    unsigned long long tid;
//    foreach (tid, rtg) {
//        removeTarget(tid);
//    }

    lastObs_ = img_seq_id;
}

// Add a new target to the targets.
void PFTracker::addTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect)
{    
    //MLOG_DBG_3("in");
    //MLOG_INF("Try to add target " + std::string::number(target_id) + " which is specified in obs " + std::string::number(obs_id)
    //         + ", rectange is (" + std::string::number(rect.x) + ", " + std::string::number(rect.y) + ", " + std::string::number(rect.width) + ", " + std::string::number(rect.height) + ")");

    std::shared_ptr<PFTarget> t;
    if ((t = createTarget(target_id, obs_id, rect)) != 0) {

        {
            //  Debug purposes.
            //  qDebug() << "Target has been created";
            // cv::Mat pat_img, mask_img;
            // pattern.download(pat_img); mask.download(mask_img);
            // cv::imshow("pattern", pat_img);
            // cv::imshow("mask", mask_img);
        }

        // Add the target to set of tracked targets.
        targets_[t->id_] = std::shared_ptr<TargetBase>(t);

        // Emit current state of the target.
        std::shared_ptr<PFTargetState> ts = std::dynamic_pointer_cast<PFTargetState>(t->state());
        //emit targetAdded(ts);

        // Track to the present.
        trackToObservation(std::shared_ptr<PFTarget>(t), (--hist_.end())->second.id_);

        //MLOG_INF("Target " + std::string::number(target_id) + " has been added");
    }
    else {
        //MLOG_ERR("Bad size of the target and/or the observation is not in the history");
    }
}

void PFTracker::removeTargets()
{
    //MLOG_DBG_3("in");
    targets_.clear();
    //MLOG_INF("Target " + std::string::number(target_id) + " has been removed");
}



std::shared_ptr<PFTarget> PFTracker::createTarget(unsigned long long target_id, unsigned long long obs_id, cv::Rect rect)
{
    //MLOG_DBG_3("in");

    if (rect.width > 0 && rect.height > 0 && hist_.count(obs_id) > 0) {

        // From the history to now.
        //std::map<unsigned long long, PFTrackerState>::iterator it = hist_.find(obs_id);
        PFTrackerState s = hist_.at(obs_id);//it.value();

        // Obtain visual data.
        // Check and correct input rectangle w.r.t. size of the image.
        cv::Rect roi = getJointPart(rect, s.obs_.size());

        cv::gpu::GpuMat pattern = (s.obs_)(roi);
        cv::gpu::GpuMat mask = (s.bg_)(roi);
        cv::gpu::GpuMat emask(roi.size(), CV_32FC1, 1.0);

        // Show a new target.

        /*cv::Mat test;
        pattern.download(test);
        cv::imshow("New target", test);*/

        if (!t_use_target_mask_)
        {
            mask = emask;
        }

        /*cv::Mat hmask;
        mask.download(hmask);
        cv::imshow("New target mask", hmask);*/


        // Set up a new target.
        float cx = roi.x + (roi.width / 2.0);
        float cy = roi.y + (roi.height / 2.0);

        return std::make_shared<PFTarget>(target_id, obs_id, cx, cy, pattern, mask,
                            t_total_num_particles_, t_logprob_, t_std_x_, t_std_y_,
                            t_std_vx_, t_std_vy_, t_std_width_, t_std_height_, t_std_angle_, rect);
    }
    else {
        return 0;
    }
}

cv::Rect PFTracker::getJointPart(cv::Rect rect, cv::Size img)
{
    //MLOG_DBG_3("in");

    // Check and correct rectangle.
    cv::Point ul, br;
    ul.x = MIN(MAX(0, rect.x), img.width - 1);
    ul.y = MIN(MAX(0, rect.y), img.height - 1);
    br.x = MIN(MAX(0, rect.x + (rect.width - 1)), img.width - 1);
    br.y = MIN(MAX(0, rect.y + (rect.height - 1)), img.height -1);

    return cv::Rect(ul.x, ul.y, (br.x - ul.x) + 1, (br.y - ul.y) + 1);
}

// Track from current state to the obs (via all data in the history).
void PFTracker::trackToObservation(std::shared_ptr<PFTarget> t, unsigned long long obs_id)
{
    //MLOG_DBG_3("in");

    // Incoming observation is old - observation mess.
    if (hist_.count(obs_id) && hist_.count(t->getCurrentObsID())) {
        // Track to the obs_id.
        PFTrackerState s;
        std::map<unsigned long long, PFTrackerState>::iterator it = (hist_.find(t->getCurrentObsID()));
        while (it != hist_.end() && ((s = it->second).id_ <= obs_id)) {
            t->toHistory();

            // Track.
            //track(obs_id, s.obs_, s.bg_, t, s.H_);
            track2(obs_id, s.obs_, s.bg_, t, s.H_);

            // Emit current state.
            //std::shared_ptr<PFTargetState> s = std::dynamic_pointer_cast<PFTargetState>(t->state());                                 // Get state.
            //TODO: emit targetTracked(s);

            ++it;
        }
    }
    else
    {
        //MLOG_ERR("Observation mess");
    }
}




// Track a target.
/*
*
* cvCreateParticle
* cvPartcileSetXxx
* cvParticleInit
* loop {
*   cvParticleTransition
*   Measurement
*   cvParticleNormalize
*   cvParticleResample
* }
* cvReleaseParticle
**/
void PFTracker::track(unsigned long long obs_id, cv::gpu::GpuMat obs, cv::gpu::GpuMat fg, std::shared_ptr<PFTarget> t, cv::Mat H)
{
    //MLOG_DBG_3("in");

    // Transition (prediction step).
    //cvParticleTransition(t->particles_);
    cvParticleTransition(t->particles_);

    if (!H.empty()) {
        // Get state before the particle's transition.
        // CvParticleState lp_state = cvParticleStateGet(t->particles_, cvParticleGetMax(t->particles_));

        // Update transformation matrix (coordinate systems are separated and this matrix represents transformation between them).
        t->H_ = H * t->H_;

//        cv::namedWindow("FG-mask");
        cv::Mat hfg;
        t->pattern_mask_.download(hfg);

//        cv::imshow("FG-mask", hfg);
//        cv::waitKey(1);

        // Evaluation...
        evaluateParticles(obs, fg, t->pattern_, t->pattern_mask_, t->particles_, t->H_);

        {
            // DEBUG
            // Show images
            // cv::Mat imgH, patternH;
            // img.download(imgH); t->pattern_.download(patternH);
            // cv::imshow("aa", imgH);
            // cv::imshow("bb", patternH);

             // Show current state - only for debug purposes.
             cv::Mat current;
             obs.download(current);
             showCurrentState(current);
        }

        // If the particle is not overlapped.... evaluate.
        if (!t->isOverlapped()) {
            // Set velocity for the best particle as the mean.
             int imax_p = cvParticleGetMax(t->particles_);
            CvParticleState cp_state = cvParticleStateGet(t->particles_, imax_p);

            Eigen::Vector2d mvel = t->getMeanVel();
            cp_state.vx = mvel(0); cp_state.vy = mvel(1);
            cvParticleStateSet(t->particles_, imax_p, cp_state);

            cvParticleNormalize(t->particles_);
            cvParticleResample(t->particles_);
        }        
    }


    // qDebug() << "Target is overllaped " << t->isOverlapped();

    // Store the current template and the template mask (w.r.t. bounding box).
    cv::Rect boundingBox = t->getBoundingBox();    

    // qDebug() << "Bounding box is " << boundingBox.x << ", " << boundingBox.y << ", " << boundingBox.x + boundingBox.width << ", " << boundingBox.y + boundingBox.height;

    // HACK - correct bounding box w.r.t. size of the image.
    double x1 = MAX(MIN(boundingBox.x, obs.size().width), 0);
    double y1 = MAX(MIN(boundingBox.y, obs.size().height), 0);
    double x2 = MAX(MIN(boundingBox.x + boundingBox.width, obs.size().width), 0);
    double y2 = MAX(MIN(boundingBox.y + boundingBox.height,  obs.size().height), 0);
    cv::Rect rBoundingBox(x1, y1, x2 - x1, y2 - y1);

    // qDebug() << "Bounding box is " << rBoundingBox.x << ", " << rBoundingBox.y << ", " << rBoundingBox.x + rBoundingBox.width << ", " << rBoundingBox.y + rBoundingBox.height;

    t->setCurrentObservationID(obs_id);
    obs(rBoundingBox).download(t->template_);            // This is not necessary...
    fg(rBoundingBox).download(t->templateMask_);

    // Show the result.
//    cv::Mat ii;
//    obs.download(ii);
//    cv::rectangle(ii, boundingBox, cv::Scalar(255, 0, 0), 1);
//    cv::imshow("Current obs", ii);
//    cv::waitKey(1);

    if (t->isOverlapped()) ++(t->numOfOverllapedInSequence_);
    else t->numOfOverllapedInSequence_ = 0;
}

// Track a target.
/*
*
* cvCreateParticle
* cvPartcileSetXxx
* cvParticleInit
* loop {
*   cvParticleTransition
*   Measurement
*   cvParticleNormalize
*   cvParticleResample
* }
* cvReleaseParticle
**/
void PFTracker::track2(unsigned long long obs_id, cv::gpu::GpuMat obs, cv::gpu::GpuMat fg, std::shared_ptr<PFTarget> t, cv::Mat H)
{
    //MLOG_DBG_3("in");

    // Transition (prediction step).
    particleTransition(t->particles_, fg, t);


    H.eye(cv::Size(3,3), CV_32FC1);

    //particleTransition(t->particles_, fg);

    if (!H.empty()) {
        // Get state before the particle's transition.
        // CvParticleState lp_state = cvParticleStateGet(t->particles_, cvParticleGetMax(t->particles_));

        // Update transformation matrix (coordinate systems are separated and this matrix represents transformation between them).
        //t->H_ = H * t->H_;

        /*cv::Mat current;
        obs.download(current);
        showCurrentState(current);*/

//        cv::namedWindow("FG-mask", cv::WINDOW_NORMAL);
        cv::Mat hfg;
        fg.download(hfg);

//        cv::imshow("FG-mask", hfg);
//        cv::waitKey(1);

        // Evaluation...
        evaluateParticles(obs, fg, t->pattern_, t->pattern_mask_, t->particles_, t->H_);

        {
            // DEBUG
            // Show images
            // cv::Mat imgH, patternH;
            // img.download(imgH); t->pattern_.download(patternH);
            // cv::imshow("aa", imgH);
            // cv::imshow("bb", patternH);

             // Show current state - only for debug purposes.
             cv::Mat current;
             obs.download(current);
             showCurrentState(current);
        }

        // If the particle is not overlapped.... evaluate.
        if (!t->isOverlapped()) {
            // Set velocity for the best particle as the mean.
             int imax_p = cvParticleGetMax(t->particles_);
            CvParticleState cp_state = cvParticleStateGet(t->particles_, imax_p);

            Eigen::Vector2d mvel = t->getMeanVel();
            cp_state.vx = mvel(0); cp_state.vy = mvel(1);
            cvParticleStateSet(t->particles_, imax_p, cp_state);

            cvParticleNormalize(t->particles_);
            cvParticleResample(t->particles_);
        }
    }


    // qDebug() << "Target is overllaped " << t->isOverlapped();

    // Store the current template and the template mask (w.r.t. bounding box).
    cv::Rect boundingBox = t->getBoundingBox();

    // qDebug() << "Bounding box is " << boundingBox.x << ", " << boundingBox.y << ", " << boundingBox.x + boundingBox.width << ", " << boundingBox.y + boundingBox.height;

    // HACK - correct bounding box w.r.t. size of the image.
    double x1 = MAX(MIN(boundingBox.x, obs.size().width), 0);
    double y1 = MAX(MIN(boundingBox.y, obs.size().height), 0);
    double x2 = MAX(MIN(boundingBox.x + boundingBox.width, obs.size().width), 0);
    double y2 = MAX(MIN(boundingBox.y + boundingBox.height,  obs.size().height), 0);
    cv::Rect rBoundingBox(x1, y1, x2 - x1, y2 - y1);

    // qDebug() << "Bounding box is " << rBoundingBox.x << ", " << rBoundingBox.y << ", " << rBoundingBox.x + rBoundingBox.width << ", " << rBoundingBox.y + rBoundingBox.height;

    t->setCurrentObservationID(obs_id);
    obs(rBoundingBox).download(t->template_);            // This is not necessary...
    fg(rBoundingBox).download(t->templateMask_);

    // Show the result.
//    cv::Mat ii;
//    obs.download(ii);
//    cv::rectangle(ii, boundingBox, cv::Scalar(255, 0, 0), 1);
//    cv::imshow("Current obs", ii);
//    cv::waitKey(1);

    if (t->isOverlapped()) ++(t->numOfOverllapedInSequence_);
    else t->numOfOverllapedInSequence_ = 0;
}

void PFTracker::ParticleStateToMat( const CvParticleState& state, CvMat* state_mat )
{
    cvmSet( state_mat, 0, 0, state.x );
    cvmSet( state_mat, 1, 0, state.y );
    cvmSet( state_mat, 2, 0, state.vx );
    cvmSet( state_mat, 3, 0, state.vy );
    cvmSet( state_mat, 4, 0, state.width );
    cvmSet( state_mat, 5, 0, state.height );
    cvmSet( state_mat, 6, 0, state.angle );
}

void PFTracker::particleTransition(CvParticle* p, cv::gpu::GpuMat fg, std::shared_ptr<PFTarget> &t)
{
    // threasholded fg;
    cv::gpu::GpuMat fgt;

    cv::Scalar smean = cv::gpu::sum(fg);
    double mean = smean.val[0] / (fg.rows * fg.cols);

    double minVal, maxVal;
    cv::Point minLoc, maxLoc;
    cv::gpu::minMaxLoc(fg, &minVal, &maxVal, &minLoc, &maxLoc);

    cv::gpu::threshold(fg, fgt, mean + (maxVal - mean) / 4, 1.0, cv::THRESH_TOZERO);

    CvParticleState max_state;
    max_state.x = maxLoc.x;
    max_state.y = maxLoc.y;
    /*max_state.x = 0.0;
    max_state.y = 0.0;*/
    max_state.vx = 0.0;
    max_state.vy = 0.0;
    max_state.angle = 0.0;
    max_state.height = t->bouding_box_.height;
    max_state.width = t->bouding_box_.width;

    /*cv::namedWindow("FG");
    cv::Mat hfg;
    fgt.download(hfg);
    cv::Scalar color(255, 0, 0);
    cv::circle(hfg, maxLoc, 30, color, 2);*/

    int i, j;
    CvMat* transits = cvCreateMat( p->num_states, p->num_particles, p->particles->type );
    CvMat* noises   = cvCreateMat( p->num_states, p->num_particles, p->particles->type );
    CvMat* noise, noisehdr;
    double std;

    // dynamics
    cvMatMul( p->dynamics, p->particles, transits );

    cv::Mat fgt_h;
    fgt.download(fgt_h);


    assert(fgt_h.depth() == CV_32FC1);
    // Iterate over thresholded fg image and paste particled to particle filter
    float* fgt_ptr = (float*)fgt_h.data;

    int particles_count = 0;
    int count_of_replaced_particles =  t_num_particles_from_movement_;


    CvMat* state_mat, hdr;
    #pragma omp parallel for
    for (int i = 0; i < 30 &&  particles_count < count_of_replaced_particles; i++)
    {
        max_state.x = maxLoc.x;
        max_state.y = maxLoc.y;
        state_mat = cvGetCol( transits, &hdr, particles_count);
        ParticleStateToMat(max_state, state_mat);
        #pragma omp atomic
        {
            particles_count++;
        }
    }

    #pragma omp parallel for
    for (int i = 0; i < fgt_h.cols * fgt_h.rows &&  particles_count < count_of_replaced_particles; i++)
    {
        if (fgt_ptr[i] > 0 && std::rand() < RAND_MAX/4) {
            max_state.x = i % fgt_h.cols;
            max_state.y = i / fgt_h.cols;
            state_mat = cvGetCol( transits, &hdr, particles_count);
            ParticleStateToMat(max_state, state_mat);
            particles_count++;
        }
    }

    while (particles_count < count_of_replaced_particles)
    {
        max_state.x = maxLoc.x;
        max_state.y = maxLoc.y;
        state_mat = cvGetCol( transits, &hdr, particles_count);
        ParticleStateToMat(max_state, state_mat);
        particles_count++;
    }

    // noise generation
    // THIS IS USED NOW
    if( p->stds == NULL )
    {
        for( i = 0; i < p->num_states; i++ )
        {
            std = cvmGet( p->std, i, 0 );
            noise = cvGetRow( noises, &noisehdr, i );
            if( std == 0.0 )
                cvZero( noise );
            else
                cvRandArr( &p->rng, noise, CV_RAND_NORMAL, cvScalar(0), cvScalar( std ) );
        }
    }
    // THIS IS NOT USED NOW:
    else
    {
        for( i = 0; i < p->num_states; i++ )
        {
            for( j = 0; j < p->num_particles; j++ )
            {
                std = cvmGet( p->stds, i, j );
                if( std == 0.0 )
                    cvmSet( noises, i, j, 0.0 );
                else
                    cvmSet( noises, i, j, cvRandGauss( &p->rng, std ) );
            }
        }
    }

    // dynamics + noise
    cvAdd(transits, noises, p->particles );

    /*
    cvAdd(p->particles, noises, transits);
    cvMatMul( p->dynamics, transits, p->particles);
    */

    /*cv::imshow("FG", hfg);*/

    cvReleaseMat( &transits );
    cvReleaseMat( &noises );

    cvParticleBound( p );
}

// Evaluate particles.
void PFTracker::evaluateParticles(cv::gpu::GpuMat img, cv::gpu::GpuMat fg, cv::gpu::GpuMat target, cv::gpu::GpuMat mask, CvParticle *p, cv::Mat H)
{
    //MLOG_DBG_3("in")

    // Convert to the CUDA representation.
    Img *img_t = img_create_header(img.cols, img.rows, static_cast<int>(img.step), (float *) img.data);
    Img *fg_t = img_create_header(fg.cols, fg.rows, static_cast<int>(fg.step), (float *) fg.data);
    Img *target_t = img_create_header(target.cols, target.rows, static_cast<int>(target.step), (float *) target.data);
    Img *mask_t = img_create_header(mask.cols, mask.rows, static_cast<int>(mask.step), (float *) mask.data);

    // Particles.
    if (!p_device_ || (p_device_->count_ != p->num_particles)) {
        particles_free_device(&p_device_);
        particles_free_host(&p_host_);

        p_device_ = particles_create_device(p->num_particles);
        p_host_ = particles_create_host(p->num_particles);
    }
    particles_copy_raw2device(p_device_, (float *) (p->particles->data.fl));

    // Transform. matrix.
    m_host_->data_[0] = H.at<double>(0, 0); m_host_->data_[1] = H.at<double>(0, 1); m_host_->data_[2] = H.at<double>(0, 2);
    m_host_->data_[3] = H.at<double>(1, 0); m_host_->data_[4] = H.at<double>(1, 1); m_host_->data_[5] = H.at<double>(1, 2);
    m_host_->data_[6] = H.at<double>(2, 0); m_host_->data_[7] = H.at<double>(2, 1); m_host_->data_[8] = H.at<double>(2, 2);
    mat_copy_host2device(m_device_, m_host_);

    // Call the GPU implemenation...
    evaluate_particles(p_device_, img_t, fg_t, target_t, mask_t, m_device_->data_);
    particles_copy_device2host(p_host_, p_device_);

    // Copy fitness back to the CvParticles structure
    for (int i = 0; i < p_host_->count_; i++) {
        cvmSet(p->weights, 0, i, (float) (p_host_->fitness_[i]));
        cvmSet(p->mask_sums, 0, i, (float) (p_host_->mask_sum_[i]));
    }



    // Release allocated resources.
    img_free_header(&img_t);
    img_free_header(&fg_t);
    img_free_header(&target_t);
    img_free_header(&mask_t);
}

void PFTracker::showCurrentState(cv::Mat gray_img)
{
    cv::Mat img;
    cv::cvtColor(gray_img, img, CV_GRAY2BGR);

    // Draw all targets.
    std::shared_ptr<PFTarget> t = nullptr;
    std::map<unsigned long long, std::shared_ptr<TargetBase> >::iterator it = targets_.begin();
    while (it != targets_.end()) {
        t = std::dynamic_pointer_cast<PFTarget>(it->second);

        // Color.
        cv::Scalar color = t->isOverlapped() ? cv::Scalar(1.0f, 0.0f, 0.0f) : cv::Scalar(0.0f, 1.0f, 0.0f);

        // Draw all particles as points.
        for (int i = 0; i < t->particles_->num_particles; i++)
        {
            CvParticleState p_state = cvParticleStateGet(t->particles_, i);
            drawParticle(p_state, t->H_, img, color);
        }

        // Draw the best particle as a red point and its velocity vector.
        int maxp_id = cvParticleGetMax(t->particles_);
        {
            CvParticleState p_state = cvParticleStateGet(t->particles_, maxp_id);
            cv::Scalar color = cv::Scalar(0.0, 0.0, 1.0);
            drawParticle(p_state, t->H_, img, color, true, true);
        }


        ++it;
    }

//    cv::namedWindow( "PFTracker - current state", cv::WINDOW_NORMAL );
//    cv::imshow("PFTracker - current state", img);
//    cv::waitKey(1);
}

void PFTracker::drawParticle(CvParticleState &p, cv::Mat &H, cv::Mat& img, cv::Scalar& color, bool drawBoundingBox, bool drawVelocityVector)
{
    // Transform to the current image.
    Eigen::Vector3d center;

    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> e_tran;
    cv::cv2eigen(H, e_tran);

    center << p.x, p.y, 1.0;
    center = e_tran * center;
    center(0) /= center(2); center(1) /= center(2); center(2) = 1.0;

    cv::circle(img, cv::Point(center(0), center(1)), 1, color, 1);

    // Draw velocity vector.
    if (drawVelocityVector) {
        Eigen::Vector3d vel;
        vel << p.x + p.vx * 10.0, p.y + p.vy * 10.0, 1.0;
        vel = e_tran * vel;
        vel(0) /= vel(2); vel(1) /= vel(2); vel(2) = 1.0;

        cv::line(img, cv::Point(vel(0), vel(1)), cv::Point(center(0), center(1)), color, 1);
    }

    // Draw bounding box (TODO)
    if (drawBoundingBox) {
        Eigen::Vector3d ul, ur, bl, br;
        ul << p.x - (p.width * TEMPLATE_SIZE) / 2.0, p.y - (p.height * TEMPLATE_SIZE) / 2.0, 1.0;
        ur << p.x + (p.width * TEMPLATE_SIZE) / 2.0, p.y - (p.height * TEMPLATE_SIZE) / 2.0, 1.0;
        bl << p.x - (p.width * TEMPLATE_SIZE) / 2.0, p.y + (p.height * TEMPLATE_SIZE) / 2.0, 1.0;
        br << p.x + (p.width * TEMPLATE_SIZE) / 2.0, p.y + (p.height * TEMPLATE_SIZE) / 2.0, 1.0;

        ul = e_tran * ul; ur = e_tran * ur;
        bl = e_tran * bl; br = e_tran * br;

        ul(0) /= ul(2); ul(1) /= ul(2); ul(2) /= 1.0;
        ur(0) /= ur(2); ur(1) /= ur(2); ur(2) /= 1.0;
        bl(0) /= bl(2); bl(1) /= bl(2); bl(2) /= 1.0;
        br(0) /= br(2); br(1) /= br(2); br(2) /= 1.0;

        cv::line(img, cv::Point(ul(0), ul(1)), cv::Point(ur(0), ur(1)), color);
        cv::line(img, cv::Point(ur(0), ur(1)), cv::Point(br(0), br(1)), color);
        cv::line(img, cv::Point(br(0), br(1)), cv::Point(bl(0), bl(1)), color);
        cv::line(img, cv::Point(bl(0), bl(1)), cv::Point(ul(0), ul(1)), color);
    }
}




// AUTO DETECTION OF MOVING OBJECT
// if (rectangle.width == 0 || rectangle.height == 0) {
    // Auto detection... - selects the closest
    // NEW TESTING START
    // cca 40ms - 50ms
    /*
    cv::gpu::GpuMat cc_ = cv::gpu::GpuMat(obs->getSize(), CV_32FC1);
    cc_ = cv::Scalar(0.12f);
    cv::gpu::GpuMat aa, bb;
    cv::gpu::compare(gfg_(cv::Rect(100, 100, 500, 500)), cc_(cv::Rect(100, 100, 500, 500)), aa, cv::CMP_GE);


    cv::gpu::erode(aa, bb, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));
    cv::gpu::dilate(bb, aa, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));
    cv::gpu::dilate(aa, bb, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(4, 4)), cv::Point(-1, -1), 2);

    cv::Mat fg;
    bb.download(fg);


    std::vector<std::vector<cv::Point> > countours;
    cv::findContours(fg, countours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    // CV_FILLED fills the connected components found
    cv::Mat mask = cv::Mat::zeros(fg.rows, fg.cols, CV_8UC1);
    cv::drawContours(mask, countours, -1, cv::Scalar(255), CV_FILLED);

    for (int i = 0; i < countours.size(); i++)
        cv::rectangle(mask, cv::boundingRect(countours[i]), cv::Scalar(255));
    */
// }
