#!/usr/bin/python

import sys
import math

if __name__ == "__main__":
  
  if(len(sys.argv) != 3):
    print("Usage:\n\ntracking_unit_legs height leg_length\n\theight\t\t\tVertical distance between ground plane and upper surface of the base plane of the camera unit [m]\n\tleg_length\t\t\tLength of the leg [m]")
    exit(1)
    
  h = float(sys.argv[1])
  l = float(sys.argv[2])
  
  a = math.acos(h / l)
    
  # Output
  print("leg angle:  %f deg" % (a / math.pi * 180.0))
  print("sin(abs(leg1_roty)) = %f" % math.sin(abs(a)))
  print("cos(abs(leg1_rotz)) = %f" % math.cos(abs(a)))
  