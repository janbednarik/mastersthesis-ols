#ifndef RERECORD_H
#define RERECORD_H

// ROS
#include <ros/ros.h>

// C++
#include <string>

// Project
#include <msgs/CameraUnitInfo.h>
#include <msgs/CameraUnitSensors.h>

class Rerecord
{
public:
    Rerecord();
    ~Rerecord();

public:
    static const std::string CAMERA_UNIT_INFO_TOPIC_REMAPPED;
    static const std::string CAMERA_UNIT_SENSORS_TOPIC;

private:
    ros::NodeHandle *nh_;   //!< Node handle

    // Publishers and subscribers
    ros::Subscriber cuiSub_;
    ros::Publisher cuSensorsPub_;

private:
    //! \brief onCUI Callback for CameraUnitInfo message. It is only used when wrongly recorded bag
    //! must be re-recorded.
    //! \param cuiMsg
    //!
    void onCUI(const msgs::CameraUnitInfoConstPtr& cuiMsg);

};

#endif // RERECORD_H
