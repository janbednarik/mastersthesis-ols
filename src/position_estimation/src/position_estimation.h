/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef POSITION_ESTIMATION_H
#define POSITION_ESTIMATION_H

/* If GROUND_TRUTH_POSITION is enabled, the node will subscribe
 * to he 'pose' topic of the artifical flying object, save the
 * last N messages and then compare to the computed estimation of the
 * location with the given timestamp.
 */
#define GROUND_TRUTH_POSITION 1
#define VISUALIZE_POS_HISTORY 1

// ROS
#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PoseStamped.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <visualization_msgs/Marker.h>

// C++
#include <map>
#include <iostream>
#include <fstream>
#include <numeric>

// Eigen
#include <eigen3/Eigen/Eigen>

// Project libraries
#include <msgs/TargetStateLocal.h>
#include <msgs/TargetState.h>

// Eigen
#include <eigen3/Eigen/Eigen>

// PCL
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_line.h>

//! \brief The Pos3DHistory class is the data type of the position history - it
//! carries 'size' records together with timestamps. It handles storing of records
//! and performs 3D speed vector estimation given linear motion model with contant
//! speed.
//!
class Pos3DHistory {
public:
    //! \brief Pos3DHistory Constructor
    //! \param maxSize Maximum number of records in the history.
    //!
    Pos3DHistory(int maxSize);

    //! \brief addPosition Adds a new position. If the size exceeds 'maxSize_',
    //! the oldes record is erased.
    //! \param newPos New 3D position.
    //! \param stamp Time stamp
    //!
    void addPosition(Eigen::Vector3d& newPos, ros::Time stamp);

    //! \brief estimateSpeed Computes the estimate of the 3D velocity vector.
    //! The linear motion model with constant velocity is considered. The
    //! 3D line is fittted to the history positions (3D points) using RANSAC
    //! and the average velocity vector along this line is computed.
    //! \param speed [out] estimated speed vector (3D)
    //! \return TRUE if successful estimation, FALSE otherwise (and 'speed' is undefined)
    //!
    bool estimateSpeed(Eigen::Vector3d &speed);

    // debug
    void print();

    void setPosHistPublisher(ros::Publisher* pub) { posHistPub_ = pub; }
    void setRansacLinePublisher(ros::Publisher* pub) { ransacLinePub_ = pub; }

private:
    int maxSize_;   //!< Maximum number of records in the history.

    pcl::PointCloud<pcl::PointXYZ>::Ptr positions_; //!< Positions given as pointcloud    
    std::vector<ros::Time> stamps_;                 //!< Timestamps where stamp_[i] correspond to point positions_[i]

    ros::Publisher *posHistPub_;
    ros::Publisher *ransacLinePub_;

    Eigen::Vector3d modelFrom_;
    Eigen::Vector3d modelTo_;

private:
    // debug
    void publishPosHistPC(pcl::PointCloud<pcl::PointXYZ>::Ptr pcloud);

    void publishVisMarkerLine(int id, Eigen::Vector3d from, Eigen::Vector3d to,
                              float r = 0.5, float g = 0.5, float b = 0.5, float lineThickness = 0.015, float headSize = 0.0);
};

//! \brief The PositionEstimation class Implements the weighted estimation of the position of the target in 3-space given
//! noisy measurements from the trackers and with respect to the target-base geometry.
//!
class PositionEstimation {
public:
    static const std::string TARGET_STATE_LOCAL_TOPIC;
    static const std::string TARGET_STATE_TOPIC;    

    static const int TARGET_POSITION_HISTORY_SIZE;

    static const int POS_3D_HISTORY_SIZE;   //!< Size of a 3D position estimates history.
    static const double OMEGA;              //!< Scalar used in 3D position estimation as a scalar for weight given by target-base geometry.

public:
    //! \brief PositionEstimation Constructor
    //!
    PositionEstimation();

    //! \brief PositionEstimation Destructor
    //!
    ~PositionEstimation();

    //! \brief update
    //!
    void update();

private:
    ros::NodeHandle* nh_;
    ros::Subscriber onTargetStateLocalSub_;
    ros::Publisher targetStatePub_;

    int CAMERA_UNITS_COUNT;
    std::string cameraUnitNS_;    

    // map [target_global_id -> map [camera_unit_id -> tgt_state_local] ]
    std::map<int, std::map<int, msgs::TargetStateLocal::ConstPtr> > targetsStates_;
    std::map<int, std::map<int, msgs::TargetStateLocal::ConstPtr> *> processQueue_;

    Eigen::Vector3d *tgtPosHistory_;
    int tgtPosHistIdx_;

    // 3D position histories for all targets - map[target_global_id -> Pos3DHistory]
    std::map<int, Pos3DHistory *> pos3DHistory_;

    int saveGPX_;

    std::ofstream gpxFile_;

private:
    void onTargetStateLocal(const msgs::TargetStateLocal::ConstPtr stateMsg);

    //! \brief estimateTargetPosition Computes the estimate of the target 3D position
    //! using the local observation from each camera unit.
    //! \param from [in] 3D positions of camera centers
    //! \param direction [in] direction vectors (cam_center -> observed 2D point on image plane)
    //! \param beliefs [in] tracker beliefs
    //! \return 3D position estimate
    //!
    Eigen::Vector3d estimateTargetPosition(std::vector<Eigen::Vector3d>& from, std::vector<Eigen::Vector3d>& direction, std::vector<float> &beliefs);

    ros::Time avgTimeStamp(std::vector<ros::Time>& stamps);

    Eigen::Vector3d getAvgPosition();

    void incCircBufIdx(int& idx, int size);

    void newGPXdata(std::ofstream &file, double latitude, double longitude, double altitude, double timestamp);   

#if GROUND_TRUTH_POSITION
public:    
    static const std::string POSE_TOPIC;
    static const int TGT_POSE_HISTORY_SIZE = 50;

public:
    void onPose(const geometry_msgs::PoseStampedConstPtr& pose);

private:
    ros::Subscriber onPoseSub_;
    std::string flying_object_ns_;

private:
    geometry_msgs::PoseStamped tgtPoseHistory_[TGT_POSE_HISTORY_SIZE];
    int tphIdx_;   

    geometry_msgs::PoseStamped *getClosestStampedPose(ros::Time stamp);

//debug
public:
    static const std::string POS_ESTIM_HISTORY_PCLOUD_TOPIC;

// debug
private:
    ros::Publisher posEstimPCPub_;  //!< Last N position estimations as pointcloud publisher (for rviz visualisation)
    ros::Publisher ransacLinemarkerPub_;  //!< ransac line speed estimation marker publisher

#endif // #if GROUND_TRUTH_POSITION

};

#endif // #ifndef POSITION_ESTIMATION_H
