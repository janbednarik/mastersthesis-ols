/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// project libraries
#include "tracker_ols.h"

// C++
#include <iostream>
#include <cmath>
#include <thread>
#include <chrono>

// ROS
#include <ros/ros.h>

using namespace std;

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

string matInfo2Str(cv::Mat m) {
    ostringstream strs;
    strs << m.rows << " x " << m.cols << ", " << type2str(m.type());
    return strs.str();
}

TrackerOLS::TrackerOLS(int frameWidth, int frameHeight, int step) :
    Tracker(frameWidth, frameHeight), step_(step), tracker_(), last_img_seq_id_(0)
{
    CSettings settings;

    if (!ros::param::get("/olsTracker/hist_capacity", settings.hist_capacity)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/hist_capacity.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/max_number_overlapped_in_sequence", settings.max_number_overlapped_in_sequence)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/max_number_overlapped_in_sequence.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/total_num_particles", settings.t_total_num_particles)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/max_number_overlapped_in_sequence.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/num_particles_from_movement", settings.t_num_particles_from_movement)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/max_number_overlapped_in_sequence.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/use_target_mask", settings.t_use_target_mask)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/max_number_overlapped_in_sequence.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/use_particle_median", settings.use_particle_median)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/use_particle_median.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/use_homography", settings.use_homography)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/use_homography.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/std_x", settings.t_std_x)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/std_x.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/std_y", settings.t_std_y)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/std_y.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/kappa", settings.kappa)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/kappa.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/rWidth", settings.rWidth)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/rWidth.");
        exit(1);
    }

    if (!ros::param::get("/olsTracker/rHeight", settings.rHeight)) {
        ROS_FATAL_STREAM("Could not get parameter olsTracker/rHeight.");
        exit(1);
    }

    tracker_.fromSettings(settings);
}

TrackerOLS::~TrackerOLS()
{
}

bool TrackerOLS::init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb)
{
    image(bb).copyTo(tgtTemplate_);
    cv::cvtColor(tgtTemplate_, tgtTemplate_, CV_BGR2GRAY);

    tgtTemplate_.convertTo(tgtTemplate_, CV_32FC1, 1/255.0);
    tgtMask_ = cv::Mat(tgtTemplate_.rows, tgtTemplate_.cols, CV_32FC1, 1.0);

    tracker_.observation(image, img_seq_id);

    tracker_.addTarget(target_id, img_seq_id, bb);
    target_id_ = target_id;   

    return true;
}

bool TrackerOLS::observe(uint32_t img_seq_id, const cv::Mat& image)
{
    if (img_seq_id > last_img_seq_id_)
    {
        tracker_.observation(image, img_seq_id);
        return true;
    }
    return false;
}

bool TrackerOLS::clean()
{
    tracker_.removeTargets();
}

float TrackerOLS::computeBelief(cv::Mat templ, cv::Mat templMask, cv::Mat cand, cv::Mat candMask)
{
    if((templ.rows != cand.rows) || templ.cols != cand.cols) {
        cv::resize(cand,        cand,       cv::Size(templ.rows, templ.cols));
        cv::resize(candMask,    candMask,   cv::Size(templ.rows, templ.cols));
    }

    double sum      = 0.0;
    double sumMax   = 0.0;
    double motion, appearance;
    for(int r = 0; r < templ.rows; ++r) {
        for(int c = 0; c < templ.cols; ++c) {
            motion      = std::exp(std::min(templMask.at<float>(r, c), candMask.at<float>(r, c)));
            appearance  = (1.0 - std::fabs(templ.at<float>(r, c) - cand.at<float>(r, c)));
            appearance  *= appearance;
            sum     += motion * appearance;
            sumMax  += motion;
        }
    }   

    return sum / sumMax;
}

bool TrackerOLS::track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb)
{
    bool success = false;
    if (img_seq_id > last_img_seq_id_)
    {
        tracker_.observation(image, img_seq_id);

        std::map<unsigned long long, std::shared_ptr<TargetBase> >* targets;

        tracker_.get_targets(&targets);

        // Target was successfully tracked.
        if(targets != nullptr && (*targets).count(target_id_) > 0) {
            success = true;
            std::shared_ptr<PFTarget> pf_target = std::dynamic_pointer_cast<PFTarget>((*targets)[target_id_]);
            cv::Rect bbCand = pf_target->getBoundingBox();
            bb = pf_target->getOriginBox();

            // Compute belief
            std::shared_ptr<TargetState> ts = pf_target->state();
            cv::Mat candMask  = ts->getTemplateMask();
            cv::resize(candMask, candMask, cv::Size(tgtTemplate_.cols, tgtTemplate_.rows));

            cv::Mat cand;
            cv::cvtColor(image(bbCand), cand, CV_BGR2GRAY);
            cv::resize(cand, cand, cv::Size(tgtTemplate_.cols, tgtTemplate_.rows));
            cand.convertTo(cand, CV_32FC1, 1/255.0);

            belief_ = computeBelief(tgtTemplate_, tgtMask_, cand, candMask);

        // Target was not found.
        } else {
            success = false;
            bb = cv::Rect(0, 0, image.cols, image.rows);
        }
    }


    if (img_seq_id >= last_img_seq_id_)
    { // already tracked image
        success = true;
    }

    last_img_seq_id_ = img_seq_id;

    return success;
}
