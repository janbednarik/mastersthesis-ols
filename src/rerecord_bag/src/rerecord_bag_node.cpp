// ROS
#include <ros/ros.h>

// Project libraries
#include "rerecord.h"

using namespace std;

const int SPIN_RATE = 50;

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "rerecord_bag");
    ros::start();

    Rerecord rerecord;

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        ros::spinOnce();
        looper.sleep();
    }

    ros::shutdown();
    return 0;
}
