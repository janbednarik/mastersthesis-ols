/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS
#include <ros/ros.h>

// C++
#include <iostream>

// project libraries
#include "manipulator_iface.h"

using namespace std;

const int SPIN_RATE = 50;

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "manipulator_iface");
    ros::start();

    ManipulatorIface manipulatorIf;

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        ros::spinOnce();
        manipulatorIf.update();
        looper.sleep();
    }

    ros::shutdown();
}
