/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef MANIPULATOR_OPROX_H
#define MANIPULATOR_OPROX_H

// Serial library (http://docs.ros.org/jade/api/serial/html/)
#include <serial/serial.h>

// ROS libraries
#include <ros/ros.h>
#include <keyboard/Key.h>
#include <sensor_msgs/Joy.h>


// Project libraries.
#include "manipulator.h"

using namespace std;

//! \brief The ManipulatorOprox class Controlling the custom manipulator manufacterd by Oprox, a.s.
//!
class ManipulatorOprox: public Manipulator
{
public:
    //! \brief ManipulatorOprox Constructor.
    //!     - Powers up the manipulator
    //!     - Initializes the stream of incoming messages.
    //! \param port
    //! \param baudrate
    //! \param timeout
    //!
    ManipulatorOprox(std::string port = "/dev/ttyUSB0", uint32_t baudrate = 115200, uint32_t timeout = 1000);

    //! \brief ~ManipulatorOprox Destructor.
    //!     - Powers down the manipulator.
    //!
    virtual ~ManipulatorOprox();

    //! \brief update Reads the message from the manipulator and stores current position.
    //!
    virtual void update();

    //! \brief move
    //! \param azimuth
    //! \param elevation
    //!
    virtual void move(double azimuth, double elevation);

    //! \brief moveAbsolute Moves the manipulator to the given absolute position [mrad].
    //! \param azimuth
    //! \param elevation
    //!
    virtual bool moveAbsolute(double azimuth, double elevation);

    //! \brief moveRelative Moves the manipulator relatively with regards to the current position [mrad].
    //! \param azimuth
    //! \param elevation
    //!
    virtual bool moveRelative(double azimuth, double elevation);

    //! \brief haltPan Halts pan motion and sets the azimuthal speed appropriately.
    //!
    virtual void haltPan();

    //! \brief haltTilt Halts tilt motion and sets the elevation speed appropriately.
    //!
    virtual void haltTilt();

    //! \brief step Performs one step in the azimuth, elevation or both at the same time
    //! in the given direction. Here a "step" means one step in the case of stepper
    //! motors or the smallest step given by the maximum resolution in case of the
    //! other motors.
    //! \param azimuth >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //! \param elevation >0 => positive rotation | <0 => negative rotation | ==0 => no motion.
    //!
    virtual void step(int azimuth, int elevation);

    //! \brief onManipMove
    //! \param mvMsg
    //!
    virtual void onManipMove(const msgs::ManipulatorMoveConstPtr& mvMsg);

public:
    enum state {        //!< Control states of the manipulator.
        S_INIT,
        S_WAIT_CORRECT_MSG,
        S_INIT_CHECK_POWER,
        S_INIT_WAIT_POSITION_CHANGED,
        S_FIND_INDEX,
        S_WAIT_INDEX_FOUND,
        S_RESET,
        S_MANUAL_CONTROL,
        S_ABSOLUTE_CONTROL,
        S_POWERED_DOWN
    };

    // Construction restrictions of the manipulator
    static const double MAX_AZIMUTH;                //!< Maximum azimuth (CCW)
    static const double MIN_AZIMUTH;                //!< Minimum azimuth (CW)
    static const double MAX_ELEVATION;              //!< Maximum elevation (CCW)
    static const double MIN_ELEVATION;              //!< Minimum elevation (CW)

    static const int RESPONSE_LENGTH = 20;          //!< The fixed length of the repsonse of the manipulator.
    static const int MAX_INIT_READ_TRIALS = 10000;    //!< Maximum number of attempts to read correct response from the manipulator.
    static const double MAX_AZIMUTH_SPEED;          //!< Maxium allowed azimuth motion speed [mrad].
    static const double MAX_ELEVATION_SPEED;        //!< Maxium allowed elevation motion speed [mrad].
    static const double MOTION_DELTA;               //!< The minimum difference between last and new position to check for motion [mrad].
    static const double MOVE_ABSOLUTE_DELTA;        //!< Absolute position control precision [mrad]

    static const double FULL_TURN_MRAD;             //!< Angle of full rotation turn in mrad.
    static const double HALF_TURN_MRAD;             //!< Angle of half rotation turn in mrad.

    // Constants used for slowdown motion during absolute position control.
    static const double AP_MIN_DISTANCE;
    static const double AP_MAX_DISTANCE;
    static const double AP_MIN_VELOCITY;
    static const double AP_MAX_VELOCITY;
    static const double AP_LIN_A;
    static const double AP_LIN_C;
    static const double AP_SIGM_A;

    static const int ZOOM_STEP      = 1;
    static const int FOCUS_STEP     = 1;
    static const int IRIS_TIME_STEP = 10;
    static const int IRIS_MAX_SPEED = 128;

private:
    serial::Serial* serialIf;           //!< Serial interface, communication with the manipulator.

    static const string commands[];     //!< List of prepared commands (corresponds with ManipulatorOprox::commands[])
    enum eCommands {
        MANIPULATOR_POWER_DOWN,
        MANIPULATOR_POWER_UP,
        MANIPULATOR_FIND_INDEX,
        MANIPULATOR_RESET_ZERO
    };

    //! \brief The eSlowdownFunction enum Lists the functions used
    //! to slowdown manipulator motion during absolute position control mode.
    //!
    enum eSlowdownFunction {
        SD_LIN,
        SD_EXP,
        SD_LOG,
        SD_SIGM
    };

    // Main cotrol loop states.
    state controlState_;                //!< State of the main control loop (see ManipulatorOprox::update())
    state nextState_;                   //!< Next required state.

    // The timer Controls the rate of reading the messages from the manipulator and changing control state.
    ros::Timer responseReadyTimer_;
    bool responseReady_;

    // Controlling maximum number of attempts to read message of correct format from the manipulator
    int readCorrectMsgTrials_;
    bool correctMsgReceived_;

    // Checking whether manipulator is in motion.
    bool inMotion_;    
    ros::Time lastMotion_;

    // Properties used for absolute motion control.
    double targetAzimuth_;
    double targetElevation_;
    bool azimuthReached_;
    bool elevationReached_;   
    double oldAzimuthDiff_;
    double oldElevationDiff_;

    // Manipuator initialization - properties to check whethet manipulator is powered up
    ros::Time changePosStart_;

    ros::Timer stepTimer_;
    ros::Duration stepDuration_;

    // Lense
    int zoomSpeed_;
    int focusSpeed_;
    int irisTimeStep_;
    int irisDirection_;

private:    
    //! \brief resetResponseReady Resets the flag which states whether response message from manipulator is ready.
    //!
    void resetResponseReady(const ros::TimerEvent&) { responseReady_ = true; }

    //! \brief send Sends the command to the manipulator via serial interface
    //! \param cmd
    //!
    void send(string cmd);

    //! \brief onKeyDown Keyboard key down callback
    //! \param key
    //!
    virtual void onKeyDown(const keyboard::Key::ConstPtr key);

    //! \brief onKeyUp Keyboard key up callback
    //! \param key
    //!
    virtual void onKeyUp(const keyboard::Key::ConstPtr key);

    //! \brief onJoy Joystick callback
    //! \param joy
    //!
    virtual void onJoy(const sensor_msgs::Joy::ConstPtr& joy);

    //! \brief checkResponseFormat Checks the format of the message received from the manipulator.
    //! \param msg
    //! \return
    //!
    bool checkResponseFormat(string msg);

    //! \brief inMotion Checks whether manipulator is in motion.
    //! \param resolution Specifies how long the zero motion must be observed before inMotion returns 'false'.
    //! \return True if the manipulatoris motion, false otherwise.
    //!
    bool inMotion();

    //! \brief extractAzimuthElevation Extracts and saves the azimuth and elevation from the response message from the manipulator.
    //! \param msg
    //! \param azimuth
    //! \param elevation
    //!
    void extractAzimuthElevation(string& msg, double &azimuth, double &elevation);


    //// Methods used for absolute postion control.
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //! \brief init Initalization.
    //!
    void initAbsoluteMotion();

    //! \brief updateAbsoluteMotion Controls the motion so that it would stop precisely at the required poistion.
    //! \return Flag stating whether required absolute position was reached.
    //!
    bool updateAbsoluteMotion();

    //! \brief stop Force stops the absolute position motion.
    //!
    void stopAbsoluteMotion() { azimuthReached_ = elevationReached_ = true; }

    //! \brief getValInRange Clips the value 'pos' so that it falls within
    //! range ['min', 'max']
    //! \param val Value to change.
    //! \param min Minimum range limit [mrad].
    //! \param max Maximum range limit [mrad].
    //! \param margin Specifies how big margin will be between new clipped
    //! value of 'pos' and boundary limits min/max [mrad].
    //! \return
    //!
    double getValInRange(double val, double min, double max, double margin);

    //! \brief dist2speed Maps distance (to required position) to speed of the manipulator.
    //! \param distance Remaining distance
    //! \param f Function used for mapping [ LIN | EXP | LOG ]
    //! \return Computed speed.
    //!
    double dist2vel(double distance, eSlowdownFunction f = SD_SIGM);

    //! \brief valInRange Checks whether given value falls within
    //! allowed range.
    //! \param val Value to ne checked
    //! \param min Minimum allowed value.
    //! \param max Maximum allowed value.
    //! \return
    //!
    bool valInRange(double val, double min, double max);

    //! \brief movingInRange Checks whether position 'pos' is in allowed range
    //! and if not it checks whether the position is at least not changing
    //! to the restricted direction.
    //! \param pos Current position.
    //! \param speed Current speed.
    //! \param min Minimum allowed position.
    //! \param max Maximum allowed position.
    //! \return
    //!
    bool movingInRange(double pos, double speed,double min, double max);

    //! \brief stopMotion An event which stops the motion when the STEP mode is used.
    //!
    void stopMotion(const ros::TimerEvent&);

    //// debug
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Force stops the motion
    void forceStop();

    void printPosition() { cout << "Current pocition of the manipulator: " << azimuth_ << " mrad, " << elevation_ << " mrad." << endl; }

    string controlStateStr(state s);
};

#endif // #define MANIPULATOR_OPROX_H
