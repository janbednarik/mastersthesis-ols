/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS
#include <ros/ros.h>

// Project libraries
#include "position_estimation.h"

using namespace std;

const int SPIN_RATE = 30;

int main(int argc, char *argv[])
{
    int returnValue = 0;

    ros::init(argc, argv, "position_estimation");
    ros::start();

    PositionEstimation posEst;

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        try {
            ros::spinOnce();
            posEst.update();
        } catch (exception &e) {
            cerr << "Position estimation: Exception: " << e.what() << endl;
        }
        looper.sleep();
    }

    ros::shutdown();
    return returnValue;
}
