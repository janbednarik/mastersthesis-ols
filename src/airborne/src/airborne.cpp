/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// C++
#include <cmath>

// Project libraries
#include "airborne.h"

// ROS libraries
#include <gazebo_msgs/ModelState.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>

using namespace std;

const string Airborne::MODEL_STATE_TOPIC =  "/gazebo/set_model_state";
const string Airborne::POSE_TOPIC =         "pose";

const double Airborne::MAX_X        = 30.0;
const double Airborne::MAX_Y        = 30.0;
const double Airborne::MAX_Z        = 50.0;
const double Airborne::MAX_ROLL     = M_PI;
const double Airborne::MAX_PITCH    = M_PI;
const double Airborne::MAX_YAW      = M_PI;
const double Airborne::MIN_X        = -30.0;
const double Airborne::MIN_Y        = -30.0;
const double Airborne::MIN_Z        = 1.0;
const double Airborne::MIN_ROLL     = -M_PI;
const double Airborne::MIN_PITCH    = -M_PI;
const double Airborne::MIN_YAW      = -M_PI;

Airborne::Airborne(double simTimeStep):
    simTimeT_(0.0), simTimeStep_(simTimeStep),
    x_(0.0), y_(0.0), z_(0.0),
    roll_(0.0), pitch_(0.0), yaw_(0.0),
    gazeboSimulation_(false), motionType_(MOTION_STATIC),
    firstTimeDynamicReconfigure_(true)
{
    nh_ = new ros::NodeHandle();

    if (!ros::param::get("/world_frame_id", worldFrameId_)) {
        ROS_FATAL_STREAM("Could not get parameter /world_frame_id");
        exit(1);
    }

    if (!ros::param::get("tf_prefix", tfPrefix_)) {
        ROS_FATAL_STREAM("Could not get parameter tf_prefix");
        exit(1);
    }

    if (!ros::param::get("/flying_object_ns", flyingObjectNS_)) {
        ROS_FATAL_STREAM("Could not get parameter flying_object_ns");
        exit(1);
    }

    if (!ros::param::get("model_name", modelName_)) {
        ROS_FATAL_STREAM("Could not get parameter model_name");
        exit(1);
    }

    std::string mt;
    if (!ros::param::get("~motion_type", mt)) {
        ROS_FATAL_STREAM("Could not get parameter motion_type.");
        exit(1);
    }

    if(mt.compare("static") == 0) {
        motionType_ = MOTION_STATIC;
    } else if(mt.compare("line_segment") == 0) {
        motionType_ = MOTION_LINE_SEGMENT;
    } else if(mt.compare("arc_motion_xy") == 0) {
        motionType_ = MOTION_ARC_XY;
    } else {
        ROS_FATAL_STREAM("Unknown motion type '" << mt << "'.");
        exit(1);
    }

    if((motionType_ == MOTION_LINE_SEGMENT) || (motionType_ == MOTION_ARC_XY)) {
        if (!ros::param::get("~speed", speed_)) {
            ROS_FATAL_STREAM("Could not get parameter speed");
            exit(1);
        }
    }

    if((motionType_ == MOTION_LINE_SEGMENT)) {
        double dirX, dirY, dirZ;

        if (!ros::param::get("~dir_x", dirX)) {
            ROS_FATAL_STREAM("Could not get parameter ~dir_x");
            exit(1);
        }

        if (!ros::param::get("~dir_y", dirY)) {
            ROS_FATAL_STREAM("Could not get parameter ~dir_y");
            exit(1);
        }

        if (!ros::param::get("~dir_z", dirZ)) {
            ROS_FATAL_STREAM("Could not get parameter ~dir_z");
            exit(1);
        }

        dir_ << dirX, dirY, dirZ;

        if (!ros::param::get("~distance", distance_)) {
            ROS_FATAL_STREAM("Could not get parameter ~distance");
            exit(1);
        }

        if (!ros::param::get("~acceleration", acceleration_)) {
            ROS_FATAL_STREAM("Could not get parameter ~acceleration");
            exit(1);
        }
    }

    if(motionType_ == MOTION_ARC_XY) {
        if (!ros::param::get("~radius", radius_)) {
            ROS_FATAL_STREAM("Could not get parameter ~radius");
            exit(1);
        }

        if (!ros::param::get("~max_angle", maxAngle_)) {
            ROS_FATAL_STREAM("Could not get parameter ~max_angle");
            exit(1);
        }
    }   

    double fromX, fromY, fromZ;

    if (!ros::param::get("~from_x", fromX)) {
        ROS_FATAL_STREAM("Could not get parameter ~from_x");
        exit(1);
    }

    if (!ros::param::get("~from_y", fromY)) {
        ROS_FATAL_STREAM("Could not get parameter ~from_y");
        exit(1);
    }

    if (!ros::param::get("~from_z", fromZ)) {
        ROS_FATAL_STREAM("Could not get parameter ~from_z");
        exit(1);
    }

    from_ << fromX, fromY, fromZ;

    if(ros::param::has("/gazebo_simulation")) {
        if(!ros::param::get("/gazebo_simulation", gazeboSimulation_)) {
            ROS_FATAL_STREAM("Could not get parameter gazebo_simulation");
            exit(1);
        }
    }

    modelStatePub_  = nh_->advertise<gazebo_msgs::ModelState>(MODEL_STATE_TOPIC, 1) ;
    posePub_        = nh_->advertise<geometry_msgs::PoseStamped>(string("/") + flyingObjectNS_ + string("/") + POSE_TOPIC, 1);

    // Create motion
    switch(motionType_) {
        case MOTION_STATIC:
            motion_ = new StaticMotion(from_);
            break;

        case MOTION_LINE_SEGMENT:
            motion_ = new PeriodicLineSegmentMotion(from_, dir_, distance_, speed_, acceleration_);
            break;

        case MOTION_ARC_XY:
            motion_ = new PeriodicArcMotionXY(from_, radius_, speed_, maxAngle_);
            break;       

        default:
            ROS_FATAL_STREAM("Unknown motion type!");
            exit(1);
    }

    // Dynamic reconfigure callback
    dynamic_reconfigure::Server<airborne::AirborneConfig>::CallbackType drCallback;

    drCallback = boost::bind(&Airborne::onDynamicReconfigure, this, _1, _2);
    server.setCallback(drCallback);
}

Airborne::~Airborne()
{
    if(motion_) delete motion_;
}


void Airborne::update()
{
    // Update simulation time.
    simTimeT_ += simTimeStep_;

    // Update model position
    motion_->update(simTimeT_, x_, y_, z_);

    // Update model position in Gazebo
    geometry_msgs::Pose pose;
    geometry_msgs::Point p;

    p.x = x_;
    p.y = y_;
    p.z = z_;

    pose.position = p;

    if(gazeboSimulation_) {
        gazebo_msgs::ModelState ms;
        ms.model_name = modelName_;
        ms.pose = pose;
        modelStatePub_.publish(ms);
    }

    // Send pose message for 'detection' node for ground truth tracker (debugging purposes).
    geometry_msgs::PoseStamped poseStamped;
    poseStamped.pose.position = pose.position;
    poseStamped.header.stamp = ros::Time::now();

    posePub_.publish(poseStamped);

    // Update model position in rviz
    tf::Transform tfFlyingObject;

    tfFlyingObject.setOrigin(tf::Vector3(x_, y_, z_));
    tfFlyingObject.setRotation(tf::Quaternion(tf::Vector3(0.0, 0.0, 1.0), 0.0));
    tfBcast_.sendTransform(tf::StampedTransform(tfFlyingObject, ros::Time::now(), worldFrameId_, tfPrefix_ + string("/") + string("uav_block")));
}

void Airborne::onDynamicReconfigure(airborne::AirborneConfig &config, uint32_t level)
{
    motion_->forceSetPosition(config.from_x, config.from_y, config.from_z);
}
