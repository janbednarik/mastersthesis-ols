/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// ROS
#include <ros/ros.h>

// C++
#include <iostream>

// project libraries
#include "airborne.h"

using namespace std;

const int SPIN_RATE = 50;

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "flying_objects_simulator");
    Airborne airborne(1.0 / SPIN_RATE);

    ros::start();

    // Main program rate.
    ros::Rate looper(SPIN_RATE);
    while(ros::ok()) {
        ros::spinOnce();
        airborne.update();
        looper.sleep();
    }

    ros::shutdown();

    return 0;
}
