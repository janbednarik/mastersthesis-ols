/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// project libraries
#include "tracker_tld.h"

// C++
#include <iostream>

// ROS
#include <ros/ros.h>

using namespace std;

TrackerTLD::TrackerTLD(int frameWidth, int frameHeight, int step) :
    Tracker(frameWidth, frameHeight), tld_(NULL), step_(step)
{
    // OpenTLD
    tld_= new tld::TLD();

    tld_->detectorCascade->imgWidth = frameWidth;
    tld_->detectorCascade->imgHeight = frameHeight;
    tld_->detectorCascade->imgWidthStep = step_;
}

TrackerTLD::~TrackerTLD()
{
    if(tld_ != NULL) delete tld_;
}

bool TrackerTLD::init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb)
{
    cv::Mat grey_frame;
    cvtColor(image, grey_frame, CV_BGR2GRAY);

    cv::Rect bbox = bb;

    tld_->selectObject(grey_frame, &bbox);
}

bool TrackerTLD::track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb)
{
    bool success = false;

    cv::Mat grey_frame;
    cvtColor(image, grey_frame, CV_BGR2GRAY);

    tld_->processImage(grey_frame);

    // Target was successfully tracked.
    if(tld_->currBB != NULL) {
        success = true;
        bb = *(tld_->currBB);

    // Target was not found.
    } else {
        success = false;
        bb = cv::Rect(0, 0, image.cols, image.rows);
    }

    return success;
}
