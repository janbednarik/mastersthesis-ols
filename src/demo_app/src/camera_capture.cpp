// Project libraries
#include "camera_capture.h"

// Eigen
#include <eigen3/Eigen/Eigen>

// OpenCV
#include <opencv2/core/eigen.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

const string CameraCapture::IMAGE_RAW_TOPIC = "/camera/image_raw";

CameraCapture::CameraCapture()
{
    nh_ = new ros::NodeHandle();
    onImgSub_ = nh_->subscribe(IMAGE_RAW_TOPIC, 30, &CameraCapture::onImage, this);
//    viewer = new pcl::visualization::PCLVisualizer("PCL viewer", false);

//    int viewPlane = -1;
//    viewer->createViewPort(0.0,0.0,1.0,1.0, viewPlane);
}

CameraCapture::~CameraCapture()
{
//    delete viewer;
}

void CameraCapture::onImage(const sensor_msgs::Image::ConstPtr img)
{
    cv_bridge::CvImagePtr cv_img;

    try {
        cv_img = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv::resize(cv_img->image, cv_img->image, cv::Size(640, 480));

    // Test Eigen
    //-----------------------------------------------------------------------------------------
    // Thresholding using mean pixel value as a threshold
    //

    Eigen::Map<Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >
            imgThresh(cv_img->image.data, cv_img->image.rows, cv_img->image.cols);

    // imgThresh.sum() does not work - why?
    int imgSum = 0;
    for(int r = 0; r < imgThresh.rows(); ++r) {
        for(int c = 0; c < imgThresh.cols(); ++c) {
            imgSum += imgThresh(r, c);
        }
    }
    int th = imgSum / (imgThresh.rows() * imgThresh.cols());

    imgThresh = (imgThresh.array() < th).select(Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>::Zero(imgThresh.rows(), imgThresh.cols()),
                                                Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>::Ones(imgThresh.rows(), imgThresh.cols()) * 255);

    // Test PCL
    //-----------------------------------------------------------------------------------------
    // Creating and displaying point cloud with X,Y coordinates corresponding to the positions of the image pixels
    // and Z coordinate corresponding to the values of the pixels.
    //

    pcl::PointCloud<pcl::PointXYZ>::Ptr pCloud(new pcl::PointCloud<pcl::PointXYZ>());

    for(int r = 0; r < imgThresh.rows(); ++r) {
        for(int c = 0; c < imgThresh.cols(); ++c) {
            pcl::PointXYZ p;
            p.x = c;
            p.y = r;
            p.z  = imgThresh(r, c);
            pCloud->push_back(p);
        }
    }

//    viewer.removeAllPointClouds();
//    viewer.addPointCloud(pCloud);

    cv::imshow("img", cv_img->image);
    cv::waitKey(1);
}
