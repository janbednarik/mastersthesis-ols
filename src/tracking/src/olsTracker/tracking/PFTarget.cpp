    //------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com)
//!
//! Description: Class for the target.
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

// Ioss libraries.
#include "PFTarget.h"
#include "state.h"
//#include "OCVUtils.h"

// OpenCVX libraries.
#include "opencvx/cvparticle.h"
#include "opencvx/cvparticle.h"
#include "opencvx/cvrect32f.h"
#include "opencvx/cvcropimageroi.h"

// Eigen library.
#include <eigen3/Eigen/Eigen>
#include <opencv2/core/eigen.hpp>

// MACROS
#define TEMPLATE_SIZE 32

#define MIN(a, b) (a) < (b) ? (a) : (b)
#define MAX(a, b) (a) > (b) ? (a) : (b)


// ------------- TARGET -------------------------
// Constructor.
TargetBase::TargetBase(unsigned long long id, unsigned long long obs_id) :
    id_(id),
    currentObsId_(obs_id)
{ ; }

// Destructor.
TargetBase::~TargetBase()
{
    ;
}


// Stores current state to the history.
void TargetBase::toHistory()
{
    std::shared_ptr<TargetState> s = state();
    hist_[s->getObsID()] = s;
}

// Returns state related to specified observation.
std::shared_ptr<TargetState> TargetBase::history(unsigned long long obs_id)
{
    if (hist_.count(obs_id)) return hist_[obs_id];
    else return std::shared_ptr<TargetState>();
}

//! Removes all data in history from obs_id to now inclusive.
void TargetBase::eraseHistoryFrom(unsigned long long obs_id)
{
    // Remove all states which have obs_id greater or equal to obs_id.
    std::map<unsigned long long, std::shared_ptr<TargetState> >::iterator it = hist_.lower_bound(obs_id);
    while (it != hist_.end())
        it = hist_.erase(it);
}

void TargetBase::eraseAllHistory()
{
    hist_.clear();
}


// Return the bounding box of the target.
cv::Rect PFTargetState::getBoundingBox()
{
    return boundingRect_;
}

// Constructor.
PFTarget::PFTarget(unsigned long long id, unsigned long long obs_id) :
    TargetBase(id, obs_id),
    particles_(0),
    numOfOverllapedInSequence_(0)
{

}

//! Constructor.
PFTarget::PFTarget(unsigned long long target_id, unsigned long long obs_id,
                   float cx, float cy,          // Center (in the image coordinates).
                   cv::gpu::GpuMat pattern, cv::gpu::GpuMat mask,
                   int num_particles,
                   bool logprob,
                   float std_x, float std_y,
                   float std_vx, float std_vy,
                   float std_width, float std_height,
                   float std_angle, cv::Rect bouding_box,
                   bool target_particle_median) :
    TargetBase(target_id, obs_id),
    particles_(0),
    bouding_box_(bouding_box),
    target_particle_median_(target_particle_median),
    numOfOverllapedInSequence_(0)
{

    cv::Mat ca(256, 1, CV_32F, cv::Scalar(255));
    cv::Mat cb(256, 1, CV_32F, cv::Scalar(255));
    cv::gpu::GpuMat gc(256, 1, CV_32F);

    cv::gpu::GpuMat ga(ca);
    cv::gpu::GpuMat gb(cb);

    ga.convertTo(ga, CV_32F);
    gb.convertTo(gb, CV_32F);

    cv::gpu::subtract(ga.reshape(1), gb.reshape(1), gc);

    // Download image data.
    pattern.download(template_);
    mask.download(templateMask_);

    // Create particles.
    unsigned int num_states = 7;        
    particles_ = cvCreateParticle(num_states, num_particles, logprob);    // Create particles.

    // CONFIG STD
    CvParticleState *std = new CvParticleState();
    std->x = std_x; std->y = std_y;
    std->vx = std_vx; std->vy = std_vy;
    std->width = std_width; std->height = std_height;
    std->angle = std_angle;

    // CONFIG BOUNDARIES - TODO.
    cvParticleStateConfig(particles_, cvSize(TEMPLATE_SIZE, TEMPLATE_SIZE), *std);

    // Create first particle.
    CvParticleState *s = new CvParticleState();
    s->x = cx;
    s->y = cy;
    s->width = (float) pattern.cols / TEMPLATE_SIZE;
    s->height = (float) pattern.rows / TEMPLATE_SIZE;
    s->angle = 0.0;
    s->vx = 0.0;
    s->vy = 0.0;

    // Init PF
    CvParticle *init_particles = cvCreateParticle(num_states, 1);
    cvParticleStateSet(init_particles, 0, *s);
    cvParticleInit(particles_, init_particles);
    cvReleaseParticle(&init_particles);    
    delete s; delete std;

    // Pattern && mask - image data.
    cv::gpu::resize(pattern, pattern_, cv::Size(TEMPLATE_SIZE, TEMPLATE_SIZE));
    cv::gpu::resize(mask, pattern_mask_, cv::Size(TEMPLATE_SIZE, TEMPLATE_SIZE));

    // Transformation matrix is identitity at the beginning.
    H_ = cv::Mat_<double>(3, 3);
    cv::setIdentity(H_);
}


// Destructor.
PFTarget::~PFTarget()
{
    // Delete particles.
    cvReleaseParticle(&particles_);
}

// Returns current state of the target.
std::shared_ptr<TargetState> PFTarget::state()
{
    std::shared_ptr<PFTargetState> s = std::make_shared<PFTargetState>(getID(), getCurrentObsID());

    int maxp_id = cvParticleGetMax(particles_);
    s->s_ = cvParticleStateGet(particles_, maxp_id);
    s->H_ = H_.clone();
    s->boundingRect_ = getBoundingBox();
    s->setIsOverlapped(isOverlapped());
    s->type_ = getType();
    s->description_ = getDescription();    

    // cv::imshow("Current template" , template_);

    s->template_ = template_.clone();
    s->templateMask_ = templateMask_.clone();

    return std::dynamic_pointer_cast<TargetState>(s);
}

// Return true if the target is overlapped.
bool PFTarget::isOverlapped()
{
    int maxp_id = cvParticleGetMax(particles_);
    return (2.0 * cvmGet(particles_->mask_sums, 0, maxp_id) < cv::gpu::sum(pattern_mask_)(0)) ? true : false;
}

// Return mean of velocities.
Eigen::Vector2d PFTarget::getMeanVel()
{ 
    // Empty history.
    if (hist_.empty()) return Eigen::Vector2d(0.0, 0.0);

    Eigen::Vector3d lpos;
    std::vector<Eigen::Vector2d> velocities;
    std::map<unsigned long long, std::shared_ptr<TargetState> >::iterator i = hist_.end();

    // Get current state.
    if (!isOverlapped()) {
        int maxp_id = cvParticleGetMax(particles_);
        CvParticleState s = cvParticleStateGet(particles_, maxp_id);
        lpos << s.x, s.y, 0.0;
    }
    else {
        // Find first state in which the target is not overlapped.
        while (--i != hist_.begin() && (i->second->isOverlapped()))
            ;

        if (i == hist_.begin())  // Not enough data
            return Eigen::Vector2d(0.0, 0.0);
        else
            lpos << std::dynamic_pointer_cast<PFTargetState>(i->second)->s_.x,
                    std::dynamic_pointer_cast<PFTargetState>(i->second)->s_.y,
                    0.0;
    }


    // Find the next state in which the target is not overlapped.
    std::shared_ptr<PFTargetState> s;
    do {
        i--;
        lpos += Eigen::Vector3d(0.0, 0.0, 1.0);
        if (!(s = std::dynamic_pointer_cast<PFTargetState>(i->second))->isOverlapped()) {
            lpos -= Eigen::Vector3d(s->s_.x, s->s_.y, 0.0);
            velocities.push_back(Eigen::Vector2d(lpos(0) / lpos(2), lpos(1) / lpos(2)));
            lpos << s->s_.x, s->s_.y, 0.0;
        }
    } while (i != hist_.begin() && velocities.size() < 20);


    if (velocities.size())  {
        // Compute the mean.
        Eigen::Vector2d vel, sum_vel;
        sum_vel << 0.0, 0.0;
        for (auto &vel : velocities)
            sum_vel += vel;


        Eigen::Vector2d res;
        res = Eigen::Vector2d(sum_vel(0) / velocities.size(), sum_vel(1) / velocities.size());

        return res;
    }
    else
        return Eigen::Vector2d(0.0, 0.0);
}


cv::Point PFTarget::getParticlesMedian()
{
    int mean_x = 0;
    int mean_y = 0;

    float variance_x = 0;
    float variance_y = 0;

    for (int i = 0; i < particles_->num_particles; i++)
    {
        CvParticleState s = cvParticleStateGet(particles_, i);
        mean_x += s.x;
        mean_y += s.y;
    }

    for (int i = 0; i < particles_->num_particles; i++)
    {
        CvParticleState s = cvParticleStateGet(particles_, i);
        variance_x += (mean_x - s.x) * (mean_x) - s.x;
        variance_y += (mean_y - s.y) * (mean_y) - s.y;
    }

    return cv::Point(mean_x / particles_->num_particles, mean_y / particles_->num_particles);
}


cv::Point2f PFTarget::getParticlesVariance(cv::Point mean)
{
    float variance_x = 0;
    float variance_y = 0;

    for (int i = 0; i < particles_->num_particles; i++)
    {
        CvParticleState s = cvParticleStateGet(particles_, i);
        variance_x += (mean.x - s.x) * (mean.x) - s.x;
        variance_y += (mean.y - s.y) * (mean.y) - s.y;
    }

    return cv::Point2f(variance_x, variance_y);
}

// Return bounding box/rectangle for the current state.
cv::Rect PFTarget::getBoundingBox()
{
    int maxp_id = cvParticleGetMax(particles_);
    CvParticleState s = cvParticleStateGet(particles_, maxp_id);

    if (target_particle_median_)
    {
        cv::Point part_median = getParticlesMedian();

        cv::Point2f part_variance = getParticlesVariance(part_median);

        if (part_variance.x < 10 && part_variance.y < 10)
        {
            s.x = part_median.x;
            s.y = part_median.y;
        }
    }

    // Corners.
    Eigen::Vector3d ul, ur, bl, br;
    ul << -(s.width * TEMPLATE_SIZE) / 2.0, -(s.height * TEMPLATE_SIZE) / 2.0, 1.0;
    ur << (s.width * TEMPLATE_SIZE) / 2.0, -(s.height * TEMPLATE_SIZE) / 2.0, 1.0;
    bl << ul(0), ul(1) + (s.height * TEMPLATE_SIZE), 1.0;
    br << ur(0), ur(1) + (s.height * TEMPLATE_SIZE), 1.0;

    /*
    qDebug() << "UL";
    std::cout << ul;
    qDebug() << "UR";
    std::cout << ur;
    qDebug() << "BL";
    std::cout << bl;
    qDebug() << "BR";
    std::cout << br;
    */

    // Rotate and translate.
    Eigen::Matrix3d rot, trans;
    rot = Eigen::AngleAxisd(s.angle, Eigen::Vector3d::UnitZ());
    trans <<    1.0, 0.0, s.x,
                0.0, 1.0, s.y,
                0.0, 0.0, 1.0;

    // Transform all corners to the image coordinate system.
    //Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> toImg;
    //cv::cv2eigen(H_, toImg);

    //Eigen::MatrixXd T = toImg * trans * rot;
    Eigen::MatrixXd T = trans * rot;
    ul = T * ul; ur = T * ur;
    br = T * br; bl = T * bl;

    ul(0) /=  ul(2); ul(1) /= ul(2); ul(2) = 1.0;
    ur(0) /=  ur(2); ur(1) /= ur(2); ur(2) = 1.0;
    br(0) /=  br(2); br(1) /= br(2); br(2) = 1.0;
    bl(0) /=  bl(2); bl(1) /= bl(2); bl(2) = 1.0;

    // Compute the bounding box.
    float x_min = MIN(MIN(ul(0), ur(0)), MIN(bl(0), br(0)));
    float x_max = MAX(MAX(ul(0), ur(0)), MAX(bl(0), br(0)));
    float y_min = MIN(MIN(ul(1), ur(1)), MIN(bl(1), br(1)));
    float y_max = MAX(MAX(ul(1), ur(1)), MAX(bl(1), br(1)));

    return cv::Rect(x_min, y_min, x_max - x_min, y_max - y_min);
}


cv::Rect PFTarget::getOriginBox()
{
    cv::Rect r = getBoundingBox();
    r.x = r.x + (r.width / 2) - (bouding_box_.width / 2);
    r.y = r.y + (r.height / 2) - (bouding_box_.height / 2);
    r.width = bouding_box_.width;
    r.height = bouding_box_.height;
    return r;
}

double PFTarget::getBestParticleWeight()
{
    int iMaxP = cvParticleGetMax(particles_);
    CvMat weights = *(particles_->weights);
    double maxWeight = 0.0;

    maxWeight = CV_MAT_ELEM(weights, double, 0, iMaxP);

    return maxWeight;
}
