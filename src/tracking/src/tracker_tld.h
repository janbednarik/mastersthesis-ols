/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef TRACKER_TLD_H
#define TRACKER_TLD_H

// project libraries
#include "tracker.h"

// OpenTLD
#include "TLD.h"

// C++
#include <cstdarg>

//! \brief The TrackerTLD class Tracker TLD adopted from authors Kalal Z.,
//! Matas J., Mikolajczyk K. - Tracking-learning-detection. IEEE Trans.
//! Pattern Anal. Mach. Intell., 34(7):1409–1422, July 2012.
//!
class TrackerTLD: public Tracker
{
public:
    //! \brief TrackerTLD Constructor
    //!
    TrackerTLD(int frameWidth, int frameHeight, int step);

    //! \brief TrackerTLD Destructor
    //!
    ~TrackerTLD();

    virtual bool init(uint32_t target_id, uint32_t img_seq_id, const cv::Mat& image, const cv::Rect& bb);

    virtual bool track(uint32_t img_seq_id, const cv::Mat& image, cv::Rect& bb);

    virtual bool observe(uint32_t img_seq_id, const cv::Mat &image) {}

    virtual bool clean() {}

private:
    int step_;

    // OpenTLD
    tld::TLD *tld_;
};

#endif // #ifndef TRACKER_TLD_H
