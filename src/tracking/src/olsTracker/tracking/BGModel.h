//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

#pragma once
#ifndef BGMODEL_H
#define BGMODEL_H

// OpenCV libraries.
#include <cv.h>
#include <opencv2/gpu/gpu.hpp>

// IOSS libraries.
//#include "BItem.h"
//#include "BObject.h"
//#include "ImageObservation.h"
//#include "CObject.h"

// Cuda implementation of some functions.
#include "cuda/Particles.h"
#include "config.h"

// NOTES:
// Only GPU version is currently supported.

//! Background model.
class BGModel
{
public:
    //! Constructor.
    BGModel() :        
        penaltyThreshold_(4000),      // Threshold for the rejection of the input image.
        hSize_(1280/4, 960/4),
        maxNumOfUnestimatedTransformationInSequence_(5),
        numOfUnestimatedTransformationInSequence_(0),
        initialized_(false),
        frameCounter_(0),
        m_host_(mat_create_host()),
        m_device_(mat_create_device()),
        gamma_(20.0f),
        beta_(0.05f),
        alpha_(10.0f),
        kappa_(1.02f),
        delta_(0.00025f)
    {
        ;
    }

    //! Destructor.
    virtual ~BGModel() {
        mat_free_device(&m_device_);
        mat_free_host(&m_host_);
    }

    //! New incoming observation.
    cv::Mat update(cv::gpu::GpuMat obs_F);

    //! Returns BG model in the device memory (GPU).
    cv::gpu::GpuMat getBG() const;

    //! Returns foreground mask in the host memory (CPU).    
    cv::gpu::GpuMat getFG() const;

    //! Load configuration from the settings.
    void fromSettings(CSettings &settings);

    //! Restart background model.
    void restart();

/*signals:
    //! This signal is emitted when the BG model is restared.
    void restarted();*/

protected:
    //! Returns homography matrix between two images - CPU version.
    cv::Mat homography(cv::Mat gray1, cv::Mat gray2, cv::Mat mask = cv::Mat(),
                       int maxCount = 100, double qLevel = 0.01,
                       double minDistance = 10.0,  double ransacThreshold = 0.1);

    //! Returns homography matrix between two images - GPU version.
    // This is not currently used (it is slower then CPU version :( ).
    cv::Mat homographyGPU(cv::gpu::GpuMat &gray1, cv::gpu::GpuMat & gray2);

    //! Evaluate the penalty for the transformation.
    float penalty(cv::Mat H);

protected:    
    // Simple pixel-wise gaussian model of the background.
    cv::Mat previous_;  //!< Previous image.
    cv::gpu::GpuMat previous_gpu_;
    cv::Mat r1_, r2_;   //!< Matrix for the compensation of resazing procedure in the homography estimation process.

    // GPU version.
    cv::gpu::GpuMat gmean_;                 //!< Bg mean.
    cv::gpu::GpuMat gvar_;                  //!< Bg variance.
    cv::gpu::GpuMat gmask_;                 //!< Mask.
    cv::gpu::GpuMat gfg_;                   //!< Foreground.

    // Observation.
    cv::gpu::GpuMat gmean_mes_;             //!< Obs mean.
    cv::gpu::GpuMat gvar_mes_;              //!< Obs variance.
    cv::gpu::GpuMat gmean_abs_diff_;

    // GPU v2.
    cv::gpu::GpuMat res_current_F_, res_current_U_;
    cv::gpu::GpuMat d1_, d2_;
    cv::gpu::GpuMat mask2_;

    Mat *m_host_;
    Mat *m_device_;

    float penaltyThreshold_;                                    //!< Threshold for the penalty function (if penalty is growen then this threshold then new observation will be rejected).
    cv::Size hSize_;                                            //!< Size of the image which is used for H computation.

    unsigned int maxNumOfUnestimatedTransformationInSequence_;  //!< Max number of unestimated transformation in sequence.
    unsigned int numOfUnestimatedTransformationInSequence_;     //!< Current number of unestimated transformation in sequence.
    bool initialized_;
    int frameCounter_;

    // Weighting coefficients .
    float gamma_;                                       //!< Weight for zoom penalization (20.0f)
    float beta_;                                        //!< Initial variance of the pixel (0.05f).
    float alpha_;                                       //!< Weight for intensity difference (10.0f).
    float kappa_;                                       //!< Multiplicative noise (1.02f)
    float delta_;                                       //!< Additive noise (0.00025f)
};


#endif



/*
//! Background state.
class BGState : public BItem
{
public:
    //! Constructor.
    BGState(std::shared_ptr<ioss::observations::ImageObservation> obs) :
        obs_(obs),
        tran_(cv::Mat(3, 3, CV_64FC1)),
        pen_(0.0)
    {
        cv::setIdentity(tran_);
    }

    //! Destructor.
    virtual ~BGState() { ; }

public:
    std::shared_ptr<ioss::observations::ImageObservation> obs_;  //!< Observation.
    cv::Mat tran_;                                          //!< Transformation to current image.
    double pen_;                                            //!< Transformation penalty.
};

*/
