Contents of this  README.txt file:

=======================================
0 DVD CONTENTS
1 SYSTEM REQUIREMENTS AND DEPENDENCIES
2 BUIDLING AND INSTALLING
3 RUNNING
4 USAGE MANUAL
=======================================

0 DVD CNOTENTS
--------------------------------------

/text           - Source files of this work (.tex), the figures and the final document (.pdf).
/poster         - The poster of this work presented at conference Excel@FIT 2016 (A1 format, .pdf).
/video          - The video overviewing this work presented at conference Excel@FIT 2016 (.mp4, h264).
/paper          - The paper published at conference Excel@FIT 2016 (.pdf).
/ols/src        - Source code of the OLS divided into separate ROS packages (.cpp, .h, other auxiliary formats).
/ols/data       - Part of the meadow dataset (.bag).
/ols/doc        - The generated documentation of the source code (.html).
/misc/photos    - Various photographs of the OLS taken during development, rectification and testing (.jpg).
/misc/videos    - Various videos documenting the development and testing of the OLS (.mp4, h264).
/README.txt     - This file.



1 SYSTEM REQUIREMENTS AND DEPENDENCIES
---------------------------------------

Hardware
    - dedicated GPU Nvidia with CUDA support (compute capability >= 5.0)
    
    - for full functionality, following hardware must be used:
        - P&T unit Flir PTU D46-70
        - camera Prosilica GT1290C
        - gamepad with two analog sticks
        
    - for reduced functionality (only physical simulator) no extra hardware is needed

Software
    - Linux Ubuntu 13.10 or 14.04
        - download page: http://releases.ubuntu.com/14.04/

    - ROS Indigo
        - installation instructions: http://wiki.ros.org/indigo/Installation/Ubuntu
        - must be installed in standard path (/opt/ros/indigo)
        
    - Additional ROS packages
        - avt_vimba_camera: http://wiki.ros.org/avt_vimba_camera
        - keyboard: http://wiki.ros.org/keyboard
        - joy: http://wiki.ros.org/joy
        - gazebo_ros_packages: http://wiki.ros.org/gazebo_ros_pkgs?distro=indigo
  
    - Qt 5.5
        - download page: http://www.qt.io/download/
        - must be installed in standard path (/opt/Qt/5.5)
    
    - CUDA Toolkit 7.5
        - download page: https://developer.nvidia.com/cuda-toolkit
        - must be installed in standard path (/usr/local/cuda)
        
    - Opencv 2.4 custom built with CUDA support
        - building instructions: http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html
        - must be installed in /opt/opencv24
        
    - GCC 4.4.x or higher        
        - download page: https://gcc.gnu.org/install/download.html
        
        
        
2 BUILDING AND INSTALLING
---------------------------------------

1) Open the terminal and run 'source /opt/ros/indigo/setup.bash'
2) Copy the /ols/src directory to your harddrive into arbitrary directory (let us call it ${OLS_ROOT}).
3) Copy the /ols/data directory to ${OLS_ROOT}/src/dataset.
4) Run 'cd ${OLS_ROOT}'.
5) Run 'catkin_make'.
6) Run 'source devel/setup.bash'.
7) Run 'cp -r ${OLS_ROOT}/src/sim_gazebo/models/terain ~/.gazebo/models/test/'



3 RUNNING
---------------------------------------

There are four demonstration .launch files (see below) that can be run from the terminal. Before running any .launch file, do the following:

1) Run 'source /opt/ros/indigo/setup.bash'
2) Run 'cd ${OLS_ROOT}'.
3) Run 'source devel/setup.bash'.

demo person
    - description: The 30 s video of a walking persn taken by two-camera setup (meadow dataset).
    - run: launch ols demo_person.launch
    
demo terain
    - description: The simulated environment in Gazebo. A terain with trees. Two camera units and one flying drone.
    - run: launch ols demo_terain.launch
    
demo 2 camera units
    - description: The simulated environment with two camera units and one drone flying along circle. It is meant to compare the performance of two-view vs three-view system. The tracking is automatically started. The history of estimated positions and estimated speed vector are visualized in rviz.
    - run: launch ols demo_2cus.launch

demo 2 camera units
    - description: The simulated environment with three camera units and one drone flying along circle. It is meant to compare the performance of two-view vs three-view system. The tracking is automatically started. The history of estimated positions and estimated speed vector are visualized in rviz.
    - run: launch ols demo_3cus.launch
    

    
4 USAGE MANUAL
---------------------------------------

                    KEYBOARD
                    
0-9 (alphanumeric)              Switch among CUs.
left/right arrows*              Rotate the P&T unit around azimuthal axis.
down/up arrows*                 Rotate the P&T unit around elevation axis.        
4, 6 (numeric)                  Change the horizontal size of the bounding box.
2, 8 (numeric)                  Change the vertical size of the bounding box.
Y                               Select TLD tracker.
U                               Select BGFG tracker.
I                               select GT tracker.
Enter                           Start/stop tracking.
T*                              Switch on/off regulation of the P&T motion.
S*                              Switch stepper mode on (rotate P&T unit by steps).
C*                              Switch continuous mode on (rotate P&T unit continusously).
Home*                           Rotate the P&T unit so as to reach azimuth = 0 rad.
End*                            Rotate the P&T unit so as to reach elevation = 0 rad.

                      MOUSE
                      
left button                     Start tracking the selected image region.

                 JOYSTICK/GAMEPAD
                 
axis-0                          Rotate the P&T unit around azimuthal axis.
axis-1                          Rotate the P&T unit around elevation axis.
axis-2                          Change the horizontal size of the bounding box.
axis-3                          Change the vertical size of the bounding box.
button-9 (start)                Start/stop tracking.         

*The window managed by keyboard ROS node must be in focus.
