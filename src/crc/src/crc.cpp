/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#include "crc.h"

using namespace std;

char crcTable[256];

void crcInit()
{
    unsigned char remainder;
    int dividend;
    unsigned char bit;

    for(dividend = 0; dividend < 256; ++dividend) {
        remainder = (unsigned char)dividend;

        for(bit = 8; bit > 0; --bit) {
            if((remainder & 0x80) == 0x80) {
                remainder = ((remainder) << 1) ^ 0x97;
            } else {
                remainder = (remainder << 1);
            }
        }
        crcTable[dividend] = (unsigned char)remainder;
    }
}

bool checkCRC(string &message)
{
    string data = message.substr(0, message.length() - 1);
    return crc8(QByteArray(data.c_str(), data.length()), data.length()) == (unsigned char)message[message.length() - 1];
}

unsigned char crc8(QByteArray message, int len)
{
    unsigned char c = 0x00;
    unsigned char i;

    for (i = 0; i < len; ++i) {
        c = crcTable[(c) ^ (unsigned char)(message[i])];
    }

    return c;
}
