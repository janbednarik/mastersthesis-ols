file(GLOB SOURCES *.cpp)
file(GLOB HEADERS *.h)


add_subdirectory(cuda)

INCLUDE_DIRECTORIES(cuda)

add_library (PFTracker STATIC ${SOURCES} ${HEADERS} )


