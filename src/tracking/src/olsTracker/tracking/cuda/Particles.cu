//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------

// Tymble libraries.
#include "Particles.h"
#include "particles_evaluation_kernel.cu"

// CUDA libraries.
#include <cuda_runtime.h>
#include <vector_types.h>
#include <device_functions.h>

// Standard libraries.
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <cstdlib>
#include <assert.h>

float ranf()
{
    return (double)rand() / (double)RAND_MAX;
}


float box_muller(float m, float s)	/* normal random variate generator */
{				        /* mean m, standard deviation s */
    float x1, x2, w, y1;
    static float y2;
    static int use_last = 0;

    if (use_last)		        /* use value from previous call */
    {
        y1 = y2;
        use_last = 0;
    }
    else
    {
        do {
            x1 = 2.0 * ranf() - 1.0;
            x2 = 2.0 * ranf() - 1.0;
            w = x1 * x1 + x2 * x2;
        } while ( w >= 1.0 );

        w = sqrt( (-2.0 * log( w ) ) / w );
        y1 = x1 * w;
        y2 = x2 * w;
        use_last = 1;
    }

    return( m + y1 * s );
}

// --------------- TRANSFORMATION MATRIX FUNCTIONS -----------------------------------
Mat *mat_create_host()
{
    Mat *m = (Mat *) malloc(sizeof(struct Mat));
    m->data_ = (double *) malloc(sizeof(double) * 9);
    return m;
}

Mat *mat_create_device()
{
    Mat *m = (Mat *) malloc(sizeof(struct Mat));
    cudaMalloc(&(m->data_), sizeof(double) * 9);
    return m;
}

void mat_free_host(Mat **m)
{
    free((*m)->data_);
    free(*m);
    *m = 0;
}

void mat_free_device(Mat **m)
{
    cudaFree((*m)->data_);
    free(*m);
    *m = 0;
}

void mat_copy_host2device(Mat *dest, Mat *src)
{
     cudaMemcpy(dest->data_, src->data_, sizeof(double) * 9, cudaMemcpyHostToDevice);
}

// ------------------------ IMAGE ---------------------------------------------------
Img* img_create_dev_ptr(Img *i)
{

    struct Img * t = 0;
    cudaMalloc(&t, sizeof(struct Img));
    cudaMemcpy(t, i, sizeof(struct Img), cudaMemcpyHostToDevice);
    assert(cudaGetLastError()==cudaSuccess);

    return t;
}

void img_free_dev_ptr(Img** i)
{
    cudaFree(*i);
    *i = 0;
}

Img* img_create_host(int width, int height)
{
    Img *i = 0;
    cudaMallocHost(&i, sizeof(struct Img));

    if (i) {
        i->height_ = height;
        i->width_ = width;
        i->pitch_data_ = width * sizeof(float);        
        cudaMallocHost(&(i->data_), width * height * sizeof(float));                
    }

    return i;
}

struct Img *img_create_device(int width, int height)
{
    struct Img *t = (struct Img *) malloc(sizeof(struct Img));
    t->width_ = width;
    t->height_ = height;
    if (cudaSuccess != cudaMallocPitch(&(t->data_), &(t->pitch_data_), t->width_ * sizeof(float), t->height_)) {
        std::cerr << "[img_create_device()]: not enough device memory!" << std::endl;
        free(t);
        return 0;
    }

    return t;
}

// Copy elements from the host to the device.
void img_copy_raw2device(Img *dest, float *data, int pitch_data)
{
    // std::cout << dest->height_ << "x" << dest->width_ << std::endl;
    cudaMemcpy2D(dest->data_, dest->pitch_data_, data, pitch_data, dest->width_ * sizeof(float), dest->height_, cudaMemcpyHostToDevice);
}

void img_copy_host2raw(float *dest, int pitch_dest, Img *src)
{
    cudaMemcpy2D(dest, pitch_dest, src->data_, src->pitch_data_, src->width_ * sizeof(float), src->width_, cudaMemcpyHostToHost);
}


// Copies elements from the host to the device.
void img_copy_host2device(Img *dest, Img *src)
{
    cudaMemcpy2D(dest->data_, dest->pitch_data_, src->data_, src->pitch_data_, src->width_ * sizeof(float), src->height_, cudaMemcpyHostToDevice);    
}

// Copies elements from the device to the host.
void img_copy_device2host(Img* dest, Img *src)
{
    cudaMemcpy2D(dest->data_, dest->pitch_data_, src->data_, src->pitch_data_, src->width_ * sizeof(float), src->height_, cudaMemcpyDeviceToHost);        
}

bool img_is_equal_host(Img *a, Img *b)
{
    if (a == b) return true;
    else if (a->width_ != b->width_ || a->height_ != b->height_) return false;

    // Comparison.
    bool equal = true;
    for (int y = 0; y < a->height_; y++) {
        for (int x = 0; x < a->width_; x++) {
            equal &= (a->data_[y * a->width_ + x] == b->data_[y * a->width_ + x]);
            std::cout << a->data_[y * a->width_ + x] << " x " << b->data_[y * a->width_ + x] << std::endl;
        }        

    }

    return equal;
}

// Deallocate host sources.
void img_free_host(Img **i)
{
    cudaFreeHost((*i)->data_);    
    cudaFreeHost(*i);
    *i = 0;
}

// Deallocate device sources.
void img_free_device(Img **i)
{
    cudaFree((*i)->data_);    
    free(*i);
    *i = 0;
}
// Allocate header in the device memory.
Img* img_create_header(int width, int height, int pitch, float *data)
{
    struct Img *t = new Img(); //(struct Img *) malloc(sizeof(struct Img));
    t->width_ = width;
    t->height_ = height;
    t->pitch_data_  = pitch;
    t->data_ = data;

    return t;
}

// Deallocate img header.
void img_free_header(Img **i)
{
    delete(*i);
    *i = 0;
}

// Copy part of the source image to the destination image.
void img_copy_device2device(Img *dst, Img *src, float x, float y, float sx, float sy, float yaw)
{
    // Transformation.
    float tc_x = -(dst->width_ - 1.0f) / 2.0f;
    float tc_y = -(dst->height_ - 1.0f) / 2.0f;

    Mat *m_host = mat_create_host();
    Mat *m_device = mat_create_device();
    m_host->data_[0] = sx * cos(yaw);   m_host->data_[1] = -sx * sin(yaw);  m_host->data_[2] = sx *(cos(yaw) *tc_x - sin(yaw) * tc_y) + x;
    m_host->data_[3] = sy * sin(yaw);   m_host->data_[4] = sy * cos(yaw);   m_host->data_[5] = sy * (sin(yaw) * tc_x + cos(yaw) * tc_y) + y;
    m_host->data_[6] = 0.0;             m_host->data_[7] = 0.0;             m_host->data_[8] = 1.0;
    mat_copy_host2device(m_device, m_host);

    // Set up kernel sources.
    cudaChannelFormatDesc channelDescA = cudaCreateChannelDesc<float>();
    cudaBindTexture2D(NULL, t_img1,  src->data_, channelDescA, src->height_, src->width_, src->pitch_data_);

    dim3 threads(16, 16, 1);
    dim3 blocks(dst->width_ / threads.x, dst->height_ / threads.y, 1);

    struct Img *dptr_src = img_create_dev_ptr(src);
    struct Img *dptr_dst = img_create_dev_ptr(dst);

    // Lunch kernel
    img_copy_kernel <<< blocks, threads >>> (dptr_dst, dptr_src, (double *) m_device->data_);

    // Free kernel sources.
    img_free_dev_ptr(&dptr_dst);
    img_free_dev_ptr(&dptr_src);

    cudaUnbindTexture(t_img1);          // Unbide texture.

    mat_free_device(&m_device);
    mat_free_host(&m_host);
}

// ------------------------ PARTICLES -----------------------------------------------
struct Particles* particles_create_dev_ptr(Particles *p)
{
    Particles *t = 0;
    cudaMalloc((void **) &t, sizeof(struct Particles));
    cudaMemcpy(t, p, sizeof(struct Particles), cudaMemcpyHostToDevice);

    return t;
}

void particles_free_dev_ptr(Particles **p)
{
    cudaFree(*p);
    *p = 0;
}

//! Returns allocated Particles structure in the host memory.
struct Particles* particles_create_host(int count)
{
    Particles *p;
    cudaMallocHost(&p, sizeof(Particles));

    if (p) {
        p->count_ = count;
        cudaMallocHost(&(p->x_),         sizeof(float) * count);
        cudaMallocHost(&(p->y_),         sizeof(float) * count);
        cudaMallocHost(&(p->s_x_),       sizeof(float) * count);
        cudaMallocHost(&(p->s_y_),       sizeof(float) * count);
        cudaMallocHost(&(p->yaw_),       sizeof(float) * count);
        cudaMallocHost(&(p->fitness_),   sizeof(float) * count);
        cudaMallocHost(&(p->mask_sum_),  sizeof(float) * count);
    }

    return p;
}

//! Returns allocated Particles structure in the device memory.
Particles* particles_create_device(int count)
{    
    Particles *p = (Particles *) malloc(sizeof(struct Particles));
    if (p) {
        p->count_ = count;
        cudaMalloc(&(p->x_),        sizeof(float) * count);
        cudaMalloc(&(p->y_),        sizeof(float) * count);
        cudaMalloc(&(p->s_x_),      sizeof(float) * count);
        cudaMalloc(&(p->s_y_),      sizeof(float) * count);
        cudaMalloc(&(p->yaw_),      sizeof(float) * count);
        cudaMalloc(&(p->fitness_),  sizeof(float) * count);
        cudaMalloc(&(p->mask_sum_), sizeof(float) * count);

        // Only for the structure in the device.
        cudaMalloc(&(p->fitness_temp_),  sizeof(float) * count * 32);       // Size has to be same as width of the template!
        cudaMalloc(&(p->mask_sum_temp_), sizeof(float) * count * 32);


        // Init array for random generator - create host

        // cudaMalloc(&(p->rnd_),  sizeof(unsigned int) * count);       // Size has to be same as width of the template!
        /*
        unsigned int *rnd;
        cudaMallocHost(&rnd, sizeof(unsigned int) * count);
        for (int i = 0; i < count; i++) rnd[i] = rand();
        cudaMemcpy(p->rnd_, rnd, sizeof(unsigned int) * count, cudaMemcpyHostToDevice);
        cudaFreeHost(rnd);
        */
    }


    return p;
}

//! Frees memory.
void particles_free_host(Particles **p)
{
    if (!p || !(*p)) return;

    cudaFreeHost((*p)->x_);
    cudaFreeHost((*p)->y_);
    cudaFreeHost((*p)->s_x_);
    cudaFreeHost((*p)->s_y_);
    cudaFreeHost((*p)->yaw_);
    cudaFreeHost((*p)->fitness_);    
    cudaFreeHost((*p)->mask_sum_);
    cudaFreeHost(*p);
    *p = 0;
}

//! Frees memory.
void particles_free_device(Particles **p)
{
    if (!p || !(*p)) return;

    cudaFree((*p)->x_);
    cudaFree((*p)->y_);
    cudaFree((*p)->s_x_);
    cudaFree((*p)->s_y_);
    cudaFree((*p)->yaw_);
    cudaFree((*p)->fitness_);
    cudaFree((*p)->mask_sum_);
    cudaFree((*p)->fitness_temp_);    
    cudaFree((*p)->mask_sum_temp_);

    free(*p);
    *p = 0;
}

//! Copies data from the host structure to the device structure.
void particles_copy_host2device(Particles *h, Particles *d)
{
    cudaMemcpy(d->x_,           h->x_,          sizeof(float) * h->count_, cudaMemcpyHostToDevice);
    cudaMemcpy(d->y_,           h->y_,          sizeof(float) * h->count_, cudaMemcpyHostToDevice);
    cudaMemcpy(d->s_x_,         h->s_x_,        sizeof(float) * h->count_, cudaMemcpyHostToDevice);
    cudaMemcpy(d->s_y_,         h->s_y_,        sizeof(float) * h->count_, cudaMemcpyHostToDevice);
    cudaMemcpy(d->yaw_,         h->yaw_,        sizeof(float) * h->count_, cudaMemcpyHostToDevice);
    cudaMemcpy(d->mask_sum_,    h->mask_sum_,   sizeof(float) * h->count_, cudaMemcpyHostToDevice);
    cudaMemcpy(d->fitness_,     h->fitness_,    sizeof(float) * h->count_, cudaMemcpyHostToDevice);
}

//! Copies data from the device structure to the host structure.
void particles_copy_device2host(Particles *h, Particles *d)
{
    cudaMemcpy(h->x_,           d->x_,          sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
    cudaMemcpy(h->y_,           d->y_,          sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
    cudaMemcpy(h->s_x_,         d->s_x_,        sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
    cudaMemcpy(h->s_y_,         d->s_y_,        sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
    cudaMemcpy(h->yaw_,         d->yaw_,        sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
    cudaMemcpy(h->mask_sum_,    d->mask_sum_,   sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
    cudaMemcpy(h->fitness_,     d->fitness_,    sizeof(float) * d->count_, cudaMemcpyDeviceToHost);
}

//! Copies data from matrix (raw) to the device structure
// Only data which is needed for evaluating is coppied; therefore velocity is not included.
void particles_copy_raw2device(Particles *p, float *raw)
{
    cudaMemcpy(p->x_, &(raw[0 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);    // x
    cudaMemcpy(p->y_, &(raw[1 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);    // y    
    /*
    cudaMemcpy(p->y_, &(raw[2 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);    // vx
    cudaMemcpy(p->y_, &(raw[3 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);    // vy
    */
    cudaMemcpy(p->s_x_, &(raw[4 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);  // width
    cudaMemcpy(p->s_y_, &(raw[5 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);  // height
    cudaMemcpy(p->yaw_, &(raw[6 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);  // angle

    /*
    // Another version
    cudaMemcpy(p->s_x_, &(raw[4 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);  // width
    cudaMemcpy(p->s_y_, &(raw[5 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);  // height

    cudaMemcpy(p->yaw_, &(raw[6 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);  // angle
    */
    // cudaMemcpy(p->x_, &(raw[2 * p->count_]), sizeof(float) * p->count_, cudaMemcpyHostToDevice);

}

//! Pattern
void particles_generate2host(Particles *p, ParticlesNoiseParameters *params)
{
    // Across all particles...
    for (int i = 0; i < p->count_; i++) {
        p->x_[i] = box_muller(params->x_mean_, params->x_s_);
        p->y_[i] = box_muller(params->y_mean_, params->y_s_);

        p->s_x_[i] = box_muller(params->s_x_mean_, params->s_x_s_);
        p->s_y_[i] = box_muller(params->s_y_mean_, params->s_y_s_);

        p->yaw_[i] = box_muller(params->yaw_mean_, params->yaw_s_);
    }
}

//! Print particles...
void particles_print_host(Particles *p)
{
    for (int i = 0; i < p->count_; i++)
        std::cout << i << ": " << p->fitness_[i] << std::endl;
}


