/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "regulator.h"

using namespace std;

SpeedFunc::SpeedFunc(double a, double b) :
    a_(a), b_(b),
    xMin_(0.0), xMax_(std::numeric_limits<double>::max())
{
    ;
}

SpeedFunc::SpeedFunc() :
    a_(0.0), b_(0.0),
    xMin_(0.0), xMax_(std::numeric_limits<double>::max())
{
    ;
}

SpeedFunc::~SpeedFunc()
{
    ;
}

SpeedFuncLin::SpeedFuncLin(double a, double b) :
    SpeedFunc(a, b)
{
    ;
}

SpeedFuncLin::SpeedFuncLin(double xMax, double yMax, double b, double xMin)
{
    a_ = (yMax - b) / xMax;
    b_ = b;
    xMin_ = xMin;
    xMax_ = xMax;
}

SpeedFuncLin::~SpeedFuncLin()
{
    ;
}

double SpeedFuncLin::getY(double x)
{
    double xAbs = std::min(xMax_, std::abs(x));
    return (xAbs < xMin_) ? 0.0 : (a_ * xAbs + b_) * ((x > 0) ? 1.0 : -1.0);
}


SpeedFuncPow::SpeedFuncPow(double a, double b, double N) :
    SpeedFunc(a, b), N_(N)
{
    ;
}

SpeedFuncPow::SpeedFuncPow(double xMax, double yMax, double b, double N, double xMin) :
    N_(N)
{
    a_ = (yMax - b) / std::pow(xMax, N_);
    b_ = b;
    xMin_ = xMin;
    xMax_ = xMax;
}

SpeedFuncPow::~SpeedFuncPow()
{
    ;
}

double SpeedFuncPow::getY(double x)
{
    double res;
    double xAbs = std::min(xMax_, std::abs(x));

    if(xAbs < xMin_) {
        res = 0.0;
    } else {
        res = (a_ * std::pow(xAbs, N_) + b_) * ((x > 0) ? 1.0 : -1.0);
    }

    return res;
}

const int Position2DPredictor::OPT_AXIS_POS_HISTORY_SIZE = 150;

Position2DPredictor::Position2DPredictor(int predHistSize) :
    predHistSize_(predHistSize), optAxisPosHistSize_(OPT_AXIS_POS_HISTORY_SIZE),
    posHistory_(NULL), t_(NULL), wx_(NULL), wy_(NULL),
    it_(0), iwx_(0), iwy_(0), iPosHist_(0), state_(INITIALIZATION),
    tAvg_(ros::Duration(0.0)), wxAvg_(0.0), wyAvg_(0.0),
    tOld_(ros::Time::now()), xOld_(0.0), yOld_(0.0),
    invoc_(0),
    xTgtDiffOld_(0.0), yTgtDiffOld_(0.0), xmOld_(0.0), ymOld_(0.0),
    stampOld_(ros::Time())
{
    posHistory_ = new StampedPos[optAxisPosHistSize_];
    t_          = new ros::Duration[predHistSize_]();
    wx_         = new double[predHistSize_]();
    wy_         = new double[predHistSize_]();

}

Position2DPredictor::~Position2DPredictor()
{
    if(posHistory_) delete[] posHistory_;
    if(t_)          delete[] t_;
    if(wx_)         delete[] wx_;
    if(wy_)         delete[] wy_;
}

void Position2DPredictor::getNextPos(double dx, double dy, double& dxEstim, double& dyEstim, ros::Time stamp, ros::Duration dt)
{
    dxEstim = 0.0;
    dyEstim = 0.0;

    StampedPos* pos = findStampedPos(stamp);
    double xm = pos->x_;
    double ym = pos->y_;
    ros::Time t = pos->stamp_;
    double xTgtDiff = dx;
    double yTgtDiff = dy;

    // Avoid division by zero if two cosecutive messages with the same timestamp arrive.
    if((t - stampOld_).toSec() == 0.0) return;

    double Dx = xTgtDiff - xTgtDiffOld_ + (xm - xmOld_);
    double Dy = yTgtDiff - yTgtDiffOld_ + (ym - ymOld_);
    double wxNew = Dx / (t - stampOld_).toSec();
    double wyNew = Dy / (t - stampOld_).toSec();
    ros::Duration tNew  = t - stampOld_;

    wxNew = (std::abs(wxNew) > 0.02) ? wxNew : 0.0;
    wyNew = (std::abs(wyNew) > 0.02) ? wyNew : 0.0;

    xmOld_ = xm;
    ymOld_ = ym;
    stampOld_ = t;
    xTgtDiffOld_ = xTgtDiff;
    yTgtDiffOld_ = yTgtDiff;

    switch(state_) {
        case INITIALIZATION:
            state_ = BUILDING_HISTORY;
            break;

        case BUILDING_HISTORY:
            t_[it_] = tNew; wx_[iwx_] = wxNew; wy_[iwy_] = wyNew;
            incCircBufIdx(it_, predHistSize_); incCircBufIdx(iwx_, predHistSize_); incCircBufIdx(iwy_, predHistSize_);

            // History filled, compute averages
            if(it_ == 0) {
                tAvg_   = timeAverage(t_, predHistSize_);
                wxAvg_  = valAverage(wx_, predHistSize_);
                wyAvg_  = valAverage(wy_, predHistSize_);
                state_ = PREDICTION;
            }
            break;

        case PREDICTION:
            // Compute new moving averages
            tAvg_  = tAvg_ + ros::Duration(tNew.toSec() / predHistSize_) - ros::Duration(t_[it_].toSec() / predHistSize_);
            wxAvg_ = wxAvg_ + wxNew / predHistSize_ - wx_[iwx_] / predHistSize_;
            wyAvg_ = wyAvg_ + wyNew / predHistSize_ - wy_[iwy_] / predHistSize_;

            t_[it_]   = tNew; wx_[iwx_] = wxNew; wy_[iwy_] = wyNew;
            incCircBufIdx(it_, predHistSize_); incCircBufIdx(iwx_, predHistSize_); incCircBufIdx(iwy_, predHistSize_);

            dxEstim = ((std::abs(wxAvg_) > 0.02) ? wxAvg_ : 0.0) * ((dt.toSec() == 0.0) ? tAvg_.toSec() : dt.toSec());
            dyEstim = ((std::abs(wyAvg_) > 0.02) ? wyAvg_ : 0.0) * ((dt.toSec() == 0.0) ? tAvg_.toSec() : dt.toSec());

            break;
    }
}

void Position2DPredictor::reset()
{
    state_ = INITIALIZATION;
    it_ = iwx_ = iwy_ = 0;
    tAvg_ = ros::Duration(0.0);
    wxAvg_ = wyAvg_ = 0.0;
    invoc_ = 0;
    xTgtDiffOld_ = 0.0;
    yTgtDiffOld_ = 0.0;
    xmOld_ = 0.0;
    ymOld_ = 0.0;
    stampOld_ = ros::Time();

    // Set the circular buffers to initial values.
    for(int i = 0; i < predHistSize_; ++i) {
        t_[i]  = ros::Duration(0.0);
        wx_[i] = 0.0;
        wy_[i] = 0.0;
    }

    for(int i = 0; i < optAxisPosHistSize_; ++i) {
        posHistory_[i] = StampedPos();
    }
}

void Position2DPredictor::updateStampedPos(double azi, double ele, ros::Time stamp)
{
    posHistory_[iPosHist_] = StampedPos(azi, ele, stamp);
    incCircBufIdx(iPosHist_, optAxisPosHistSize_);
}

Position2DPredictor::StampedPos *Position2DPredictor::findStampedPos(ros::Time &stamp)
{
    int idx = iPosHist_;
    decCircBufIdx(idx, optAxisPosHistSize_);

    double minTimeDiff = std::numeric_limits<double>::max();
    int bestIdx = -1;

    for(int i = 0; i < optAxisPosHistSize_; ++i) {
        double diff = std::abs((stamp - posHistory_[idx].stamp_).toSec());

        if(diff < minTimeDiff) {
            minTimeDiff = diff;
            bestIdx = idx;
        } else {
            break;
        }
        decCircBufIdx(idx, optAxisPosHistSize_);
    }

    assert(bestIdx != - 1);

    return &(posHistory_[bestIdx]);
}

double Position2DPredictor::valAverage(double *valArray, int num)
{
    double sum = 0.0;

    for(int i = 0; i < num; ++i) {
        sum += valArray[i];
    }

    return sum / num;
}

ros::Duration Position2DPredictor::timeAverage(ros::Duration *timeArray, int num)
{
    ros::Duration timeSum = ros::Duration(0.0);

    for(int i = 0; i < num; ++i) {
        timeSum += timeArray[i];
    }

    return ros::Duration(timeSum.toSec() / num);
}

Regulator::Regulator(SpeedFunc *speedFunc, Position2DPredictor *predictor) :
    sf_(speedFunc), pred_(predictor)
{
    ;
}

Regulator::~Regulator()
{
    if(sf_)     delete sf_;
    if(pred_)   delete pred_;
}

void Regulator::regulate(double aziAng, double eleAng, double& aziSpeed, double& eleSpeed, ros::Time stamp, ros::Duration period)
{
    double estTgtAziDiff;
    double estTgtEleDiff;

    pred_->getNextPos(aziAng, eleAng, estTgtAziDiff, estTgtEleDiff, stamp, period);

    aziSpeed = sf_->getY(aziAng + estTgtAziDiff);
    eleSpeed = sf_->getY(eleAng + estTgtEleDiff);
}

void Regulator::reset()
{
    pred_->reset();
}

void Regulator::updateStampedPos(double azi, double ele, ros::Time stamp)
{
    pred_->updateStampedPos(azi, ele, stamp);
}


// DEBUG

void Position2DPredictor::printValArray(double* valArray)
{
    cout << "[";
    for(int i = 0; i < predHistSize_; ++i) {
        cout << valArray[i];
        if(i < predHistSize_ - 1) cout << ", ";
    }
    cout << "]";
}

void Position2DPredictor::printTimeArray(ros::Duration *timeArray)
{
    cout << "[";
    for(int i = 0; i < predHistSize_; ++i) {
        cout << timeArray[i].toSec();
        if(i < predHistSize_ - 1) cout << ", ";
    }
    cout << "]";
}
