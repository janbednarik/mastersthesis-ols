//------------------------------------------------------------------------------
//!
//! Project:     IOSS
//! Authors:     David Herman (herman.dav AT gmail.com), Jiri Zupka (jzupka@rcesystems.cz)
//!
//! Description: Background model.
//!
//------------------------------------------------------------------------------
//!
//!             This project was financially supported by project
//!                  TIP FR-TI1/195 funds provided by MPO CR.
//!
//------------------------------------------------------------------------------
// TODO: optimization on the GPU (use stream class)

// OpenCV libraries.

// Qt libraries.

// IOSS libraries.
#include "BGModel.h"
#include <eigen3/Eigen/Eigen>
#include <opencv2/core/eigen.hpp>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/calib3d/calib3d.hpp>
//#include "TicTac.h"
//#include "OCVUtils.h"
//#include "MLog.h"

cv::Mat BGModel::update(cv::gpu::GpuMat obs_F)
{
    frameCounter_++;

    if (!initialized_) {
        // Init..
        // First time.
        cv::gpu::GpuMat tmp1, tmp2;
        cv::gpu::resize(obs_F, tmp1, hSize_);
        tmp1.convertTo(tmp2, CV_8UC1, 255.0f);
        tmp2.download(previous_);

        //tmp1.convertTo(previous_gpu_, CV_8UC1, 255.0f);


        gmask_ = cv::gpu::GpuMat(obs_F.size(), CV_8UC1);

        // Model init.
        gvar_ = cv::gpu::GpuMat(obs_F.size(), CV_32FC1);
        gvar_ = cv::Scalar(0.05f);
        gfg_ = cv::gpu::GpuMat(obs_F.size(), CV_32FC1);
        gfg_ = cv::Scalar(0.0f);

        gmean_ = obs_F.clone();

        // Compute ratio.
        double rWidth = obs_F.size().width / hSize_.width;
        double rHeight = obs_F.size().height / hSize_.height;

        r1_ = (cv::Mat_<double>(3, 3) <<
            1.0 / rWidth, 0.0, 0.0,
            0.0, 1.0 / rHeight, 0.0,
            0.0, 0.0, 1.0);

        r2_ = (cv::Mat_<double>(3, 3) <<
            rWidth, 0.0, 0.0,
            0.0, rHeight, 0.0,
            0.0, 0.0, 1.0);

        cv::Mat1d iM(3, 3);
        cv::setIdentity(iM);

        numOfUnestimatedTransformationInSequence_ = 0;
        initialized_ = true;
        return iM; // Return identity matrix.
    }
    else {
        // Resize to the appropriate size for lower computation consumption.
        cv::Mat current;
        cv::gpu::resize(obs_F, res_current_F_,  hSize_);
        res_current_F_.convertTo(res_current_U_, CV_8UC1, 255.0f);
        res_current_U_.download(current);

        /*cv::Mat m;
        obs_F.download(m);
        cv::namedWindow( "Display2", cv::WINDOW_NORMAL );
        cv::imshow("Display2", m);*/

        cv::Mat T = homography(previous_, current);
        //cv::Mat T = homographyGPU(previous_gpu_, res_current_U_);

        if (T.empty() || penalty(T) > penaltyThreshold_ ) {
            current.copyTo(previous_);
            //res_current_U_.copyTo(previous_gpu_);
            if (!T.empty()) { }//MLOG_WRN("Transformation matrix cannot be estalished (penalty is " + std::string::number(penalty(T)) + ")"); }
            else { }//MLOG_WRN("Transformation matrix cannot be estalished - no keypoints."); }

            if (++numOfUnestimatedTransformationInSequence_ > maxNumOfUnestimatedTransformationInSequence_) {
                //MLOG_WRN("Number of attempts to estimate the transformation has been reached. Background model is going to restart");
                restart();
            }

            // Transformation cannot be established.
            return cv::Mat();
        }
        else
        {
            numOfUnestimatedTransformationInSequence_  = 0;
            cv::Mat H = r2_ * T * r1_;                                  // Resizing compensation.

            // Transform bg model to the current frame.
            gmean_mes_ = obs_F;
            gmask_ = cv::Scalar(255);

            // Warp perspective - we use our own implementation because the opencv function gpu::warpPerspective works incorrectly
            // (for some transformations returns black image (tested with OpenCV 2.4.2 and 2.4.3)).
            cv::gpu::GpuMat tmp = cv::gpu::GpuMat(gmean_.rows, gmean_.cols, gmean_.type());

            // Compute inverse transformation (it is better to map pixels from the destination to the source because the tessel units are utilized).
            cv::Mat H_inv;
            cv::invert(H, H_inv);
            cv::Mat coeffsMat(3, 3, CV_64F, reinterpret_cast<void*>(m_host_->data_));
            H_inv.convertTo(coeffsMat, coeffsMat.type());
            mat_copy_host2device(m_device_, m_host_);

            // Convert to the CUDA structures.
            Img *img_in = img_create_header(gmean_.cols, gmean_.rows, gmean_.step, (float *) gmean_.data);
            Img *img_out = img_create_header(tmp.cols, tmp.rows, tmp.step, (float *) tmp.data);

            // Warp gmean.
            warp_perspective(img_in, img_out, m_device_->data_); tmp.copyTo(gmean_);

            // Warp gvar.
            img_in->data_ = (float *) gvar_.data;
            warp_perspective(img_in, img_out, m_device_->data_); tmp.copyTo(gvar_);

            // Warp mask - get mask respectively.
            warp_perspective_get_mask(gmask_.data, gmask_.cols, gmask_.rows, gmask_.step, m_device_->data_, 0, 255);

            // Release allocated resources.
            img_free_header(&img_in);
            img_free_header(&img_out);

            // UPDATE THE MODEL.
            // Compute variance for measurement.
            cv::gpu::absdiff(gmean_, gmean_mes_, gmean_abs_diff_);
            cv::gpu::subtract(gmean_abs_diff_, gvar_, gvar_mes_);
            cv::gpu::multiply(gvar_mes_, alpha_, gvar_mes_);
            cv::gpu::exp(gvar_mes_, gvar_mes_);

            // Update BG mean.
            cv::gpu::multiply(gmean_, gvar_mes_, d1_);
            cv::gpu::multiply(gmean_mes_, gvar_, d2_);
            cv::gpu::add(d1_, d2_, d1_);
            cv::gpu::add(gvar_mes_, gvar_, d2_);
            cv::gpu::divide(d1_, d2_, gmean_);

            // Update BG variance.
            // Compute new variance.
            cv::gpu::GpuMat tmp2;
            tmp2 = cv::gpu::GpuMat(gvar_.rows, gvar_.cols, gvar_.type());
            cv::gpu::multiply(gvar_, gvar_mes_, d1_);
            cv::gpu::add(gvar_, gvar_mes_, d2_);

            cv::gpu::compare(gmean_abs_diff_, gvar_, mask2_, cv::CMP_GT);
            tmp2.setTo(kappa_, mask2_);
            cv::gpu::multiply(gvar_, tmp2, tmp2);
            cv::gpu::divide(d1_, d2_, gvar_);
            tmp2.copyTo(gvar_, mask2_);

            cv::gpu::add(gvar_, delta_, gvar_);           // Add process noise.

            // Fill empty parts.
            gmean_mes_.copyTo(gmean_, gmask_);
            gvar_.setTo(beta_, gmask_);

            // Compute fg.
            cv::gpu::absdiff(gmean_, gmean_mes_, gfg_);

            // DEBUG.
            {
//                //qDebug() << "Time the model" << t.tac();
//                                cv::Mat absfg;
//                                gfg_.download(absfg);
//                                cv::imshow("Foreground", absfg);
////                if (frameCounter_ % 10 == 0) {
////                    // qDebug() << "Conv";
////                    cv::Mat tmp;
////                    absfg.convertTo(tmp, CV_8U, 255.0f);
////                    cv::imwrite("./res/" + std::string(std::string::number(frameCounter_) + "_mask.png").toStdString(), tmp);
////                }
                /*cv::Mat bg;
                gmean_.download(bg);
                cv::namedWindow("BG");
                cv::imshow("BG", bg);*/

                /*cv::Mat fg;
                gfg_.download(fg);

                double minVal, maxVal;
                cv::Point minLoc, maxLoc;
                cv::gpu::minMaxLoc(gfg_, &minVal, &maxVal, &minLoc, &maxLoc);

                cv::namedWindow("FG");
                cv::Scalar color(255, 0, 0);
                cv::circle(fg, maxLoc, 30, color, 2);
                cv::imshow("FG", fg);*/
            }

            current.copyTo(previous_);
            //res_current_U_.copyTo(previous_gpu_);
            return H;
            //return cv::Mat();
        }
    }
}

// Return current background model.
cv::gpu::GpuMat BGModel::getBG() const
{
    return gmean_;
}

// Return current foreground mask.
cv::gpu::GpuMat BGModel::getFG() const
{
    //cv::subtract(gfg_, gfg_, gfg_);
    return gfg_;
}

// Load configuration from the settings.
void BGModel::fromSettings(CSettings &settings)
{
    // Load params.
    hSize_ = cv::Size(settings.rWidth, settings.rHeight);

    penaltyThreshold_ = settings.penalty_threshold;
    maxNumOfUnestimatedTransformationInSequence_ = settings.max_num_of_unestimated_transformation_in_sequence;
    gamma_ = settings.gamma;
    beta_ = settings.beta;
    alpha_ = settings.alpha;
    kappa_ = settings.kappa;
    delta_ = settings.delta;

    restart();
}



// Return homography matrix between two images.
// TODO: cross checking.
cv::Mat BGModel::homography(cv::Mat gray1, cv::Mat gray2, cv::Mat mask, int maxCount, double qLevel, double minDistance, double ransacThreshold)
{
    std::vector<cv::Point2f> points1;
    std::vector<cv::Point2f> points2;

    std::vector<cv::Point2f> cor_points1;
    std::vector<cv::Point2f> cor_points2;

    std::vector<uchar> status; // status of tracked features
    std::vector<float> err;    // error in tracking

    //cv::Mat m;
    //gmean_.download(m);
    /*cv::namedWindow( "Display", cv::WINDOW_NORMAL );
    cv::imshow("Display", gray1);
    cv::namedWindow( "Display2", cv::WINDOW_NORMAL );
    cv::imshow("Display2", gray2);*/

    cv::waitKey(1);

    // Get good features.
    cv::goodFeaturesToTrack(gray1,          // the image
                            points1,        // the output detected features
                            maxCount,       // the maximum number of features
                            qLevel,         // quality level
                            minDistance,
                            cv::noArray(),
                            3,
                            false);   // min distance between two features

    if (points1.size() < 10) return cv::Mat();

    // Compute new positions of good features.
    cv::calcOpticalFlowPyrLK(gray1, gray2, points1, points2, status, err);

    if (points2.size() < 10) return cv::Mat();

    // Removes all points where the correspondent was not found.
    for (int i = 0; i < points1.size(); i++){
        if (status[i]) {
            cor_points1.push_back(points1[i]);
            cor_points2.push_back(points2[i]);
        }
    }

    // If we have enough amount of pairs then homography matrix is computed.
    if (cor_points1.size() > 10) return cv::findHomography(cor_points1, cor_points2, CV_RANSAC, ransacThreshold);
    else return cv::Mat();
}


// Computes penalty for the estimated homography transformation.
float BGModel::penalty(cv::Mat H)
{
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>  e_tran;

    cv::cv2eigen(H, e_tran);

    Eigen::Vector3d ul1, ur1, bl1, br1;
    Eigen::Vector3d ul2, ur2, bl2, br2;
    Eigen::Vector3d dul, dur, dbl, dbr;

    ul1 << 0.0, 0.0, 0.0;
    bl1 << 0.0, gmean_.rows, 0.0;
    br1 << gmean_.cols, gmean_.rows, 0.0;
    ur1 << gmean_.cols, 0.0, 0.0;

    ul2 = e_tran * ul1;
    ur2 = e_tran * ur1;
    bl2 = e_tran * bl1;
    br2 = e_tran * br1;

    dul = ul2 - ul1;
    dur = ur2 - ur1;
    dbl = bl2 - bl1;
    dbr = br2 - br1;

    double pos_penalty = dul.norm() + dur.norm() + dbl.norm() + dbr.norm();
    double size1 = (ul1 - br1).norm();
    double size2 = (ul2 - br2).norm();
    double size_penalty = abs(size1 - size2);

    return gamma_ * size_penalty + pos_penalty;
}

// Restart background model.
void BGModel::restart()
{
    // Release data.
    gmean_.release();
    gvar_.release();
    gmask_.release();
    previous_.release();
    gfg_.release();

    numOfUnestimatedTransformationInSequence_ = 0;
    initialized_ = false;

    frameCounter_ = 0;

    //emit restarted();
}

// Compute homography on the GPU.
// THIS IS SLOWER THAN CPU VERSION :(
cv::Mat BGModel::homographyGPU(cv::gpu::GpuMat & gray1, cv::gpu::GpuMat & gray2)
{
     cv::gpu::GpuMat corners1, corners2, status;

     /*cv::Mat a, b;

     gray1.download(a);
     gray2.download(b);*/

    // Get good features.
    cv::gpu::GoodFeaturesToTrackDetector_GPU features(20, 0.01, 10.0);
    features(gray1, corners1);

    // Compute new positions of good features.
    cv::gpu::PyrLKOpticalFlow optFlow;
    optFlow.sparse(gray1, gray2, corners1, corners2, status);

    cv::Mat corn1, corn2;
    corners1.download(corn1);
    corners2.download(corn2);

    // Compute homography matrix.
    if (corn1.cols*corn1.rows < 4 || corn2.cols*corn2.rows < 4)
    {
        return cv::Mat();
    }
    return cv::findHomography(corn1, corn2, CV_RANSAC, 0.1);
}
