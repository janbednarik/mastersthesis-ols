/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef MOTION_H
#define MOTION_H

// Eigen
#include <eigen3/Eigen/Eigen>

//! \brief The Motion class Base class descriing the motion of the flying object.
//! It computes the position given the current simulation time.
//!
class Motion
{
public:
    Motion();
    ~Motion();

    //! \brief update Updates the position of the model
    //! \param t current simulation time
    //! \param x
    //! \param y
    //! \param z
    //!
    virtual void update(double t, double& x, double& y, double& z) = 0;

    virtual void forceSetPosition(double x, double y, double z) = 0;

protected:
    double speed_;  //!< Object speed [m/s].
};

//! \brief The PeriodicArcMotion class implements periodic motion along a part of the circle
//! bounded by +/- maxAngle. The motion starts at alpha = 0 rad.
//!
class PeriodicArcMotionXY : public Motion
{
public:
    //! \brief ArcMotion Constructor
    //! \param radius Circle radius [m].
    //! \param speed Circumference speed of the object [m/s].
    //! \param maxAngle positive angular limit (i.e. half of the range) [rad].
    //! \param z fixed value of Z axis [m].
    //!
    PeriodicArcMotionXY(Eigen::Vector3d center, double radius, double speed, double maxAngle);

    ~PeriodicArcMotionXY();

    virtual void update(double t, double& x, double& y, double& z);

    virtual void forceSetPosition(double x, double y, double z);

private:
    Eigen::Vector3d center_;
    double radius_;     //!< Radius of the circle the arc is a part of.
    double maxAngle_;
    double period_;
    double period2_;    //!< one half of the period.
    double period4_;    //!< one quarter of the period.
    double period34_;   //!< three quarters of the period.    
};

//! \brief The StaticMotion class Implements static target.
//!
class StaticMotion : public Motion
{
public:
    //! \brief StaticMotion Constructor.
    //! \param x Initial and fixed X position.
    //! \param y Initial and fixed Y position.
    //! \param z Initial and fixed Z position.
    //!
    StaticMotion(Eigen::Vector3d pos);

    ~StaticMotion();

    virtual void update(double t, double& x, double& y, double& z);

    virtual void forceSetPosition(double x, double y, double z);

private:
    double x_;
    double y_;
    double z_;
};

//! \brief The PeriodicLineSegmentMotion class Implements the accelerated linear motion of the target:
//! s = s0 + v0t + at^2/2.
//!
class PeriodicLineSegmentMotion : public Motion
{
public:
    PeriodicLineSegmentMotion(Eigen::Vector3d from, Eigen::Vector3d direction, double distance, double speed, double acceleration);

    ~PeriodicLineSegmentMotion();

    virtual void update(double t, double& x, double& y, double& z);

    virtual void forceSetPosition(double x, double y, double z);

private:
    Eigen::Vector3d from_;
    Eigen::Vector3d direction_;
    double distance_;
    double acceleration_;

    double period_;     //!< Time period of the motion.
    double period2_;    //!< One half of the time period.
};

#endif // #define MOTION_H
