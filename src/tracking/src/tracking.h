/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

#ifndef DETECTION_H
#define DETECTION_H

/* If DEBUG is enabled:
 *  - visual markers, namely lines, are drawn between a camera unit and a tracked target
*/
#define DEBUG 1

/* If GROUND_TRUTH_TRACKER is enabled:
 * - The TrackerDelayedGT is created instead of the real tracker.
 * - Topic ~/set_model_state is subscribed
 */
#define GROUND_TRUTH_TRACKER 1
#define TLD_TRACKER 1
#define OLS_TRACKER 1

// ROS libraries
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include <gazebo_msgs/ModelState.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <geometry_msgs/Pose.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// Eigen
#include <eigen3/Eigen/Eigen>

// Project libraries
#include <msgs/StartDetection.h>
#include <msgs/StopDetection.h>
#include <msgs/NewTargetID.h>
#include <msgs/TargetStateLocal.h>
#include <msgs/ManipulatorMove.h>
#include <msgs/CameraUnitInfo.h>
#include <msgs/Regulate.h>
#include <msgs/SetTargetObject.h>
#include <msgs/TargetObject.h>
#include "target.h"
#include "tracker_tld.h"
#include "tracker_ols.h"
#include "tracker_fixed_point.h"

// C++
#include <unordered_map>

// PCL
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

//! \brief The Tracking class Main tracking node class. It keeps the
//! information about targets and trackers.
//!
class Tracking
{
public:
    static const std::string IMAGE_RAW_TOPIC;
    static const std::string IMAGE_PROCESSED_TOPIC;
    static const std::string START_DETECTION_TOPIC;
    static const std::string STOP_DETECTION_TOPIC;
    static const std::string NEW_TARGET_ID_TOPIC;
    static const std::string NEW_TARGET_TOPIC;
    static const std::string TARGET_STATE_LOCAL_TOPIC;
    static const std::string CAMERA_UNIT_INFO_TOPIC;
    static const std::string REGULATE_TOPIC;
    static const std::string SET_TARGET_OBJECT_TOPIC;
    static const std::string ENV_MODEL_TOPIC;
    static const int IMAGE_BUFFER_SIZE = 10;

    // Default camera properties constants
    static const double DEF_CAMERA_FPS;         //!< Default FPS of the camera.

    //! \brief The tracker type
    enum ETrackerType {
        TRACKER_NO_TRACKER = -1,
        TRACKER_GROUND_TRUTH = 0,
        TRACKER_OLS = 1,
        TRACKER_TLD = 2
    };

public:    
    //! \brief Detection Constructor
    //!
    Tracking();

    //! \brief ~Detection Destructor
    //!
    ~Tracking();

    //! \brief udpate Main update function
    //!
    void update();

private:
    // ROS structures
    ros::NodeHandle* nh_;
    ros::Subscriber onImgSub_;
    ros::Subscriber startDetectionSub_;
    ros::Subscriber stopDetectionSub_;
    ros::Subscriber newTargetIdSub_;
    ros::Subscriber setTargetObjectSub_;    //!< SetTargetObject message
    ros::Subscriber cameraUnitInfoSub_;
    ros::Publisher newTargetPub_;
    ros::Publisher localTargetStatePub_;
    ros::Publisher manipMovePub_;           //!< Publisher of the messages controlling the motion of the manipulator.
    ros::Publisher regulatePub_;            //!< Publisher of the message 'Regulate' controlling the motion of the manipulator during tracking.
    ros::Publisher imageProcessedPub_;      //!< Publisher of the wither original or processed (in case tracking is in progress) image.
    ros::Publisher pcPub_;                  //!< Environemtn model point cloud publisher.
    tf::TransformListener tfListener_;

    sensor_msgs::Image lastFrame_;
    bool haveNewFrame_;

    std::string unit_idx_;          //!< Index of this camera unit.
    std::string master_ns_;         //!< Namespaces of the master subsystem
    std::string flying_object_ns_;  //!< Namaspace of the flying objects.
    std::string tf_prefix_;    

    std::string tfCamera_;
    std::string tfFocus_;

    sensor_msgs::Image bufImg_[IMAGE_BUFFER_SIZE];
    int idxBI_; //!< Indexvisualization_msgs pointing at the newest frame in the gufImg.

    std::unordered_map<int, Target*> targets_;
    std::unordered_map<int, Tracker*> trackers_;

    Tracker* tracker_;
    bool ols_tracker_running_;

    ETrackerType currentTracker_;

    bool trackingSuccessful_;
    cv::Rect bbToDraw_;

    // Camera properties
    int screenWidth_;
    int screenHeight_;
    double pixelWidth_;
    double pixelHeight_;
    cv::Point frameCenter_;
    double fps_;

    // Controling manipualtor motion
    bool manipInMotion_;

    double maxImgTimeDiff_;     //!< Maximum time difference between two camera frames to be considered as the same frame by findStampedImage().

    /*! Rotation transformation from standard ROS coordinate frame
     * to standard camera screen coordinate frame.
     * ROS coordinate frame:    Y left, Z up, X forward
     * Screen coordinate frame: X left. Y up, Z forward
     */
    Eigen::Matrix3d tfROS2screen_;

    /*!
     * \brief tfCamera2Screen_ Translation and rotation transformation from 'camera' frame (i.e. CCD sensor)
     * to 'screen' frame in pixel units. I.e. in 'camera' frame the middle of the CCD sensor is considered
     * to have coordinates [0, 0] px, while in 'screen' frame the middle of the CCD sensor is considered
     * to have coordinates [screenWidth / 2, screenHeight / 2].
     *
     * screen frame:                                camera frame:
     *
     * [0,0]  x
     *   +--->------+                                +----------+
     *   |          |                                |     \ y  |
     * y v          |                                |     |    |
     *   |          |                                | <---+    |
     *   |          |                                |  x [0,0] |
     *   +----------+                                +----------+
     */
    Eigen::Matrix3d tfCamera2Screen_;    

    // Environemtn model:
    pcl::PointCloud<pcl::PointXYZ>::Ptr envModel_;
    sensor_msgs::PointCloud2 envModelMsg_;
    bool haveEnvModel_;

private:    
    //! \brief onImage Video stream callback
    //! \param img
    //!
    void onImage(const sensor_msgs::Image::ConstPtr img);

    //! \brief startDetection Callback function, starts detection of the object given by
    //! the roi (in message) in the frame with the given time stamp (in the message).
    //! \param msg
    //!
    void onStartDetection(const msgs::StartDetection msg);

    //! \brief onStopDetection Temporary method to erase target when tracker fails.
    //! \param stopMsg
    //!
    void onStopDetection(const msgs::StopDetection::ConstPtr stopMsg);

    //! \brief onNewTargetId NewTargetID message callback. Assigns the global unique id
    //! to the local ID generated byt this detector.
    //! \param msg
    //!
    void onNewTargetId(const msgs::NewTargetID::ConstPtr msg);

    //! \brief onCameraUnitInfo CameraUnitInfo message callback. Saves
    //! current data about the camera unit.
    //! \param cui
    //!
    void onCameraUnitInfo(const msgs::CameraUnitInfo::ConstPtr cui);

    void onSetTargetObject(const msgs::SetTargetObject::ConstPtr stoMsg);

    //! \brief findStampedImage Goes through the input images buffer and attempts to
    //! find the image with the given time stamp 'stamp'.
    //! \param img
    //! \param stamp
    //! \return
    //!
    bool findStampedImage(sensor_msgs::Image& img, ros::Time stamp);

    //! \brief targetDirection Computes and returns the 3D vector which is the
    //! direction from he camera's optical chip to the given target.
    //! \param id local ID of the target.
    //! \param focalPoint position of the focal point (output parameter)
    //! \param direction direction from focal point to target (output parameter)
    //! \param stamp time stamp for the moment for which we are computing the directions
    //! \return
    //!
    void targetDirection(int id, tf::Vector3 &focalPoint, tf::Vector3 &direction, ros::Time stamp);

    //! \brief moveManip Sends the ManipulatorMove message to the manipualtor with the
    //! given speeds 'azi' and 'ele' (given by mrads).
    //!
    void moveManip(double azi, double ele);

    //! \brief regulateManip Sends the Regulate message to the manipulator in order
    //! to regulate the motion during tracking. The objective is to keep the tracked
    //! object as close to the optical axis as possible.
    //! \param dx Pixel distance between the screen center and the object in X (horiz.) axis.
    //! \param dy Pixel distance between the screen center and the object in Y (vert.) axis.
    //! \param stamp Time stamp of the frame for which the tracking was performed.
    //!
    void regulateManip(int dx, int dy, ros::Time stamp);

    //! \brief euclideanDistance Computes Euclidean dostance between two 2D points.
    //! \param p
    //! \param q
    //! \return
    //!
    double euclideanDistance(cv::Point p, cv::Point q);

    //! \brief pixelDist2Angle Transforms the pixel distance on the image sensor screen
    //! to angle between the optical axis and the direction to the target.
    //! \param dist Pixel distance.
    //! \param focalLength Focal length of the camera [m].
    //! \param cellSize Cell size of the image sensor [m].
    //! \return Angle [rad]
    //!

    //! \brief pointsInCone Returns the 'indices' of those points in point cloud 'points',
    //! for which the angle between the 'coneAxis' and that point is max 'angle'. Both
    //! input 'points' and 'coneAxis' must be normalizad vector (i.e. length = 1).
    //! \param points [in]
    //! \param coneAxis [in]
    //! \param angle [in]
    //! \param indices [out]
    //!
    void pointsInCone(Eigen::Matrix3Xf& points, Eigen::Vector3f& coneAxis, double angle, std::vector<int>& indices);

#if GROUND_TRUTH_TRACKER
public:
    static const std::string POSE_TOPIC;
    static const double GT_TRAKER_MU;       //!< Mean time delay [ms].
    static const double GT_TRAKER_SIGMA;    //!< Std deviation for time delay [ms].
    static const double PX_NOISE_MU;
    static const double PX_NOISE_SIGMA;

private:
    typedef message_filters::sync_policies::ApproximateTime<geometry_msgs::PoseStamped, msgs::CameraUnitInfo> TimeSyncPolicy;

    message_filters::Subscriber<geometry_msgs::PoseStamped> flyingObjectPoseSyncSub_;
    message_filters::Subscriber<msgs::CameraUnitInfo> cameraUnitInfoSyncSub_;
    message_filters::Synchronizer<TimeSyncPolicy> *timeSync_;

    int xGT_;   //!< Ground truth X pixel coordinate of the tracked target.
    int yGT_;   //!< Ground truth Y pixel coordinate of the tracked target.
    ros::Time stampGT_;
    int xGTDelayed_;
    int yGTDelayed_;
    ros::Time stampGTDelayed_;

    ros::Timer gtTrackerTimer_;
    ros::Timer startGTTrackeTimer_;

    bool startGTTrackerFlag_;

    // Structures for generating random numbers using Gaussian distribution.
    std::random_device rd_;
    std::mt19937 *gen_;
    std::normal_distribution<double> *d_;

    //! \brief getNewDelay Returns new time delay [s] given by Gaussian distribution.
    //! \return
    //!
    double getNewDelay();

    //! \brief onPoseAndCameraUnitInfo Callback synchronizing PoseStamped and CameraUnitInfo messages. This
    //! is only used when 'ground truth tracker' is used in irder to create the ground truth position of the
    //! tracked target projected on the sensor of the camera (2D position - pixel coordinates).
    //! \param pose
    //! \param cuInfo
    //!
    void onPoseAndCameraUnitInfo(const geometry_msgs::PoseStampedConstPtr& pose, const msgs::CameraUnitInfoConstPtr& cuInfo);

    //! \brief onGTtrackTimer Callback for ground truth tracker. Each time a callback is invoked,
    //! a tracker provides the ground truth target position delayed by the time last set for the timer.
    //! This simulates the computation delay of the real tracker.
    //!
    void onGTtrackTimer(const ros::TimerEvent&);

    void startGTTracker(const ros::TimerEvent& event);
#endif


#if DEBUG
private:
    ros::Publisher markerPub_;

    std::ofstream pxDistFile_;

    std::normal_distribution<double> *dPXNoise_;

    //! \brief drawLine Sends a Marker message so that the rviz could
    //! draw a line linking the camera at a focal point and the tracked object.
    //!
    void drawLine(tf::Vector3 &from, tf::Vector3 &to, ros::Time stamp);

    //! \brief savePXDist Saves the distance between the GT target and current optical axis to file.
    //! \param dist
    //!
    void savePXDist(double dist);
#endif

};

#endif // DETECTION_H
