// Project
#include "rectification.h"

// ROS libraries
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

// OpenCV libraries
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// C++
#include <cmath>
#include <fstream>

using namespace std;

const string Rectification::IMAGE_RAW_TOPIC         = "/camera/image_raw";
const string Rectification::KEY_DOWN_TOPIC          = "keyboard/keydown";
const string Rectification::KEY_UP_TOPIC            = "keyboard/keyup";
const string Rectification::JOY_TOPIC               = "joy";
const string Rectification::JOINT_STATES_TOPIC      = "joint_states";
const string Rectification::MANIP_MOVE_CMD_TOPIC    = "manipMove";

const float Rectification::ZOOM_STEP                = 0.1;
const float Rectification::ZOOM_MIN                 = 1.0;
const float Rectification::ZOOM_MAX                 = 10.0;

const cv::Scalar Rectification::RED     = cv::Scalar(0,     0,      255 );
const cv::Scalar Rectification::GREEN   = cv::Scalar(0,     255,    0   );
const cv::Scalar Rectification::BLUE    = cv::Scalar(255,   0,      0   );
const cv::Scalar Rectification::BLACK   = cv::Scalar(0,     0,      0   );
const cv::Scalar Rectification::WHITE   = cv::Scalar(255,   255,    255 );

Rectification::Rectification(): toolSelection_(CCD_ROWS),
    mvLeft_(false), mvRight_(false), mvUp_(false), mvDown_(false), mvSpeed_(1), mvActiveCmds_(0), mvActiveCmdsOld_(-1),
    azimuth_(0.0), elevation_(0.0), zoom_(1.0), lctrlDown_(false), focalLength_(0.0)
{
    // Load parameters from ROS parameter server.
    if (!ros::param::get("/master_ns", master_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter overvire_unit_ns");
        exit(1);
    }

    if (!ros::param::get("/camera_unit_ns", camera_unit_ns_)) {
        ROS_FATAL_STREAM("Could not get parameter overvire_unit_ns");
        exit(1);
    }

    if (!ros::param::get("tf_prefix", tf_prefix_)) {
        ROS_FATAL_STREAM("Could not get parameter tf_prefix");
        exit(1);
    }   

    // Frame size of the camera - width
    if (!ros::param::get(string("/") + camera_unit_ns_ + string("/camera/width"), frameWidth_)) {
        ROS_FATAL_STREAM("Could not get parameter width");
        exit(1);
    }

    // Frame size of the camera - height
    if (!ros::param::get(string("/") + camera_unit_ns_ + string("/camera/height"), frameHeight_)) {
        ROS_FATAL_STREAM("Could not get parameter height");
        exit(1);
    }

    if (!ros::param::get("file_optical_axis", fileOptAxis_)) {
        ROS_FATAL_STREAM("Could not get parameter file_optical_axis");
        exit(1);
    }

    if (!ros::param::get("file_elevation_angle", fileEleAngle_)) {
        ROS_FATAL_STREAM("Could not get parameter file_elevation_angle");
        exit(1);
    }

    if (!ros::param::get("file_betatl1_angle", fileTL1Angle_)) {
        ROS_FATAL_STREAM("Could not get parameter file_betatl1_angle");
        exit(1);
    }

    if (!ros::param::get(string("/") + camera_unit_ns_ + string("/camera/pixel_w"), pixelWidth_)) {
        ROS_FATAL_STREAM("Could not get parameter pixel_w");
        exit(1);
    }

    if (!ros::param::get(string("/") + camera_unit_ns_ + string("/camera/pixel_h"), pixelHeight_)) {
        ROS_FATAL_STREAM("Could not get parameter pixel_h");
        exit(1);
    }

    if (!ros::param::get("manipulator_type", manipType_)) {
        ROS_FATAL_STREAM("Could not get parameter manipulator_type");
        exit(1);
    }

    string image_raw_topic = string("/") + camera_unit_ns_ + string("/") + IMAGE_RAW_TOPIC;

    nh_ = new ros::NodeHandle();
    onImgSub_           = nh_->subscribe(image_raw_topic, 5, &Rectification::onImage, this);
    keyDownSub_         = nh_->subscribe(string("/") + KEY_DOWN_TOPIC,  5, &Rectification::onKeyDown, this);
    keyUpSub_           = nh_->subscribe(string("/") + KEY_UP_TOPIC,    5, &Rectification::onKeyUp,   this);
    joySub_             = nh_->subscribe(string("/") + JOY_TOPIC,       1, &Rectification::onJoy,     this);
    jointStateSub_      = nh_->subscribe(string("/") + camera_unit_ns_ + string("/") + JOINT_STATES_TOPIC, 5, &Rectification::onJointStates, this);
    joyPub_             = nh_->advertise<sensor_msgs::Joy>(JOY_TOPIC,     1);
    keyDownPub_         = nh_->advertise<keyboard::Key>(KEY_DOWN_TOPIC,   5);
    keyUpPub_           = nh_->advertise<keyboard::Key>(KEY_UP_TOPIC,     5);
    manipMoveCmdPub_    = nh_->advertise<msgs::ManipulatorMove>(MANIP_MOVE_CMD_TOPIC, 1);

    // Initialize crosshairs
    chOA_.x(frameWidth_ / 2);
    chOA_.y(frameHeight_ / 2);
    chOA_.setBoundaries(frameWidth_ - 1, 0, frameHeight_ - 1, 0);

    chDEA_.x(frameWidth_ / 2);
    chDEA_.y(frameHeight_ / 2);
    chDEA_.setBoundaries(frameWidth_ - 1, 0, frameHeight_ - 1, 0);

    chBTL1_.x(frameWidth_ / 2);
    chBTL1_.y(frameHeight_ / 2);
    chBTL1_.setBoundaries(frameWidth_ - 1, 0, frameHeight_ - 1, 0);
}

Rectification::~Rectification()
{
    ;
}

void Rectification::update()
{
    switch(toolSelection_) {
        case CCD_ROWS:
            break;

        case OPTICAL_AXIS:
            if(ros::Time::now() - mvStart_ > ros::Duration(0.5)) {
                if(mvLeft_)  chOA_.move(-mvSpeed_, 0);
                if(mvRight_) chOA_.move(mvSpeed_, 0);
                if(mvUp_)    chOA_.move(0, -mvSpeed_);
                if(mvDown_)  chOA_.move(0, mvSpeed_);
            }
            break;

        case DEFAULT_ELEVATION_ANGLE:
            break;
    }
}

void Rectification::saveDataOptAxis()
{
    // DEBUG
    cout << "Saving optical axis rectification data to file " << fileOptAxis_ << endl;

    ofstream file;
    file.open(fileOptAxis_.c_str(), ios::out);

    if(file.is_open()) {
        file << "frame_width [px]:      " << frameWidth_      << endl;
        file << "frame_height [px]:     " << frameHeight_     << endl;        
        file << "pixel_width [um]:      " << pixelWidth_      << endl;
        file << "pixel_height [um]:     " << pixelHeight_     << endl;
        file << "focal_length [mm]:     " << (focalLength_ * 1000.0) << endl;
        file << "center_x [px]:         " << chOA_.x()        << endl;
        file << "center_y [px]:         " << chOA_.y()        << endl;

        file.close();
    } else {
        ROS_FATAL_STREAM(string("Could not open file ") + fileOptAxis_ +
                         string(" for writing, the parameters will not be saved."));
    }
}

void Rectification::saveDataEleAngle()
{
    // DEBUG
    cout << "Saving elevation angle rectification data to file " << fileEleAngle_ << endl;

    ofstream file;
    file.open(fileEleAngle_.c_str(), ios::out);

    if(file.is_open()) {
        file << "elevation [mrad]: " << elevation_ << endl;
        file.close();
    } else {
        ROS_FATAL_STREAM(string("Could not open file ") + fileEleAngle_ +
                         string(" for writing, the parameters will not be saved."));
    }
}

void Rectification::saveBetaTL1Angle()
{
    // DEBUG
    cout << "Saving BetaTL1 angle to file " << fileTL1Angle_ << endl;

    ofstream file;
    file.open(fileTL1Angle_.c_str(), ios::out);

    if(file.is_open()) {
        file << "BetaTL1 [mrad]: " << azimuth_ << endl;
        file.close();
    } else {
        ROS_FATAL_STREAM(string("Could not open file ") + fileTL1Angle_ +
                         string(" for writing, the parameters will not be saved."));
    }
}

void Rectification::rectifyCCDRows(cv::Mat &frame)
{
    cv::Mat flipped;

    // Flip around y-axis
    cv::flip(frame, flipped, 1);

    frame = (frame + flipped) / 2.0;

    // Zoom
    zoom(frame);
}

void Rectification::rectifyOpticalAxis(cv::Mat& frame)
{
    // Draw crosshair
    chOA_.draw(frame);

    // Zoom
    zoom(frame);

    // Draw text - offsets
    ostringstream  strx, stry;
    strx << "x: " << chOA_.x() << " px";
    stry << "y: " << chOA_.y() << " px";

    cv::putText(frame, strx.str(), cv::Point2i(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0.0, 0.0, 255.0), 1, CV_AA);
    cv::putText(frame, stry.str(), cv::Point2i(10, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0.0, 0.0, 255.0), 1, CV_AA);
}

void Rectification::findDefaultElevationAngle(cv::Mat& frame)
{
//    tf::StampedTransform tfAzi, tfEle;
    tf::StampedTransform tfEle;

    if(manipType_ == "oprox") {
//        tfListener_.lookupTransform(tf_prefix_ + "/base_azi_interface", tf_prefix_ + "/manip_azi", ros::Time(0), tfAzi);
        tfListener_.lookupTransform(tf_prefix_ + "/manip_azi",          tf_prefix_ + "/manip_ele", ros::Time(0), tfEle);
    } else if(manipType_ == "ptud4670") {
//        tfListener_.lookupTransform(tf_prefix_ + "/base",      tf_prefix_ + "/manip_azi", ros::Time(0), tfAzi);
        tfListener_.lookupTransform(tf_prefix_ + "/manip_azi", tf_prefix_ + "/manip_ele", ros::Time(0), tfEle);
    }

//    tf::Vector3 tfAziAxis = tfAzi.getRotation().getAxis();
//    tf::Vector3 tfEleAxis = tfEle.getRotation().getAxis();
//    azimuth_   = tfAzi.getRotation().getAngle() *        tfAzi.getRotation().getAxis().z() * 1000.0;
    elevation_ = tfEle.getRotation().getAngle() * (-1) * tfEle.getRotation().getAxis().y() * 1000.0;

    // Draw crosshair
    chDEA_.draw(frame);

    // Zoom
    zoom(frame);

    // Display current elevation.
    ostringstream strEle;
    strEle << "Elevation: " << elevation_ << " mrad";
    cv::putText(frame, strEle.str(), cv::Point2i(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0.0, 0.0, 255.0), 1, CV_AA);
}

void Rectification::findBetaTL1(cv::Mat& frame)
{
    tf::StampedTransform tfAzi;

    if(manipType_ == "oprox") {
        tfListener_.lookupTransform(tf_prefix_ + "/base_azi_interface", tf_prefix_ + "/manip_azi", ros::Time(0), tfAzi);
    } else if(manipType_ == "ptud4670") {
        tfListener_.lookupTransform(tf_prefix_ + "/base",      tf_prefix_ + "/manip_azi", ros::Time(0), tfAzi);
    }

//    tf::Vector3 tfAziAxis = tfAzi.getRotation().getAxis();
    azimuth_   = tfAzi.getRotation().getAngle() *        tfAzi.getRotation().getAxis().z() * 1000.0;

    // Draw crosshair
    chBTL1_.draw(frame);

    // Zoom
    zoom(frame);

    // Display current azimuth.
    ostringstream strAzi;
    strAzi << "Azimuth: " << azimuth_ << " mrad";
    cv::putText(frame, strAzi.str(), cv::Point2i(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0.0, 0.0, 255.0), 1, CV_AA);
}

void Rectification::onImage(const sensor_msgs::Image::ConstPtr img)
{
    cv_bridge::CvImagePtr cv_img;

    try {
        cv_img = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv::Mat frame;
    cv_img->image.copyTo(frame);

    switch(toolSelection_) {
        case CCD_ROWS:
            rectifyCCDRows(frame);
            break;

        case OPTICAL_AXIS:
            rectifyOpticalAxis(frame);
            break;

        case DEFAULT_ELEVATION_ANGLE:
            findDefaultElevationAngle(frame);
            break;

        case BETA_TL1:
            findBetaTL1(frame);
            break;
    }

    cv::imshow("img", frame);
    cv::waitKey(1);
}

void Rectification::onJointStates(const sensor_msgs::JointState::ConstPtr jointStates)
{
    for(int i = 0; i < jointStates->name.size(); ++i) {
        if(jointStates->name[i].compare("camera2focus") == 0) {
            focalLength_ = jointStates->position[i];
            break;
        }
    }
}

void Rectification::setAbsPos()
{
    string azimuthStr, elevationStr;
    double azimuth, elevation;

    try {
        cout << "Enter azimuth [mrad]: ";
        getline(cin, azimuthStr);

        cout << "Enter elevation [mrad]: ";
        getline(cin, elevationStr);

        azimuth     = stod(azimuthStr);
        elevation   = stod(elevationStr);

        // debug
        cout << "Azimuth: " << azimuth << endl;
        cout << "Elevation: " << elevation << endl;
    } catch(exception &e) {
        cerr << "Excpetion: setAbsPos: " << e.what() << endl;
        return;
    }


}

void Rectification::onKeyDown(const keyboard::Key::ConstPtr key)
{        
    bool mvTimeoutStarted = false;

    switch (key->code) {
        // Ctrl key is pressed (key composition commands)
        case keyboard::Key::KEY_LCTRL: lctrlDown_ = true; break;

        // Select tools using num keys.
        case keyboard::Key::KEY_1: toolSelection_ = CCD_ROWS;                   break;
        case keyboard::Key::KEY_2: toolSelection_ = OPTICAL_AXIS;               break;
        case keyboard::Key::KEY_3: toolSelection_ = DEFAULT_ELEVATION_ANGLE;    break;
        case keyboard::Key::KEY_4: toolSelection_ = BETA_TL1;                   break;

        // Move azimuth / elevation to the origin
        case keyboard::Key::KEY_a:
        case keyboard::Key::KEY_e:
            keyDownPub_.publish(key);
            break;

        // Zoom
        case keyboard::Key::KEY_KP_PLUS:
            updateZoom(ZOOM_STEP);
            break;
        case keyboard::Key::KEY_KP_MINUS:
            updateZoom(-ZOOM_STEP);
            break;

        // Lense
        case keyboard::Key::KEY_PAGEUP:
        case keyboard::Key::KEY_PAGEDOWN:
        case keyboard::Key::KEY_HOME:
        case keyboard::Key::KEY_END:
        case keyboard::Key::KEY_INSERT:
        case keyboard::Key::KEY_DELETE:
            keyDownPub_.publish(key);
            break;

        default:
            break;
    }

    switch(toolSelection_) {
        case CCD_ROWS:
            switch (key->code) {
                // Manipulator move commands
                case keyboard::Key::KEY_UP:
                case keyboard::Key::KEY_DOWN:
                case keyboard::Key::KEY_LEFT:
                case keyboard::Key::KEY_RIGHT:
                case keyboard::Key::KEY_s:
                case keyboard::Key::KEY_c:
                    keyDownPub_.publish(key);
                    break;
            }
            break;

        case OPTICAL_AXIS:
            switch (key->code) {
                // Manipulator move commands
                case keyboard::Key::KEY_UP:
                case keyboard::Key::KEY_DOWN:
                case keyboard::Key::KEY_LEFT:
                case keyboard::Key::KEY_RIGHT:
                    keyDownPub_.publish(key);
                    break;

                // Move crosshair.
                case keyboard::Key::KEY_KP4:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ += 1;
                    chOA_.move(-1, 0);
                    mvLeft_ = true;
                    break;

                case keyboard::Key::KEY_KP6:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ += 1;
                    chOA_.move(1, 0);
                    mvRight_ = true;
                    break;

                case keyboard::Key::KEY_KP8:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ += 1;
                    chOA_.move(0, -1);
                    mvUp_ = true;
                    break;

                case keyboard::Key::KEY_KP2:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ += 1;
                    chOA_.move(0, 1);
                    mvDown_ = true;
                    break;

                // Reset the position of the crosshair (center).
                case keyboard::Key::KEY_r:
                    chOA_.x(frameWidth_ / 2);
                    chOA_.y(frameHeight_ / 2);
                    break;

                // Save data to file / stepper motion mode
                case keyboard::Key::KEY_s:
                    if(lctrlDown_) {
                        saveDataOptAxis();
                    } else {
                        keyDownPub_.publish(key);
                    }
                    break;

                // Continuous motion mode.
                case keyboard::Key::KEY_c:
                    keyDownPub_.publish(key);
                    break;

                default:
                    break;
            }

            if(mvActiveCmdsOld_ == 0 && mvActiveCmds_ > 0) {
                mvStart_ = ros::Time::now();
            }

            break;

        case DEFAULT_ELEVATION_ANGLE:
             switch (key->code) {
                 // Move commands
                 case keyboard::Key::KEY_UP:
                 case keyboard::Key::KEY_DOWN:
                 case keyboard::Key::KEY_LEFT:
                 case keyboard::Key::KEY_RIGHT:
                     keyDownPub_.publish(key);
                     break;

                 // Save data to file / stepper motion mode
                 case keyboard::Key::KEY_s:
                     if(lctrlDown_) {
                         saveDataEleAngle();
                     } else {
                         keyDownPub_.publish(key);
                     }
                     break;

                 // Continuous motion mode.
                 case keyboard::Key::KEY_c:
                     keyDownPub_.publish(key);
                     break;
             }

            break;

        case BETA_TL1:
             switch (key->code) {
                 // Move commands
                 case keyboard::Key::KEY_UP:
                 case keyboard::Key::KEY_DOWN:
                 case keyboard::Key::KEY_LEFT:
                 case keyboard::Key::KEY_RIGHT:
                     keyDownPub_.publish(key);
                     break;

                 // Save data to file / stepper motion mode
                 case keyboard::Key::KEY_s:
                     if(lctrlDown_) {
                         saveBetaTL1Angle();
                     } else {
                         keyDownPub_.publish(key);
                     }
                     break;

                 // Continuous motion mode.
                 case keyboard::Key::KEY_c:
                     keyDownPub_.publish(key);
                     break;
             }

            break;
    }

    // debug
//    cout << "rect: key pressed: " << key->code << endl;
}

void Rectification::onKeyUp(const keyboard::Key::ConstPtr key)
{
    switch (key->code) {
        case keyboard::Key::KEY_LCTRL:
            lctrlDown_ = false;
            break;

        // Move azimuth / elevation to the origin
        case keyboard::Key::KEY_a:
        case keyboard::Key::KEY_e:
            keyUpPub_.publish(key);
            break;

        // Select tools using num keys.
        case keyboard::Key::KEY_1:
        case keyboard::Key::KEY_2:
        case keyboard::Key::KEY_3:
        case keyboard::Key::KEY_4:
            break;

        // Zoom
        case keyboard::Key::KEY_KP_PLUS:
            break;
        case keyboard::Key::KEY_KP_MINUS:
            break;

        // Lense
        case keyboard::Key::KEY_PAGEUP:
        case keyboard::Key::KEY_PAGEDOWN:
        case keyboard::Key::KEY_HOME:
        case keyboard::Key::KEY_END:
        case keyboard::Key::KEY_INSERT:
        case keyboard::Key::KEY_DELETE:
            keyUpPub_.publish(key);
            break;

        default:
            break;
    }

    switch(toolSelection_) {
        case CCD_ROWS:
            switch (key->code) {
                case keyboard::Key::KEY_UP:
                case keyboard::Key::KEY_DOWN:
                case keyboard::Key::KEY_LEFT:
                case keyboard::Key::KEY_RIGHT:
                case keyboard::Key::KEY_s:
                case keyboard::Key::KEY_c:
                    keyUpPub_.publish(key);
                break;
            }
            break;

        case OPTICAL_AXIS:
            switch (key->code) {
                case keyboard::Key::KEY_UP:
                case keyboard::Key::KEY_DOWN:
                case keyboard::Key::KEY_LEFT:
                case keyboard::Key::KEY_RIGHT:
                case keyboard::Key::KEY_s:
                case keyboard::Key::KEY_c:
                    keyUpPub_.publish(key);
                break;

                case keyboard::Key::KEY_KP4:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ -= 1;
                    mvLeft_ = false;
                    break;

                case keyboard::Key::KEY_KP6:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ -= 1;
                    mvRight_ = false;
                    break;

                case keyboard::Key::KEY_KP8:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ -= 1;
                    mvUp_ = false;
                    break;

                case keyboard::Key::KEY_KP2:
                    mvActiveCmdsOld_ = mvActiveCmds_;
                    mvActiveCmds_ -= 1;
                    mvDown_ = false;
                    break;

                default:
                    break;
            }

            break;

        case DEFAULT_ELEVATION_ANGLE:
            switch (key->code) {
                // Move commands
                case keyboard::Key::KEY_UP:
                case keyboard::Key::KEY_DOWN:
                case keyboard::Key::KEY_LEFT:
                case keyboard::Key::KEY_RIGHT:
                case keyboard::Key::KEY_s:
                case keyboard::Key::KEY_c:
                    keyUpPub_.publish(key);
                    break;
            }
            break;

        case BETA_TL1:
            switch (key->code) {
                // Move commands
                case keyboard::Key::KEY_UP:
                case keyboard::Key::KEY_DOWN:
                case keyboard::Key::KEY_LEFT:
                case keyboard::Key::KEY_RIGHT:
                case keyboard::Key::KEY_s:
                case keyboard::Key::KEY_c:
                    keyUpPub_.publish(key);
                    break;
            }
            break;
    }

    // debug
//    cout << "key released" << endl;
}

void Rectification::onJoy(const sensor_msgs::Joy::ConstPtr& joy)
{
    static bool buttonDown = false;

    /* Only process a resned /joy messages to control node if
       the DEFAULT_ELEVATION_ANGLE tool is selected. */
    if(toolSelection_ == DEFAULT_ELEVATION_ANGLE) {
        joyPub_.publish(joy);

        if(!buttonDown) {
            // debug
    //            cout << joy->axes[0] << ", " << joy->axes[1] << endl;
        }
        else
        {
            buttonDown = false;
        }
    }
}

void Rectification::updateZoom(float z)
{
    zoom_ += z;
    zoom_ = std::min(ZOOM_MAX, std::max(ZOOM_MIN, zoom_));

}

void Rectification::zoom(cv::Mat& frame)
{
    int w, h;
    w = (int)(((float)frame.cols / zoom_) + 0.5);
    w += w & 1;
    h = (int)(((float)frame.rows / zoom_) + 0.5);
    h += h & 1;

    cv::Mat roi;
    cv::resize(frame(cv::Rect((frame.cols - w) / 2, (frame.rows - h) / 2, w, h)), roi, cv::Size(frame.cols, frame.rows));
    roi.copyTo(frame);
}
