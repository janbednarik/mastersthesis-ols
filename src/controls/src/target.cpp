/************************************************************************************************/
//
// Author:  Jan Bednarik (jan.bednarik@hotmail.cz)
// Date:    2016-05-25
// Project: Master's thesis Optical Localization of VEry Distant Targets in Multi-camera Systems
//
/************************************************************************************************/

// Project libraries
#include "target.h"

// C++
#include <iostream>

using namespace std;

int Target::newId_ = 0;

Target::Target()
{
    // debug
    cout << "Controls: Target: creating a new target" << endl;

    id_ = Target::newId_++;
}

Target::~Target()
{
    ;
}
