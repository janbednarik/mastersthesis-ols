#include "rerecord.h"

const std::string Rerecord::CAMERA_UNIT_INFO_TOPIC_REMAPPED = "camera_unit_info";
const std::string Rerecord::CAMERA_UNIT_SENSORS_TOPIC       = "camera_unit_sensors";

Rerecord::Rerecord() : nh_(NULL)
{
    nh_ = new ros::NodeHandle();

    cuiSub_         = nh_->subscribe(CAMERA_UNIT_INFO_TOPIC_REMAPPED, 5, &Rerecord::onCUI, this);
    cuSensorsPub_   = nh_->advertise<msgs::CameraUnitSensors>(CAMERA_UNIT_SENSORS_TOPIC, 5);
}

Rerecord::~Rerecord()
{
    if(nh_) delete nh_;
}

void Rerecord::onCUI(const msgs::CameraUnitInfoConstPtr &cuiMsg)
{
    msgs::CameraUnitSensors cuSensorsMsg;

    cuSensorsMsg.header.stamp   = cuiMsg->header.stamp;
    cuSensorsMsg.azimuth        = cuiMsg->aziumth / 1000.0;
    cuSensorsMsg.elevation      = cuiMsg->elevation / 1000.0;
    cuSensorsMsg.f              = cuiMsg->I[0];

    cuSensorsPub_.publish(cuSensorsMsg);
}
